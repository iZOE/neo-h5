/* eslint-disable global-require */
const plugin = require('tailwindcss/plugin')

module.exports = {
  corePlugins: {
    outline: false,
  },
  purge: ['./public/**/*.html', './src/**/*.{js,jsx,ts,tsx,vue}'],
  darkMode: 'class',
  theme: {
    screens: {
      xs: '320px',
      sm: '600px',
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: {
        DEFAULT: '#FFFFFF',
      },
      gray: {
        50: '#F9F9F9',
        100: '#ECECEC',
        200: '#DDDDDD',
        300: '#CCCCCC',
        400: '#BBBBBB',
        500: '#454545',
        600: '#333333',
        650: '#2F2E36',
        700: '#161616'
      },
      black: {
        DEFAULT: '#000000',
      },
      red: {
        DEFAULT: '#EE5650',
      },
      orange: {
        DEFAULT: '#F5A623',
      },
      yellow: {
        DEFAULT: '#FFD81F',
      },
      green: {
        'DEFAULT': '#44C5BE',
      },
      blue: {
        50: '#F7F9FF',
        75: '#738DFF',
        100: '#51A1FF',
        200: '#1D8AEB',
        300: '#1A72C0',
      },
      purple: {
        100: '#676783',
        200: '#47475B',
        700: '#353340',
        800: '#2A2A38',
        900: '#22222E',
      },
      platinum: {
        100: '#B9C0C7',
        200: '#95A3B0',
        250: '#7C95B4',
        300: '#5E7184',
        400: '#406C82',
        500: '#214556',
      },
    },
    fontSize: {
      'title-1': ['30px', '42px'],
      'title-2': ['28px', '39px'],
      'title-3': ['26px', '36px'],
      'title-4': ['24px', '34px'],
      'title-5': ['22px', '31px'],
      'title-6': ['18px', '25px'],
      'title-7': ['16px', '22px'],
      'title-8': ['14px', '20px'],
      'body-1': ['18px', '24px'],
      'body-2': ['16px', '22px'],
      'body-3': ['15px', '21px'],
      'body-4': ['14px', '20px'],
      'body-5': ['13px', '18px'],
      'body-6': ['12px', '16px'],
      'body-7': ['11px', '15px'],
      'body-8': ['10px', '14px'],
    },
    spacing: {
      0: '0px',
      1: '4px',
      2: '8px',
      3: '12px',
      4: '16px',
      5: '20px',
      6: '24px',
      7: '28px',
      8: '32px',
      9: '36px',
      10: '40px',
      11: '44px',
      12: '48px',
    },
    borderRadius: {
      none: '0px',
      DEFAULT: '4px',
      md: '12px',
      lg: '20px',
      full: '9999px',
    },
    boxShadow: {
      'none': 'none',
      'b-0': '0px 1px 2px rgba(0, 0, 0, 0.1)',
      'b-1': '0px 1px 2px rgba(0, 0, 0, 0.25)',
      'b-2': '0px 2px 4px rgba(0, 0, 0, 0.2)',
      'b-3': '0px 12px 12px rgba(0, 0, 0, 0.2)',
      't-2': '0px -2px 4px rgba(0, 0, 0, 0.2)',
    },
    extend: {
      zIndex: {
        '9999': '9999',
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    plugin(({ addVariant, e }) => {
      addVariant('big', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => `.big .${e(`big${separator}${className}`)}`)
      })
    }),
  ],
}
