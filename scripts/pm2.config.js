module.exports = {
    apps: [
        {
            name: 'official_web',
            script: 'server.js',
            instances: 2,
            env: {
                "PORT": 80,
                "NODE_ENV": "production"
            },
            args: '--max-old-space-size=2000',
            max_memory_restart: '1024M',
            exec_mode: 'cluster',
            error_file: './logs/web.log',
            out_file: './logs/web.log',
            log_date_format: 'YYYY-MM-DD HH:mm:ss Z',
            restart_delay: 5000,
        },
    ],
};