export const VT999 = {
  Sport: [
    // CMD 体育
    {
      gameProviderId: 22,
      gameProviderTypeId: 33,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 5,
    },
    // SBO 体育
    {
      gameProviderId: 23,
      gameProviderTypeId: 35,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 6,
    },
    // 沙巴体育
    {
      gameProviderId: 5,
      gameProviderTypeId: 7,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 2,
    },
    // IM 体育
    {
      gameProviderId: 4,
      gameProviderTypeId: 4,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 3,
    },
    // IM 电竞
    {
      gameProviderId: 4,
      gameProviderTypeId: 5,
      gameType: 'E-Sport',
      gameLobbyEntry: true,
      nth: 2,
    },
    // 雷火电竞
    {
      gameProviderId: 18,
      gameProviderTypeId: 29,
      gameType: 'E-Sport',
      gameLobbyEntry: true,
      nth: 5,
    },
  ],
  Lottery: [
    // VT 彩票
    // {
    //   title: 'Pages.Home.VTLottery',
    //   url: '/games/Lottery/vt',
    //   gameType: 'Lottery',
    //   name: 'vt',
    // },
    // OK368 彩票
    {
      gameProviderId: 28,
      gameProviderTypeId: 42,
      gameType: 'Lottery',
      gameLobbyEntry: true,
      nth: 2,
    },
    // VR 彩票
    {
      gameProviderId: 33,
      gameProviderTypeId: 48,
      gameType: 'Lottery',
      gameLobbyEntry: true,
      nth: 9,
    },
  ],
  LiveDealer: [
    // WM 真人
    {
      gameProviderId: 20,
      gameProviderTypeId: 31,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 4,
    },
    // Sexy 百家乐
    {
      gameProviderId: 21,
      gameProviderTypeId: 32,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 2,
    },
    // SBO 真人
    {
      description: 'SBO真人',
      gameProviderId: 23,
      gameProviderTypeId: 34,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 6,
    },
    // AG 真人
    {
      gameProviderId: 2,
      gameProviderTypeId: 2,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 1,
    },
    // BG 真人
    {
      gameProviderId: 6,
      gameProviderTypeId: 13,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 5,
    },
    // DG 真人
    {
      gameProviderId: 19,
      gameProviderTypeId: 30,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 3,
    },
    //  BBIN 真人
    {
      gameProviderId: 27,
      gameProviderTypeId: 40,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 7,
    },
  ],
  CockFight: [
    // SV388 斗鸡
    {
      gameProviderId: 21,
      gameProviderTypeId: 46,
      gameType: 'CockFight',
      gameLobbyEntry: true,
      nth: 1,
    },
  ],
  Poker: [
    // V8棋牌
    {
      gameProviderId: 26,
      gameProviderTypeId: 39,
      gameType: 'Poker',
      gameLobbyEntry: true,
      nth: 5,
    },
  ],
  Slot: [
    // CQ9 电子
    {
      gameProviderId: 24,
      gameProviderTypeId: 37,
      gameType: 'Slot',
      nth: 7,
    },
    // CG 电子
    {
      gameProviderId: 25,
      gameProviderTypeId: 38,
      gameType: 'Slot',
      nth: 8,
    },
    // SBO 电子
    {
      gameProviderId: 23,
      gameProviderTypeId: 36,
      gameType: 'Slot',
      nth: 6,
    },
    // AG 电子
    {
      gameProviderId: 2,
      gameProviderTypeId: 6,
      gameType: 'Slot',
      gameLobbyEntry: true,
      nth: 4,
    },
    // MG 电子
    {
      gameProviderId: 10,
      gameProviderTypeId: 21,
      gameType: 'Slot',
      nth: 2,
    },
    // JDB 老虎机
    {
      gameProviderId: 29,
      gameProviderTypeId: 43,
      gameType: 'Slot',
      nth: 9,
    },
    // PG 电子
    {
      gameProviderId: 32,
      gameProviderTypeId: 47,
      gameType: 'Slot',
      gameLobbyEntry: false,
      nth: 10,
    },
    // // FC 电子
    // {
    //   gameProviderId: 35,
    //   gameProviderTypeId: 49,
    //   gameType: 'Slot',
    //   gameLobbyEntry: true,
    //   nth: 11,
    // },
    // // JL 电子
    // {
    //   gameProviderId: 36,
    //   gameProviderTypeId: 50,
    //   gameType: 'Slot',
    //   gameLobbyEntry: false,
    //   nth: 12,
    // },
  ],
  Fishing: [
    // BG 捕鱼大师
    {
      gameProviderId: 6,
      gameProviderTypeId: 14,
      gameType: 'Fishing',
      gameLobbyEntry: true,
      nth: 2,
    },
    // BG 西游捕鱼
    {
      gameProviderId: 6,
      gameProviderTypeId: 15,
      gameType: 'Fishing',
      gameLobbyEntry: true,
      nth: 4,
    },
    // CQ9 捕鱼
    {
      gameProviderId: 24,
      gameProviderTypeId: 41,
      gameType: 'Fishing',
      nth: 3,
    },
  ],
}

export const STA01 = {
  Sport: [
    // CMD 体育
    {
      gameProviderId: 22,
      gameProviderTypeId: 33,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 5,
    },
    // 沙巴体育
    {
      gameProviderId: 5,
      gameProviderTypeId: 7,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 2,
    },
    // IM 体育
    {
      gameProviderId: 4,
      gameProviderTypeId: 4,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 3,
    },
    // IM 电竞
    {
      gameProviderId: 4,
      gameProviderTypeId: 5,
      gameType: 'E-Sport',
      gameLobbyEntry: true,
      nth: 2,
    },
    // 雷火电竞
    {
      gameProviderId: 18,
      gameProviderTypeId: 29,
      gameType: 'E-Sport',
      gameLobbyEntry: true,
      nth: 5,
    },
    // OB 体育
    {
      gameProviderId: 30,
      gameProviderTypeId: 44,
      gameType: 'Sport',
      gameLobbyEntry: true,
      nth: 7,
    },
  ],
  Lottery: [
    // XH 彩票
    {
      url: '/games/Lottery/xh',
      gameType: 'Lottery',
      name: 'xh',
      nth: 1,
    },
  ],
  LiveDealer: [
    // AG 真人
    {
      gameProviderId: 2,
      gameProviderTypeId: 2,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 1,
    },
    // BG 真人
    {
      gameProviderId: 6,
      gameProviderTypeId: 13,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 5,
    },
    // DG 真人
    {
      gameProviderId: 19,
      gameProviderTypeId: 30,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 3,
    },
    // OB 真人
    {
      gameProviderId: 31,
      gameProviderTypeId: 45,
      gameType: 'LiveDealer',
      gameLobbyEntry: true,
      nth: 8,
    },
  ],
  Poker: [
    // 開元棋牌
    {
      gameProviderId: 9,
      gameProviderTypeId: 20,
      gameType: 'Poker',
      nth: 1,
    },
    // 博樂
    {
      gameProviderId: 7,
      gameProviderTypeId: 17,
      gameType: 'Poker',
      gameLobbyEntry: true,
      nth: 2,
    },
    // IM
    {
      gameProviderId: 12,
      gameProviderTypeId: 23,
      gameType: 'Poker',
      nth: 3,
    },
    // 樂遊
    {
      gameProviderId: 3,
      gameProviderTypeId: 3,
      gameType: 'Poker',
      gameLobbyEntry: true,
      nth: 4,
    },
  ],
  Slot: [
    // MG 电子
    {
      gameProviderId: 10,
      gameProviderTypeId: 21,
      gameType: 'Slot',
      nth: 2,
    },
    // AG 电子
    {
      gameProviderId: 2,
      gameProviderTypeId: 6,
      gameType: 'Slot',
      gameLobbyEntry: true,
      nth: 4,
    },
    // BG 电子
    {
      gameProviderId: 6,
      gameProviderTypeId: 16,
      gameType: 'Slot',
      gameLobbyEntry: true,
      nth: 3,
    },
  ],
  Fishing: [
    // BG 捕鱼大师
    {
      gameProviderId: 6,
      gameProviderTypeId: 14,
      gameType: 'Fishing',
      gameLobbyEntry: true,
      nth: 1,
    },
    // BG 西游捕鱼
    {
      gameProviderId: 6,
      gameProviderTypeId: 15,
      gameType: 'Fishing',
      gameLobbyEntry: true,
      nth: 2,
    },
    // AG 捕鱼
    {
      gameProviderId: 2,
      gameProviderTypeId: 18,
      gameType: 'Fishing',
      gameLobbyEntry: true,
      nth: 3,
    },
  ],
}

export default {
  VT999,
  STA01,
}
