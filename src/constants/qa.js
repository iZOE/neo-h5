import IconSignUp from 'components/icons/SignUp'
import IconAccountCircle from 'components/icons/AccountCircle'
import IconAgent from 'components/icons/Agent'
import IconCertificate from 'components/icons/Certificate'
import IconWithdrawHistory from 'components/icons/WithdrawHistory'
import IconDepositHistory from 'components/icons/DepositHistory'
import IconBlockchain from 'components/icons/Blockchain'
import IconGamePad from 'components/icons/GamePad'
import IconBetting from 'components/icons/Betting'
import IconGameType from 'components/icons/GameType'

// registration_problem: '注册问题',
// recharge_problem: '充值问题',
// game_problem: '游戏问题',
// betting_problem: '投注问题',
// lottery_introduction: '彩种介绍',
// security_problem: '安全问题',
// user_problem: '用户专区',
// agent_problem: '代理专区',
// withdraw_problem: '提现问题',
// crypto_problem: '虚拟货币',

export default [
  {
    title: 'registration_problem',
    icon: <IconSignUp width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'how_to_register_account',
        answer: 'how_to_register_account_answer',
      },
      {
        question: 'how_to_do_when_registration_typo',
        answer: 'how_to_do_when_registration_typo_answer',
      },
      {
        question: 'how_to_do_when_not_receive_certification_letter',
        answer: 'how_to_do_when_not_receive_certification_letter_answer',
      },
    ],
  },
  {
    title: 'user_problem',
    icon: <IconAccountCircle width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'how_to_change_the_password',
        answer: 'how_to_change_the_password_answer',
      },
      {
        question: 'how_to_get_back_my_password',
        answer: 'how_to_get_back_my_password_answer',
      },
      {
        question: 'where_can_i_change_my_profile',
        answer: 'where_can_i_change_my_profile_answer',
      },
      {
        question: 'how_to_bind_my_account_to_my_email',
        answer: 'how_to_bind_my_account_to_my_email_answer',
      },
      {
        question: 'where_can_i_check_the_transaction_record',
        answer: 'where_can_i_check_the_transaction_record_answer',
      },
    ],
  },
  {
    title: 'agent_problem',
    icon: <IconAgent width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'how_to_open_an_account_to_the_next_level',
        answer: 'how_to_open_an_account_to_the_next_level_answer',
      },
      {
        question: 'where_can_i_check_the_teams_information',
        answer: 'where_can_i_check_the_teams_information_answer',
      },
    ],
  },
  {
    title: 'security_problem',
    icon: <IconCertificate width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'is_betting_safe',
        answer: 'is_betting_safe_answer',
      },
      {
        question: 'how_to_avoid_account_theft',
        answer: 'how_to_avoid_account_theft_answer',
      },
      {
        question: 'what_should_i_do_if_i_forget_my_password',
        answer: 'what_should_i_do_if_i_forget_my_password_answer',
      },
      {
        question: 'will_my_account_be_stolen',
        answer: 'will_my_account_be_stolen_answer',
      },
      {
        question: 'can_protect_the_safety_of_the_platform',
        answer: 'can_protect_the_safety_of_the_platform_answer',
      },
      {
        question: 'is_money_safe',
        answer: 'is_money_safe_answer',
      },
      {
        question: 'is_it_safe_to_buy_the_lottery',
        answer: 'is_it_safe_to_buy_the_lottery_answer',
      },
      {
        question: 'will_the_data_be_leaked',
        answer: 'will_the_data_be_leaked_answer',
      },
    ],
  },
  {
    title: 'recharge_problem',
    icon: <IconDepositHistory width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'what_way_can_recharge',
        answer: 'what_way_can_recharge_answer',
      },
      {
        question: 'how_to_use_online_bank_recharge',
        answer: 'how_to_use_online_bank_recharge_answer',
      },
    ],
  },
  {
    title: 'withdraw_problem',
    icon: <IconWithdrawHistory width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'what_are_the_conditions_for_withdraw_cash',
        answer: 'what_are_the_conditions_for_withdraw_cash_answer',
      },
      {
        question: 'how_to_withdraw_money',
        answer: 'how_to_withdraw_money_answer',
      },
      {
        question: 'what_is_stream',
        answer: 'what_is_stream_answer',
      },
      {
        question: 'what_is_stream_limit',
        answer: 'what_is_stream_limit_answer',
      },
      {
        question: 'how_long_to_deposit_account',
        answer: 'how_long_to_deposit_account_answer',
      },
    ],
  },
  {
    title: 'crypto_problem',
    icon: <IconBlockchain width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'how_to_use_third_party_payment_channels_recharge',
        answer: 'how_to_use_third_party_payment_channels_recharge_answer',
      },
    ],
  },
  {
    title: 'game_problem',
    icon: <IconGamePad width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'how_to_buy_lottery',
        answer: 'how_to_buy_lottery_answer',
      },
      {
        question: 'how_to_use_trace_number',
        answer: 'how_to_use_trace_number_answer',
      },
      {
        question: 'how_many_games',
        answer: 'how_many_games_answer',
      },
      {
        question: 'what_should_i_do_with_betting_slip_when_no_lottery',
        answer: 'what_should_i_do_with_betting_slip_when_no_lottery_answer',
      },
      {
        question: 'how_to_exchange_when_won_lottery',
        answer: 'how_to_exchange_when_won_lottery_answer',
      },
      {
        question: 'what_is_limit_red',
        answer: 'what_is_limit_red_answer',
      },
      {
        question: 'what_is_the_amount_of_limit',
        answer: 'what_is_the_amount_of_limit_answer',
      },
    ],
  },
  {
    title: 'betting_problem',
    icon: <IconBetting width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'what_is_trace_number',
        answer: 'what_is_trace_number_answer',
      },
      {
        question: 'what_are_the_trace_number_mode',
        answer: 'what_are_the_trace_number_mode_answer',
      },
      {
        question: 'what_are_the_betting_mode',
        answer: 'what_are_the_betting_mode_answer',
      },
      {
        question: 'why_the_platform_will_have_a_bet_limit',
        answer: 'why_the_platform_will_have_a_bet_limit_answer',
      },
      {
        question: 'can_i_cancel_the_bet_order',
        answer: 'can_i_cancel_the_bet_order_answer',
      },
    ],
  },
  {
    title: 'deposit_guide',
    icon: <IconDepositHistory width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'deposit_guide >>> question',
        answer: 'deposit_guide >>> answer',
      },
    ],
  },
  {
    title: 'lottery_introduction',
    icon: <IconGameType width="24" height="24" fillColor="#51A1FF" />,
    problem: [
      {
        question: 'introduction_ssc',
        answer: 'SSC',
      },
      {
        question: 'introduction_ex5',
        answer: 'Ex5',
      },
      {
        question: 'introduction_fc3d',
        answer: 'Fc3d',
      },
      {
        question: 'introduction_pl3',
        answer: 'Pl3',
      },
      {
        question: 'introduction_pk10',
        answer: 'Pk10',
      },
      {
        question: 'introduction_xyft',
        answer: 'Xyft',
      },
      {
        question: 'introduction_lucky28',
        answer: 'Lucky28',
      },
      {
        question: 'introduction_mark6',
        answer: 'Mark6',
      },
      {
        question: 'introduction_k3',
        answer: 'K3',
      },
      {
        question: 'introduction_dicecup',
        answer: 'Dicecup',
      },
    ],
  },
]
