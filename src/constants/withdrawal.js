export const WITHDRAWAL_TYPE = {
  CREDIT_CARD: 1,
  CRYPTO_CURRENCY: 2,
}

export const TURNOVER_TYPE = {
  TRANSFER: 0, // 轉賬
  WITHDRAWAL: 1, // 提現
}

export const TURNOVER_SUCCESS_ALERT_TYPE = {
  TIMES: 'times', // 次數超過
  AMOUNT: 'amount', // 金額超過
}

export const TURNOVER_STATUS = {
  SUCCESS: 2, // 通過
  FAIL: 3, // 未通過
}
