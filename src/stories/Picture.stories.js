import React from 'react';
import Picture from '../components/core/Picture'

export default {
  title: 'Components/Picture',
  component: Picture,
};

const Template = (args) => (
  <Picture
    {...args}
    webp="https://via.placeholder.com/300/2FA0C1/FFF.webp?text=WEBP"
    png="https://via.placeholder.com/300/DEE666/FFF.png?text=PNG"
  />
);

export const Primary = Template.bind({});
Primary.args = {
  label: 'Picture',
};
