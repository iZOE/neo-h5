import React from 'react'
import { RecoilRoot } from 'recoil'
import ThemeProvider from '../components/shared/ThemeProvider'
import InputComponent from '../components/core/Input'

export default {
  title: 'Components/Input',
  component: InputComponent,
}

const Template = args => (
  <RecoilRoot>
    <ThemeProvider>
      <InputComponent {...args} />
    </ThemeProvider>
  </RecoilRoot>
)

export const Input = Template.bind({})
Input.args = {
  label: 'Label',
  placeholder: 'place holder',
  withDelete: false,
}

export const WarningInput = Template.bind({})
WarningInput.args = {
  label: 'Label',
  placeholder: 'place holder',
  error: 'yabai',
  withDelete: false,
}
