import React from 'react'
import { AnimateSharedLayout } from 'framer-motion'
import InputComp from '../layouts/LoginAndSignup/TemplateA/components/Input'

export default {
  title: 'Module/Login-Signup/TemplateA',
  component: InputComp,
}

const Template = args => (
  <AnimateSharedLayout>
    <InputComp {...args} />
    <InputComp {...args} />
  </AnimateSharedLayout>
)

export const Input = Template.bind({})
Input.args = {
  variant: 'label',
  errorMsg: '',
}
