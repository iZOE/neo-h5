import React from 'react'
import { RecoilRoot } from 'recoil'
import ThemeProvider from '../components/shared/ThemeProvider'
import NavigationBar from '../components/core/NavigationBar'

export default {
  title: 'Components/NavigationBar',
  component: NavigationBar,
}

const Template = args => (
  <RecoilRoot>
    <ThemeProvider>
      <NavigationBar {...args} />
    </ThemeProvider>
  </RecoilRoot>
)

export const Default = Template.bind({})
Default.args = {
  title: 'Navigation',
}
