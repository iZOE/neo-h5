import React from 'react'
import { RecoilRoot } from 'recoil'
import ThemeProvider from '../components/shared/ThemeProvider'
import Button from '../components/core/Button'

export default {
  title: 'Components/Button',
  component: Button,
  argTypes: {
    variant: {
      control: {
        type: 'select',
        options: ['primary', 'secondary', 'tertiary'],
      },
    },
  },
}

const Template = args => (
  <RecoilRoot>
    <ThemeProvider>
      <Button {...args} />
    </ThemeProvider>
  </RecoilRoot>
)

export const Primary = Template.bind({})
Primary.args = {
  variant: 'primary',
  text: 'I`m Primary Button',
}

export const Secondary = Template.bind({})
Secondary.args = {
  variant: 'secondary',
  text: 'I`m Secondary Button',
}

export const Tertiary = Template.bind({})
Tertiary.args = {
  variant: 'tertiary',
  text: 'I`m Tertiary Button',
}
