import React from 'react'
import { RecoilRoot } from 'recoil'
import Dialog from '../components/core/Dialog'
import Picture from '../components/core/Picture'
import ThemeProvider from '../components/shared/ThemeProvider'

export default {
  title: 'Components/Dialog',
  component: Dialog,
}

const Template = args => (
  <RecoilRoot>
    <ThemeProvider>
      <Dialog
        isOpen
        onCancel={() => {
          console.log('cancel')
        }}
        cancelText="Cancel Only"
        {...args}
      >
        <div className="text-center text-title-8">You can put anything here...</div>
        <Picture
          webp="https://via.placeholder.com/300x1000/2FA0C1/FFF.webp?text=WEBP"
          png="https://via.placeholder.com/300x1000/DEE666/FFF.png?text=PNG"
        />
        <div className="text-body-4 text-center">You can put anything here...</div>
      </Dialog>
    </ThemeProvider>
  </RecoilRoot>
)

export const TemplateA = Template.bind({})
TemplateA.args = {}
