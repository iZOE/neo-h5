import React from 'react'
import { RecoilRoot } from 'recoil'
import Alert from '../components/core/Alert'
import ThemeProvider from '../components/shared/ThemeProvider'

export default {
  title: 'Components/Alert',
  component: Alert,
}

const Template = args => (
  <RecoilRoot>
    <ThemeProvider>
      <Alert
        isOpen
        title="Alert Title"
        content="Alert Content"
        {...args}
        onOk={() => { console.log('OKKKKKKKK') }}
      />
    </ThemeProvider>
  </RecoilRoot>
)

export const TemplateA = Template.bind({})
TemplateA.args = {}
