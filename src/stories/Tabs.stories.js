import React from 'react'
import { RecoilRoot } from 'recoil'
import ThemeProvider from '../components/shared/ThemeProvider'
import Tabs from '../components/core/Tabs'

export default {
  title: 'Components/Tabs',
  component: Tabs,
}

const tabData = [
  {
    tabTitle: 'tab1',
    tabPanel: <div>tab1PrimaryPanel</div>,
    secondTabPanel: <div>tab1SecondPanel</div>,
  },
  {
    tabTitle: 'tab2',
    tabPanel: <div>tab2PrimaryPanel</div>,
  },
]

const Template = args => (
  <RecoilRoot>
    <ThemeProvider>
      <Tabs {...args} data={tabData} />
    </ThemeProvider>
  </RecoilRoot>
)

export const Default = Template.bind({})
Default.args = {
  fontSize: '14px',
  lineHeight: '24px',
  outlined: true,
  borderRadius: '24px',
  borderColor: '#51A1FF',
  bg: '#FFF',
  textAlign: true,
  fontWeight: 'bold',
}
