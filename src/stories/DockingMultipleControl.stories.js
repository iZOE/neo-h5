import React from 'react'
import { RecoilRoot } from 'recoil'
import ThemeProvider from '../components/shared/ThemeProvider'
import DockingMultipleControl from '../components/core/DockingMultipleControl'
import DockingItem from '../components/core/DockingMultipleControl/components/DockingItem'

export default {
  title: 'Components/DockingMultipleControl',
  component: DockingMultipleControl,
}

const Template = ({ isShow }) => (
  <RecoilRoot>
    <ThemeProvider>
      <DockingMultipleControl isShow={isShow}>
        <DockingItem href="" key="1" name="option 1" />
        <DockingItem href="" key="2" name="option 2" />
        <DockingItem href="" key="3" name="option 3" />
      </DockingMultipleControl>
    </ThemeProvider>
  </RecoilRoot>
)

export const Default = Template.bind({})
Default.args = {
  isShow: false,
}
