export default new Promise((resolve, reject) => {
  try {
    fetch('https://version.apl-01.z0229z1m.com/api/v2/check?agentcode=STA01&platform=4').then(
      res => {
        res.json().then(data => resolve(data))
      },
    )
  } catch (err) {
    reject(err)
  }
})
