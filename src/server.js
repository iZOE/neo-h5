const Koa = require('koa')
const Router = require('@koa/router')
const MobileDetect = require('mobile-detect')
const send = require('koa-send')
const fs = require('fs')

const dev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3000

const setting = {
  STA01: {
    image: 'webclip/sta01.png',
    name: '星辉体育',
  },
  VT999: {
    image: 'webclip/vt999.png',
    name: 'VT 999',
  },
}

const server = new Koa()
const router = new Router()

server.use(async (ctx, next) => {
  ctx.res.statusCode = 200
  ctx.res.cookie = ctx.cookies
  await next()
})

router.get('/setup.mobileconfig', ctx => {
  const { host, protocol } = ctx
  ctx.body = iPhoneWebClip(`${protocol}://${host}`, process.env.AGENT_CODE)
  ctx.type = 'application/octet-stream'
})

router.get('/manifest.(json|webmanifest)', ctx => {
  const { host, protocol } = ctx

  ctx.body = androidWebClip(`${protocol}://${host}`, process.env.AGENT_CODE)
  ctx.type = 'application/manifest+json'
})

router.get('/setup', async ctx => {
  const { req } = ctx
  const md = new MobileDetect(req.headers['user-agent'])
  if (md.is('iPhone') && md.userAgent() === 'Safari') {
    ctx.redirect('/setup.mobileconfig')
  } else {
    ctx.redirect('/login')
  }

  // ctx.redirect('/android')
})

router.get(/_next|\.(webp|json|js|png|svg|jpg|ico)/gi, async ctx => {
  await send(ctx, ctx.path, { root: './out' })
})

router.get('(.*)', async ctx => {
  let path = `${ctx.path}.html`
  if (ctx.path === '/') {
    path = 'index.html'
  }
  await send(ctx, path, { root: './out' })
})

server.use(router.routes()).listen(port, err => {
  if (err) {
    throw err
  }

  console.info(`> Ready on http://localhost:${port}`)
})

const iPhoneWebClip = (host, agentCode) => {
  const brand = setting[agentCode].name
  const bitmap = fs.readFileSync(`public/${setting[agentCode].image}`)
  const icon = Buffer.from(bitmap).toString('base64')
  return `<?xml version="1.0" encoding="UTF-8"?>
        <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
        <plist version="1.0">
        <dict>
        <key>PayloadContent</key>
        <array>
            <dict>
                <key>FullScreen</key>
                <true/>
                <key>Icon</key>
                <data>
                ${icon}
                </data>
                <key>IgnoreManifestScope</key>
                <true/>
                <key>IsRemovable</key>
                <true/>
                <key>Label</key>
                <string>${brand}</string>
                <key>PayloadDescription</key>
                <string>${brand}</string>
                <key>PayloadDisplayName</key>
                <string>${brand}</string>
                <key>PayloadIdentifier</key>
                <string>com.apple.webClip.managed.921077A1-E8D4-43B6-B76C-48DF2010A0B0</string>
                <key>PayloadType</key>
                <string>com.apple.webClip.managed</string>
                <key>PayloadUUID</key>
                <string>921077A1-E8D4-43B6-B76C-48DF2010A0B0</string>
                <key>PayloadVersion</key>
                <integer>1</integer>
                <key>Precomposed</key>
                <false/>
                <key>URL</key>
                <string>${host}</string>
            </dict>
        </array>
        <key>PayloadDisplayName</key>
        <string>${brand}</string>
        <key>PayloadIdentifier</key>
        <string>${brand}</string>
        <key>PayloadRemovalDisallowed</key>
        <false/>
        <key>PayloadType</key>
        <string>Configuration</string>
        <key>PayloadUUID</key>
        <string>3C6C766C-1A87-488D-B64B-E0825F730E85</string>
        <key>PayloadVersion</key>
        <integer>1</integer>
        </dict>
        </plist>`
}

const androidWebClip = (host, agentCode) => {
  const brand = setting[agentCode].name
  return `{
      "name": "${brand}",
      "short_name": "${brand}",
      "start_url": "/",
      "display": "standalone",
      "background_color": "#90cdf4",
      "theme_color": "#90cdf4",
      "orientation": "portrait-primary",
      "icons": [
        {
          "src": "${setting[agentCode].image}",
          "sizes": "1024x1024",
          "type": "image/png"
        }
      ]
    }`
}
