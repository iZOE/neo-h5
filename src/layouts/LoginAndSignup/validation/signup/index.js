import { FormattedMessage } from 'react-intl'

const examinedRequired = value => (value ? true : false)

// return true代表通過驗證
export const signUpFormValidate = {
  accountId: {
    required: examinedRequired,
    length: value => {
      if (
        value.length < 6 ||
        value.length > 16 ||
        !value.match(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/g)
      ) {
        return false
      }
      return true
    },
  },
  password: {
    required: examinedRequired,
    length: value => {
      if (
        value.length < 6 ||
        value.length > 16 ||
        !value.match(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/g)
      ) {
        return false
      }
      return true
    },
  },
  confirmPassword: {
    required: examinedRequired,
  },
  referralCode: {
    length: value => {
      if (!value) {
        return true
      }

      return value.length === 8
    },
  },
  nickName: {
    plainText: nickName => {
      if (!nickName) {
        return true
      }
      return nickName.length < 1 || nickName.length > 14 || nickName.match(/[ \t]/g)
    },
    digital: nickName => {
      if (!nickName) {
        return true
      }
      const nickNameDigital = nickName ? nickName.match(/\d/g) : null
      if (!nickNameDigital) {
        return true
      }
      return nickNameDigital.length > 7
    },
  },
}

export const errorMsg = {
  accountId: {
    required: <FormattedMessage id={`signUp.inputValidate.accountRequired`} />,
    length: <FormattedMessage id={`signUp.inputValidate.accountIdLength`} />,
  },
  password: {
    required: <FormattedMessage id={`signUp.inputValidate.passwordRequired`} />,
    length: <FormattedMessage id={`signUp.inputValidate.noAllDigits`} />,
  },
  confirmPassword: {
    required: <FormattedMessage id={`signUp.inputValidate.confirmPasswordRequired`} />,
    validate: <FormattedMessage id={`signUp.inputValidate.confirmPasswordSame`} />,
  },
  referralCode: {
    length: <FormattedMessage id={`signUp.inputValidate.referralCodeLength`} />,
  },
  nickName: {
    plainText: <FormattedMessage id={`signUp.inputValidate.nickNamePlainText`} />,
    digital: <FormattedMessage id={`signUp.inputValidate.nickNameNoAllDigit`} />,
  },
}
