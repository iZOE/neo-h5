import styled, { css } from 'styled-components'
import typography from '@styled-system/typography'

const complexBG = css`
  background: ${props => (props.theme.darkMode ? '#214556' : '#ddd')};
`
export const SwitchWrapper = styled.div`
  width: 38px;
  height: 20px;
  ${props =>
    props.checked
      ? `background: linear-gradient(90deg,${props.colorSet.switch.bg[0]} 0%, ${props.colorSet.switch.bg[1]} 100%)`
      : complexBG};
  display: inline-flex;
  justify-content: ${props => (props.checked ? 'flex-end' : 'flex-start')};
  border-radius: 100px;
  cursor: pointer;
  vertical-align: middle;

  .handle {
    margin: 2px;
    width: 16px;
    height: 16px;
    background-color: #fff;
    border-radius: 8px;
  }
`

export const LabelWrapper = styled.span`
  font-weight: bold;
  color: #1d8aeb;
  margin-right: 6px;
  ${typography}
`
