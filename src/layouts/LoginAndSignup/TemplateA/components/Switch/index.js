import React, { useState, useEffect, useCallback } from 'react'
import PropTypes from 'prop-types'
import { motion } from 'framer-motion'
import { LabelWrapper, SwitchWrapper } from './Styled'

const spring = {
  type: 'spring',
  stiffness: 700,
  damping: 30,
}

function Switch({ label, checked, onClick }) {
  const [isCheck, setIsCheck] = useState(checked)
  const handleSwitch = useCallback(() => {
    if (onClick) {
      onClick()
    }
    setIsCheck(!isCheck)
  }, [isCheck])

  const colorSet = JSON.parse(process.env.NEXT_PUBLIC_LOGIN_AND_SIGNUP_COLOR_SET)

  return (
    <div>
      {label ? <LabelWrapper fontSize={['12px', '14px']}>{label}</LabelWrapper> : null}
      <SwitchWrapper
        data-testid="template-a-components-switch"
        checked={isCheck}
        colorSet={colorSet}
        onClick={handleSwitch}
      >
        <motion.div className="handle" layout transition={spring} />
      </SwitchWrapper>
    </div>
  )
}

Switch.propTypes = {
  label: PropTypes.string,
  checked: PropTypes.bool,
  onClick: PropTypes.func,
}
Switch.defaultProps = {
  label: '',
  checked: false,
  onClick: undefined,
}

export default Switch
