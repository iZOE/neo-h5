/* eslint-disable max-len */
import Picture from 'components/core/Picture'

function BackgroundWrapper() {
  const mediaPath = `/pages/login/${process.env.NEXT_PUBLIC_AGENT_CODE}/bg`

  return (
    <>
      <div className="absolute -z-2 inset-0 bg-gray-100 dark:bg-purple-900">
        <Picture png={`${mediaPath}.png`} webp={`${mediaPath}.webp`} />
      </div>
      <div className="overflow-hidden absolute -z-1 inset-0 w-full h-full bg-gradient-to-b from-white dark:from-purple-900" />
    </>
  )
}

export default BackgroundWrapper
