import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import { useOpenCustomerService } from 'hooks/useOpenCustomerService'

const TextLink = ({ children, ...props }) => {
  return (
    <span className="text-body-8 font-semibold" {...props}>
      {children}
    </span>
  )
}

function LinkGroup() {
  const router = useRouter()
  const openCustomerService = useOpenCustomerService()
  const agentCode = process.env.NEXT_PUBLIC_AGENT_CODE
  const showAntiDNSInject = agentCode !== 'VT999'

  return (
    <div className="flex justify-evenly items-center text-blue-200">
      <Link href="/download">
        <TextLink>
          <FormattedMessage id="linkGroup.appDownload" />
        </TextLink>
      </Link>
      <span className="line">|</span>
      <TextLink onClick={openCustomerService}>
        <FormattedMessage id="linkGroup.contactCustomerService" />
      </TextLink>
      {showAntiDNSInject && (
        <>
          <span className="line">|</span>
          <TextLink onClick={() => router.push('https://dns.6242mr9h.com/')}>
            <FormattedMessage id="linkGroup.antiDNSInject" />
          </TextLink>
        </>
      )}
    </div>
  )
}

export default LinkGroup
