import React from 'react'
import PropTypes from 'prop-types'
import { useRecoilValue } from 'recoil'
import { darkModeState } from '../../../../../atoms/darkModeState'
import LoadingIcon from './LoadingIcon'
import { ButtonWrapper } from './Styled'

const SubmitButton = ({ name, isLoading, disabled, onClick }) => {
  const darkMode = useRecoilValue(darkModeState)
  const colorSet = JSON.parse(process.env.NEXT_PUBLIC_LOGIN_AND_SIGNUP_COLOR_SET)

  return (
    <ButtonWrapper
      type="submit"
      fontSize={['14px', '16px']}
      darkMode={darkMode}
      colorSet={colorSet}
      disabled={disabled}
      onClick={onClick}
    >
      {isLoading ? <LoadingIcon /> : name}
    </ButtonWrapper>
  )
}

SubmitButton.propTypes = {
  name: PropTypes.string.isRequired,
  isLoading: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
}

SubmitButton.defaultProps = {
  name: '',
  isLoading: false,
  disabled: true,
  onClick: undefined,
}

export default SubmitButton
