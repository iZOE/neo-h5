import React from 'react'
import IconLoading from 'components/icons/Loading'
import { LoadingIcon } from './Styled'

export default function LoadingIconComponent() {
  return (
    <LoadingIcon className="btn-loading">
      <IconLoading width={24} height={24} fill="#fff" />
    </LoadingIcon>
  )
}
