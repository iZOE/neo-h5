import styled from 'styled-components'
import typography from '@styled-system/typography'

export const ButtonWrapper = styled.button`
  background: linear-gradient(
    90deg,
    ${props => props.colorSet.submitBtn.bg[0]} 0%,
    ${props => props.colorSet.submitBtn.bg[1]} 100%
  );
  color: #fff;
  width: 100%;
  height: 44px;
  border-radius: 100px;
  border: none;
  ${typography}
  &:disabled {
    color: ${props => (props.theme.darkMode ? '#406c82' : '#fff')};
    background: ${props =>
      props.theme.darkMode ? '#214556' : props.colorSet.submitBtn.disabled.bg};
  }
`
