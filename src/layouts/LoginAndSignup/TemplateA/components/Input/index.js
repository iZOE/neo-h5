import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import dynamic from 'next/dynamic'
import { motion, AnimatePresence } from 'framer-motion'

const IconPasswordClose = dynamic(() => import('../../../../../components/icons/PasswordClose.svg'))
const IconPasswordOpen = dynamic(() => import('../../../../../components/icons/PasswordOpen.svg'))

export const INPUT_TYPE = {
  TEXT: 'text',
  PASSWORD: 'password',
  NUMBER: 'number',
}

const coercionExcludeZero = (value, considerEmptyStr = false) => {
  if (value !== '' && Number(value) === 0) {
    return true
  }

  if (considerEmptyStr && value === '') {
    return true
  }

  return !!value
}

function Input({
  name,
  type: typeFromProps = INPUT_TYPE.TEXT,
  initialValue = '',
  placeholder = '',
  onChange,
  onFocus: propsOnFocus,
  onBlur: propsOnBlur,
  propsRef,
  errorMsg = '',
  iconComponent: IconComponent,
  readonly,
  ...rest
}) {
  const [isLiftPlaceHolder, setIsLiftPlaceHolder] = useState(!!coercionExcludeZero(initialValue))
  const [isPasswordOpen, setIsPasswordOpen] = useState(false)

  useEffect(() => {
    const result = !!coercionExcludeZero(initialValue)
    setIsLiftPlaceHolder(prev => result)
  }, [initialValue])

  const handleBlur = nowInputValue => {
    const hasValueOnInput = coercionExcludeZero(nowInputValue)
    if (propsOnBlur) {
      propsOnBlur(nowInputValue)
    }
    if (hasValueOnInput) {
      return
    }
    if (!isLiftPlaceHolder) {
      setIsLiftPlaceHolder(prev => true)
    } else {
      setIsLiftPlaceHolder(prev => false)
    }
  }

  const handleFocus = value => {
    if (propsOnFocus) {
      propsOnFocus(value)
    }
    setIsLiftPlaceHolder(prev => true)
  }

  const composedOnChange = nowValue => {
    if (coercionExcludeZero(nowValue, true)) {
      if (!isLiftPlaceHolder) {
        setIsLiftPlaceHolder(prev => true)
      }
    } else {
      setIsLiftPlaceHolder(prev => false)
    }
    if (onChange) {
      onChange(nowValue)
    }
  }
  const labelId = `label-id-${name}`

  return (
    <>
      <motion.div layout>
        <div className="flex items-center justify-between border-b border-blue-100 border-solid">
          <div readOnly={readonly} className="relative flex items-center h-11 w-full">
            <label
              className="flex text-blue-100 ml-3"
              data-testid="label-for-placeholder"
              htmlFor={labelId}
            >
              {IconComponent && <IconComponent />}
            </label>
            <div className="absolute text-blue-100 z-0 ml-11">
              {isLiftPlaceHolder ? (
                <motion.div
                  data-testid="placeholder-motion"
                  init={false}
                  animate={{ originX: 0, y: '-50%', scale: 0.625 }}
                  transition={{ duration: 0.5 }}
                >
                  <label className="mr-3" htmlFor={labelId}>
                    {placeholder}
                  </label>
                </motion.div>
              ) : (
                <motion.div
                  data-testid="placeholder-motion"
                  init={{ x: 0, y: '-50%', scale: 0.625 }}
                  animate={{ y: 0, scale: 1 }}
                  transition={{ duration: 0.5 }}
                >
                  <label className="mr-3" htmlFor={labelId}>
                    {placeholder}
                  </label>
                </motion.div>
              )}
            </div>
            <input
              data-testid="template-a-form-input"
              ref={propsRef}
              id={labelId}
              name={name}
              className={`text-body-4 pl-2 pt-5 bg-transparent border-none ${
                readonly
                  ? 'text-platinum-200 dark:text-platinum-400'
                  : 'text-gray-600 dark:text-white'
              }`}
              onChange={evt => {
                const nowValue = evt.target.value
                composedOnChange(nowValue)
              }}
              onFocus={evt => {
                const nowInputValue = evt.target.value
                handleFocus(nowInputValue)
              }}
              onBlur={evt => {
                const nowInputValue = evt.target.value
                handleBlur(nowInputValue)
              }}
              type={isPasswordOpen ? INPUT_TYPE.TEXT : typeFromProps}
              readOnly={readonly}
              {...rest}
            />
          </div>
          {typeFromProps === INPUT_TYPE.PASSWORD && (
            <label
              htmlFor="input-label-password"
              className="pr-4 text-right text-blue-100"
              onClick={() => {
                setIsPasswordOpen(prev => !prev)
              }}
            >
              {isPasswordOpen ? <IconPasswordOpen /> : <IconPasswordClose />}
            </label>
          )}
        </div>
      </motion.div>
      <AnimatePresence>
        {errorMsg && (
          <motion.div layout>
            <div className=" text-red text-body-8 mt-1">{errorMsg}</div>
          </motion.div>
        )}
      </AnimatePresence>
    </>
  )
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'password', 'number']).isRequired,
  placeholder: PropTypes.string.isRequired,
}

export default Input
