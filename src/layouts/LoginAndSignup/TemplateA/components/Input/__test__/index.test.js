import React from 'react'
import { render, fireEvent, waitFor } from '@testing-library/react'
import Input, { INPUT_TYPE } from '../index'

test('check is this input component collaborate with onChange, onBlur properly', () => {
  let holder = ''
  const onChange = v => {
    holder = v
  }
  const { getByTestId } = render(
    <Input
      name="account"
      onChange={onChange}
      placeholder="測試placeholder"
      type={INPUT_TYPE.TEXT}
    />,
  )
  const inputElement = getByTestId('input1')
  expect(inputElement.value).toEqual('')
  fireEvent.change(inputElement, { target: { value: 'morris001' } })

  expect(inputElement.value).toEqual('morris001')
  expect(holder).toEqual('morris001')
})

// 還找不到原因為什麼transform一直會是none  但是framer-motion測試大概就是這樣寫 這個test case先skip
test.skip('test is left label control the placeholder label properly', async () => {
  let holder = ''
  const onChange = v => {
    holder = v
  }
  const { getByTestId } = render(
    <Input
      name="account"
      onChange={onChange}
      placeholder="測試placeholder"
      type={INPUT_TYPE.TEXT}
    />,
  )
  const labelElement = getByTestId('labelForPlaceholder')
  fireEvent.click(labelElement)

  const liftablePlaceholderLabel = getByTestId('placeholderMotion')

  await waitFor(
    () => {
      expect(liftablePlaceholderLabel).toHaveStyle(
        'transform: translateY(70%) scale(1) translateZ(0px); transform-origin: 0% 50% 0px;',
      )
    },
    { timeout: 2000 },
  )
})
