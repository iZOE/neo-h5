import { useEffect } from 'react'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import { motion } from 'framer-motion'
import Logo from 'components/shared/Logo'
import BackgroundWrapper from './components/BackgroundWrapper'
import LinkGroup from './components/LinkGroup'
import style from './templateA.module.scss'

const spring = {
  type: 'spring',
  stiffness: 500,
  damping: 30,
}

export default function LoginAndSignupTemplateA({ children }) {
  const intl = useIntl()
  const router = useRouter()

  const RedirectButtons = [
    {
      url: '/login',
      name: intl.formatMessage({ id: 'login' }),
    },
    {
      url: '/signup',
      name: intl.formatMessage({ id: 'signup' }),
    },
  ]

  useEffect(() => {
    const { pathname } = router
  }, [router])

  const handleClick = path => {
    router.push(path)
  }

  const getTabBgColor = () => {
    const colorSet = JSON.parse(process.env.NEXT_PUBLIC_LOGIN_AND_SIGNUP_COLOR_SET)
    const bg = `linear-gradient(90deg, ${colorSet.switchTab.bg[0]} 0%, ${colorSet.switchTab.bg[1]} 100%)`
    return bg
  }

  return (
    <div className="flex-1 relative overflow-hidden h-screen">
      <div className="relative z-10">
        <div className={`${style.logo_style} block my-8 mx-auto`} onClick={() => router.push('/')}>
          <Logo />
        </div>

        <div className="px-6 pb-9 sm:pb-[57px] my-0 mx-auto" style={{ maxWidth: '576px' }}>
          <div
            className="flex w-full box-border rounded-full p-1 border border-solid border-blue-100"
            style={{
              height: '42px',
            }}
          >
            {RedirectButtons.map(({ url, name }) => (
              <div key={url} style={{ position: 'relative', flex: 1 }}>
                {router.pathname === url && (
                  <motion.div
                    layoutId="outline"
                    transition={spring}
                    className="absolute rounded-full inset-0 h-8"
                    style={{
                      background: getTabBgColor(),
                    }}
                  />
                )}
                <button
                  type="button"
                  onClick={() => {
                    handleClick(url)
                  }}
                  className="absolute top-2/4 left-2/4 w-full border-none bg-transparent"
                  style={{
                    transform: 'translate(-50%,-50%)',
                    color: router.pathname === url ? '#fff' : '#51a1ff',
                    lineHeight: '17px',
                  }}
                >
                  {name}
                </button>
              </div>
            ))}
          </div>
          {children}
          <LinkGroup />
        </div>
      </div>
      <BackgroundWrapper />
    </div>
  )
}
