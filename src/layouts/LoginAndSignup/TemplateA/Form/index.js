import React, { useEffect, useState, useCallback } from 'react'
import { useForm } from 'react-hook-form'
import Union from 'components/icons/Union.svg'
import Lock from 'components/icons/Lock.svg'
import Certificate from 'components/icons/Certificate.svg'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import Input, { INPUT_TYPE } from '../components/Input'
import SubmitButton from '../components/SubmitButton'
import { signUpFormValidate, errorMsg } from '../../validation/signup'
import { getSessionKey, callSignupAPI, apiSuccessHandler, apiErrorHandler } from './logics'

const SignupForms = props => {
  const {
    errors,
    register,
    getValues,
    setValue,
    handleSubmit: handleRHFSubmit,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      accountId: '',
      password: '',
      confirmPassword: '',
      nickName: '',
      referralCode: '',
    },
  })

  const [sessionKey, setSessionKey] = useState('')
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [defaultReferralCode, setDefaultReferralCode] = useState('')

  useEffect(() => {
    getSessionKey(getMessageByTermPath).then(res => {
      setSessionKey(prev => res)
    })
  }, [])

  const router = useRouter()
  const intl = useIntl()

  const getMessageByTermPath = useCallback(
    pathToTerm =>
      intl.formatMessage({
        id: pathToTerm,
      }),
    [],
  )

  const onSignupSuccess = useCallback(() => {
    router.push('/login')
  }, [])

  const onSignupApiError = useCallback(() => {
    setIsSubmitting(prev => false)
  }, [])

  const _8010ErrorCodeHandler = useCallback(() => {
    router.push('/service')
  }, [])

  useEffect(() => {
    if (JSON.parse(process.env.NEXT_PUBLIC_WITH_REFERRALCODE)) {
      const { agent, referralCode: referralCodeFromURL } = router.query
      console.log(agent, referralCodeFromURL)
      if (agent) {
        setValue('agent', agent)
      }

      if (referralCodeFromURL) {
        setValue('referralCode', referralCodeFromURL)
        setDefaultReferralCode(prev => referralCodeFromURL)
      }
    }
  }, [router?.query?.agent, router?.query?.referralCode])

  const onSubmit = async values => {
    setIsSubmitting(prev => true)
    const agent = getValues('agent')
    const requestParameterObj = {
      ...values,
      sessionKey,
      agentCode: process.env.NEXT_PUBLIC_AGENT_CODE,
    }

    const [resultData, error] = await callSignupAPI(requestParameterObj, agent)
    const resultCode = resultData?.code
    if (error) {
      apiErrorHandler(error, onSignupApiError, getMessageByTermPath, _8010ErrorCodeHandler)
    } else if (resultCode !== '0000') {
      const _error = {
        data: {
          code: resultCode,
          responseMessage: resultData?.responseMessage,
        },
      }
      apiErrorHandler(_error, onSignupApiError, getMessageByTermPath, _8010ErrorCodeHandler)
    } else {
      apiSuccessHandler(getMessageByTermPath, onSignupSuccess)
    }
  }

  return (
    <div className="my-6 sm:my-12">
      <form onSubmit={handleRHFSubmit(onSubmit)}>
        <div className="mb-2">
          <Input
            type={INPUT_TYPE.TEXT}
            propsRef={register({
              validate: signUpFormValidate.accountId,
            })}
            name="accountId"
            placeholder={intl.formatMessage({ id: 'signUp.placeholder.accountId' })}
            errorMsg={errorMsg?.accountId[errors?.accountId?.type]}
            iconComponent={Union}
          />
        </div>

        <div className="mb-2">
          <Input
            type={INPUT_TYPE.PASSWORD}
            propsRef={register({
              validate: signUpFormValidate.password,
            })}
            name="password"
            placeholder={intl.formatMessage({ id: 'signUp.placeholder.password' })}
            errorMsg={errorMsg?.password[errors?.password?.type]}
            iconComponent={Lock}
          />
        </div>
        <div className="mb-2">
          <Input
            type={INPUT_TYPE.PASSWORD}
            propsRef={register({
              required: true,
              validate: value => value === getValues('password'),
            })}
            name="confirmPassword"
            placeholder={intl.formatMessage({ id: 'signUp.placeholder.confirmPassword' })}
            errorMsg={errorMsg?.confirmPassword[errors?.confirmPassword?.type]}
            iconComponent={Lock}
          />
        </div>
        {JSON.parse(process.env.NEXT_PUBLIC_WITH_REFERRALCODE) && (
          <div className="mb-2">
            <Input
              type="text"
              initialValue={defaultReferralCode}
              propsRef={register({
                validate: signUpFormValidate.referralCode,
              })}
              name="referralCode"
              readonly={!!defaultReferralCode}
              placeholder={intl.formatMessage({ id: 'signUp.placeholder.referralCode' })}
              errorMsg={errorMsg?.referralCode[errors?.referralCode?.type]}
              iconComponent={Certificate}
            />
          </div>
        )}
        <div className="mt-8">
          <SubmitButton
            type="submit"
            isLoading={isSubmitting}
            disabled={!isDirty || !isValid || isSubmitting}
            name={intl.formatMessage({ id: 'signUp.submitBTNText' })}
          />
        </div>
      </form>
    </div>
  )
}

export default SignupForms
