import { signupWithAgentRequest, signupRequest, fetchSessionKey } from '../apis'

export async function apiSuccessHandler(getMessageByTermPath, onSignupSuccess) {
  const message = (await import('components/core/Alert/message')).default
  message({
    title: getMessageByTermPath('signUp.errorCodes.0000.title'),
    content: getMessageByTermPath('signUp.errorCodes.0000.content'),
    okText: getMessageByTermPath('signUp.errorCodes.0000.confirm'),
    onOk: onSignupSuccess,
    closable: false,
  })
}

export async function apiErrorHandler(
  error,
  onSignupError,
  getMessageByTermPath,
  _8010ErrorCodeHandler,
) {
  const message = (await import('components/core/Alert/message')).default

  const messages =
    error?.data?.responseMessage || getMessageByTermPath(`signUp.${error?.data?.code}.content`)

  if (error?.data?.code === '8010') {
    // 註冊IP超過上限
    message({
      title: 'ERROR',
      content: messages,
      okText: getMessageByTermPath(`signUp.errorCodes.${error?.data?.code}.confirm`),
      onOk: _8010ErrorCodeHandler,
      cancelText: getMessageByTermPath('cancel'),
    })
  } else {
    message({
      title: 'ERROR',
      content: messages,
      okText: getMessageByTermPath(`signUp.errorCodes.${error?.data?.code}.confirm`),
    })
  }
  typeof onSignupError === 'function' && onSignupError()
}

export async function callSignupAPI(dataObj, agent) {
  // 看要call signup or signupWithAgentRequest
  // 如果是agent, 先check agent是否合法
  if (agent) {
    // 代理連結註冊
    try {
      const signupRes = await signupWithAgentRequest({ agent, body: dataObj })
      const resultData = signupRes?.data
      return [resultData, null]
    } catch (error) {
      return [null, error]
    }
  } else {
    // 直客註冊
    try {
      const signupRes = await signupRequest({ body: dataObj })
      const resultData = signupRes?.data
      return [resultData, null]
    } catch (error) {
      return [null, error]
    }
  }
}

export const getSessionKey = async getMessageByTermPath => {
  try {
    const sessionKeyRes = await fetchSessionKey()
    if (sessionKeyRes.data) {
      return sessionKeyRes.data.data
    }
  } catch (error) {
    apiErrorHandler(error, null, getMessageByTermPath)
  }
}
