import fetcher from 'libs/fetcher'

export const signupWithAgentRequest = ({ body, agent }) =>
  fetcher({
    url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/affiliatelink/${agent}/account}`,
    method: 'post',
    headers: { 'X-Platform': '4' },
    data: body,
    withToken: false,
  })

export const signupRequest = ({ body }) => {
  const currentDomain = document.location.host
  return fetcher({
    url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/account?domain=${currentDomain}`,
    method: 'post',
    headers: { 'X-Platform': '4' },
    data: body,
    withToken: false,
  })
}

export const fetchSessionKey = () =>
  fetcher({
    url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/sessionkey`,
    withToken: false,
  })
