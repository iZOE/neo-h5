import { useCallback } from 'react'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import { motion } from 'framer-motion'
import dynamic from 'next/dynamic'
import { useHandleError } from 'hooks/useHandleError'
import { useMeBank } from 'pages-lib/withdraw-manage/hooks'
import style from './withdraw-manage.module.scss'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const spring = {
  type: 'spring',
  stiffness: 500,
  damping: 30,
}

export default function WithdrawManageTabLayout({ children }) {
  const intl = useIntl()
  const router = useRouter()

  const [data, error] = useMeBank()
  useHandleError(error)
  const bankCards = data?.bankCards
  const cryptoCurrencies = data?.cryptoCurrencies

  const handleClickNavBack = useCallback(() => {
    router.push('/user')
  }, [])

  const RedirectButtons = [
    {
      url: '/withdraw-manage/banks',
      name: intl.formatMessage({ id: 'banks' }),
    },
    {
      url: '/withdraw-manage/crypto',
      name: intl.formatMessage({ id: 'crypto_currency_wallet' }),
    },
  ]

  const handleClick = path => {
    router.push(path)
  }

  return (
    <div className="flex-1 relative overflow-hidden h-screen dark:bg-purple-900">
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'withdraw_account_management' })}
      />
      <div className="relative">
        <div className={style.layout}>
          <div
            className="flex w-full box-border rounded-full p-1 mb-6 mt-6 bg-white dark:bg-purple-700"
            style={{
              height: '40px',
            }}
          >
            {RedirectButtons.map(({ url, name }) => (
              <div key={url} style={{ position: 'relative', flex: 1 }}>
                {router.pathname === url && (
                  <motion.div
                    layoutId="outline"
                    transition={spring}
                    className="absolute rounded-full inset-0 h-8 bg-platinum-300"
                  />
                )}
                <button
                  type="button"
                  onClick={() => {
                    handleClick(url)
                  }}
                  className="absolute top-2/4 left-2/4 w-full border-none text-body-6"
                  style={{
                    transform: 'translate(-50%,-50%)',
                    color: router.pathname === url ? '#fff' : '#95A3B0',
                    lineHeight: '17px',
                  }}
                >
                  {name}
                </button>
              </div>
            ))}
          </div>
          {(typeof children === 'function' && children(bankCards, cryptoCurrencies)) ||
            (function () {
              throw new Error(
                'children of layout/BankAndCryptoCurrencyWallet must need to be function',
              )
            })()}
        </div>
      </div>
    </div>
  )
}
