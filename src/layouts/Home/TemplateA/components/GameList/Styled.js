import styled from 'styled-components'
import { colors } from '../../constants/colors'

export const NavBar = styled.div`
  width: ${({ fixed }) => (fixed ? 'calc(25% - 5px)' : '25%')};
  background: ${({ theme }) =>
    theme.darkMode ? colors.darkBgColor : colors.lightBlue};
  color: ${colors.sliver};
  border-radius: 16px;
  top: ${({ userInfoHeight }) => `${userInfoHeight + 15}px`};
  position: ${({ fixed }) => (fixed ? 'fixed' : 'initial')};
`
const backgroungColors = {
  0: 'linear-gradient(270deg, #F88581 -7.28%, #DA5F5F 100%)',
  1: 'linear-gradient(270deg, #5FDCD9 -7.22%, #778BDD 100%)',
  2: 'linear-gradient(270deg, #FFB36F 0%, #E78E81 100%)',
  3: 'linear-gradient(180deg, #8CEC7A -13.4%, #82CA45 100%)',
  4: 'linear-gradient(270deg, #CC8DD8 0.68%, #7762B2 100%)',
  5: 'linear-gradient(270deg, #EF96CB 0%, #DD66B8 100%)',
  6: 'linear-gradient(270deg, #F3E93D -7.22%, #FEB927 100%)',
  7: 'linear-gradient(270deg, #6CE8E8 -7.28%, #24A1A8 100%)'
}

export const GameList = styled.div`
  width: 75%;
  padding-left: 10px;
  margin-left: ${({ fixed }) => (fixed ? '25%' : '0')};
`

export const Gamebutton = styled.div`
  height: 75px;
  margin: 10px 0;
  border-radius: 20px;
  position: relative;
  overflow: hidden;
  background: ${props => backgroungColors[Number(props.nth) % 8]};
  &:first-child {
    margin-top: 0;
  }
  &:last-child {
    margin-bottom: 0;
  }
  .name {
    color: #fff;
    position: absolute;
    text-align: right;
    right: 23.5px;
    top: 50%;
  }
  picture > img {
    height: 100%;
    object-fit: cover;
  }
`
