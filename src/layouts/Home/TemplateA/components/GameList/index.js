import { useState, useEffect, useMemo } from 'react'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import smoothscroll from 'smoothscroll-polyfill'
import useSession from 'hooks/useSession'
import GameButton from './components/GameButton'
import Nav from './components/Nav'
import { NavBar, GameList } from './Styled'

export default function Element({ sliders, gameList }) {
  const gameListByAgent = gameList[process.env.NEXT_PUBLIC_AGENT_CODE]
  const [activeGame, setActivedGame] = useState(Object.keys(gameListByAgent)[0])
  const [scrollY, setScrollY] = useState(0)
  const [userInfoHeight, setUserInfoHeight] = useState(0)
  const bannerHeight = sliders.length > 0 ? 144 : 0

  const [hasSession] = useSession()

  const { data: gameProviderStatus } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/status`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
  )

  const { gameCategory, gameButtons } = useMemo(() => {
    const agentCode = process.env.NEXT_PUBLIC_AGENT_CODE
    const category =
      agentCode === 'STA01'
        ? Object.keys(gameListByAgent).filter(c => c !== 'Lottery') // STA01 lottery 暫時移除
        : Object.keys(gameListByAgent)
    const games = []
    for (let i = 0; i < category.length; i++) {
      gameListByAgent[category[i]].map((game, index) => {
        const gameStatus = gameProviderStatus?.find(
          list => list.gameProviderTypeId === game.gameProviderTypeId,
        )
        game.index = index
        game.serviceStatus = gameStatus?.serviceStatus || null
        games.push(game)
      })
    }
    return {
      gameCategory: category,
      gameButtons: games,
    }
  }, [gameListByAgent, gameProviderStatus])

  const scrollToActivedCategory = gameType => {
    window.scrollTo({
      top: document.getElementById(gameType).offsetTop - userInfoHeight - 15,
      behavior: 'smooth',
    })
  }

  function handleScroll() {
    setScrollY(window.pageYOffset)
  }

  useEffect(() => {
    smoothscroll.polyfill()
    const memberInfoHeight = hasSession ? 30 : 0
    setUserInfoHeight(document.getElementById('quickLink').offsetHeight + memberInfoHeight)
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [hasSession])

  return (
    <div className="w-full flex items-start px-3 pt-4" style={{ paddingBottom: '92px' }}>
      <NavBar userInfoHeight={userInfoHeight} fixed={scrollY >= bannerHeight}>
        {gameCategory.map((game, i) => (
          <Nav
            key={game}
            gameType={game}
            active={activeGame === game}
            onClick={gameType => {
              setActivedGame(gameType)
              scrollToActivedCategory(gameType)
            }}
          />
        ))}
      </NavBar>
      <GameList fixed={scrollY >= bannerHeight}>
        {gameButtons.map((game, i) => (
          <GameButton
            key={i}
            game={game}
            scrollY={scrollY}
            lastGame={i === gameButtons.length - 1}
            gameCategory={gameCategory}
            userInfoHeight={userInfoHeight}
            setActivedGame={gameType => setActivedGame(gameType)}
          />
        ))}
      </GameList>
    </div>
  )
}
