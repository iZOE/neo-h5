import { useRef, useEffect, useCallback } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import { mutate } from 'swr'
import fetcher from 'libs/fetcher'
import Picture from 'components/core/Picture'
import useSession from 'hooks/useSession'
import { Gamebutton } from '../../Styled'

const GAME_PROVIDER_STATUS = {
  HIDE: 0,
  ACTIVE: 1,
  LOCK: 2,
  AGENT_LOCK: 3,
}

export default function GameButton({
  game,
  scrollY,
  setActivedGame,
  lastGame,
  userInfoHeight,
  gameCategory,
}) {
  const elm = useRef(null)
  const router = useRouter()
  const [hasToken] = useSession()
  const intl = useIntl()
  const categoryId = game.index === 0 ? game.gameType : ''
  const buttonHalfHeight = 35

  useEffect(() => {
    if (scrollY + userInfoHeight + 15 >= elm?.current?.offsetTop) {
      setActivedGame(game.gameType)
    }
    if (lastGame) {
      const previousCateOffsetTop = document.getElementById(gameCategory[gameCategory.length - 2])
        .offsetTop
      if (scrollY + buttonHalfHeight > previousCateOffsetTop) {
        setActivedGame(game.gameType)
      }
    }
  }, [scrollY])

  const onClick = useCallback(async () => {
    mutate(`${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/status`)
    const message = (await import('components/core/Alert/message')).default
    if (!hasToken) {
      message({
        title: intl.formatMessage({ id: 'dialog.login_tips.title' }),
        content: intl.formatMessage({ id: 'dialog.login_tips.content' }),
        okText: intl.formatMessage({ id: 'dialog.login_tips.okText' }),
        cancelText: intl.formatMessage({ id: 'dialog.login_tips.cancelText' }),
        onOk: () => router.push('/login'),
      })
    } else if (game.serviceStatus !== GAME_PROVIDER_STATUS.ACTIVE) {
      message({
        content: intl.formatMessage({ id: 'dialog_home.content.game_closed' }),
        okText: intl.formatMessage({ id: 'close' }),
      })
    } else if (!game.closed) {
      if (game.url) {
        router.push(game.url)
      } else if (game.gameLobbyEntry) {
        if (
          game.gameProviderId === 27 ||
          game.gameProviderId === 28 ||
          (game.gameProviderId === 2 && game.gameProviderTypeId === 6)
        ) {
          // BBIN, Ok368, AG Slot 另開視窗
          const { gameProviderId, gameProviderTypeId, gameType } = game
          try {
            const res = await fetcher({
              url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/${gameProviderId}/login`,
              method: 'post',
              withToken: true,
              data: {
                gameProviderTypeId,
                gameType,
                deviceType: 2,
              },
            })
            if (res.data.code === '0000') {
              const thirdPartyUrl = res.data.data?.loginUrl
              window.open(thirdPartyUrl, '_blank')
            }
          } catch (err) {
            message({
              content: err?.data?.message,
              okText: intl.formatMessage({ id: 'confirm' }),
            })
          }
        } else {
          router.push(`/game/${game.gameProviderId}?gameProviderTypeId=${game.gameProviderTypeId}`)
        }
      } else {
        router.push(
          `/game/${game.gameProviderId}/list?gameProviderTypeId=${game.gameProviderTypeId}`,
        )
      }
    }
  }, [])
  return (
    <Gamebutton nth={game.nth} key={game.name} ref={elm} id={categoryId} onClick={onClick}>
      <div className="name text-[14px] sm:text-[20px] leading-[22px] sm:leading-7 mt-[-11px] sm:mt-[-14px]">
        {game.closed ? (
          <FormattedMessage id="gameProviderType.comingSoon" />
        ) : (
          <FormattedMessage id={`gameProviderType.${game.name || game.gameProviderTypeId}`} />
        )}
      </div>
      <Picture
        src={`/lobby/${game.gameType}${
          String(game.nth).length < 2 ? `0${game.nth}` : game.nth
        }@3x.png`}
        png={`/lobby/${game.gameType}${
          String(game.nth).length < 2 ? `0${game.nth}` : game.nth
        }@3x.png`}
        webp={`/lobby/${game.gameType}${
          String(game.nth).length < 2 ? `0${game.nth}` : game.nth
        }.webp`}
      />
    </Gamebutton>
  )
}
