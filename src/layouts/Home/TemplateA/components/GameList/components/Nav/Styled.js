import styled from 'styled-components'

export const Wrapper = styled.div.withConfig({
  componentId: 'LobbyNav',
})`
  text-align: center;
  margin: 20px auto;
  svg {
    display: block;
    margin: 0 auto;
    path {
      fill: #95a3b0;
    }
  }
  &.active {
    color: #738dff;
    svg {
      path {
        fill: url(#Sport_svg__paint0_linear);
      }
    }
  }
`
