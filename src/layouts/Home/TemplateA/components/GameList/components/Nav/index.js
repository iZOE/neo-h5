import { motion } from 'framer-motion'
import { FormattedMessage } from 'react-intl'
import IconLottery from 'components/icons/Lottery.svg'
import IconLiveDealer from 'components/icons/LiveDealer.svg'
import IconSport from 'components/icons/Sport.svg'
import IconPoker from 'components/icons/Poker.svg'
import IconSlot from 'components/icons/Slot.svg'
import IconFishing from 'components/icons/Fishing.svg'
import IconCockFight from 'components/icons/CockFight.svg'
import { Wrapper } from './Styled'

const gameIcons = {
  Lottery: <IconLottery />,
  Sport: <IconSport />,
  LiveDealer: <IconLiveDealer />,
  Poker: <IconPoker />,
  Slot: <IconSlot />,
  Fishing: <IconFishing />,
  CockFight: <IconCockFight />,
}

export default function Nav({ gameType, active, onClick }) {
  return (
    <Wrapper onClick={() => onClick(gameType)} className={active ? 'active' : ''}>
      <motion.div
        animate={{
          x: 0,
          y: 0,
          scale: active ? 1.38 : 1,
          rotate: 0,
        }}
      >
        {gameIcons[gameType]}
      </motion.div>
      <div className="text-[11px] sm:text-[18px] leading-[15px] sm:leading[24px] mt-[7.5px]">
        <FormattedMessage id={`gameList.${gameType}`} />
      </div>
    </Wrapper>
  )
}
