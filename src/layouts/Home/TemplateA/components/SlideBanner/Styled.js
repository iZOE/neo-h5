import styled from 'styled-components'

export const Wrapper = styled.div.withConfig({
  componentId: 'SlideBanner',
})`
  position: relative;
  z-index: 100;
  height: 144px;
  .slick-dots {
    width: 15px;
    top: 0px;
    bottom: 0px;
    flex-wrap: wrap;
    align-content: center;
  }
  .slick-dots li {
    margin: 3px;
    width: auto;
    height: auto;
    display: block;
  }
  .slick-dots li button {
    width: 4px;
    height: 4px;
    background: none;
    border: 1px solid #95a3b0;
    box-sizing: border-box;
    border-radius: 50px;
    padding: 0;
  }
  .slick-dots li.slick-active button {
    background: #95a3b0;
    opacity: 0.5;
    height: 8px;
  }
  .slick-dots li button:before {
    display: none;
  }
`

export const ButtonWrapper = styled.button.withConfig({
  componentId: 'SlideBannerButton',
})`
  height: 144px;
  img {
    height: 100%;
  }
`

export const InvisibleWrapper = styled.button.withConfig({
  componentId: 'Invisible',
})`
  position: absolute;
  width: 44px;
  height: 44px;
`
