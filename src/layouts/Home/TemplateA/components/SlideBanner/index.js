import { useRouter } from 'next/router'
import Slider from 'react-slick'
import Picture from 'components/core/Picture'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { Wrapper, ButtonWrapper, InvisibleWrapper } from './Styled'

const settings = {
  adaptiveHeight: false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 5000,
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  pauseOnHover: false,
  appendDots: dots => <ul style={{ display: 'flex' }}>{dots}</ul>,
}

/**
 * from database
 */
const TARGET_PAGE = {
  WEB_VIEW: 0,
  LOTTERY: 1,
  TREND: 2,
  CONFIG: 3,
  IMAGE: 4,
  THIRD_PARTY_GAME: 5,
}

function SlideImage({ data }) {
  const router = useRouter()
  const {
    targetPage,
    imageUrl,
    targetUrl,
    lotteryCodeName,
    gameProviderID,
    gameProviderTypeID,
  } = data
  function handleClick() {
    if (targetPage === TARGET_PAGE.LOTTERY) {
      router.push(`/game/${lotteryCodeName}`)
    } else if (targetPage === TARGET_PAGE.THIRD_PARTY_GAME) {
      router.push(`/thirdpartygame/${gameProviderID}/${gameProviderTypeID}`)
    } else if (targetPage !== TARGET_PAGE.IMAGE) {
      router.push(targetUrl || '/')
    }
  }
  return (
    // eslint-disable-next-line jsx-a11y/control-has-associated-label
    <>
      <InvisibleWrapper />
      <ButtonWrapper className="border-none overflow-hidden" type="button" onClick={handleClick}>
        <Picture
          png={`${process.env.NEXT_PUBLIC_END_POINT_STORAGE}/${imageUrl}.png`}
          webp={`${process.env.NEXT_PUBLIC_END_POINT_STORAGE}/${imageUrl}.webp`}
          alt="logo"
        />
      </ButtonWrapper>
    </>
  )
}

export default function SlideBanner({ sliders }) {
  if (sliders.length === 0) {
    return null
  }

  return (
    <Wrapper>
      <Slider {...settings}>
        {sliders.map((image, index) => (
          <SlideImage key={`slide_${index.toString()}`} data={image} />
        ))}
      </Slider>
    </Wrapper>
  )
}
