import { useState, useEffect, useRef } from 'react'
import useSWR, { cache } from 'swr'
import { motion } from 'framer-motion'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import IconLoudly from 'components/icons/Loudly.svg'

const AnnounceModal = dynamic(() => import('pages-lib/index/components/AnnounceModal'))

const noticeBoardAPIAddr = `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/im/noticeboards?agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`

const additionalStyle = { backgroundColor: 'rgba(0, 0, 0, 0.3)', zIndex: 100 }

export default function Marquee({ sliders }) {
  const [displayMarqueeIndex, setDisplayMarqueeIndex] = useState(0)
  const [isShowAnnounceModal, setIsShowAnnounceModal] = useState(false)
  const openedModalMarqueeIndex = useRef(0)

  const { data: container, error } = useSWR(
    noticeBoardAPIAddr,
    async url => {
      const res = await fetcher({ url })
      const items = res?.data?.data.container.filter(m => m.isMarqueeEnable)
      return items
    },
    {
      revalidateOnFocus: false,
      revalidateOnMount: !cache.has(noticeBoardAPIAddr),
    },
  )

  useEffect(() => {
    if (!error && container) {
      const timer = setTimeout(() => {
        const nextIndex = displayMarqueeIndex + 1 === container.length ? 0 : displayMarqueeIndex + 1
        setDisplayMarqueeIndex(nextIndex)
        if (!isShowAnnounceModal) {
          openedModalMarqueeIndex.current = nextIndex
        }
      }, 5000)

      return () => {
        clearTimeout(timer)
      }
    }
  }, [container, displayMarqueeIndex, error, isShowAnnounceModal])

  if (!container || container.length === 0) {
    return null
  }
  const marginTop = sliders.length > 0 ? '-mt-5 sm:-mt-8' : ''

  const handleOpen = () => setIsShowAnnounceModal(true)
  const handleClose = () => {
    openedModalMarqueeIndex.current = displayMarqueeIndex
    setIsShowAnnounceModal(false)
  }

  return (
    <div
      data-testid="marquee"
      className={`relative overflow-hidden flex h-5 sm:h-8 ${marginTop}`}
      style={additionalStyle}
    >
      <div className="flex items-center mx-[10px]">
        <IconLoudly />
      </div>
      <div>
        {!error && container && (
          <div className="mt-[2px] sm:mt-[6px]">
            <div className="text-body-6 text-white" onClick={handleOpen}>
              <motion.div
                key={container[displayMarqueeIndex]?.subject}
                initial={{ y: -20 }}
                animate={{ y: 0 }}
                exit={{ opacity: 20 }}
              >
                {container[displayMarqueeIndex]?.subject}
              </motion.div>
            </div>
            <AnnounceModal
              isOpen={isShowAnnounceModal}
              handleClose={handleClose}
              data={[container[openedModalMarqueeIndex.current]]}
            />
          </div>
        )}
      </div>
    </div>
  )
}
