import UserData from './components/UserData'
import Menu from './components/Menu'
import { Wrapper } from './Styled'

export default function UserInfo() {
  return (
    <Wrapper>
      <UserData />
      <Menu />
    </Wrapper>
  )
}
