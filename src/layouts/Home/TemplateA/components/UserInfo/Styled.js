/* eslint-disable no-confusing-arrow */
import styled from 'styled-components'
import { colors } from '../../constants/colors'

export const Wrapper = styled.div`
  padding: 0 12.8px;
  position: sticky;
  top: 0;
  z-index: 100;
  background: ${({ theme }) =>
    theme.darkMode ? colors.gradient.dark : colors.gradient.lightSliver};
  box-shadow: ${({ theme }) =>
    theme.darkMode ? colors.boxShadow.dark : colors.boxShadow.lightSliver};
  .member {
    &::after {
      width: 100%;
      margin: 8px 5px 3px;
      content: '';
      display: block;
      border-top: ${({ theme }) => theme.darkMode ? `1px solid #454545` : `1px solid #FFFFFF`};
    }
  }
  .quick-link {
    > div {
      color: ${colors.sliver};
      text-decoration: none;
      svg {
        margin: 0 auto;
      }
    }
  }
`
