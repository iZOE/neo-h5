import { FormattedMessage } from 'react-intl'
import useRouterPushBeforeCheckLogin from 'hooks/useRouterPushBeforeCheckLogin'
import IconRecharge from 'components/icons/Recharge.svg'
import IconTransfer from 'components/icons/Transfer.svg'
import IconVIP from 'components/icons/VIP.svg'
import IconWithdraw from 'components/icons/Withdraw.svg'
import Text from '../Text'

export default function Menu() {
  const pushBeforeCheck = useRouterPushBeforeCheckLogin()

  return (
    <div className="flex justify-around quick-link" id="quickLink">
      <div
        className="py-1 text-center"
        onClick={() => {
          pushBeforeCheck('/deposit')
        }}
      >
        <IconRecharge />
        <Text size={6} fontWeight={600} display="block">
          <FormattedMessage id="recharge" />
        </Text>
      </div>
      <div
        className="py-1 text-center"
        onClick={() => {
          pushBeforeCheck('/withdrawal')
        }}
      >
        <IconWithdraw />
        <Text size={6} fontWeight={600} display="block">
          <FormattedMessage id="withdraw" />
        </Text>
      </div>
      <div
        className="py-1 text-center"
        onClick={() => {
          pushBeforeCheck('/transfer')
        }}
      >
        <IconTransfer />
        <Text size={6} fontWeight={600} display="block">
          <FormattedMessage id="transfer" />
        </Text>
      </div>
      <div
        className="py-1 text-center"
        onClick={() => {
          pushBeforeCheck('/vip')
        }}
      >
        <IconVIP />
        <Text size={6} fontWeight={600} display="block">
          <FormattedMessage id="vip" />
        </Text>
      </div>
    </div>
  )
}
