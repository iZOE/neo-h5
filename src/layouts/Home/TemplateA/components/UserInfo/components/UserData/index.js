import { FormattedMessage } from 'react-intl'
import Link from 'next/link'
import useUserData from 'hooks/useUserData'
import dynamic from 'next/dynamic'

const Currency = dynamic(() => import('components/core/Currency'))

export default function UserData() {
  const { userData, balance } = useUserData()

  if (!userData) {
    return null
  }

  return (
    <div className="member flex justify-between flex-wrap text-platinum-300 dark:text-white">
      <div>
        <div className="inline-flex text-body-5 text-platinum-300 dark:text-white">
          {userData.account}
        </div>
        <Link href="/vip">
          <div
            className="inline-flex ml-2 px-1 bg-platinum-200 text-white rounded"
            style={{ paddingTop: '2px', paddingBottom: '2px' }}
          >
            <div className="text-body-6">VIP{userData.levelName}</div>
          </div>
        </Link>
      </div>
      <div className="font-semibold">
        <span className="text-body-5">
          <FormattedMessage id="userInfo.wallet" />
        </span>
        {balance && <Currency value={balance} />}
      </div>
    </div>
  )
}
