import styled from '@emotion/styled'
import { typography, display } from 'styled-system'

const Box = styled('span')(typography, display)

export default function Text({ size, children, ...rest }) {
  const fontStyle = {
    7: {
      fontSize: [11, 13],
      linehight: [15, 18],
    },
    6: {
      fontSize: [12, 18],
      linehight: [16, 24],
    },
    5: {
      fontSize: [13, 15],
      linehight: [18, 20],
    },
  }
  return (
    <Box
      linehight={fontStyle[size].linehight}
      fontSize={fontStyle[size].fontSize}
      {...rest}
    >
      {children}
    </Box>
  )
}
