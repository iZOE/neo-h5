import { useTheme } from 'styled-components'
import SlideBanner from './components/SlideBanner'
import Marquee from './components/Marquee'
import GameList from './components/GameList'
import UserInfo from './components/UserInfo'

export default function TemplateA({ sliders, gameList }) {
  return (
    <div className="my-0 mx-auto max-w-screen-sm bg-white dark:bg-purple-900">
      <SlideBanner sliders={sliders} />
      <Marquee sliders={sliders} />
      <UserInfo />
      <GameList sliders={sliders} gameList={gameList} />
    </div>
  )
}
