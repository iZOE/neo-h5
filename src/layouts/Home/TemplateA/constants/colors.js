export const colors = {
  white: '#FFFFFF',
  deepBlue: '#5E7184',
  lightBlue: '#EBF1FC',
  sliver: '#95A3B0',
  darkBgColor: '#161616',
  gradient: {
    lightSliver: 'linear-gradient(179.05deg, #f2f7ff 0.52%, #d9e3f2 98.92%)',
    dark: 'linear-gradient(179.04deg, #3A3845 0.53%, #20202C 92.9%)',
  },
  boxShadow: {
    lightSliver: '0px 2px 4px rgba(124, 149, 180, 0.899694)',
    dark: '3px 2px 4px #15151F',
  },
}
