import useSWR from 'swr'
import fetcher from 'libs/fetcher'

export default function useGameProviderData(gameProviderId) {
  const result = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider`,
    async url => {
      const res = await fetcher({ url, withToken: true })

      const data =
        res?.data?.data.find(
          list => list.gameProviderId === Number(gameProviderId)
        ) || {}
      return data
    },
    {
      revalidateOnFocus: false,
    }
  )

  return {
    gameProviderData: result,
  }
}
