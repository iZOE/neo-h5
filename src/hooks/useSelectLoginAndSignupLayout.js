import dynamic from 'next/dynamic'
import TemplateA from '../layouts/LoginAndSignup/TemplateA'
import TemplateB from '../layouts/LoginAndSignup/TemplateB'

/*
const TemplateA = dynamic(() => import('../layouts/LoginAndSignup/TemplateA'), {
  loading: () => <p>Loading...</p>,
})

const TemplateB = dynamic(() => import('../layouts/LoginAndSignup/TemplateB'), {
  loading: () => <p>Loading...</p>,
})
*/

export default function useSelectLoginAndSignupLayout({ type }) {
  let LayoutComponent = TemplateA
  if (type === 'B') {
    LayoutComponent = TemplateB
  }

  return LayoutComponent
}
