import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import { useRecoilState } from 'recoil'
import useLocalStorage from 'hooks/useLocalStorage'
import { tokenState } from 'atoms/tokenState'

export default function useSession() {
    const intl = useIntl()
    const router = useRouter()
    const [storedNoShowAnnounceModal, setStoredNoShowAnnounceModal] = useLocalStorage(
        'noShowAnnounceModal',
    )

    const [token, setToken, removeToken] = useLocalStorage('token')

    async function clear() {
        const message = (await import('components/core/Alert/message')).default
        message({
            content: intl.formatMessage({ id: 'dialog.logout_tips.content' }),
            okText: intl.formatMessage({ id: 'confirm' }),
            cancelText: intl.formatMessage({ id: 'cancel' }),
            onOk: () => {
                removeToken()
                setStoredNoShowAnnounceModal(false)
                router.push('/login')
            },
        })
    }

    return [token, clear]
}
