import useCountdown from '../useCountdown'

import { renderHook, act } from '@testing-library/react-hooks'

jest.setTimeout(10000)

test('can return seconds, and return countdown seconds continuously', async () => {
  const { result, waitForValueToChange } = renderHook(() => useCountdown(600))
  const [remainSeconds1] = result.current
  expect(remainSeconds1).toBe(600)

  await waitForValueToChange(() => result.current, { timeout: 1050 })

  const [remainSeconds2] = result.current
  expect(remainSeconds2).toEqual(599)
  await waitForValueToChange(() => result.current, { timeout: 1050 })
  const [remainSeconds3] = result.current
  expect(remainSeconds3).toEqual(598)
})

test('can return seconds, and reset the countdown seconds by the exported API: resetTimer func call', async () => {
  const { result, waitForValueToChange } = renderHook(() => useCountdown(600))
  const [remainSeconds1, resetTimer] = result.current
  expect(remainSeconds1).toBe(600)

  await waitForValueToChange(() => result.current, { timeout: 1050 })

  const [remainSeconds2] = result.current
  expect(remainSeconds2).toEqual(599)
  await waitForValueToChange(() => result.current, { timeout: 1050 })
  const [remainSeconds3] = result.current
  expect(remainSeconds3).toEqual(598)
  act(() => {
    resetTimer()
  })
  const [remainSeconds4] = result.current
  expect(remainSeconds4).toEqual(600)
  await waitForValueToChange(() => result.current, { timeout: 1050 })
  const [remainSeconds5] = result.current
  expect(remainSeconds5).toEqual(599)
})

test('when invoke resetTimer, if give different countdown time, the customHook will use it to countdown', async () => {
  const { result, waitForValueToChange } = renderHook(() => useCountdown(600))
  const [remainSeconds1, resetTimer] = result.current
  expect(remainSeconds1).toBe(600)

  await waitForValueToChange(() => result.current, { timeout: 1050 })

  const [remainSeconds2] = result.current
  expect(remainSeconds2).toEqual(599)
  await waitForValueToChange(() => result.current, { timeout: 1050 })
  const [remainSeconds3] = result.current
  expect(remainSeconds3).toEqual(598)
  act(() => {
    resetTimer(800)
  })
  const [remainSeconds4] = result.current
  expect(remainSeconds4).toEqual(800)
  await waitForValueToChange(() => result.current, { timeout: 1050 })
  const [remainSeconds5] = result.current
  expect(remainSeconds5).toEqual(799)
})

test('it can countdown to 0 and return 0', async () => {
  const { result, waitForValueToChange } = renderHook(() => useCountdown(6))
  const [remainSeconds1] = result.current
  expect(remainSeconds1).toBe(6)

  await waitForValueToChange(() => result.current[0], { timeout: 1050 })
  const [remainSeconds2] = result.current
  expect(remainSeconds2).toEqual(5)

  await waitForValueToChange(() => result.current[0], { timeout: 1050 })
  const [remainSeconds3] = result.current
  expect(remainSeconds3).toEqual(4)

  await waitForValueToChange(() => result.current[0], { timeout: 1050 })
  const [remainSeconds4] = result.current
  expect(remainSeconds4).toEqual(3)

  await waitForValueToChange(() => result.current[0], { timeout: 1050 })
  const [remainSeconds5] = result.current
  expect(remainSeconds5).toEqual(2)

  await waitForValueToChange(() => result.current[0], { timeout: 1050 })
  const [remainSeconds6] = result.current
  expect(remainSeconds6).toEqual(1)

  await waitForValueToChange(() => result.current[0], { timeout: 1050 })
  const [remainSeconds7] = result.current
  expect(remainSeconds7).toEqual(0)
})
