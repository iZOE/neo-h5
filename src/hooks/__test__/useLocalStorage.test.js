import { renderHook, act } from '@testing-library/react-hooks'
import useLocalStorage from '../useLocalStorage'

test('update localstorage value', () => {
  const { result } = renderHook(() => useLocalStorage('token'))
  const [storedValue, setValue] = result.current
  expect(storedValue).toBe(undefined)

  act(() => {
    setValue('two')
  })

  expect(storedValue).toBe(undefined)
})
