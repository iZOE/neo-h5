import { useCallback, useEffect } from 'react'
import { useRecoilValue, useSetRecoilState, useResetRecoilState } from 'recoil'
import { useRouter } from 'next/router'
import { pageStepState } from '../atoms/pageStepState'

export function useStepData(key) {
  const { data } = useRecoilValue(pageStepState)

  if (data && key) {
    return data[key]
  }

  return data
}

export function useStepView(views, min, max) {
  const { currentStep } = useRecoilValue(pageStepState)
  const router = useRouter()
  const resetPageStepState = useResetRecoilState(pageStepState)

  const stepFromQuery = router.query && router.query.step
  useEffect(() => {
    if (stepFromQuery) {
      if (Number(stepFromQuery) !== Number(currentStep)) {
        router.back()
      }
    }
  }, [stepFromQuery])

  useEffect(
    () => () => {
      resetPageStepState()
    },
    [],
  )

  if (stepFromQuery && Number(stepFromQuery) !== Number(currentStep)) {
    return () => <></>
  }
  return views[router.query && router.query.step <= max ? router.query.step : min]
}

export default function useStep() {
  const setCurrentStep = useSetRecoilState(pageStepState)
  const router = useRouter()
  const gotoNext = useCallback(nextData => {
    const nextQueryStep = router.query && router.query.step ? Number(router.query.step) + 1 : 2
    setCurrentStep(({ data }) => ({
      currentStep: nextQueryStep,
      data: {
        ...data,
        ...nextData,
      },
    }))
    router.push(`?step=${nextQueryStep}`, undefined, { shallow: true })
  }, [])
  const gotoPrevious = useCallback(nextData => {
    const nextQueryStep = router.query && router.query.step > 1 ? Number(router.query.step) - 1 : 1
    setCurrentStep(({ data }) => ({
      currentStep: nextQueryStep,
      data: {
        ...data,
        ...nextData,
      },
    }))
    router.push(`?step=${nextQueryStep}`, undefined, { shallow: true })
  }, [])
  const gotoStep = (step, nStepData) => {
    if (nStepData) {
      setCurrentStep(({ data }) => ({
        currentStep: step,
        data: {
          ...data,
          ...nStepData,
        },
      }))
    } else {
      setCurrentStep(({ data }) => ({
        currentStep: step,
        data: {
          ...data,
        },
      }))
    }
    router.push(`?step=${step}`, undefined, { shallow: true })
  }

  return {
    gotoNext,
    gotoPrevious,
    gotoStep,
  }
}
