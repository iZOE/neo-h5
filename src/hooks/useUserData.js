import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import useSession from 'hooks/useSession'

export default function useUserData() {
    const [hasSession] = useSession()

    const {data: userData, error} = useSWR(
        hasSession ? `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me` : null,
        async url => {
            const res = await fetcher({url, withToken: true})
            return res.data.data
        },
        {
            revalidateOnFocus: false,
        }
    )

    const {data: balance, mutate: mutateBalance} = useSWR(
        hasSession
            ? `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/1/wallet`
            : null,
        async url => {
            const res = await fetcher({url, withToken: true})
            return res.data.data.money
        },
        {
            revalidateOnFocus: false,
        }
    )

    return {
        userData,
        balance,
        mutateBalance,
    }
}
