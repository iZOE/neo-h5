import { useEffect } from 'react'
import { useIntl } from 'react-intl'

export function useHandleError(error, getContent, okText) {
  const intl = useIntl()
  useEffect(async () => {
    if (error) {
      const message = (await import('components/core/Alert/message')).default
      const msg = getContent ? getContent(error) : error?.responseMessage || error?.message || error
      message({
        content: msg,
        okText: okText || intl.formatMessage({ id: 'close' }),
      })
    }
  }, [error])
}
