import useSWR from 'swr'
import fetcher from 'libs/fetcher'

export default function useWalletData(gameProviderId) {
  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}/v2/api/game-provider/${gameProviderId}/wallet`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
    {
      revalidateOnFocus: false,
    }
  )

  return {
    wallet: data,
  }
}
