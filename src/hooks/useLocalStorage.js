import { useState, useEffect } from 'react'

export default function useLocalStorage(key, initialValue) {
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const item = window.localStorage.getItem(key)
      return item ? JSON.parse(item) : initialValue
    } catch (error) {
      return initialValue
    }
  })

  const setValue = value => {
    try {
      const valueToStore = value instanceof Function ? value(storedValue) : value
      setStoredValue(valueToStore)
      window.localStorage.setItem(key, valueToStore)
    } catch (error) {
      console.log(error)
    }
  }

  const remove = () => {
    try {
      setStoredValue(null)
      window.localStorage.removeItem(key)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    function handler() {
      const item = window.localStorage.getItem(key)
      const newValue = item ? JSON.parse(item) : null
      setStoredValue(newValue)
    }

    window.addEventListener('storage', handler, false)

    return () => {
      window.removeEventListener('storage', handler, false)
    }
  }, [key])

  return [storedValue, setValue, remove]
}
