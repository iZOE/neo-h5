/* eslint-disable no-confusing-arrow */
import React, { useCallback, useMemo } from 'react'
import useSWR from 'swr'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import fetcher from 'libs/fetcher'
import { showDockingMultipleControl } from 'components/core/DockingMultipleControl/openDocking'
// import showMessage from 'components/core/Alert/message'
import useSession from 'hooks/useSession'
import CustomerServiceOptionList from './components/CustomerServiceOptionList'

export function useOpenCustomerService() {
  const intl = useIntl()
  const router = useRouter()
  const [hasToken] = useSession()

  const url = useMemo(
    () =>
      hasToken
        ? 'v2/api/customerservice/otherContact'
        : `v2/api/customerservice/otherContact/visitor?agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`,
    [hasToken],
  )

  const transDataOnFetcherSuccess = useCallback(res => {
    const options = res.data.data
    if (options.length === 0) {
      return []
    }

    return options.map(item => {
      let href
      switch (item.mode) {
        case 2:
          href = `tel:${item.content}`
          break
        case 3:
          href = `${item.content}`
          break
        // 如果mode === 1, 純文字, item.content應該是null
        default:
          href = item.content
      }
      return { href, name: item.name }
    })
  }, [])

  const { data: options, error } = useSWR(
    [url, hasToken],
    (url, hasToken) => {
      if (!hasToken) {
        return JSON.parse(process.env.NEXT_PUBLIC_NOT_LOGIN_CUSTOMER_SERVICE_OPTIONS)
      }
      return fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}${url}`,
        withToken: hasToken,
      }).then(transDataOnFetcherSuccess)
    },
    { revalidateOnFocus: false },
  )

  if (error) {
    return async () => {
      const message = (await import('components/core/Alert/message')).default
      message({
        title: intl.formatMessage({ id: 'common.apiErrorTitle' }),
        content: intl.formatMessage({ id: 'common.apiErrorContent' }),
      })
    }
  }

  // initial executed, the data from useSWR is undefined
  if (!options) {
    return () => {}
  }

  if (options && options.length === 0) {
    return () => {
      router.push('/service')
    }
  }

  return () => {
    showDockingMultipleControl(
      options,
      intl.formatMessage({ id: 'cancel' }),
      CustomerServiceOptionList,
    )
  }
}
