import styled from 'styled-components'

export const CancelButtonWrapper = styled.div`
  & > button {
    margin: 8px 16px;
    width: calc(100vw - 16px * 2);
  }
`
