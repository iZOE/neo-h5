import React from 'react'
import DockingItem from 'components/core/DockingMultipleControl/components/DockingItem'
import Button from 'components/core/Button'
import { CancelButtonWrapper } from './Styled'

const stopPropagation = evt => evt.stopPropagation()

const CustomerServiceOptionList = ({ data, textOnCancelButton, onCancel }) => {
  return (
    <>
      {data?.map(item => (
        <DockingItem onClick={stopPropagation} href={item.href} key={item.name} name={item.name} />
      ))}
      <CancelButtonWrapper>
        <Button variant="primary" onClick={onCancel}>
          {textOnCancelButton}
        </Button>
      </CancelButtonWrapper>
    </>
  )
}

export default CustomerServiceOptionList
