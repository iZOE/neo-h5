import { useEffect, useState, useCallback } from 'react'

export default function useCountdown(seconds) {
  const [remainSeconds, setRemainSeconds] = useState(Number(Number.isNaN(seconds) ? 0 : seconds))

  const resetTimer = useCallback(resetSeconds => {
    setRemainSeconds(prev => (resetSeconds ? resetSeconds : seconds))
  }, [])

  useEffect(() => {
    const intervalID = setInterval(() => {
      setRemainSeconds(prev => (prev === 0 ? 0 : prev - 1))
    }, 1000)

    return () => {
      clearInterval(intervalID)
    }
  }, [])

  return [remainSeconds, resetTimer]
}
