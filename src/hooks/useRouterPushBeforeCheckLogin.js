import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import useSession from './useSession'

export default function useRouterPushBeforeCheckLogin() {
  const intl = useIntl()
  const router = useRouter()
  const [hasSession] = useSession()

  const handleCheckLogin = async (url, checkWithoutSession) => {
    // 活動頁的特殊處理
    if (checkWithoutSession) {
      const token = window.localStorage.getItem('token')
      if (token && url) {
        router.push(url)
      } else {
        const message = (await import('components/core/Alert/message')).default
        message({
          title: intl.formatMessage({ id: 'dialog.login_tips.title' }),
          content: intl.formatMessage({ id: 'dialog.login_tips.content' }),
          okText: intl.formatMessage({ id: 'dialog.login_tips.okText' }),
          cancelText: intl.formatMessage({ id: 'dialog.login_tips.cancelText' }),
          onOk: () => {
            router.push('/login')
          },
        })
      }
    } else if (hasSession) {
      if (url) {
        router.push(url)
      }
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        title: intl.formatMessage({ id: 'dialog.login_tips.title' }),
        content: intl.formatMessage({ id: 'dialog.login_tips.content' }),
        okText: intl.formatMessage({ id: 'dialog.login_tips.okText' }),
        cancelText: intl.formatMessage({ id: 'dialog.login_tips.cancelText' }),
        onOk: () => {
          router.push('/login')
        },
      })
    }
  }

  return handleCheckLogin
}
