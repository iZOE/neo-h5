import { render } from '@testing-library/react'
import Picture from '../core/Picture'

test('should get media path', () => {
  const { getByAltText } = render(<Picture />);
  const image = getByAltText('alt');

  expect(image.src).toContain('');
})
