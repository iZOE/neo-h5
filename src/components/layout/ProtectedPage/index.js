import { useEffect } from 'react'
import { useRouter } from 'next/router'
import fetcher from 'libs/fetcher'

import useSession from '../../../hooks/useSession'

function AftCheck({ children }) {
  const router = useRouter()

  const updateUserWalletFrom3rd = async () => {
    await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/aft`,
      method: 'post',
      withToken: true,
    })
  }

  const updateUserWalletTo3rd = async gameProviderId => {
    await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/aft/game-provider/${gameProviderId}`,
      method: 'post',
      withToken: true,
    })
  }

  useEffect(() => {
    const autoTransfer = localStorage.getItem('is_auto_transfer')
    const { gameProviderId } = router?.query
    if (autoTransfer === null || autoTransfer === 'true') {
      if (!gameProviderId) {
        updateUserWalletFrom3rd()
      } else {
        updateUserWalletTo3rd(gameProviderId)
      }
    }
  })

  return children
}

export default function ProtectedPage({ children }) {
  const [hasToken] = useSession()

  if (hasToken) {
    return <AftCheck>{children}</AftCheck>
  }

  return <div>no protected</div>
}
