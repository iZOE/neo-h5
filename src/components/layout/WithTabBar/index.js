import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import { useSetRecoilState } from 'recoil'
import { pageTransitionState } from 'atoms/pageTransitionState'
import useRouterPushBeforeCheckLogin from 'hooks/useRouterPushBeforeCheckLogin'
import IconCustomerService from 'components/icons/CustomerService.svg'
import IconEvent from 'components/icons/Event.svg'
import IconGamePad from 'components/icons/GamePad.svg'
import IconAccount from 'components/icons/Account.svg'
import { TabBar, IconButton } from './Styled'

const TABS = [
  {
    nameId: 'tabBar.button1',
    icon: <IconGamePad />,
    url: '/',
  },
  {
    nameId: 'tabBar.button2',
    icon: <IconCustomerService />,
    url: '/service',
  },
  {
    nameId: 'tabBar.button3',
    icon: <IconEvent />,
    url: '/activity',
    needCheckSession: true,
  },
  {
    nameId: 'tabBar.button4',
    icon: <IconAccount />,
    url: '/user',
  },
]

function TabButton({ url, title, icon, needCheckSession, order }) {
  const router = useRouter()
  const setPageTransition = useSetRecoilState(pageTransitionState)
  const pushBeforeCheck = useRouterPushBeforeCheckLogin()
  return (
    <Link href={url}>
      <div
        className="flex-1"
        onClick={event => {
          if (needCheckSession) {
            event.preventDefault()
            pushBeforeCheck(url, true)
          }
          // use current url to find index
          const currentUrlIndex = TABS.findIndex(tab => tab.url === router.pathname)
          let direction = '<'
          if (currentUrlIndex > order) {
            direction = '>'
          }
          setPageTransition(prev => ({
            ...prev,
            direction,
          }))
        }}
      >
        <IconButton active={router.pathname === url}>
          {icon}
          <div className="text-body-5 sm:text-body-7">{title}</div>
        </IconButton>
      </div>
    </Link>
  )
}

export default function WithTabBar({ children }) {
  return (
    <>
      {children}
      <TabBar className="w-full fixed bottom-0 flex items-center justify-center">
        <div className="flex py-2 sm:py-3 text-center justify-around w-full sm:w-[600px]">
          {TABS.map((tab, index) => (
            <TabButton
              key={tab.url}
              order={index}
              title={<FormattedMessage id={tab.nameId} />}
              icon={tab.icon}
              url={tab.url}
              needCheckSession={tab.needCheckSession}
            />
          ))}
        </div>
      </TabBar>
    </>
  )
}
