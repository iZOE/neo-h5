/* eslint-disable no-confusing-arrow */
import styled from 'styled-components'

const colors = {
  gradient: {
    lightSliver: 'linear-gradient(179.05deg, #f2f7ff 0.52%, #d9e3f2 98.92%)',
    dark: 'linear-gradient(179.04deg, #3A3845 0.53%, #20202C 92.9%)',
  },
  boxShadow: {
    lightSliver: '0px 2px 4px rgba(124, 149, 180, 0.899694)',
    dark: '3px 2px 4px #15151F',
  },
}

export const TabBar = styled.div`
  background: ${({ theme }) =>
    theme.darkMode ? colors.gradient.dark : colors.gradient.lightSliver};
  box-shadow: ${({ theme }) =>
    theme.darkMode ? colors.boxShadow.dark : colors.boxShadow.lightSliver};
  padding-bottom: constant(safe-area-inset-bottom, 20px); /* iOS 11.0-iOS 11.1 */
  padding-bottom: env(safe-area-inset-bottom, 20px); /*iOS 11.2 */
`

export const IconButton = styled.div`
  a {
  }
  svg {
    display: block;
    margin: 0 auto;
    path {
      fill: ${({ active }) => (active ? '#738DFF' : '#95A3B0')};
    }
  }
  color: ${({ active }) => (active ? '#738DFF' : '#95A3B0')};
`
