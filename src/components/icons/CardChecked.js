import SvgWrapper from './Wrapper'

export default function IconCardChecked({ width = 24, height = 24, fillColor = 'white' }) {
  return (
    <SvgWrapper w={width} h={height}>
      <path
        fill={fillColor}
        fillRule="evenodd"
        clipRule="evenodd"
        d="M12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21ZM16.8912 9.31139C17.0632 9.09533 17.0274 8.78077 16.8114 8.60881C16.5953 8.43684 16.2807 8.47259 16.1088 8.68865L10.9032 15.229L7.86986 11.8943C7.68405 11.6901 7.36782 11.6751 7.16355 11.8609C6.95927 12.0467 6.94431 12.363 7.13012 12.5672L10.5587 16.3365C10.6573 16.4449 10.7985 16.5046 10.945 16.4997C11.0915 16.4949 11.2285 16.4261 11.3198 16.3114L16.8912 9.31139Z"
      />
    </SvgWrapper>
  )
}
