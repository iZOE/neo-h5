import SvgWrapper from './Wrapper'

export default function IconProtocolERC20({ width = 24, height = 24 }) {
  return (
    <SvgWrapper w={width} h={height}>
      <path
        d="M11.7819 2L11.6475 2.45574V15.6802L11.7819 15.8141L17.9353 12.1855L11.7819 2Z"
        fill="#44C5BE"
        fillOpacity="0.8"
      />
      <path
        d="M11.7818 2L5.62842 12.1855L11.7818 15.8141V9.39534V2Z"
        fill="#44C5BE"
        fillOpacity="0.6"
      />
      <path
        d="M11.7818 16.9763L11.7061 17.0684V21.7793L11.7818 22L17.9389 13.3496L11.7818 16.9763Z"
        fill="#44C5BE"
        fillOpacity="0.8"
      />
      <path d="M11.7818 22V16.9763L5.62842 13.3496L11.7818 22Z" fill="#44C5BE" fillOpacity="0.4" />
      <path d="M11.7817 15.8141L17.935 12.1856L11.7817 9.39545V15.8141Z" fill="#44C5BE" />
      <path
        d="M5.62842 12.1856L11.7817 15.8141V9.39545L5.62842 12.1856Z"
        fill="#44C5BE"
        fillOpacity="0.8"
      />
    </SvgWrapper>
  )
}
