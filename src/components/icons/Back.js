import SvgWrapper from './Wrapper'

const Back = ({ w, h, fillColor = 'text-platinum-200' }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M16.1897 3.37534C16.5183 3.8066 16.4517 4.4359 16.041 4.78091L7.44735 12L16.041 19.2192C16.4517 19.5642 16.5183 20.1935 16.1897 20.6247C15.8612 21.056 15.2619 21.1259 14.8512 20.7809L4.39844 12L14.8512 3.21917C15.2619 2.87416 15.8612 2.94408 16.1897 3.37534Z"
    />
  </SvgWrapper>
)

export default Back
