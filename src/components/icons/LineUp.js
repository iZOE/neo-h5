import SvgWrapper from './Wrapper'

const LineUp = ({ width, height, fillColor }) => (
  <SvgWrapper w={width} h={height} fillColor={fillColor} vw={16} vh={16}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M13.3033 10.9779C13.1297 11.1515 12.8091 11.1908 12.6355 11.0172L8.00001 6.38175L3.36453 11.0172C3.19096 11.1908 2.87027 11.1515 2.69671 10.9779C2.52314 10.8044 2.48386 10.4837 2.65742 10.3101L8.00001 4.96754L13.3426 10.3101C13.5162 10.4837 13.4769 10.8044 13.3033 10.9779Z"
    />
  </SvgWrapper>
)

export default LineUp
