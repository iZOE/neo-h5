import SvgWrapper from './Wrapper'

const TriangleDown = ({ w, h, fillColor, vw, vh }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor} vw={vw} vh={vh}>
    <path fillRule="evenodd" clipRule="evenodd" d="M7.99999 12L13.6568 6.34315H2.34314L7.99999 12Z" />
  </SvgWrapper>
)

export default TriangleDown
