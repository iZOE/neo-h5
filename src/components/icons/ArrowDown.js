import SvgWrapper from './Wrapper'

const ArrowDown = ({ w, h, fillColor = 'text-platinum-200', vw, vh }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor} vw={vw} vh={vh}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M2.6967 5.98959C2.87027 5.81602 3.19096 5.77674 3.36452 5.9503L8 10.5858L12.6355 5.9503C12.809 5.77674 13.1297 5.81602 13.3033 5.98959C13.4769 6.16315 13.5162 6.48384 13.3426 6.65741L8 12L2.65742 6.65741C2.48385 6.48384 2.52313 6.16315 2.6967 5.98959Z"
    />
  </SvgWrapper>
)

export default ArrowDown
