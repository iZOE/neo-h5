import classnames from 'classnames'

const ArrowUp = ({ width = 16, height = 16, fillColor = 'black', className = '' }) => <svg width={width}
                                                                                             height={height}
                                                                                             viewBox='0 0 12 8'
                                                                                             fill='none'
                                                                                             className={classnames('fill-current', className)}
                                                                                             xmlns='http://www.w3.org/2000/svg'>
    <path fillRule='evenodd' clipRule='evenodd'
          d='M11.3033 6.97794C11.1297 7.15151 10.8091 7.19079 10.6355 7.01723L6.00001 2.38175L1.36453 7.01723C1.19096 7.19079 0.870272 7.15151 0.696705 6.97794C0.523139 6.80438 0.483856 6.48369 0.657422 6.31012L6.00001 0.967535L11.3426 6.31012C11.5162 6.48369 11.4769 6.80438 11.3033 6.97794Z' />
</svg>


export default ArrowUp