import SvgWrapper from './Wrapper'

export default function IconProtocolChecked({ width = 50, height = 15 }) {
  return (
    <SvgWrapper w={width} h={height} vw={width} vh={height}>
      <path d="M0 0H50V15H12C5.37258 15 0 9.62742 0 3V0Z" fill="#1D8AEB" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M30.7216 3.09228C31.0088 3.26049 31.0864 3.6014 30.895 3.85372L25.4721 11L21.1831 7.2319C20.939 7.01747 20.939 6.66981 21.1831 6.45538C21.4271 6.24095 21.8229 6.24095 22.0669 6.45538L25.2778 9.27628L29.8549 3.24456C30.0464 2.99225 30.4344 2.92406 30.7216 3.09228Z"
        fill="white"
      />
    </SvgWrapper>
  )
}
