import classnames from 'classnames'


const ArrowDown = ({ width = 24, height = 24, fillColor = 'black', className = '' }) => <svg width='16' height='16'
                                                                                              viewBox='0 0 16 16'
                                                                                              className={classnames(className)}
                                                                                              xmlns='http://www.w3.org/2000/svg'>
    <path d='M1 5L8 12L15 5' stroke='#95A3B0' strokeLinecap='round' fill={fillColor} />
</svg>

export default ArrowDown