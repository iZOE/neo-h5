const Crypto = ({width = 24, height = 24, fillColor = 'white', className = ""}) =>
    <svg width={width} height={height} className={className} viewBox="0 0 53 74" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M26.3569 -10.5193L25.7914 -8.6021V47.0303L26.3569 47.5934L52.2429 32.3289L26.3569 -10.5193Z" fill={fillColor} fillOpacity="0.8"/>
        <path d="M26.357 -10.5193L0.471008 32.3289L26.357 47.5936V20.5913V-10.5193Z" fill={fillColor} fillOpacity="0.6"/>
        <path d="M26.3569 52.4829L26.0383 52.8703V72.6878L26.3569 73.6164L52.2583 37.2261L26.3569 52.4829Z" fill={fillColor} fillOpacity="0.8"/>
        <path d="M26.3571 73.6161V52.4826L0.471024 37.2258L26.3571 73.6161Z" fill={fillColor} fillOpacity="0.4"/>
        <path d="M26.3568 47.5932L52.2425 32.3289L26.3568 20.5913V47.5932Z" fill={fillColor} fillOpacity="0.8"/>
        <path d="M0.471218 32.329L26.3568 47.5932V20.5913L0.471218 32.329Z" fill={fillColor} fillOpacity="0.8"/>
    </svg>


export default Crypto;