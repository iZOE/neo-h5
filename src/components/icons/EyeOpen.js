import SvgWrapper from './Wrapper'

const EyeOpen = ({ w = 24, h = 24, fillColor = 'text-platinum-200' }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M21 12c0 3.67-3.81 7-9 7-5.19 0-9-3.33-9-7s3.81-7 9-7c5.19 0 9 3.33 9 7zm1 0c0 4.418-4.477 8-10 8S2 16.418 2 12s4.477-8 10-8 10 3.582 10 8zm-7 0a3 3 0 11-6 0 3 3 0 016 0zm1 0a4 4 0 11-8 0 4 4 0 018 0z"
    />
  </SvgWrapper>
)

export default EyeOpen
