import SvgWrapper from './Wrapper'

const Spin = ({ w = 16, h = 16, fillColor = 'text-blue-200' }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8 2C4.68629 2 2 4.68629 2 8C2 11.3137 4.68629 14 8 14C8.94318 14 9.83287 13.7831 10.6244 13.3974L11.5006 15.1952C10.442 15.7111 9.25334 16 8 16C3.58172 16 0 12.4183 0 8C0 3.58172 3.58172 0 8 0C12.4183 0 16 3.58172 16 8H14C14 4.68629 11.3137 2 8 2Z"
    />
  </SvgWrapper>
)

export default Spin
