import SvgWrapper from './Wrapper'

const LineDown = ({ width, height, fillColor }) => (
  <SvgWrapper w={width} h={height} fillColor={fillColor} vw={16} vh={16}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M2.69669 5.98959C2.87026 5.81602 3.19095 5.77674 3.36452 5.9503L7.99999 10.5858L12.6355 5.9503C12.809 5.77674 13.1297 5.81602 13.3033 5.98959C13.4769 6.16315 13.5161 6.48384 13.3426 6.65741L7.99999 12L2.65741 6.65741C2.48384 6.48384 2.52313 6.16315 2.69669 5.98959Z"
    />
  </SvgWrapper>
)

export default LineDown
