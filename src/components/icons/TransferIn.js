import SvgWrapper from './Wrapper'

const TransferIn = ({ w, h, fillColor }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor}>
    <path fillRule="evenodd" clipRule="evenodd" d="M3.34007 11.0621C2.88664 11.6005 2.88664 12.3995 3.34007 12.9379L10.5534 21.5024C11.398 22.5051 13 21.8909 13 20.5645L13 16L20 16C20.5523 16 21 15.5523 21 15L21 9C21 8.44772 20.5523 8 20 8L13 8L13 3.43553C13 2.10906 11.398 1.49491 10.5534 2.49763L3.34007 11.0621Z" />
  </SvgWrapper>
)

export default TransferIn
