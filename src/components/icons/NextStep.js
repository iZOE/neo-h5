import SvgWrapper from './Wrapper'

export default function IconNextStep({ width = 10, height = 18, fillColor = 'text-blue-200' }) {
  return (
    <SvgWrapper w={width} h={height} vw={10} vh={18} fillColor={fillColor}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.984318 16.955C0.723969 16.6947 0.665043 16.2136 0.925393 15.9533L7.87861 9.00007L0.925392 2.04685C0.665042 1.7865 0.723967 1.30547 0.984317 1.04512C1.24467 0.78477 1.7257 0.725844 1.98605 0.986194L9.99993 9.00007L1.98605 17.0139C1.7257 17.2743 1.24467 17.2154 0.984318 16.955Z"
      />
    </SvgWrapper>
  )
}
