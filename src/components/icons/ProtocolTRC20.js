import SvgWrapper from './Wrapper'

export default function IconProtocolTRC20({ width = 25, height = 24 }) {
  return (
    <SvgWrapper w={width} h={height}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.6665 4L17.8719 6.42985L21.6665 9.992L11.209 22L3.6665 4ZM19.371 10.7655L13.5176 11.9182L12.6481 18.8126L19.371 10.7655ZM6.35823 6.92039L11.3041 18.7792L12.2137 11.5838L6.35823 6.92039ZM17.363 7.95719L14.0937 10.4984L19.0817 9.42736L17.363 7.95719ZM7.17033 5.67494L12.5236 10.3338L16.0619 7.39407L7.17033 5.67494Z"
        fill="#F5A623"
      />
    </SvgWrapper>
  )
}
