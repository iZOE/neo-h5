import SvgWrapper from './Wrapper'

const Selected = ({ w, h, fillColor = 'text-platinum-200', vw, vh }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor} vw={vw} vh={vh}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9.72162 0.0922765C10.0088 0.260489 10.0864 0.601398 9.89497 0.853717L4.47211 8L0.183057 4.2319C-0.0610189 4.01747 -0.0610191 3.66981 0.183057 3.45538C0.427133 3.24095 0.822858 3.24095 1.06693 3.45538L4.27783 6.27628L8.85491 0.244565C9.04638 -0.00775431 9.43442 -0.0759362 9.72162 0.0922765Z"
      fill="white"
    />
  </SvgWrapper>
)

export default Selected
