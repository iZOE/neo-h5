import SvgWrapper from './Wrapper'

const ArrowRight = ({ w, h, fillColor = 'text-blue-200' }) => (
  <SvgWrapper w={w} h={h} fillColor={fillColor}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5.98959 13.3032C5.81602 13.1296 5.77674 12.8089 5.9503 12.6354L10.5858 7.99988L5.9503 3.36441C5.77674 3.19084 5.81602 2.87015 5.98959 2.69658C6.16315 2.52302 6.48384 2.48373 6.65741 2.6573L12 7.99988L6.65741 13.3425C6.48384 13.516 6.16315 13.4768 5.98959 13.3032Z"
    />
  </SvgWrapper>
)

export default ArrowRight
