import classnames from 'classnames'


const ArrowRight = ({ width = 24, height = 24, fillColor = 'black', className="" }) => <svg className={classnames(className)} width={width} height={height} viewBox={`0 0 12 20`}>
    <path fillRule="evenodd" clipRule="evenodd"
          d="M5.98959 13.3033C5.81602 13.1297 5.77674 12.8091 5.9503 12.6355L10.5858 8.00001L5.9503 3.36453C5.77674 3.19096 5.81602 2.87027 5.98959 2.69671C6.16315 2.52314 6.48384 2.48386 6.65741 2.65742L12 8.00001L6.65741 13.3426C6.48384 13.5162 6.16315 13.4769 5.98959 13.3033Z"
          fill={fillColor} />
  </svg>

export default ArrowRight;