export default function IconLoading({ width, height, fillColor }) {
  return (
    <svg width={width} height={height} viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path
          d="M12.2857 0.571289C18.7554 0.571289 24 5.81586 24 12.2856C24 18.7553 18.7554 23.9999 12.2857 23.9999C5.81598 23.9999 0.571411 18.7553 0.571411 12.2856C0.571411 11.7336 0.609697 11.1913 0.683411 10.6599C0.631411 11.101 0.604554 11.5496 0.604554 12.005C0.604554 18.301 5.70855 23.405 12.0046 23.405C18.3017 23.405 23.4051 18.301 23.4051 12.005C23.4051 5.70843 18.3011 0.605003 12.0051 0.605003C11.5497 0.605003 11.1011 0.631289 10.6594 0.683289C11.1908 0.609003 11.7337 0.571289 12.2857 0.571289Z"
          fill={fillColor}
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width={width} height={height} fill={fillColor} />
        </clipPath>
      </defs>
    </svg>
  )
}
