const SvgWrapper = ({ w = 24, h = 24, fillColor = 'text-platinum-200', vw, vh, children }) => (
  <svg className={`mx-auto fill-current ${fillColor}`} width={w} height={h} viewBox={`0 0 ${vw || w} ${vh || h}`} fill="none" xmlns="http://www.w3.org/2000/svg">
    {children}
  </svg>
)

export default SvgWrapper
