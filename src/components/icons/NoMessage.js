export default function IconNoMessage({ width, height, fillColor }) {
  return (
    <svg width={width} height={height} viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M79.063 53.357V73.071H9.936V53.357H22.104L31.982 68.143H57.016L66.891 53.357H79.061H79.063ZM64.25 9H24.75L5 48.429V78H84V48.429L64.25 9ZM10.52 48.429L27.802 13.929H61.198L78.479 48.429H64.25L54.375 63.214H34.625L24.748 48.43H10.52V48.429Z" fill={fillColor} />
    </svg>
  )
}
