import SvgWrapper from '../Wrapper'

const Level8 = ({ w, h, vw, vh }) => (
  <SvgWrapper w={w} h={h} vw={vw} vh={vh}>
    <g clip-path="url(#clip0)">
      <g filter="url(#filter0_d)">
        <path d="M49.3936 20.0341L38.1156 23.9816L26.8355 20.0341L24.7681 7.68206L32.4982 11.5235L38.1156 2L43.7309 11.5235L51.461 7.68206L49.3936 20.0341Z" fill="url(#paint0_linear)" />
        <path d="M38.116 2L38.2584 24.0086L43.4615 22.1097L43.7312 11.5235L38.116 2Z" fill="url(#paint1_linear)" />
        <path d="M51.4611 7.68207L47.1299 20.8265L49.3937 20.0341L51.4611 7.68207Z" fill="url(#paint2_linear)" />
        <path d="M43.7309 11.5235L51.4611 7.68207L47.1299 20.8265L43.4612 22.1097L43.7309 11.5235Z" fill="url(#paint3_linear)" />
        <path d="M24.7681 7.6821L29.1014 20.8265L26.8355 20.0341L24.7681 7.6821Z" fill="url(#paint4_linear)" />
        <path d="M21.1123 61.8571L27.0513 65.2742V35.1668H21.1123V61.8571Z" fill="url(#paint5_linear)" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M20.7922 34.8557H27.3714V65.8198L20.7922 62.0343V34.8557ZM21.4325 35.4778V61.6797L26.7312 64.7285V35.4778H21.4325Z" fill="#F4CD73" />
        <path d="M55.0931 61.8571L49.1562 65.2742V35.1668H55.0931V61.8571Z" fill="url(#paint6_linear)" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M48.8359 34.8557H55.413V62.0343L48.8359 65.82V34.8557ZM49.4762 35.4778V64.7284L54.7727 61.6798V35.4778H49.4762Z" fill="#F4CD73" />
        <path d="M38.1025 71.6385L28.6006 66.1686V35.1668H47.6281V66.154L38.1025 71.6385Z" fill="url(#paint7_linear)" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M28.2805 34.8557H47.9483V66.3312L38.1025 72L28.2805 66.3458V34.8557ZM28.9207 35.4778V65.9912L38.1025 71.2769L47.308 65.9767V35.4778H28.9207Z" fill="#F4CD73" />
        <path d="M64.5366 47.1591H11.6687C11.3773 47.1591 11.1119 46.9636 10.9867 46.6557L8.99051 41.6995C8.75744 41.1214 9.12215 40.4579 9.67245 40.4579H66.5349C67.0852 40.4579 67.45 41.1214 67.2169 41.6995L65.2207 46.6557C65.0934 46.9636 64.8279 47.1591 64.5366 47.1591Z" fill="#FFF7D7" />
        <path d="M64.5366 47.6311H11.6687C11.3773 47.6311 11.1119 47.4356 10.9867 47.1278L8.99051 42.1716C8.75744 41.5934 9.12215 40.9299 9.67245 40.9299H66.5349C67.0852 40.9299 67.45 41.5934 67.2169 42.1716L65.2207 47.1278C65.0934 47.4356 64.8279 47.6311 64.5366 47.6311Z" fill="url(#paint8_linear)" />
        <path d="M70.2533 31.496H5.74912C5.45995 31.496 5.1945 31.3005 5.07149 30.9969L3.07098 26.0303C2.84007 25.4542 3.20046 24.7949 3.74861 24.7949H72.2516C72.7976 24.7949 73.1602 25.4542 72.9292 26.0303L70.9287 30.9969C70.8057 31.3005 70.5424 31.496 70.2533 31.496Z" fill="#FFF7D7" />
        <path d="M70.2533 31.9681H5.74912C5.45995 31.9681 5.1945 31.7726 5.07149 31.4689L3.07098 26.5023C2.84007 25.9262 3.20046 25.2669 3.74861 25.2669H72.2516C72.7976 25.2669 73.1602 25.9262 72.9292 26.5023L70.9287 31.4689C70.8057 31.7747 70.5424 31.9681 70.2533 31.9681Z" fill="url(#paint9_linear)" />
        <path d="M67.3873 39.3264H8.6128C8.32147 39.3264 8.05818 39.1309 7.93517 38.8252L5.93682 33.8627C5.70375 33.2866 6.0663 32.6252 6.61445 32.6252H69.3879C69.936 32.6252 70.2986 33.2866 70.0655 33.8627L68.0671 38.8252C67.9441 39.133 67.6787 39.3264 67.3873 39.3264Z" fill="#FFF7D7" />
        <path d="M67.3873 39.8006H8.6128C8.32147 39.8006 8.05818 39.6051 7.93517 39.2994L5.93682 34.3369C5.70375 33.7608 6.0663 33.0994 6.61445 33.0994H69.3879C69.936 33.0994 70.2986 33.7608 70.0655 34.3369L68.0671 39.2994C67.9441 39.6051 67.6787 39.8006 67.3873 39.8006Z" fill="url(#paint10_linear)" />
        <path d="M59.4112 46.4478V25.3315C59.4112 24.4933 58.9472 23.7175 58.194 23.2974L39.2204 12.7403C38.4673 12.3201 37.5371 12.3201 36.784 12.7403L17.8104 23.2974C17.0572 23.7175 16.5933 24.4912 16.5933 25.3315V46.4478C16.5933 47.286 17.0572 48.0618 17.8104 48.4819L36.784 59.039C37.5371 59.4592 38.4673 59.4592 39.2204 59.039L58.194 48.4819C58.9472 48.0597 59.4112 47.286 59.4112 46.4478Z" fill="#FFF7D7" />
        <g filter="url(#filter1_d)">
          <path d="M50.7056 39.1705C53.7225 38.8482 56.6251 37.854 59.4112 36.5105V25.7474C59.4112 24.9093 58.9472 24.1335 58.194 23.7134L39.2204 13.1562C38.4673 12.7361 37.5371 12.7361 36.784 13.1562L17.8104 23.7134C17.0572 24.1335 16.5933 24.9072 16.5933 25.7474V38.1078C27.5001 41.6414 39.378 40.3207 50.7056 39.1705Z" fill="url(#paint11_linear)" />
        </g>
        <g filter="url(#filter2_d)">
          <path d="M50.7057 39.1705C39.376 40.3206 27.5002 41.6413 16.5891 38.1077V46.8637C16.5891 47.7019 17.0531 48.4776 17.8063 48.8978L36.7798 59.4549C37.533 59.875 38.4631 59.875 39.2163 59.4549L58.1899 48.8978C58.943 48.4776 59.407 47.7039 59.407 46.8637V36.5104C56.6253 37.8539 53.7248 38.8481 50.7057 39.1705Z" fill="url(#paint12_linear)" />
        </g>
        <path d="M21.011 27.5526V45.0605C21.011 45.4931 21.2505 45.8945 21.639 46.1108L37.3712 54.8647C37.7596 55.081 38.2387 55.081 38.6293 54.8647L54.3615 46.1108C54.75 45.8945 54.9895 45.4931 54.9895 45.0605V27.5526C54.9895 27.12 54.75 26.7186 54.3615 26.5023L38.6293 17.7483C38.2409 17.532 37.7618 17.532 37.3712 17.7483L21.639 26.5023C21.2505 26.7186 21.011 27.1179 21.011 27.5526Z" fill="url(#paint13_linear)" />
        <path d="M22.0186 28.4449V44.1662C22.0186 44.8047 22.3725 45.3954 22.9465 45.7157L37.0731 53.5753C37.6472 53.8956 38.355 53.8956 38.929 53.5753L53.0556 45.7157C53.6297 45.3954 53.9836 44.8047 53.9836 44.1662V28.4449C53.9836 27.8064 53.6297 27.2157 53.0556 26.8954L38.929 19.0358C38.355 18.7155 37.6472 18.7155 37.0731 19.0358L22.9465 26.8954C22.3725 27.2157 22.0186 27.8064 22.0186 28.4449Z" fill="url(#paint14_radial)" />
        <path d="M27.2239 59.4531L21.1123 56.0485V57.9099L27.2239 61.4289V59.4531Z" fill="#F4CD73" />
        <path d="M47.8183 60.1437L38.1157 65.5242L28.3721 60.0938V62.0904L38.1027 67.6913L47.8183 62.0987V60.1437Z" fill="#F4CD73" />
        <path d="M48.9663 59.5072V61.4373L55.093 57.9099L55.2074 56.0485L48.9663 59.5072Z" fill="#F4CD73" />
        <g filter="url(#filter3_d)">
          <path d="M29.7048 42.0002L33.1708 30.5358H34V30.0002H31.1808V30.5358H32.5075L30.0033 38.9259L27.1675 30.5358H28.4776V30.0002H24V30.5358H24.8458L28.1294 40.3738C28.4057 41.2201 28.6213 41.7093 28.7761 41.8416C28.9088 41.9474 29.1023 42.0002 29.3565 42.0002H29.7048Z" fill="url(#paint15_linear)" />
        </g>
        <g filter="url(#filter4_d)">
          <path d="M40 42C39.9333 41.6667 39.7833 41.4867 39.55 41.46H38.6667V30.54H40V30H35V30.54H36.3333V41.46H35V42H40Z" fill="url(#paint16_linear)" />
        </g>
        <g filter="url(#filter5_d)">
          <path d="M46 42C45.9333 41.6667 45.7833 41.4867 45.55 41.46H44.6667V30.54H46V30H41V30.54H42.3333V41.46H41V42H46Z" fill="url(#paint17_linear)" />
        </g>
        <g filter="url(#filter6_d)">
          <path d="M52 42C51.9333 41.6667 51.7833 41.4867 51.55 41.46H50.6667V30.54H52V30H47V30.54H48.3333V41.46H47V42H52Z" fill="url(#paint18_linear)" />
        </g>
      </g>
    </g>
    <defs>
      <filter id="filter0_d" x="-0.0730526" y="2" width="76.1461" height="76.1461" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="3.07305" />
        <feGaussianBlur stdDeviation="1.53653" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter1_d" x="11.9837" y="11.3046" width="52.037" height="36.7749" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="3.07305" />
        <feGaussianBlur stdDeviation="2.30479" />
        <feColorMatrix type="matrix" values="0 0 0 0 0.379167 0 0 0 0 0.182 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter2_d" x="11.9795" y="34.9738" width="52.037" height="32.4788" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="3.07305" />
        <feGaussianBlur stdDeviation="2.30479" />
        <feColorMatrix type="matrix" values="0 0 0 0 0.379167 0 0 0 0 0.182 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter3_d" x="20" y="27.0002" width="18" height="20" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter4_d" x="31" y="27" width="13" height="20" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter5_d" x="37" y="27" width="13" height="20" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter6_d" x="43" y="27" width="13" height="20" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <linearGradient id="paint0_linear" x1="24.769" y1="12.9901" x2="51.4615" y2="12.9901" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F6F4AF" />
        <stop offset="1" stop-color="#F5E079" />
      </linearGradient>
      <linearGradient id="paint1_linear" x1="38.1156" y1="13.0041" x2="43.7319" y2="13.0041" gradientUnits="userSpaceOnUse">
        <stop stop-color="#EACB3A" />
        <stop offset="1" stop-color="#E8D477" />
      </linearGradient>
      <linearGradient id="paint2_linear" x1="47.1291" y1="14.2551" x2="51.4616" y2="14.2551" gradientUnits="userSpaceOnUse">
        <stop stop-color="#DA6F0E" />
        <stop offset="1" stop-color="#E9B253" />
      </linearGradient>
      <linearGradient id="paint3_linear" x1="43.4608" y1="14.8967" x2="51.4616" y2="14.8967" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F5E079" />
        <stop offset="1" stop-color="#F6F4AF" />
      </linearGradient>
      <linearGradient id="paint4_linear" x1="28.6338" y1="22.9945" x2="25.3752" y2="9.47001" gradientUnits="userSpaceOnUse">
        <stop stop-color="#EACB3A" />
        <stop offset="1" stop-color="#E8D477" />
      </linearGradient>
      <linearGradient id="paint5_linear" x1="24.0817" y1="68.3404" x2="24.0817" y2="53.7407" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F3AE4F" />
        <stop offset="1" stop-color="#E17D44" />
      </linearGradient>
      <linearGradient id="paint6_linear" x1="52.1239" y1="68.3404" x2="52.1239" y2="53.7407" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F3AE4F" />
        <stop offset="1" stop-color="#E17D44" />
      </linearGradient>
      <linearGradient id="paint7_linear" x1="38.1151" y1="75.3516" x2="38.1151" y2="57.6663" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F3AE4F" />
        <stop offset="1" stop-color="#E17D44" />
      </linearGradient>
      <linearGradient id="paint8_linear" x1="38.1027" y1="21.188" x2="38.1027" y2="48.2236" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFE3B8" />
        <stop offset="1" stop-color="#FFD04F" />
      </linearGradient>
      <linearGradient id="paint9_linear" x1="38.001" y1="21.1881" x2="38.001" y2="48.2237" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFF4AC" />
        <stop offset="1" stop-color="#FFCF48" />
      </linearGradient>
      <linearGradient id="paint10_linear" x1="38.001" y1="21.188" x2="38.001" y2="48.2236" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFF5AF" />
        <stop offset="1" stop-color="#FFD150" />
      </linearGradient>
      <linearGradient id="paint11_linear" x1="33.5527" y1="18.6043" x2="39.9414" y2="38.9171" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F3E898" />
        <stop offset="1" stop-color="#EAB240" />
      </linearGradient>
      <linearGradient id="paint12_linear" x1="36.0547" y1="36.1252" x2="41.9516" y2="54.8743" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F3E898" />
        <stop offset="1" stop-color="#EAB240" />
      </linearGradient>
      <linearGradient id="paint13_linear" x1="42.7888" y1="51.6614" x2="34.2064" y2="22.0235" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FDEB8A" />
        <stop offset="1" stop-color="#CF7E2E" />
      </linearGradient>
      <radialGradient id="paint14_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(33.2524 27.8394) scale(31.9151 30.7581)">
        <stop stop-color="#EBC04B" />
        <stop offset="1" stop-color="#E67D38" />
      </radialGradient>
      <linearGradient id="paint15_linear" x1="33.1816" y1="30.0002" x2="33.1816" y2="42.0002" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFEFC1" />
        <stop offset="1" stop-color="#FFD068" />
      </linearGradient>
      <linearGradient id="paint16_linear" x1="33.6678" y1="30.0002" x2="33.6678" y2="42.1002" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFEFC1" />
        <stop offset="1" stop-color="#FFD068" />
      </linearGradient>
      <linearGradient id="paint17_linear" x1="34.222" y1="30.0002" x2="34.222" y2="42.1002" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFEFC1" />
        <stop offset="1" stop-color="#FFD068" />
      </linearGradient>
      <linearGradient id="paint18_linear" x1="40.222" y1="30.0002" x2="40.222" y2="42.1002" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFEFC1" />
        <stop offset="1" stop-color="#FFD068" />
      </linearGradient>
      <clipPath id="clip0">
        <rect width="76" height="76" fill="white" />
      </clipPath>
    </defs>
  </SvgWrapper>
)

export default Level8
