import SvgWrapper from '../Wrapper'

const Level4 = ({ w, h, vw, vh }) => (
  <SvgWrapper w={w} h={h} vw={vw} vh={vh}>
    <g clip-path="url(#clip0)">
      <g filter="url(#filter0_d)">
        <path d="M28.5542 65.9338L24.4417 58.4597L15.8799 58.6444L25.1569 42.6815L37.8312 49.9709L28.5542 65.9338Z" fill="url(#paint0_linear)" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M24.9747 42L38.5158 49.788L28.5422 66.9493L24.1496 58.966L15 59.1634L24.9747 42ZM25.3391 43.363L16.7597 58.1254L24.7338 57.9534L28.566 64.9183L37.1466 50.1539L25.3391 43.363Z" fill="#B8B8B8" />
        <path d="M17.1453 56.4669L25.7071 56.2822L29.8195 63.754L31.0666 61.6107L26.9243 54.1891L18.4015 54.3031L17.1453 56.4669Z" fill="#B8B8B8" />
        <path d="M60.0947 58.5859L51.5306 58.4764L47.4823 65.9848L38.0654 50.1017L50.6778 42.7051L60.0947 58.5859Z" fill="url(#paint1_linear)" />
        <path fill-rule="evenodd" clip-rule="evenodd" d="M50.8543 42.0222L60.9793 59.0971L51.8272 58.9802L47.5032 67L37.3794 49.9246L50.8543 42.0222ZM38.7518 50.2788L47.4618 64.9696L51.2343 57.9728L59.2105 58.0747L50.5017 43.388L38.7518 50.2788Z" fill="#B8B8B8" />
        <path d="M46.1987 63.8164L50.2447 56.3081L58.8088 56.4175L57.5457 54.2856L49.0114 54.2263L44.9219 61.664L46.1987 63.8164Z" fill="#B8B8B8" />
        <path d="M64.0078 45.0786H12.2081C11.8987 45.0786 11.6167 44.8643 11.4838 44.5268L9.36337 39.0934C9.1158 38.4595 9.5032 37.7321 10.0877 37.7321H66.1282C66.7128 37.7321 67.1002 38.4595 66.8526 39.0934L64.7322 44.5268C64.5992 44.8643 64.3173 45.0786 64.0078 45.0786Z" fill="#D4D4D4" />
        <path d="M64.0078 45.5985H12.2081C11.8987 45.5985 11.6167 45.3842 11.4838 45.0467L9.36337 39.6133C9.1158 38.9794 9.5032 38.2521 10.0877 38.2521H66.1282C66.7128 38.2521 67.1002 38.9794 66.8526 39.6133L64.7322 45.0467C64.5992 45.3819 64.3173 45.5985 64.0078 45.5985Z" fill="url(#paint2_linear)" />
        <path d="M70.0801 27.9073H5.92017C5.613 27.9073 5.33105 27.6929 5.20038 27.36L3.0754 21.9152C2.83012 21.2836 3.21294 20.5608 3.79519 20.5608H72.2051C72.785 20.5608 73.1701 21.2836 72.9248 21.9152L70.7999 27.36C70.6669 27.6952 70.3872 27.9073 70.0801 27.9073Z" fill="#D4D4D4" />
        <path d="M70.0801 28.4272H5.92017C5.613 28.4272 5.33105 28.2129 5.20038 27.88L3.0754 22.4351C2.83012 21.8035 3.21294 21.0807 3.79519 21.0807H72.2051C72.785 21.0807 73.1701 21.8035 72.9248 22.4351L70.7999 27.88C70.6669 28.2129 70.3872 28.4272 70.0801 28.4272Z" fill="url(#paint3_linear)" />
        <path d="M67.0358 36.4942H8.962C8.65253 36.4942 8.37287 36.2799 8.24221 35.9447L6.11951 30.5044C5.87194 29.8728 6.25705 29.1478 6.8393 29.1478H69.1608C69.743 29.1478 70.1281 29.8728 69.8806 30.5044L67.7579 35.9447C67.6272 36.2799 67.3452 36.4942 67.0358 36.4942Z" fill="#D4D4D4" />
        <path d="M67.0358 37.0118H8.962C8.65253 37.0118 8.37287 36.7974 8.24221 36.4623L6.11951 31.022C5.87194 30.3904 6.25705 29.6653 6.8393 29.6653H69.1608C69.743 29.6653 70.1281 30.3904 69.8806 31.022L67.7579 36.4623C67.6272 36.7974 67.3452 37.0118 67.0358 37.0118Z" fill="url(#paint4_linear)" />
        <rect x="6.53784" y="12.5433" width="62.9381" height="41.5341" fill="url(#pattern0)" />
        <path d="M60.7432 44.2988V21.1491C60.7432 20.2302 60.2504 19.3797 59.4504 18.9192L39.2963 7.34543C38.4962 6.88486 37.5082 6.88486 36.7082 7.34543L16.5541 18.9192C15.7541 19.3797 15.2612 20.2279 15.2612 21.1491V44.2988C15.2612 45.2177 15.7541 46.0682 16.5541 46.5288L36.7082 58.1025C37.5082 58.5631 38.4962 58.5631 39.2963 58.1025L59.4504 46.5288C60.2504 46.0682 60.7432 45.2177 60.7432 44.2988Z" fill="#D4D4D4" />
        <g filter="url(#filter1_d)">
          <path d="M51.496 36.323C54.7007 35.9696 57.7839 34.8797 60.7432 33.4068V21.6073C60.7432 20.6885 60.2504 19.838 59.4504 19.3774L39.2963 7.80369C38.4962 7.34311 37.5082 7.34311 36.7082 7.80369L16.5541 19.3774C15.7541 19.838 15.2612 20.6862 15.2612 21.6073V35.1579C26.8466 39.0318 39.4613 37.5816 51.496 36.323Z" fill="url(#paint5_linear)" />
        </g>
        <g filter="url(#filter2_d)">
          <path d="M51.4962 36.3231C39.4615 37.584 26.8468 39.0318 15.2568 35.158V44.7571C15.2568 45.676 15.7497 46.5265 16.5497 46.9871L36.7038 58.5608C37.5039 59.0214 38.4918 59.0214 39.2919 58.5608L59.446 46.9871C60.246 46.5265 60.7389 45.6783 60.7389 44.7571V33.4069C57.7841 34.8798 54.7009 35.9674 51.4962 36.3231Z" fill="url(#paint6_linear)" />
        </g>
        <path d="M19.9539 23.5843V42.7781C19.9539 43.2523 20.2083 43.6924 20.6209 43.9295L37.332 53.5264C37.7446 53.7635 38.2535 53.7635 38.6684 53.5264L55.3794 43.9295C55.7921 43.6924 56.0465 43.2546 56.0465 42.7781V23.5843C56.0465 23.11 55.7921 22.67 55.3794 22.4328L38.6684 12.836C38.2558 12.5988 37.7469 12.5988 37.332 12.836L20.6209 22.4328C20.2083 22.67 19.9539 23.11 19.9539 23.5843Z" fill="url(#paint7_linear)" />
        <path d="M21.022 24.5624V41.7976C21.022 42.4976 21.3979 43.1451 22.0077 43.4963L37.0132 52.1127C37.623 52.4639 38.3749 52.4639 38.9846 52.1127L53.9902 43.4963C54.6 43.1451 54.9759 42.4976 54.9759 41.7976V24.5624C54.9759 23.8624 54.6 23.2149 53.9902 22.8637L38.9846 14.2473C38.3749 13.8962 37.623 13.8962 37.0132 14.2473L22.0077 22.8637C21.3979 23.2149 21.022 23.8624 21.022 24.5624Z" fill="url(#paint8_radial)" />
        <g filter="url(#filter3_d)">
          <path d="M44.1277 41L49.6733 25.714H51V25H46.4892V25.714H48.6119L44.6053 36.9008L40.068 25.714H42.1642V25H35V25.714H36.3532L41.607 38.8314C42.0492 39.9598 42.3941 40.6121 42.6418 40.7884C42.8541 40.9295 43.1636 41 43.5705 41H44.1277Z" fill="url(#paint9_linear)" />
        </g>
        <g filter="url(#filter4_d)">
          <path d="M33 41C32.8933 40.5556 32.6533 40.3156 32.28 40.28H30.8667V25.72H33V25H25V25.72H27.1333V40.28H25V41H33Z" fill="url(#paint10_linear)" />
        </g>
      </g>
    </g>
    <defs>
      <filter id="filter0_d" x="-1" y="7" width="78" height="68" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
      </pattern>
      <filter id="filter1_d" x="11.2612" y="7.45825" width="53.482" height="38.2084" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter2_d" x="11.2568" y="33.4069" width="53.482" height="33.4994" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter3_d" x="31" y="22" width="24" height="24" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter4_d" x="21" y="22" width="16" height="24" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <linearGradient id="paint0_linear" x1="21.269" y1="63.9088" x2="25.7316" y2="56.1485" gradientUnits="userSpaceOnUse">
        <stop stop-color="#828282" />
        <stop offset="1" stop-color="#4F4F4F" />
      </linearGradient>
      <linearGradient id="paint1_linear" x1="54.7487" y1="63.9044" x2="50.2188" y2="56.182" gradientUnits="userSpaceOnUse">
        <stop stop-color="#828282" />
        <stop offset="1" stop-color="#4F4F4F" />
      </linearGradient>
      <linearGradient id="paint2_linear" x1="38.1081" y1="16.6079" x2="38.1081" y2="46.2468" gradientUnits="userSpaceOnUse">
        <stop stop-color="#EBEBEB" />
        <stop offset="1" stop-color="#C8C8C8" />
      </linearGradient>
      <linearGradient id="paint3_linear" x1="37.9999" y1="16.6079" x2="37.9999" y2="46.2468" gradientUnits="userSpaceOnUse">
        <stop stop-color="#EBEBEB" />
        <stop offset="1" stop-color="#C8C8C8" />
      </linearGradient>
      <linearGradient id="paint4_linear" x1="37.9998" y1="16.6079" x2="37.9998" y2="46.2469" gradientUnits="userSpaceOnUse">
        <stop stop-color="#EBEBEB" />
        <stop offset="1" stop-color="#C8C8C8" />
      </linearGradient>
      <linearGradient id="paint5_linear" x1="33.2748" y1="13.7751" x2="40.4611" y2="35.914" gradientUnits="userSpaceOnUse">
        <stop stop-color="#EEEEEE" />
        <stop offset="1" stop-color="#A7A7A7" />
      </linearGradient>
      <linearGradient id="paint6_linear" x1="35.9324" y1="32.9833" x2="42.5656" y2="53.418" gradientUnits="userSpaceOnUse">
        <stop stop-color="#EEEEEE" />
        <stop offset="1" stop-color="#A7A7A7" />
      </linearGradient>
      <linearGradient id="paint7_linear" x1="43.0855" y1="50.0156" x2="33.4236" y2="17.6868" gradientUnits="userSpaceOnUse">
        <stop stop-color="#E4E4E4" />
        <stop offset="1" stop-color="#5A5A5A" />
      </linearGradient>
      <radialGradient id="paint8_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(32.9559 23.8997) scale(33.9008 33.7199)">
        <stop stop-color="#C9C9C9" />
        <stop offset="1" stop-color="#4F4F4F" />
      </radialGradient>
      <linearGradient id="paint9_linear" x1="38.8872" y1="25" x2="38.8872" y2="41" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FDFDFD" />
        <stop offset="1" stop-color="#D8D8D8" />
      </linearGradient>
      <linearGradient id="paint10_linear" x1="37.1733" y1="25" x2="37.1733" y2="41.1333" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FDFDFD" />
        <stop offset="1" stop-color="#D8D8D8" />
      </linearGradient>
      <clipPath id="clip0">
        <rect width="76" height="76" fill="white" />
      </clipPath>
    </defs>
  </SvgWrapper>
)

export default Level4
