import SvgWrapper from '../Wrapper'

const Level2 = ({ w, h, vw, vh }) => (
  <SvgWrapper w={w} h={h} vw={vw} vh={vh}>
    <g filter="url(#filter0_d)">
      <path d="M62.9999 49.6796V23.8109C62.9999 22.7841 62.4581 21.8337 61.5787 21.3191L39.4223 8.386C38.5429 7.87133 37.4569 7.87133 36.5775 8.386L14.4236 21.3216C13.5442 21.8363 13.0024 22.7841 13.0024 23.8134V49.6821C13.0024 50.7089 13.5442 51.6593 14.4236 52.1739L36.5775 65.107C37.4569 65.6217 38.5429 65.6217 39.4223 65.107L61.5762 52.1739C62.4581 51.6567 62.9999 50.7063 62.9999 49.6796Z" fill="#FFD4C7" />
      <path d="M52.8353 40.7645C56.358 40.3696 59.7471 39.1517 63.0001 37.5058V24.3205C63.0001 23.2937 62.4583 22.3433 61.5789 21.8286L39.4225 8.89559C38.5431 8.38091 37.4571 8.38091 36.5777 8.89559L14.4238 21.8312C13.5444 22.3459 13.0027 23.2937 13.0027 24.323V39.4651C25.7402 43.7914 39.6065 42.1735 52.8353 40.7645Z" fill="url(#paint0_linear)" />
      <path d="M52.8352 40.7645C39.6063 42.1735 25.74 43.7914 13 39.4625V50.1891C13 51.2159 13.5418 52.1663 14.4212 52.681L36.575 65.614C37.4544 66.1287 38.5405 66.1287 39.4199 65.614L61.5737 52.681C62.4531 52.1663 62.9949 51.2185 62.9949 50.1891V37.5058C59.7469 39.1517 56.3578 40.3696 52.8352 40.7645Z" fill="url(#paint1_linear)" />
      <path d="M18.1631 26.532V47.9801C18.1631 48.5101 18.4428 49.0018 18.8963 49.2668L37.2655 59.9908C37.7191 60.2558 38.2785 60.2558 38.7345 59.9908L57.1037 49.2668C57.5573 49.0018 57.837 48.5126 57.837 47.9801V26.532C57.837 26.0021 57.5573 25.5103 57.1037 25.2453L38.7345 14.5213C38.281 14.2563 37.7216 14.2563 37.2655 14.5213L18.8963 25.2453C18.4428 25.5103 18.1631 26.0021 18.1631 26.532Z" fill="url(#paint2_linear)" />
      <path d="M19.3374 27.6251V46.8845C19.3374 47.6667 19.7506 48.3903 20.4209 48.7827L36.9154 58.4112C37.5856 58.8035 38.4121 58.8035 39.0824 58.4112L55.5768 48.7827C56.2471 48.3903 56.6603 47.6667 56.6603 46.8845V27.6251C56.6603 26.8429 56.2471 26.1193 55.5768 25.7269L39.0824 16.0984C38.4121 15.7061 37.5856 15.7061 36.9154 16.0984L20.4209 25.7269C19.7506 26.1193 19.3374 26.8429 19.3374 27.6251Z" fill="url(#paint3_radial)" />
    </g>
    <g filter="url(#filter1_d)">
      <path d="M37 46C36.8933 45.5 36.6533 45.23 36.28 45.19H34.8667V28.81H37V28H29V28.81H31.1333V45.19H29V46H37Z" fill="url(#paint4_linear)" />
    </g>
    <g filter="url(#filter2_d)">
      <path d="M47 46C46.8933 45.5 46.6533 45.23 46.28 45.19H44.8667V28.81H47V28H39V28.81H41.1333V45.19H39V46H47Z" fill="url(#paint5_linear)" />
    </g>
    <defs>
      <filter id="filter0_d" x="9" y="8" width="58.0001" height="66" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter1_d" x="25" y="25" width="16" height="26" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <filter id="filter2_d" x="35" y="25" width="16" height="26" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feFlood flood-opacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="1" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <linearGradient id="paint0_linear" x1="32.8061" y1="15.5704" x2="40.9437" y2="40.2308" gradientUnits="userSpaceOnUse">
        <stop stop-color="#F3D2B2" />
        <stop offset="1" stop-color="#CA6832" />
      </linearGradient>
      <linearGradient id="paint1_linear" x1="35.7271" y1="37.0345" x2="43.2383" y2="59.7966" gradientUnits="userSpaceOnUse">
        <stop stop-color="#E2C4A8" />
        <stop offset="1" stop-color="#D38154" />
      </linearGradient>
      <linearGradient id="paint2_linear" x1="43.59" y1="56.0672" x2="32.6442" y2="20.0401" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FAB482" />
        <stop offset="1" stop-color="#A05A3B" />
      </linearGradient>
      <radialGradient id="paint3_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(32.4555 26.8839) scale(37.2646 37.6802)">
        <stop stop-color="#FFCEA9" />
        <stop offset="1" stop-color="#CA6832" />
      </radialGradient>
      <linearGradient id="paint4_linear" x1="33" y1="28" x2="33" y2="46" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFF0F0" />
        <stop offset="1" stop-color="#FFC09C" />
      </linearGradient>
      <linearGradient id="paint5_linear" x1="43" y1="28" x2="43" y2="46" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFF0F0" />
        <stop offset="1" stop-color="#FFC09C" />
      </linearGradient>
    </defs>
  </SvgWrapper>
)

export default Level2
