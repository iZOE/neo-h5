import * as dayjs from 'dayjs'
import Currency from '../core/Currency'
import { AnimateSharedLayout, motion } from 'framer-motion'
import React, { memo, useContext, useState } from 'react'
import { useIntl } from 'react-intl'
import ArrowDown from '../icons/ArrowDown'
import classnames from 'classnames'
import { Context } from './reducer/reducer'
import ArrowUp from '../icons/ArrowUp'

const sidebar = {
    open: {
        height: '100%',
        overflow: 'auto',
        transition: {
            type: 'spring',
            duration: 0.5
        },
    },
    closed: {
        height: '0',
        overflow: 'hidden',
        transition: {
            delay: 0.5,
            type: 'spring',
            stiffness: 400,
            damping: 40,
        },
    },
}

const AuthDetails = ({ data, date, open }) => {
    const intl = useIntl()
    const [isOpen, setToggle] = useState(open)

    const toggle = () => {
        setToggle(!isOpen)
    }

    return <li className="border-b" key={date}
               onClick={toggle}>
        <div
            className="text-body-4 flex justify-between bg-white py-3 pl-4 text-gray-600 dark:bg-purple-800 dark:text-white">
            <p className="flex">{date}</p>
            <p className="flex" style={{ marginRight: '18px' }}>{
                isOpen ?
                    <ArrowUp width={12} height={12} className={'text-platinum-200 pt-1'} /> :
                    <ArrowDown w={16} h={16} fillColor={'text-platinum-200 dark:text-white'} />
            }</p>
        </div>
        <motion.div
            initial={true}
            animate={isOpen ? 'open' : 'closed'}
            className="w-full">
            <motion.div initial={true} variants={sidebar} style={{ height: 0, overflow: 'hidden' }}>
                {
                    data.map((item, index) => {
                        return <div
                            className={classnames('bg-gray-50 mt-px px-1 dark:bg-purple-900', { ['border-b-8 border-gray-100']: data.length - 1 > index })}
                            key={index}>
                            <div className="flex flex-1 p-3 text-body-6 justify-between border-b border-t">
                                <p className="flex text-platinum-200">
                                    {intl.messages['submit.items.id']}
                                </p>
                                <p className="flex text-right text-platinum-300 dark:text-platinum-100">
                                    {item.id}
                                </p>
                            </div>
                            <div className="flex flex-1 p-3 text-body-6 justify-between border-b">
                                <p className="flex text-platinum-200">
                                    {intl.messages['submit.items.type']}
                                </p>
                                <p className="flex text-right text-platinum-300 dark:text-platinum-100">
                                    {item.auditTypeText}
                                </p>
                            </div>
                            <div className="flex flex-1 p-3 text-body-6 justify-between border-b">
                                <p className="flex text-platinum-200">
                                    {intl.messages['submit.items.item']}
                                </p>
                                <p className="flex text-right text-platinum-300 dark:text-platinum-100">
                                    {item.financialText}
                                </p>
                            </div>
                            <div className="flex flex-1 p-3 text-body-6 justify-between border-b">
                                <p className="flex text-platinum-200">
                                    {intl.messages['submit.items.amount']}
                                </p>
                                <p className="flex text-right">
                                    <p className="flex text-right text-platinum-300 dark:text-platinum-100">
                                        <Currency value={item.amount} intClassName="text-body-6"
                                                  floatClassName="text-body-6" />
                                    </p>
                                </p>
                            </div>
                            <div className="flex flex-1 p-3 text-body-6 justify-between border-b">
                                <p className="flex text-platinum-200">
                                    {intl.messages['submit.items.multiple']}
                                </p>
                                <p className="flex text-right ">
                                    <p className="flex text-right text-platinum-300 dark:text-platinum-100">
                                        {item.betMultiple}
                                    </p>
                                </p>
                            </div>
                            <div className="flex flex-1 p-3 text-body-6 justify-between border-b">
                                <p className="flex text-platinum-200">
                                    {intl.messages['submit.items.betting']}
                                </p>
                                <p className="flex text-right">
                                    <p className="flex text-right text-platinum-300 dark:text-platinum-100">
                                        <Currency value={item.shortBagAmount} intClassName="text-body-6"
                                                  floatClassName="text-body-6" />
                                    </p>
                                </p>
                            </div>
                            <div className="flex flex-1 p-3 text-body-6 justify-between">
                                <p className="flex text-platinum-200">
                                    {intl.messages['submit.items.status']}
                                </p>
                                <p className="flex text-right">
                                    <p className="flex text-right text-red">
                                        {item.auditStatusText}
                                    </p>
                                </p>
                            </div>
                        </div>
                    })
                }
            </motion.div>
        </motion.div>


    </li>
}

const WithdrawDetail = ({ back }) => {
    const intl = useIntl()
    const {
        withdraw,
        dispatch,
    } = useContext(Context)
    let data = {}

    const {
        turnOverInfo: {
            lastSettlementTime,
            totalValidBetAmount,
        },
        records,
    } = withdraw.turnover

    records.forEach((item) => {
        const k = data[item.date] || []
        k.push(item)
        data[item.date] = k
    })

    return <AnimateSharedLayout>
        <div className="mt-11 pt-4 px-3">
            <p className="text-platinum-200 text-body-6">
                {intl.messages['submit.items.last_time']}
                <span>{dayjs.unix(lastSettlementTime).format('YYYY-MM-DD HH:mm:ss')}</span>

            </p>
            <p className="text-platinum-200 text-body-6 pt-1">{intl.messages['submit.items.last_time_hint']}</p>
            <p className="text-platinum-200 text-body-6 pt-2">
                {intl.messages['submit.items.turnover']}
                <Currency value={totalValidBetAmount} />
            </p>
        </div>
        <ul className="mt-4">
            {
                Object.keys(data).map((key, index) => {
                    return <AuthDetails data={data[key]} date={key} open={index === 0} />
                })
            }
        </ul>


    </AnimateSharedLayout>
}

export default memo(WithdrawDetail)