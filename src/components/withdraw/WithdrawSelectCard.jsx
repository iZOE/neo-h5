import { useRouter } from 'next/router'
import React, { memo, useCallback, useContext, useEffect, useMemo, useState } from 'react'
import { Context } from './reducer/reducer'
import SwitchTab from './SwitchTab'
import CardList from './CardList'
import classnames from 'classnames'
import Currency from '../core/Currency'
import { FormattedMessage, useIntl } from 'react-intl'
import IconLoading from '../icons/Refresh'
import IconHelp from 'components/icons/Help.js'
import { Decimal } from 'decimal.js'
import showMessage from '../core/Alert/message'
import * as dayjs from 'dayjs'
import fetcher from '../../libs/fetcher'
import ArrowRight from '../icons/ArrowRight'
import Accounting from './AccountInfo'
import Dialog from '../core/Dialog'
import displayToast from '../core/Toast/toast'

const ValidateProcessBar = ({ totalValidBetAmount, shortBagAmount, displayDetail, close }) => {
    const intl = useIntl()
    const router = useRouter()

    return <Dialog
        isOpen={true}
        okText={intl.messages[`validate.turnover.ok`]}
        onClose={close}
        onOk={() => {
            router.push(`/`)
        }}
        cancelText={intl.messages[`validate.turnover.cancel`]}
        closable={true}
        onCancel={() => {
            router.push(`/service`)
        }}
    >
        <h4 className="text-title-7 text-platinum-200 font-semibold text-center">
            {intl.messages[`validate.turnover.title`]}
        </h4>
        <p className="text-body-6 text-platinum-200 text-center">
            <div>
                <div className="mt-6 px-8">
                    <ul className="w-full h-1 bg-gray-200 rounded-full">
                        <li className="bg-blue-100 h-1 rounded-full"
                            style={{ width: `${totalValidBetAmount / (totalValidBetAmount + shortBagAmount)}%` }} />
                    </ul>
                </div>
                <p className="text-body-6 mt-1">
                    <FormattedMessage id={'validate.turnover.body'}
                                      values={{
                                          need: <span
                                              className="text-blue-200 font-semibold text-body-1">{Math.ceil(shortBagAmount)}</span>,
                                      }} />
                </p>
                <div className="mt-3 pb-6 text-body-4 font-semibold flex justify-end">
                    <button className={'text-blue-200 flex-row flex'}
                            onClick={displayDetail}>

                    <span className={'flex text-body-6 font-semibold'}>
                        {intl.messages['validate.turnover.detail']}
                    </span>
                        <span className={'flex'}><ArrowRight fillColor={'#1D8AEB'} width={16} height={20} /></span>
                    </button>
                </div>
            </div>
        </p>
    </Dialog>
}

const CryptoWithdraw = memo(({ accountBalance, update, rate }) => {
    const intl = useIntl()
    const {
        withdraw,
        dispatch,
    } = useContext(Context)
    const [isLoading, setLoading] = useState(false)
    const [rateIndex, setRate] = useState(0)

    const exchangeResult = useMemo(() => {
        if (withdraw.balance === '') {
            return 0
        }

        return new Decimal(withdraw.balance).div(new Decimal(rate.rates[rateIndex].exchangeRate)).toFixed(2, 1)
    }, [withdraw.card, rateIndex, withdraw.balance])

    const handleUpdateRate = async () => {
        setLoading(true)
        if (dayjs().unix() - sessionStorage.getItem('cryptoRateTime') > 60) {
            let result
            try {
                result = await fetcher({
                    url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}${path}`,
                    withToken: true,
                })

            } catch (e) {
            }

            setLoading(false)
            return result
        }

        setTimeout(() => {
            setLoading(false)
        }, 800)

    }

    const handleCurrencyDescription = useCallback(async () => {
        showMessage({
            title: intl.messages['exchange.title'],
            content: intl.messages['exchange.body'],
        })
    }, [])

    const updateTab = (index) => () => {
        setRate(index)
    }

    return <>
        <ul className="w-full text-blue-200">
            {
                rate.rates.map((item, index) => {
                    return <li className="flex text-center text-body-6 w-full dark:bg-purple-800" key={index}
                               onClick={updateTab(index)}>
                        <p className={classnames('w-full text-center z-10 leading-4 text-body-6 py-3', { ['border-blue-200 border-b-2']: rateIndex === index })}>
                            {
                                item.currencyName
                            }
                        </p>
                    </li>
                })
            }
        </ul>
        <Accounting intl={intl} accountBalance={accountBalance} />

        <div
            className="flex justify-between bg-gray-50 dark:bg-purple-700 rounded w-full mx-3 mt-4 text-gray-500 dark:text-white"
            style={{ padding: '14px 12px' }}>
            <div>
                    <span className=" text-body-4 leading-5">
                        {intl.messages['current_exchange']}:
                    </span>
                <Currency value={rate.rates[rateIndex].exchangeRate}
                          floatClassName={'text-body-6'}
                          intClassName={'text-body-4'} />
            </div>
            <div className={classnames({ ['animate-spin']: isLoading })} onClick={handleUpdateRate}>
                <IconLoading width="24" height="24" fillColor="#1D8AEB" />
            </div>
        </div>
        <div
            className="border-solid border border-gray-300 w-full mx-3 dark:border-purple-100 dark:bg-purple-700 rounded mb-1 py-3 px-4">
            <input type={'number'}
                   value={withdraw.balance}
                   onChange={update}
                   min={0}
                   className="w-full text-right text-title-6 bg-transparent text-gray-500 dark:text-white dark:placeholder-purple-100"
                   placeholder={intl.messages['withdraw_amount']} />
            <div className="text-right text-body-4 text-gray-400 dark:placeholder-purple-100">
                ≈USDT <Currency value={exchangeResult} />{' '}
                <span
                    className="inline-block relative"
                    style={{ top: '2px' }}
                    onClick={handleCurrencyDescription}
                >
                      <IconHelp width="14" height="14" fillColor="#BBB" />
                    </span>
            </div>
        </div>
    </>
})

const BankWithdraw = ({ accountBalance, update }) => {
    const intl = useIntl()
    const {
        withdraw,
        dispatch,
    } = useContext(Context)
    return <>
        <Accounting accountBalance={accountBalance} />
        <input type={'number'}
               value={withdraw.balance}
               onChange={update}
               min={0}
               className={classnames('input-outline shadow-none rounded border-gray-300 items-center text-right',
                   'mx-3', 'py-3', 'px-4', 'mt-4', 'w-full border placeholder-gray-300',
                   'dark:bg-purple-800 dark:placeholder-purple-100 dark:text-white dark:border-purple-100')}
               placeholder={intl.messages['withdraw_amount']} />
    </>
}

const WithdrawSelectCard = ({ accountBalance, cards, rate }) => {
    const intl = useIntl()
    const router = useRouter()
    const { tab } = router.query
    const [disable, toDisable] = useState(true)
    const [status, setStatus] = useState(0)
    const {
        withdraw,
        dispatch,
    } = useContext(Context)
    const crypto = rate[withdraw.card.accountName] || rate['ERC20']

    useEffect(() => {
        const mappings = {
            banks: {
                list: 'bankCards',
                page: 'crypto',
            },
            crypto: {
                list: 'cryptoCurrencies',
                page: 'banks',
            },
        }
        if (cards[mappings[tab].list].length === 0) {
            showMessage({
                content: intl.messages[`validate.no_${tab}_card.body`],
                okText: intl.messages[`validate.no_${tab}_card.ok`],
                cancelText: intl.messages[`validate.no_${tab}_card.cancel`],
                onOk: () => {
                    router.push(`/withdraw-manage/${tab}`)
                },
                onCancel: () => {
                    router.push(`/withdraw/${mappings[tab].page}?step=2`)
                },
            })
        }
    }, [tab])

    const displayDetail = useCallback(() => {
        router.push(`/withdraw/${tab}?step=99`)
    })

    const handleNextStep = async () => {
        if (withdraw.balance > accountBalance.accountBalance) {
            displayToast({
                message: intl.messages['validate.balance'],
            })

            return
        }

        if (withdraw.balance > accountBalance.maxWithdrawalAmount) {
            displayToast({
                message: intl.messages['validate.withdraw-limit'],
            })

            return
        }

        const res = await fetcher({
            url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}/v1/api/Turnover`,
            method: 'post',
            data: {
                'amount': withdraw.balance,
                'type': 1,
            },
            withToken: true,
            isRefreshToken: true,
        })

        const {
            data: {
                turnOverInfo: {
                    statusText,
                    status,
                },
            },
        } = res.data

        dispatch({
            type: 'WITHDRAW_TURNOVER_SUMMARY',
            value: {
                turnover: res.data.data,
            },
        })

        if (status === 2) {
            router.push(`/withdraw/${tab}?step=${withdraw.balance >= accountBalance.fundTransferSMSLimitAmount ? 10 : 3}`)
            return

        }

        setStatus(status)
    }

    const handleInputBalance = (e) => {
        const value = e.target.value
        dispatch({
            type: 'WITHDRAW_INPUT_BALANCE',
            value: {
                value,
            },
        })
        toDisable(validateNext(value))
    }

    const validateNext = (value) => {
        if (tab === 'banks') {
            return !(value >= accountBalance.minWithdrawAmount && value <= accountBalance.maxWithdrawalAmount)
        }

        return !(value >= crypto.amount && value <= accountBalance.maxWithdrawalAmount)
    }

    return <section className="pt-11">
        <SwitchTab />
        <CardList tab={tab}
                  list={(tab === 'banks' ? cards.bankCards : cards.cryptoCurrencies)}
                  name={cards.accountName} />

        <section className="flex flex-wrap mx-auto bg-white dark:bg-purple-800">
            {
                tab === 'banks' ?
                    <BankWithdraw accountBalance={accountBalance} update={handleInputBalance} /> :
                    <CryptoWithdraw accountBalance={accountBalance} update={handleInputBalance}
                                    rate={crypto} />
            }

            <button
                disabled={disable}
                onClick={handleNextStep}
                className={classnames('mt-4', 'w-full', 'rounded-full', 'mx-3 py-4 pt-3 bg-blue-200 text-white',
                    'disabled:bg-gray-200 dark:disabled:bg-purple-700 dark:disabled:text-purple-200')}>
                {intl.messages['next']}
            </button>

            <section className="px-3 my-4">
                <p className="text-platinum-200 text-body-4">
                    {intl.messages['notice.title']}
                </p>
                <ul className="">
                    {
                        Object.keys(intl.messages).filter(x => x.startsWith('notice.select')).map((item, index) => {
                            return <li
                                className="text-platinum-200 dark:text-platinum-300 text-body-4 flex flex-1 justify-start">
                                <span className="flex">{index + 1}.</span>
                                <span className="flex">
                                    <FormattedMessage id={item}
                                                      values={accountBalance && {
                                                          min: tab === 'banks' ? accountBalance.minWithdrawAmount : crypto.amount,
                                                          max: accountBalance.maxWithdrawalAmount,
                                                          sms: accountBalance.fundTransferSMSLimitAmount,
                                                      }} />
                                </span>

                            </li>
                        })
                    }
                </ul>
            </section>
        </section>
        {
            status === 3 ?
                <ValidateProcessBar totalValidBetAmount={withdraw.turnover.turnOverInfo.totalValidBetAmount}
                                    shortBagAmount={withdraw.turnover.turnOverInfo.shortBagAmount}
                                    displayDetail={displayDetail}
                                    close={() => {
                                        setStatus(1)
                                    }} /> : ''
        }
    </section>
}

export default WithdrawSelectCard