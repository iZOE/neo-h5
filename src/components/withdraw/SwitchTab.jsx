
import React, { useCallback, useState } from 'react'
import { useIntl } from 'react-intl'
import {useRouter} from 'next/router'
import classnames from 'classnames'
import {AnimateSharedLayout, motion} from 'framer-motion'

const TabItem = ({switchTab, tabName, displayName}) => {

    const router = useRouter();
    const {tab} = router.query;

    const spring = {
        type: 'spring',
        stiffness: 500,
        damping: 30,
    }

    return <li className="relative w-full flex flex-grow box-border rounded-full text-center "
               onClick={switchTab(tabName)}>
        <span className={classnames("w-full text-body-6 text-center z-10 text-platinum-200 leading-10", {["text-white"]: tab === tabName})}>
            {displayName}
        </span>
        {tab === tabName && (
            <motion.div
                layout
                layoutId="outline"
                transition={spring}
                className="absolute rounded-full inset-0 w-full z-0 text-white"
                animate={{backgroundColor: '#5E7184'}}
                initial={false}/>
        )}
    </li>;
}

const SwitchTab = () => {
    const intl = useIntl();
    const router = useRouter();
    const switchTab = useCallback((tabName) => () => {
        router.replace(`/withdraw/${tabName}?step=2`)
    }, []);

    return <AnimateSharedLayout>
        <div className="mt-6 flex flex-wrap mx-3 mb-4">
            <ul className="flex flex-grow rounded-full mb-2 bg-white dark:bg-purple-700 p-1">
                <TabItem switchTab={switchTab} tabName={'banks'} displayName={intl.messages["tab.banks"]}/>
                <TabItem switchTab={switchTab} tabName={'crypto'} displayName={intl.messages["tab.crypto"]}/>
            </ul>
        </div>
    </AnimateSharedLayout>
}

export default SwitchTab;