import { memo, useContext, useEffect } from 'react'
import classnames from 'classnames'
import Slider from 'react-slick'
import BankCard from 'components/icons/BankCard'
import Crypto from 'components/icons/Crypto.jsx'
import { Context } from './reducer/reducer'

import style from './withdraw.module.scss'


const BankCardItem = memo(({ cardInfo, className, backgroundIcon }) => {
    return <div className={classnames(style.card, className, 'text-white', 'mb-8', "relative")}>
        <div className={classnames(style.background, 'z-0', "absolute", "inset-0", "overflow-hidden", "h-full")}>
            {backgroundIcon}
            <div className='bg-black' />
            <div className='bg-black' />
            <div className='bg-black' />
        </div>
        <div className={classnames(style.card_content)}>
            <p className={classnames(style.card_name, "font-semibold")}>{cardInfo.name}</p>
            <p className={classnames(style.card_number)}>{cardInfo.number}</p>
            <p className={classnames(style.card_real_name, 'absolute bottom-12')}>{cardInfo.accountName}</p>
        </div>

    </div>
})


const CardList = memo(({ tab, name, list = [] }) => {
    const {
        withdraw,
        dispatch,
    } = useContext(Context)
    const settings = {
        centerMode: true,
        dots: false,
        centerPadding: '13.5px',
        slidesToShow: 1,
        swipeToSlide: true,
        afterChange: function(index) {
            dispatch({
                type: 'SELECT_WITHDRAW_CARD',
                value: info(list[index]),
                tab,
            })
        },
    }

    if (list.length === 0) {
        return <div className={style.card_1} />
    }

    const isBank = () => {
        return tab === 'banks'
    }

    const icon = () => {
        return isBank() ?
            <BankCard width={58} height={70} className={classnames(style.background_bank, 'z-0 absolute top-1')} /> :
            <Crypto width={58} height={70} className={classnames(style.background_crypto, 'z-0 absolute top-1 opacity-40')} />
    }

    const info = (item) => {
        return isBank() ?
            {
                accountName: name,
                name: item.bankName,
                number: item.cardNumber,
                id: item.cardId
            } :
            {
                accountName: item.protocol,
                name: item.nickName,
                number: item.address,
                id: item.id
            }
    }

    useEffect(() => {
        dispatch({
            type: 'SELECT_WITHDRAW_CARD',
            value: info(list[0]),
            tab,
        })
    }, [list, tab])

    return <Slider {...settings}>
        {
            list.map((item, index) => {
                return <BankCardItem cardInfo={info(item)}
                                     className={style[`card_${(index %4) +1}`]}
                                     backgroundIcon={icon()}
                                     key={index} />
            })
        }
    </Slider>
})

export default CardList;