import React from 'react'
import classnames from 'classnames'
import { useIntl } from 'react-intl'
import WalletIcon from '../icons/Wallet.jsx'
import Currency from '../core/Currency'
import FreeWithdraw from '../icons/FreeWithdraw.jsx'

const AccountInfo = ({name, icon, className, children}) => {
    return <li className={classnames("w-full flex flex-grow box-border rounded-md bg-gray-50 dark:bg-purple-700", className)}>
        <div className="w-full relative">
            <div className={classnames("px-3 py-4 ")}>
                {children}
                <p className={classnames("text-platinum-200", "text-body-6")}>{name}</p>
            </div>
            <div className="absolute right-0 bottom-0">
                {icon}
            </div>
        </div>
    </li>;
}

const Accounting = ({ accountBalance }) => {
    const intl = useIntl()
    return <ul className={classnames('flex flex-grow w-11/12 px-3 mt-4')}>
        <AccountInfo name={intl.messages['balance']}
                     className={'mr-2'}
                     icon={<WalletIcon width={34} height={50} fillColor={'#EE5650'}
                                       className='absolute right-0 bottom-0 rounded-br-md opacity-20' />}>
            {accountBalance &&
            <Currency value={accountBalance.accountBalance} floatClassName={'text-body-6 text-gray-500 dark:text-white'}
                      intClassName={'text-body-4 text-gray-500 dark:text-white'} />}
        </AccountInfo>
        <AccountInfo name={intl.messages['free_withdraw']}
                     icon={<FreeWithdraw width={34} height={45} fillColor={'#44C5BE'}
                                         className='absolute right-0 bottom-0 rounded-br-md opacity-20' />}>
            {accountBalance &&
            <span className={classnames('text-body-4 text-gray-500 dark:text-white')}>
                        {accountBalance.withdrawalCount}
                    </span>
            }
        </AccountInfo>
    </ul>
}

export default Accounting;