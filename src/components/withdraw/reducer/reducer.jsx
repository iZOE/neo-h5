import { createContext } from 'react'

export const Context = createContext({})

const turnoverChange = (state, action, tab) => {
    if (action.value.nextStep) {
        return {
            ...state,
            turnover: action.value.turnover,
        }
    }

    return {
        ...state,
        turnover: action.value.turnover,
    }
}

export const withdrawReducer = (state, action) => {
    switch (action.type) {
        case "INPUT_WITHDRAW_PASSWORD":
            return {
                ...state,
                password: action.value,
            }
        case "WITHDRAW_TURNOVER_SUMMARY":
            return turnoverChange(state, action)
        case "WITHDRAW_TURNOVER_DETAIL":
            return {
                ...state,
                step: 99,
                back: action.value
            }
        case 'INPUT_WITHDRAW_SMS':
            return {
                ...state,
                sms: action.value
            }
        case 'WITHDRAW_INPUT_BALANCE':
            return {
                ...state,
                balance: action.value.value,
                isValidBalance: action.value.isValidBalance
            }
        case "SELECT_WITHDRAW_CARD":
            return {
                ...state,
                card: action.value
            }
        default:
            return state
    }
}

export const withdrawInitialState = {
    step: 1,
    balance: '',
    card: {},
    back: 1,
    password: "",
    turnover: {},
    sms: ''
}