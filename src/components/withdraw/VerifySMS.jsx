import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react'
import fetcher from '../../libs/fetcher'
import { Controller, useForm } from 'react-hook-form'
import showError from '../../libs/showError'
import Input from '../../layouts/LoginAndSignup/TemplateA/components/Input'
import Select from '../core/Select'
import Container from '../core/Container'
import { FormattedMessage, useIntl } from 'react-intl'
import Button from '../core/Button'
import ExpiredTime from '../../pages-lib/withdraw-manage/smsFundPassValidateSteps/sms/components/ExpiredTime'
import GetCodeButton from '../../pages-lib/withdraw-manage/smsFundPassValidateSteps/sms/components/GetCodeButton'
import hiddenThePhone from '../../pages-lib/withdraw-manage/smsFundPassValidateSteps/sms/libs'
import { Context } from './reducer/reducer'
import { useRouter } from 'next/router'


const SetupPhoneNumber = () => {
    const intl = useIntl()
    const [isSubmitting, setIsSubmitting] = useState(false)
    const {
        control,
        handleSubmit,
        errors,
        getValues,
        setValue,
        watch,
        formState: { isDirty, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
    })

    const phoneValidator = {
        required: value => !!value,
        length: value => {
            const phoneNumberLength = process.env.NEXT_PUBLIC_AGENT_CODE === 'VT999' ? 10 : 11
            return value.length === phoneNumberLength
        },
    }

    const OPTIONS = JSON.parse(process.env.NEXT_PUBLIC_PHONE_COUNTRY_CODE_LIST)
    const isShouldDisabled = (isSubmitting, watchContactNumber, isDirty, isValid) => {
        if (isValid) {
            return false
        }

        return isSubmitting || !watchContactNumber || !isDirty || !isValid
    }
    const clearValue = useCallback(() => {
        setValue('ContactNumber', '', { shouldValidate: true })
    }, [])

    const contactNumberErrorMsg = {
        required: <FormattedMessage id="phone.validate.ContactNumber.required" />,
        length: <FormattedMessage id="phone.validate.ContactNumber.length" />,
    }

    const watchContactNumber = watch('ContactNumber', '')

    const onSubmit = useCallback(async formData => {
        const _contactNumber = formData['ContactNumber']
        setIsSubmitting(true)
        try {
            const res = await fetcher({
                url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/contact?ContactNumber=${_contactNumber}`,
                method: 'put',
                withToken: true,
            })
            setIsSubmitting(false)
            if (res.data.code === '0000') {
                gotoNext({
                    contactNumber: res.data.data.contactNumber,
                    expiredTime: res.data.data.expiredTime,
                    lockDuration: res.data.data.lockDuration,
                })
            }
        } catch (error) {
            setIsSubmitting(prev => false)
            showError(error)
        }
    }, [])

    return <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mt-4" />
            <Controller
                name="locale"
                control={control}
                render={({ onChange, value }) => (
                    <Select
                        label={intl.formatMessage({ id: 'phone.select_label' })}
                        options={OPTIONS}
                        name="country"
                        onChange={onChange}
                    />
                )}
            />
            <div className="mt-4" />
            <div className="mt-4">
                <Controller
                    name={'local'}
                    control={control}
                    rules={{ validate: phoneValidator }}
                    render={({ onChange, value }) => (
                        <Input
                            label={intl.formatMessage({ id: 'phone.phone_label' })}
                            placeholder={intl.formatMessage({ id: 'phone.step1.placeholder' })}
                            onChange={onChange}
                            width={1}
                            type="number"
                            value={value}
                            withDelete
                            clearValue={clearValue}
                            name={'ContactNumber'} />
                    )}
                />
            </div>
            {errors && (
                <div className="text-red mt-1 text-body-8">
                    {contactNumberErrorMsg[errors?.ContactNumber?.type]}
                </div>
            )}
            <div className="h-6" />
            <div className="flex">
                <div className="flex-1">
                    <Button
                        variant="primary"
                        isLoading={isSubmitting}
                        type="submit"
                        disabled={isShouldDisabled(isSubmitting, watchContactNumber, isDirty, isValid)}
                    >
                        {intl.formatMessage({ id: 'phone.step1.submit' })}
                    </Button>
                </div>
            </div>
        </form>
    </Container>
}

const VerifyPhone = ({ phone }) => {
    const intl = useIntl()
    const router = useRouter()
    const {tab} = router.query;
    const cachedHiddenPhone = useMemo(() => hiddenThePhone(phone), [phone])
    const [isMappingCodeDisabled, setIsMappingCodeDisabled] = useState(false)
    const [isSubmitting, setIsSubmitting] = useState(false)
    const resetCounter = useRef({})
    const {
        control,
        handleSubmit,
        errors,
        setValue,
        watch,
        formState: { isDirty, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
    })
    const clearValue = useCallback(() => {
        setValue('MappingCode', '')
    }, [])
    const watchMappingCode = watch('MappingCode', '')

    const {
        dispatch,
    } = useContext(Context)

    const mappingCodeValidator = {
        required: value => !!value,
        length: value => {
            return value.length === 4

        },
    }
    const mappingCodeErrorMsg = {
        required: <FormattedMessage id="phone.validate.MappingCode.required" />,
        length: <FormattedMessage id="phone.validate.MappingCode.length" />,
    }
    const getVerifyCode = async (number) => {
        setIsSubmitting(true)
        try {
            const {
                data: {
                    code,
                    data: {
                        expiredTime,
                        lockDuration,
                    },
                },
            } = await fetcher({
                url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/contact?ContactNumber=${number}`,
                method: 'put',
                withToken: true,
            })
            setIsSubmitting(false)
            if (code === '0000') {
                const { resetExpireTime, resetLockDuration } = resetCounter.current
                resetExpireTime(expiredTime)
                setIsMappingCodeDisabled(false)
                resetLockDuration(lockDuration)
            }
        } catch (error) {
            setIsSubmitting(false)
            showError(error)
        }
    }
    useEffect(async () => {
        await getVerifyCode(phone)
    }, [])
    const whenMappingCodeExpired = useCallback(() => {
        setIsMappingCodeDisabled(true)
    }, [])
    const onSubmit = useCallback(async (formData) => {
        console.info(formData)
        setIsSubmitting(true)

        dispatch({
            type: 'INPUT_WITHDRAW_SMS',
            value: formData.MappingCode,
        })
        router.push(`/withdraw/${tab}?step=3`)
    }, [])

    return <Container withNavigationBar>
        <div className="mt-4 text-platinum-300 text-body-4">
            {intl.formatMessage({ id: 'phone.mapping_code_sent' })}
        </div>
        <div className="mt-2 mb-4 text-blue-200 text-body-4">{cachedHiddenPhone}</div>
        <form onSubmit={handleSubmit(onSubmit)}>
            <Controller
                name={'MappingCode'}
                control={control}
                rules={{ validate: mappingCodeValidator }}
                render={({ onChange, value }) => (
                    <Input
                        placeholder={intl.formatMessage({ id: 'phone.step2.placeholder' })}
                        onChange={onChange}
                        width={1}
                        disabled={isMappingCodeDisabled}
                        type="number"
                        value={value}
                        withDelete
                        clearValue={clearValue}
                    />
                )}
            />
            {errors && (
                <div className="text-red mt-1 text-body-8">
                    {mappingCodeErrorMsg[errors?.MappingCode?.type]}
                </div>
            )}
            <div className="h-1" />
            <ExpiredTime
                sec={0}
                collector={resetCounter}
                whenMappingCodeExpired={whenMappingCodeExpired}
            />
            <div className="flex mt-6">
                <div className="flex-1">
                    <Button
                        variant="primary"
                        isLoading={isSubmitting}
                        type="submit"
                        disabled={isSubmitting || !watchMappingCode || !isDirty || !isValid}
                    >
                        {intl.formatMessage({ id: 'phone.step2.submit' })}
                    </Button>
                </div>
            </div>
        </form>
        <div className="flex justify-center mt-6">
            <GetCodeButton
                isSubmitting={isSubmitting}
                sec={false}
                onClick={() => getVerifyCode(phone || phone)}
                collector={resetCounter}
            />
        </div>
    </Container>
}

const VerifySMS = ({ verified, phone }) => {

    if (verified) {
        return <VerifyPhone phone={phone} />
    }

    return <SetupPhoneNumber />

}

export default VerifySMS