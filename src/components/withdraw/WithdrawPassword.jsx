import React, { useCallback, useState } from 'react'
import { FormattedMessage, useIntl } from 'react-intl'
import Link from 'next/link'
import fetcher from '../../libs/fetcher'
import showError from '../../libs/showError'
import Input from 'components/core/Input'
import Button from '../core/Button'
import Container from '../core/Container'
import { useRouter } from 'next/router'
import useStep, { useStepData } from '../../hooks/useStep'
import { Controller, useForm } from 'react-hook-form'

const FUND_PASSWORD_FORM_FIELD_NAME = 'password'

const fundPasswordValidator = {
    required: value => !!value,
    length: value => {
        if (value.length !== 6) {
            return false
        }
        return true
    },
}

const fundPasswordErrorMsg = {
    required: <FormattedMessage id="fund_password.step1.validate.password.required" />,
    length: <FormattedMessage id="fund_password.step1.validate.password.length" />,
}

const WithdrawPassword = ({ callback }) => {
    const intl = useIntl()
    const [isSubmitting, setIsSubmitting] = useState(false)
    const {
        control,
        handleSubmit,
        errors,
        getValues,
        setValue,
        watch,
        formState: { isDirty, isValid },
    } = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
    })

    const watchFundPassword = watch(FUND_PASSWORD_FORM_FIELD_NAME, '')

    const onSubmit = useCallback(async formData => {
        const password = formData[FUND_PASSWORD_FORM_FIELD_NAME]
        setIsSubmitting(prev => true)
        try {
            const res = await fetcher({
                url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/funds/verify`,
                method: 'post',
                withToken: true,
                data: { password },
            })
            setIsSubmitting(prev => false)
            if (res?.data?.code === '0000') {
                callback(password)
            }
        } catch (error) {
            setIsSubmitting(prev => false)
            showError(error)
        }
    }, [])

    const clearValue = useCallback(() => {
        setValue(FUND_PASSWORD_FORM_FIELD_NAME, '')
    }, [])

    return (
        <>
            <Container withNavigationBar>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-4">
                        <Controller
                            name={FUND_PASSWORD_FORM_FIELD_NAME}
                            control={control}
                            rules={{ validate: fundPasswordValidator }}
                            render={({ onChange, value }) => (
                                <Input
                                    placeholder={intl.formatMessage({
                                        id: 'fund_password.step1.password.placeholder',
                                    })}
                                    onChange={onChange}
                                    width={1}
                                    type="password"
                                    value={value}
                                    withDelete
                                    clearValue={clearValue}
                                />
                            )}
                        />
                    </div>
                    {errors && (
                        <div className="text-red mt-1 text-body-8">
                            {fundPasswordErrorMsg[errors?.password?.type]}
                        </div>
                    )}
                    <div className="h-6" />
                    <div className="flex">
                        <div className="flex-1">
                            <Button
                                variant="primary"
                                isLoading={isSubmitting}
                                type="submit"
                                disabled={isSubmitting || !watchFundPassword || !isDirty || !isValid}
                            >
                                {intl.formatMessage({ id: 'fund_password.step1.submit' })}
                            </Button>
                        </div>
                    </div>
                    <div className="flex mt-4 text-body-6 text-platinum-200 dark:text-platinum-300">
                        <div className="flex-1">
                            <FormattedMessage
                                id="fund_password.step1.explanation1"
                                values={{
                                    br: <br />,
                                    cskh: (
                                        <Link href="/service">
                                          <span
                                              className="no-underline text-body-8 text-blue-200 not-italic font-semibold">
                                            <FormattedMessage id="fund_password.cskh" />
                                          </span>
                                        </Link>
                                    ),
                                }}
                            />
                        </div>
                    </div>
                </form>
            </Container>
        </>
    )
}

export default WithdrawPassword