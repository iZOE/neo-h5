import { FormattedMessage, useIntl } from 'react-intl'
import React, { useCallback, useContext } from 'react'
import { Context } from './reducer/reducer'
import fetcher from '../../libs/fetcher'
import classnames from 'classnames'
import Currency from '../core/Currency'
import ArrowRight from '../icons/ArrowRight'
import Link from 'next/link'
import { useRouter } from 'next/router'
import showMessage from 'components/core/Alert/message'
import IconHelp from '../icons/Help'

const WithdrawSubmit = () => {
    const intl = useIntl()
    const router = useRouter()
    const { tab } = router.query
    const {
        withdraw,
        dispatch,
    } = useContext(Context)

    const isBank = () => {
        return tab === 'banks'
    }
    const accountMasking = (bankCard) => {
        const len = isBank() ? 4 : 5
        return bankCard.substr(bankCard.length - len, len)
    }

    const WithdrawRequestBody = () => {
        if (isBank()) {
            return {
                'fundPassword': withdraw.password,
                'memberBankId': withdraw.card.id,
                'requestAmount': withdraw.balance,
                'bankBranchName': null,
                'ValidationCode': withdraw
            }
        }

        return {
            'fundPassword': withdraw.password,
            'cryptoCurrencyId': withdraw.card.id,
            'requestAmount': withdraw.balance,
            'currencyId': 4,
            'ValidationCode': withdraw
        }
    }

    const handleNextStep = async () => {
        try {
            await fetcher({
                url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/${isBank() ? 'Withdrawal' : 'Withdrawal/CryptoCurrency'}`,
                method: 'post',
                data: WithdrawRequestBody(),
                withToken: true,
                isRefreshToken: true,
            })

            showMessage({
                content: intl.messages['success'],
                okText: intl.formatMessage({ id: 'confirm' }),
                closable: false,
                onOk: () => {
                    router.push('/user')
                },
            })
        } catch (e) {

            console.info(e)
            const {
                responseMessage,
            } = e.data
            showMessage({
                content: responseMessage,
                okText: intl.messages[`confirm`],
            })
        }
    }

    const displayDetail = useCallback(() => {
        router.push(`/withdraw/${tab}?step=99`)
    })

    const {
        turnOverInfo: {
            administrationFee,
            preferAmount,
            withdrawalFee,
            actuallyAmount,
        },
    } = withdraw.turnover

    return <section className="pt-11">
        <section className={classnames('px-3', 'bg-white', 'mt-2', 'dark:bg-purple-800')}>
            <div
                className="flex flex-1 border-b items-center py-3 justify-between text-platinum-100 dark:text-platinum-200 border-gray-100 dark:border-gray-500">
                <p className="flex font-semibold text-title-8 text-platinum-300">
                    {
                        intl.messages[`submit.${tab}.title`]
                    }
                </p>
                <p className="flex flex-col text-body-6">
                    <p className="flex">
                        <p className="w-full text-right text-body-6">{withdraw.card.name}</p>
                    </p>
                    <p className="flex text-right text-body-8">
                        <FormattedMessage id={`submit.${tab}.card`}
                                          values={{ card: accountMasking(withdraw.card.number) }} />
                    </p>
                </p>
            </div>
            <div
                className="flex flex-1 pt-4 text-body-4 font-semibold justify-between text-platinum-100 dark:text-platinum-200">
                <p className="flex">
                    {intl.messages['submit.balance']}
                </p>
                <p className="flex text-right">
                    <Currency value={withdraw.balance} intClassName="text-body-4" floatClassName="text-body-8 pt-1" />
                </p>
            </div>
            <div
                className="flex flex-1 pt-3 text-body-6 font-semibold justify-between text-platinum-100 dark:text-platinum-200">
                <p className="flex">
                    -{intl.messages['submit.administration_fee']}
                    <span className="inline-block relative">
                      <IconHelp width="14" height="14" fillColor="#BBB" />
                    </span>
                </p>
                <p className="flex text-right">
                    <Currency value={administrationFee} intClassName="text-body-4" floatClassName="text-body-8 pt-1" />
                </p>
            </div>
            <div
                className="flex flex-1 pt-3 text-body-6 font-semibold justify-between text-platinum-100 dark:text-platinum-200">
                <p className="flex">
                    -{intl.messages['submit.prefer_amount']}
                    <span className="inline-block relative">
                      <IconHelp width="14" height="14" fillColor="#BBB" />
                    </span>
                </p>
                <p className="flex text-right">
                    <Currency value={preferAmount} intClassName="text-body-4" floatClassName="text-body-8 pt-1" />
                </p>
            </div>
            <div
                className="flex flex-1 pt-3 text-body-6 font-semibold justify-between text-platinum-100 dark:text-platinum-200">
                <p className="flex">
                    -{intl.messages['submit.withdrawal_fee']}
                    <span className="inline-block relative">
                      <IconHelp width="14" height="14" fillColor="#BBB" />
                    </span>
                </p>
                <p className="flex text-right">
                    <Currency value={withdrawalFee} intClassName="text-body-4" floatClassName="text-body-8 pt-1" />
                </p>
            </div>
            <div
                className="flex flex-1 pt-3 text-body-4 font-semibold justify-between text-platinum-300 dark:text-white">
                <p className="flex ">
                    ={intl.messages['submit.actually_amount']}
                </p>
                <p className="flex text-right">
                    <Currency value={actuallyAmount} intClassName="text-body-4" floatClassName="text-body-8 pt-1" />
                </p>
            </div>

            <div
                className="pt-2 pb-3 text-body-4 font-semibold border-b flex justify-end border-gray-100 dark:border-gray-500">
                <button className={'text-blue-200 flex-row flex'}
                        onClick={displayDetail}>
                    <span className={'flex text-body-6 font-semibold'}>
                        {intl.messages['submit.detail']}
                    </span>
                    <span className={'flex'}><ArrowRight fillColor={'#1D8AEB'} width={16} height={18} /></span>
                </button>
            </div>

            <div className="px-3 py-4">
                <p className="text-platinum-200 text-body-4">
                    {intl.messages['notice.title']}
                </p>
                <ul className="">
                    {
                        Object.keys(intl.messages).filter(x => x.startsWith('notice.submit.')).map((item, index) => {
                            return <li className="text-platinum-200 text-body-4 flex flex-1 justify-start">
                                <span className="flex">{index + 1}.</span>
                                <span className="flex">
                                    <FormattedMessage id={item} values={{
                                        cs: <Link href={'/service'}>
                                            <a className="text-blue-100 font-semibold">
                                                {intl.messages['fund_password.cskh']}
                                            </a>
                                        </Link>,
                                    }} /></span>
                            </li>
                        })
                    }
                </ul>

            </div>
        </section>

        <section className={classnames('mt-6', 'px-3')}>
            <button
                className="w-full bg-blue-200 px-3 py-4 text-center rounded-full text-body-4 font-semibold disabled:bg-gray-200 text-white"
                onClick={handleNextStep}>
                {intl.messages['submit.button']}
            </button>
        </section>
    </section>
}

export default WithdrawSubmit