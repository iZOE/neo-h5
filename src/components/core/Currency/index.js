import { useMemo } from 'react'

export default function Currency({
  value = 0,
  float = 2,
  intClassName = 'text-body-5',
  floatClassName = 'text-body-7',
}) {
  if (typeof value !== 'string' && typeof value !== 'number') {
    console.error(`"${typeof numbers}" is not a number type`)
  }
  if (Number.isNaN(Number(value))) {
    console.error(`"${value} is not a number"`)
  }

  const { integer, floatAfterSign } = useMemo(() => {
    const numberArray = String(Number(value).toFixed(float)).split('.')
    return {
      integer: Number(numberArray[0]).toLocaleString(),
      floatAfterSign: numberArray[1],
    }
  }, [value])

  if (value === '-') {
    return value
  }

  return (
    <>
      <span className={intClassName}>{`${integer}.`}</span>
      <span className={floatClassName}>{floatAfterSign}</span>
    </>
  )
}
