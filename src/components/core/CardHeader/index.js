function CardHeader({ title, subTitle }) {
  return (
    <div className="flex items-baseline justify-between border-b pb-2 border-gray-100 dark:border-gray-500">
      {title}
      <div className="text-platinum-200 text-body-6">{subTitle}</div>
    </div>
  )
}

export default CardHeader
