import React, { useContext } from 'react'
import { ThemeContext } from 'styled-components'

export default function Container({
  withNavigationBar,
  bg,
  noPadding,
  noPaddingTop,
  noHighScreen,
  children,
}) {
  const themeContext = useContext(ThemeContext)
  return (
    <div
      data-testid="shared-component-container"
      className={`transition duration-150 ease-in-out w-full sm:w-[600px] m-0 sm:mx-auto pb-3 ${withNavigationBar ? 'pt-11 sm:pt-12' : 'pt-[10px]'
        } ${noPaddingTop && 'pt-0'} ${noPadding ? 'px-0' : 'px-3'} ${noHighScreen ? '' : 'h-screen'
        }`}
      style={{
        backgroundColor: bg || themeContext.sharedComponent.container.bg,
      }}
    >
      {children}
      <div
        className="transition duration-150 ease-in-out fixed inset-0 z-[-99]"
        style={{
          backgroundColor: themeContext.sharedComponent.container.bg,
        }}
      />
    </div>
  )
}
