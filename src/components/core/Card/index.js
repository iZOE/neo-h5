const BORDER_COLOR = {
  red: 'border-red',
  platinum: 'border-platinum-200',
  blue: 'border-blue-200',
}

function Card({ onClick, children, color }) {
  return (
    <div
      className={`py-2 px-4 my-2 relative bg-white dark:bg-gray-650 border-l-[3px] ${BORDER_COLOR[color]}`}
      onClick={onClick}
    >
      {children}
    </div>
  )
}

export default Card
