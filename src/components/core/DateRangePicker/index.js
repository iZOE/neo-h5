import { useState, useRef } from 'react'
import { FormattedMessage } from 'react-intl'
import * as dayjs from 'dayjs'

const now = dayjs()

export default function DateRangePicker({
  onChange,
  startTime = now.subtract(2, 'week').startOf('d').unix(),
  endTime = now.endOf('d').unix(),
}) {
  const endInput = useRef(null)

  const [state, setState] = useState({
    start: startTime,
    end: endTime,
  })

  function onDateChange(e) {
    const { value, name } = e.target
    if (name === 'start') {
      // if (dayjs(value).isAfter(dayjs.unix(state.end))) {
      //   updateField('end', value)
      // } else {
      //   endInput.current.focus()
      // }
      updateField('end', dayjs(value).add(14, 'day'))
    }
    if (name === 'end') {
      // if (dayjs(value).isBefore(dayjs.unix(state.start))) {
      //   updateField('start', value)
      // }
      updateField('start', dayjs(value).subtract(14, 'day'))
    }
    updateField(name, value)
  }

  function updateField(name, value) {
    setState(prevState => ({
      ...prevState,
      [name]: dayjs(value).unix(),
    }))
    onChange({ [name]: dayjs(value).unix() })
  }

  function DateInput({ date, name, inputRef }) {
    return (
      <div className="relative py-[5px] px-5 bg-[#197dd6]">
        <span>{dayjs.unix(date).format('YYYY/MM/DD')}</span>
        <input
          type="date"
          value={dayjs.unix(date).format('YYYY-MM-DD')}
          name={name}
          className="w-full absolute left-0 opacity-0"
          onChange={e => onDateChange(e)}
          ref={inputRef}
        />
      </div>
    )
  }

  return (
    <div className="p-3 bg-blue-200 text-white flex justify-around items-baseline">
      <div className="text-title-8">
        <FormattedMessage id="last2weeks" />
      </div>
      <div className="text-body-6 flex items-center">
        <DateInput date={state.start} name="start" />
        <div className="mx-[10px]">~</div>
        <DateInput date={state.end} name="end" inputRef={endInput} />
      </div>
    </div>
  )
}
