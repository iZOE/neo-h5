import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { motion } from 'framer-motion'

const spring = {
  type: 'spring',
  stiffness: 700,
  damping: 30,
}

function Switch({ label, checked, onClick }) {
  const [isCheck, setIsCheck] = useState()

  useEffect(() => {
    setIsCheck(checked)
  }, [checked])

  function handleSwitch() {
    if (onClick) {
      onClick()
    }
  }

  return (
    <div className="flex items-center">
      {label ? (
        <span
          className="font-semibold text-body-6 sm:text-body-4 text-blue-200"
          style={{ marginRight: '6px' }}
        >
          {label}
        </span>
      ) : null}
      {typeof isCheck !== 'undefined' && (
        <div
          data-testid="shared-component-switch"
          style={{ width: '38px' }}
          className={`${String(isCheck)} ${isCheck ? 'bg-blue-200 justify-end' : 'bg-platinum-200 justify-start'
            } h-5 inline-flex rounded-full align-middle`}
          onPointerUp={handleSwitch}
        >
          <motion.div
            className="handle w-4 h-4 bg-white rounded-full"
            style={{ margin: '2px' }}
            layout
            transition={spring}
          />
        </div>
      )}
    </div>
  )
}

Switch.propTypes = {
  label: PropTypes.string,
  checked: PropTypes.bool,
  onClick: PropTypes.func,
}
Switch.defaultProps = {
  label: '',
  checked: false,
  onClick: undefined,
}

export default Switch
