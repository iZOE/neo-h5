
export default function EmptyWithIcon({
  icon,
  text
}) {

  return (
    <div className="text-center">
      <div className="inline-block">{icon}</div>
      <p className="mt-2 text-body-6 text-platinum-100">
        {text}
      </p>
    </div>
  )
}
