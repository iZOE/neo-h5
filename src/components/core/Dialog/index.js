import { useCallback, useState, useEffect } from 'react'
import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'
import { DialogOverlay } from '@reach/dialog'
import { AnimatePresence, motion } from 'framer-motion'
import Footer from './components/Footer'

const IconClose = dynamic(() => import('components/icons/Close'))

const style = {
  normal: {
    wrapperMb: 'mb-12',
    outterWrapper: 'overflow-y-scroll pt-2 px-4 text-platinum-200 pb-8',
    contentWrapper:
      'relative z-9999 p-0 w-full rounded box-border bg-white dark:bg-purple-800 mx-6',
    maxHeight: 480,
    width: ['calc(100% - 48px)', '366px'],
  },
  smaller: {
    wrapperMb: 'mb-3',
    outterWrapper: 'overflow-y-scroll pt-2 px-4 text-platinum-200',
    contentWrapper:
      'relative z-9999 p-0 w-full rounded box-border bg-white dark:bg-purple-800 mx-12',
    maxHeight: 374,
    width: ['74%'],
  },
}

const Dialog = ({
  isOpen,
  okText,
  onOk,
  cancelText,
  onCancel,
  onClose,
  closable,
  isDismissive,
  children,
  smaller,
  ...props
}) => {
  const _style = smaller ? style.smaller : style.normal
  const [isDialogShown, setIsDialogShown] = useState(isOpen)
  const onToggleDialog = useCallback(() => setIsDialogShown(prevState => !prevState), [])
  const hasFooter = !(typeof okText === 'undefined') || !(typeof cancelText === 'undefined')

  useEffect(() => {
    setIsDialogShown(isOpen)
  }, [isOpen])

  useEffect(() => {
    if (!isDialogShown && onClose) {
      onClose()
    }
  }, [isDialogShown])

  function handleOk() {
    if (onOk) {
      onOk()
      setIsDialogShown(false)
    }
  }

  const handleDissmiss = () => isDismissive && onToggleDialog()

  return (
    <>
      <AnimatePresence>
        {isDialogShown && (
          <DialogOverlay
            style={{ background: 'rgba(0, 0, 0, 0.5)', zIndex: 999 }}
            isOpen={isDialogShown}
            onDismiss={handleDissmiss}
          >
            <div className="flex items-center justify-center h-full">
              <motion.div
                {...props}
                className={_style.contentWrapper}
                aria-label="core-dialog"
                data-testid="shared-component-dialog"
                style={{
                  maxHeight: _style.maxHeight,
                  width: _style.width,
                }}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
              >
                <motion.div
                  className={_style.wrapperMb}
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  exit={{ opacity: 0 }}
                >
                  <div className="h-8">
                    {closable && (
                      <div className="absolute top-2 right-2" onClick={onToggleDialog}>
                        <IconClose />
                      </div>
                    )}
                  </div>
                  <div className={_style.outterWrapper} style={{ maxHeight: 440 }}>
                    {children}
                  </div>
                  {hasFooter && (
                    <Footer
                      onCancel={onCancel}
                      onOk={handleOk}
                      cancelText={cancelText}
                      okText={okText}
                    />
                  )}
                </motion.div>
              </motion.div>
            </div>
          </DialogOverlay>
        )}
      </AnimatePresence>
    </>
  )
}

Dialog.propTypes = {
  isOpen: PropTypes.bool,
  isDismissive: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  closable: PropTypes.bool,
}

Dialog.defaultProps = {
  isOpen: false,
  isDismissive: false,
  closable: true,
}

export default Dialog
