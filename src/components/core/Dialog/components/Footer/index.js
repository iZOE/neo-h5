import dynamic from 'next/dynamic'

const Button = dynamic(() => import('components/core/Button'))

export default function Footer({ onCancel, cancelText, onOk, okText }) {
  const hasCancelButton = typeof cancelText !== 'undefined'
  const hasOkButton = typeof onOk !== 'undefined'
  const needSpace = hasCancelButton && hasOkButton

  return (
    <div className="core-dialog-footer p-2 absolute left-0 bottom-0 w-full h-12 rounded-b border-t border-gray-100 dark:border-gray-500 bg-white dark:bg-purple-800">
      <div className="flex">
        {hasCancelButton && (
          <div className="flex-1">
            <Button variant="secondary" onClick={onCancel} height="32px">
              <span className="text-body-6">{cancelText}</span>
            </Button>
          </div>
        )}
        {needSpace && <div className="w-3" />}
        {hasOkButton && (
          <div className="flex-1">
            <Button variant="primary" onClick={onOk} height="32px">
              <span className="text-body-6">{okText}</span>
            </Button>
          </div>
        )}
      </div>
    </div>
  )
}
