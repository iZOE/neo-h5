const Title = ({ children }) => (
  <h4 className="text-title-7 text-platinum-200 font-semibold text-center mb-2">
    {children}
  </h4>
)

export default Title
