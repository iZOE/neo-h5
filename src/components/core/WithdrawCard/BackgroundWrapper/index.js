import Picture from 'components/core/Picture'
import { CARD_TYPE } from '../index'
import style from './BackgroundWrapper.module.scss'

function BackgroundWrapper({ type = 'bank' }) {
  const mediaPath =
    type === CARD_TYPE.BANK
      ? '/components/withdrawCard/BankCardBg'
      : '/components/withdrawCard/BTCBg'

  return (
    <>
      <div className={style.cardBackground}>
        <Picture
          className={style.backgroundLikeImg}
          png={`${mediaPath}.png`}
          webp={`${mediaPath}.webp`}
          hFull
        />
      </div>
    </>
  )
}

export default BackgroundWrapper
