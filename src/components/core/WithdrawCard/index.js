import React from 'react'
import CheckIcon from 'components/icons/CardChecked.js'
import BackgroundWrapper from './BackgroundWrapper'
import style from './withdrawcard.module.scss'

export const CARD_TYPE = {
  BANK: 'bank',
  WALLET: 'wallet',
}

const WALLET_BACKGORUND = {
  ERC20: 'linear-gradient(270deg, #6CE8E8 -7.28%, #24A1A8 100%)',
  Omni: 'linear-gradient(270deg, #EF96CB 0%, #DD66B8 100%)',
  TRC20: 'linear-gradient(270deg, #EF96CB 0%, #DD66B8 100%)',
}

function WithdrawCard({ type, data, checked, onCardClick }) {
  if (type === CARD_TYPE.BANK) {
    return (
      <div
        className={`z-0 rounded-md relative ${style.cardSize} mb-2 shadow-b-1`}
        style={{ backgroundColor: data.colorCode }}
      >
        <BackgroundWrapper type={CARD_TYPE.BANK} />
        <div className={`z-20 absolute top-0 left-0 ${style.cardSize}`} onClick={onCardClick}>
          <div className="flex flex-col mt-3 mb-6 ml-3">
            <div className="text-body-4 text-white mb-3">{data.bankName}</div>
            <div className="text-body-6 text-white mb-2">{data.cardNumber}</div>
            <div className="text-body-4 text-white">{data.accountName}</div>
          </div>
        </div>
        {checked && (
          <div className="z-30 absolute top-3 right-3">
            <CheckIcon alt="checked" />
          </div>
        )}
      </div>
    )
  }
  return (
    <div
      className={`z-0 rounded-md relative ${style.cardSize} mb-2 shadow-b-1`}
      style={{
        background: WALLET_BACKGORUND[data.protocol],
      }}
    >
      <BackgroundWrapper type={CARD_TYPE.WALLET} />
      <div className={`z-20 absolute top-0 left-0 ${style.cardSize}`} onClick={onCardClick}>
        <div className="flex flex-col mt-3 mb-6 ml-3">
          <div className="text-body-4 text-white mb-3">{data.nickName}</div>
          <div className="text-body-6 text-white mb-2">{data.address}</div>
          <div className="text-body-4 text-white">{data.protocol}</div>
        </div>
      </div>
      {checked && (
        <div className="z-30 absolute top-3 right-3">
          <CheckIcon alt="checked" />
        </div>
      )}
    </div>
  )
}

export default WithdrawCard
