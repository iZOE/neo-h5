import styled from 'styled-components'
import { motion } from 'framer-motion'
import IconLineRight from 'components/icons/LineRight'
import Switch from '../Switch'

const Wrapper = styled.button.withConfig({
  componentId: 'ListItem',
})`
  position: relative;
  background: ${props => (props.theme.darkMode ? '#2A2A38' : '#ffffff')};
  .icon {
    background: ${props => (props.theme.darkMode ? '#2A2A38' : '#ffffff')};
  }
  .switch {
    width: 44px;
  }
  .icon-line-right {
    path {
      fill: #95a3b0;
    }
  }
  .tip {
    font-size: 14px;
  }
  &:after {
    content: '';
    height: 1px;
    border-bottom: 1px solid ${props => props.theme.sharedComponent.container.bg};
    position: absolute;
    bottom: 0;
    left: ${props => (props.hasIcon ? '48px' : '0')};
    right: 0;
    transition: all 0.05s ease-in;
  }
`

export default function ListItem({
  children,
  icon,
  useAllow,
  allowTips,
  useSwitch,
  value,
  onClick,
  height
}) {
  return (
    <Wrapper
      data-testid="shared-component-list-item"
      className={`transition duration-150 ease-in-out w-full`}
      style={{ height: `${height}` || '44px' }}
      type="button"
      onClick={onClick}
      hasIcon={!!icon}
    >
      <motion.div
        className="text-left items-center flex py-3 px-4"
        whileTap={{ backgroundColor: '#ececec' }}
      >
        {icon && <div className="icon">{icon}</div>}
        {children}
        {useSwitch && (
          <div className="switch">
            <Switch checked={value} />
          </div>
        )}
        {useAllow && (
          <div className="flex items-center">
            {allowTips && <div className="tip">{allowTips}</div>}
            <div className="icon-line-right">
              <IconLineRight width="16" height="16" vw="16" vh="16" fillColor="text-platinum-200" />
            </div>
          </div>
        )}
      </motion.div>
    </Wrapper>
  )
}
