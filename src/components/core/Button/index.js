import React from 'react'
import LoadingIcon from './LoadingIcon'
import { Wrapper } from './Styled'

export default function Button({
  variant,
  text,
  height,
  width,
  type,
  isLoading,
  disabled,
  onClick,
  children,
}) {
  return (
    <Wrapper
      variant={variant}
      data-testid="shared-component-button"
      className="transition duration-150 ease-in-out focus:ring-transparent focus:ring-0 px-4 rounded-full font-semibold"
      width={width || 1}
      height={height || ['44px', '48px']}
      type={type}
      disabled={disabled}
      onPointerUp={onClick}
    >
      {isLoading ? (
        <LoadingIcon variant={variant} />
      ) : (
        children || <span className="shared-component-button-text">{text}</span>
      )}
    </Wrapper>
  )
}
