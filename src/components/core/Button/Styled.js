import styled from 'styled-components'
import css from '@styled-system/css'
import variant from '@styled-system/variant'
import layout from '@styled-system/layout'

export const Wrapper = styled('button')(
  layout,
  css({
    fontSize: '14px',
  }),
  ({ theme, disabled }) =>
    variant({
      variants: {
        primary: {
          border: 'none',
          color: disabled
            ? theme.sharedComponent.button.primary.disabled.color
            : theme.sharedComponent.button.primary.normal.color,
          bg: disabled
            ? theme.sharedComponent.button.primary.disabled.bg
            : theme.sharedComponent.button.primary.normal.bg,
        },
        secondary: {
          borderWidth: disabled ? 0 : '1px',
          borderColor: disabled ? 0 : theme.sharedComponent.button.secondary.normal.borderColor,
          borderStyle: 'solid',
          color: disabled
            ? theme.sharedComponent.button.secondary.disabled.color
            : theme.sharedComponent.button.secondary.normal.color,
          bg: disabled ? theme.sharedComponent.button.secondary.disabled.bg : 'transparent',
        },
        tertiary: {
          border: 'none',
          color: disabled
            ? theme.sharedComponent.button.tertiary.disabled.color
            : theme.sharedComponent.button.tertiary.normal.color,
          bg: disabled
            ? theme.sharedComponent.button.tertiary.disabled.bg
            : theme.sharedComponent.button.tertiary.normal.bg,
        },
      },
    }),
)
