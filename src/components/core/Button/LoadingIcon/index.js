import React from 'react'
import IconLoading from '../../../icons/Loading'
import { LoadingIcon } from './Styled'

export default function LoadingIconComponent({ width = 24, height = 24, variant = 'primary' }) {
  return (
    <LoadingIcon className="btn-loading">
      <IconLoading
        width={width}
        height={height}
        fillColor={variant === 'primary' ? '#fff' : '#95A3B0'}
      />
    </LoadingIcon>
  )
}
