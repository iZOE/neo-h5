/* eslint-disable no-confusing-arrow */
import styled from 'styled-components'
import PasswordClose from '../../icons/PasswordClose.svg'
import PasswordOpen from '../../icons/PasswordOpen.svg'

export const Wrapper = styled.div.withConfig({
  componentId: 'SharedInput',
})`
  input {
    background-color: ${props => props.theme.sharedComponent.input.bg};
    box-sizing: border-box;
    color: ${props => props.theme.sharedComponent.input.color};
    border-radius: 4px;
    &::placeholder {
      color: ${props => props.theme.sharedComponent.input.placeholderColor};
    }
  }
  input[type='date'] {
    -webkit-appearance: none;
  }
  &:focus-within {
    .input-outline {
      border: 1px solid
        ${({ theme, hasError }) =>
          hasError
            ? theme.sharedComponent.input.warning.borderColor
            : theme.sharedComponent.input.focus.borderColor};
    }
  }
`

export const InputOutline = styled.div`
  border-radius: 4px;
  border: 1px solid ${props => props.theme.sharedComponent.input.borderColor};
  background-color: ${props => props.theme.sharedComponent.input.bg};
`

export const IconPassWordClose = styled(PasswordClose)`
  path {
    fill: ${props => props.theme.sharedComponent.input.passwordIconColor};
  }
`

export const IconPassWordOpen = styled(PasswordOpen)`
  path {
    fill: ${props => props.theme.sharedComponent.input.passwordIconColor};
  }
`
