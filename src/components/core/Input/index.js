import React, { useState, useContext } from 'react'
import { ThemeContext } from 'styled-components'
import dynamic from 'next/dynamic'
import { Wrapper, InputOutline, IconPassWordClose, IconPassWordOpen } from './Styled'

const IconDelete = dynamic(() => import('../../icons/Delete.svg'))
const IconEyeClose = dynamic(() => import('components/icons/EyeClose'))
const IconEyeOpen = dynamic(() => import('components/icons/EyeOpen'))

export default function Input(props) {
  const themeContext = useContext(ThemeContext)
  const [inputType, setInputType] = useState(() => props.type)
  const {
    label,
    width,
    type,
    name,
    error,
    withDelete = false,
    height = 'h-11 sm:h-12',
    padding = 'py-0 px-3',
    text = 'text-body-4',
    clearValue,
    value,
    disabled = false,
  } = props

  const agentCode = process.env.NEXT_PUBLIC_AGENT_CODE

  function handleChangePasswordDisplay() {
    const nextInputType = inputType === 'password' ? 'text' : 'password'
    setInputType(nextInputType)
  }

  function handleClearValue() {
    if (clearValue) {
      clearValue()
    }
  }

  return (
    <Wrapper width={width} hasError={error}>
      {label && (
        <label
          className="label block text-body-4 font-semibold mb-1"
          style={{ color: themeContext.sharedComponent.input.label.color }}
          htmlFor={name}
        >
          {label}
        </label>
      )}
      <InputOutline className={`input-outline flex items-center box-content ${height}`}>
        <div className="w-full" style={{ height: 'inherit' }}>
          {name === 'phone' && (
            <span className="absolute p-3 text-platinum-300 dark:text-white text-title-8">
              {agentCode === 'VT999' ? '+84' : '+86'}
            </span>
          )}
          <input
            {...props}
            className={`border-none ${padding} w-full h-full ${text} disabled:bg-gray-100 dark:disabled:bg-purple-700 disabled:placeholder-gray-300 dark:disabled:placeholder-purple-200`}
            type={inputType}
            disabled={disabled}
          />
        </div>
        {withDelete && value && (
          <div className="w-11 sm:w-12 h-full flex items-center justify-center">
            <IconDelete onClick={handleClearValue} />
          </div>
        )}
        {type === 'password' && (
          <div className="w-11 sm:w-12 h-full border-none bg-transparent">
            <div
              className="flex justify-center items-center w-full h-full"
              onPointerUp={handleChangePasswordDisplay}
            >
              {inputType === 'password' ? <IconEyeClose /> : <IconEyeOpen />}
            </div>
          </div>
        )}
      </InputOutline>
      {error && (
        <div
          className="error-message mt-1 text-body-8"
          style={{ color: themeContext.sharedComponent.input.warning.messageColor }}
        >
          {error}
        </div>
      )}
    </Wrapper>
  )
}
