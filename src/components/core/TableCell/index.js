import { motion } from 'framer-motion'
import { useState } from 'react'
import { FormattedMessage } from 'react-intl'

function TableCell({ label, content, contentComponent, right, collapse }) {
  const [hide, setHide] = useState(collapse)

  function Content() {
    if (hide) {
      return null
    }
    if (contentComponent) {
      return (
        <div className="bg-gray-100 dark:bg-black -mb-3 -mx-3 px-3 text-body-6 big:text-body-4">
          {contentComponent}
        </div>
      )
    }
    /*
    return (
      <motion.div
        initial="collapsed"
        animate="open"
        exit="collapsed"
        variants={{
          open: { opacity: 1, height: 'auto' },
          collapsed: { opacity: 0, height: 0 },
        }}
        transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
      >
        <div className="text-gray-500 text-body-6">{content || '--'}</div>
      </motion.div>
    )
    */
    return (
      <div className="text-gray-500 dark:text-white text-body-6 big:text-body-4">
        {content || '--'}
      </div>
    )
  }

  function Right() {
    if (collapse) {
      return (
        <div className="text-body-6 text-gray-500 dark:text-platinum-200">
          <FormattedMessage id="more" />
        </div>
      )
    }
    if (!right) return null
    return <div className="text-body-6">{right}</div>
  }

  return (
    <div
      className="py-3 border-gray-100 dark:border-gray-500 border-b"
      onClick={() => collapse && setHide(!hide)}
    >
      <div className="flex justify-between text-body-6">
        <div className="text-platinum-200 pb-1">{label}</div>
        <Right />
      </div>
      <Content />
    </div>
  )
}

export default TableCell
