import React from 'react'
import { FormattedMessage } from 'react-intl'

export default function FetchMoreButton({ onClick, currentIndex, totalCount }) {
  const totalPage = Math.ceil(totalCount / 10)
  if (currentIndex >= totalPage) return null

  return (
    <div className="text-center">
      <button type="button" className="text-body-6 dark:text-white" onClick={onClick}>
        <FormattedMessage id="fetch_more" />
      </button>
    </div>
  )
}
