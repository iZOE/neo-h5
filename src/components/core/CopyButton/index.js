import { useCallback } from 'react'
import { useIntl } from 'react-intl'
import Button from 'components/core/Button'

const CopyButton = ({ content, buttonText }) => {
  const intl = useIntl()

  const handleCopyPromoCode = useCallback(async () => {
    const toast = (await import('components/core/Toast/toast')).default
    const textArea = document.createElement('textarea')
    textArea.value = content

    // Avoid scrolling to bottom
    textArea.style.top = '0'
    textArea.style.left = '0'
    textArea.style.position = 'fixed'

    document.body.appendChild(textArea)
    textArea.focus()
    textArea.select()
    document.execCommand('copy')

    document.body.removeChild(textArea)

    toast({
      message: intl.formatMessage({ id: 'copy_button.result.success' }),
    })
  }, [])

  return (
    <Button variant="secondary" onClick={handleCopyPromoCode}>
      <h4 className="text-title-8 font-semibold">{buttonText}</h4>
    </Button>
  )
}

export default CopyButton
