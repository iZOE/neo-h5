export default function List({ children }) {
  return (
    <div data-testid="shared-component-list" className="mb-2">
      {children}
    </div>
  )
}
