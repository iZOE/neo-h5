import ReactDOM from 'react-dom'
import { ThemeProvider } from 'styled-components'
import { theme } from '../../shared/ThemeProvider'
import Alert from './index'

export default function showMessage({
  title, content, okText, cancelText, onOk, onCancel, closable }) {
  const targetEle = document.createElement('alert-template')
  const onClose = () => {
    setTimeout(() => {
      const unmountResult = ReactDOM.unmountComponentAtNode(targetEle)
      if (unmountResult && targetEle.parentNode) {
        targetEle.parentNode.removeChild(targetEle)
      }
    }, 500)
  }

  const handleOk = () => {
    if (typeof onOk === 'function') {
      onOk()
    } else {
      onClose()
    }
  }
  const handleCancel = () => {
    if (typeof onCancel === 'function') {
      onCancel()
      onClose()
    } else {
      onClose()
    }
  }

  document.body.appendChild(targetEle)
  ReactDOM.render(
    <ThemeProvider theme={theme.config}>
      <Alert
        isOpen
        onClose={onClose}
        okText={okText}
        onOk={handleOk}
        cancelText={cancelText}
        onCancel={handleCancel}
        title={title}
        content={content}
        closable={closable}
      />
    </ThemeProvider>,
    targetEle,
  )
}
