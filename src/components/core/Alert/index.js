import dynamic from 'next/dynamic'

const Dialog = dynamic(() => import('components/core/Dialog'))
const Title = dynamic(() => import('components/core/Dialog/components/Title'))

const Alert = ({
  isOpen,
  isDismissive,
  okText,
  onOk,
  cancelText,
  closable,
  onCancel,
  onClose,
  title,
  content,
}) => (
  <Dialog
    isOpen={isOpen}
    isDismissive={isDismissive}
    onClose={onClose}
    okText={okText}
    onOk={onOk}
    cancelText={cancelText}
    closable={closable}
    onCancel={onCancel}
  >
    {title && <Title>{title}</Title>}
    {content && (
      <p className="text-body-2 text-platinum-200 text-center">
        {content}
      </p>
    )}
  </Dialog>
)

export default Alert
