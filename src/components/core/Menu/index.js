import { useState } from 'react'
import useSWR from 'swr'
import { useRecoilValue } from 'recoil'
import { useIntl } from 'react-intl'
import { motion, AnimateSharedLayout } from 'framer-motion'
import fetcher from 'libs/fetcher'
import { darkModeState } from 'atoms/darkModeState'
import style from './menu.module.scss'

function Menu({ data, selected = 0, onTypeChange, isFitWidth = false }) {
  const intl = useIntl()
  const [currentSelectId, setCurrentSelectId] = useState(selected)
  const rowClass = `${
    style.row_scroll_hide
  } overflow-x-scroll whitespace-nowrap sticky h-10 shadow-b-2 ${
    isFitWidth ? 'flex justify-between' : ''
  }`
  const itemClass = `relative inline-block ${isFitWidth ? 'flex-1 text-center' : ''}`

  return (
    <div className="fixed top-11 w-full z-40 bg-gray-100 dark:bg-black">
      <div className={rowClass}>
        <AnimateSharedLayout>
          {data?.map(d => (
            <div className={itemClass} key={d.id}>
              <button
                className="px-4 py-3 h-10 bg-transparent border-0 text-body-6"
                onPointerDown={() => {
                  setCurrentSelectId(d.id)
                  if (onTypeChange) {
                    onTypeChange(d.id)
                  }
                }}
              >
                <span className={currentSelectId === d.id ? 'text-blue-200' : 'text-platinum-200'}>
                  {d.name}
                </span>
              </button>
              {currentSelectId === d.id && (
                <motion.div
                  className="absolute bottom-0 w-full bg-blue-200"
                  style={{ height: '2px' }}
                  layoutId="act-menu"
                />
              )}
            </div>
          ))}
        </AnimateSharedLayout>
      </div>
    </div>
  )
}

export default Menu
