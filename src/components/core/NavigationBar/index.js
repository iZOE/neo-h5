import React, { useContext } from 'react'
import styled, { ThemeContext } from 'styled-components'

export const Wrapper = styled.div.withConfig({
  componentId: 'NavigationBar',
})`
  background-color: ${({ theme }) => theme.sharedComponent.navigationBar.bg};
  width: fill-available;
  padding-bottom: constant(safe-area-inset-top); /* iOS 11.0-iOS 11.1 */
  padding-bottom: env(safe-area-inset-top); /*iOS 11.2 */
`

export default function NavigationBar({
  fixed,
  right,
  onLeftClick,
  onRightClick,
  title,
  centerComponent,
  left,
  hide,
}) {
  const themeContext = useContext(ThemeContext)
  if (hide) {
    return null
  }
  return (
    <Wrapper
      data-testid="shared-component-navigation-bar"
      className="transition duration-150 ease-in-out w-full top-0 fixed z-50">
      <div
        className="flex text-center items-center h-11 sm:h-12 justify-between"
        style={{ width: '-webkit-fill-available' }}>
        <div
          className="flex items-center text-center justify-center w-12 sm:w-9 z-9999"
          style={{ height: 'inherit' }}
          onPointerUp={onLeftClick}>
          {left}
        </div>
        <div className="flex items-center text-center absolute w-full flex-1 z-0 justify-center">
          {title && (
            <div
              className="w-full text-center"
              style={{
                color: themeContext.sharedComponent.navigationBar.centerTextColor,
              }}
            >
              {title}
            </div>
          )}
          {!title && centerComponent && centerComponent}
        </div>
        <div
          className="flex items-center text-center justify-center mr-3 z-9999"
          onClick={onRightClick}
          style={{ minWidth: '36px' }}
        >
          {right}
        </div>
      </div>
    </Wrapper>
  )
}
