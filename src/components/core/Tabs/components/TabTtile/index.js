import PropTypes from 'prop-types'
import { TabTitleWrapper } from './Styled'

function TabTitle({
  isActive,
  dataTab,
  onChange,
  title,
  count,
  lineHeight,
  outlined,
  borderRadius,
  borderColor,
  bg,
  fontSize,
  textAlign,
  fontWeight
}) {
  return (
    <TabTitleWrapper
      className={`tab-title ${isActive === dataTab ? 'active' : ''}`}
      onClick={onChange}
      count={count}
      isActive={isActive === dataTab}
      lineHeight={lineHeight}
      outlined={outlined}
      borderRadius={borderRadius}
      borderColor={borderColor}
      bg={bg}
      fontSize={fontSize}
      textAlign={textAlign}
      fontWeight={fontWeight}
    >
      {title}
    </TabTitleWrapper>
  )
}

TabTitle.propTypes = {
  isActive: PropTypes.number,
  dataTab: PropTypes.number,
  onChange: PropTypes.func,
  count: PropTypes.number,
  lineHeight: PropTypes.string,
  outlined: PropTypes.bool,
  borderRadius: PropTypes.string,
  borderColor: PropTypes.string,
  bg: PropTypes.string,
  fontSize: PropTypes.string,
  textAlign: PropTypes.bool,
  fontWeight: PropTypes.string,
}

TabTitle.defaultProps = {
  isActive: true,
  dataTab: 0,
  onChange: undefined,
  count: undefined,
  lineHeight: undefined,
  outlined: false,
  borderRadius: undefined,
  borderColor: undefined,
  bg: undefined,
  fontSize: '12px',
  textAlign: undefined,
  fontWeight: 'normal',
}

export default TabTitle
