import styled, { css } from 'styled-components'

export const TabTitleWrapper = styled.div`
  ${({ bg }) => bg && css`background: ${bg};`}
  background: ${({ theme, isActive }) => (isActive && `linear-gradient(to right, ${theme.sharedComponent.tabs.active.bg[0]} 0%, ${theme.sharedComponent.tabs.active.bg[1]} 100%)`)};
  width: ${({ count }) => `calc(100% / ${count})`};
  color: ${({ theme, isActive }) => (isActive ? theme.sharedComponent.switchtab.active.color : theme.sharedComponent.switchtab.normal.color)};
${({ fontSize }) => fontSize && css`font-size: ${fontSize};`}
${({ fontWeight }) => typeof fontWeight === 'string' && css`font-weight: ${fontWeight || 'normal'}`};
${({ textAlign }) => (textAlign && textAlign === true && css`text-align: center;`)}
${({ lineHeight }) => lineHeight && typeof lineHeight === 'string' && css`line-height: ${lineHeight};`}
${({ outlined, borderColor }) => outlined && typeof borderColor === 'string' && css`
    border-width: 1px;
    border-style: solid;
    border-color: ${borderColor || 'transparent'};
  `}
${({ borderRadius }) => typeof borderRadius === 'string' && css`border-radius: ${borderRadius};`}
overflow: hidden;
`
