import { useEffect, useState } from 'react'
import TabTitle from './components/TabTtile'

function PrimaryTabPanel({ content }) {
  return (
    <div className="primary-panel">
      {content}
    </div>
  )
}

function SecondTabPanel({ content }) {
  return (
    <div className="second-panel">
      {content}
    </div>
  )
}

function Tabs(props) {
  const {
    data,
    initActiveIndex,
    lineHeight,
    outlined,
    borderRadius,
    borderColor,
    bg,
    fontSize,
    textAlign,
    fontWeight,
    ...tabsGroupStyles
  } = props

  const [active, setActive] = useState()

  useEffect(() => {
    setActive(initActiveIndex || 0)
  }, [initActiveIndex])

  const listTitle = data.map((item, index) => <TabTitle
    key={`${item.tabTitle}_${String(index)}`}
    isActive={active}
    onChange={() => setActive(index)}
    dataTab={index}
    count={data.length}
    title={item.tabTitle}
    lineHeight={lineHeight}
    outlined={outlined}
    borderRadius={borderRadius}
    borderColor={borderColor}
    bg={bg}
    fontSize={fontSize}
    textAlign={textAlign}
    fontWeight={fontWeight}
  />)

  return (
    <div className="relative flex w-full flex-col z-[2]">
      <div className="flex tab-title" style={{ ...tabsGroupStyles }}>{listTitle}</div>
      {data.map(
        (item, index) =>
          active === index && (
            <>
              <PrimaryTabPanel
                key={`PrimaryTabPanel_${String(index)}`}
                dataTab={index}
                content={item.tabPanel}
              />
              {item.secondTabPanel && (
                <SecondTabPanel
                  key={`SecondTabPanel_${String(index)}`}
                  dataTab={index}
                  content={item.secondTabPanel}
                />
              )}
            </>
          ),
      )}
    </div>
  )
}

export default Tabs
