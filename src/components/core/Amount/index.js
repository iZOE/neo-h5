export default function Amount({ value }) {
  let displayValue = value
  if (value > 0) {
    displayValue = `+${String(displayValue)}`
  }
  return <span className="text-red">{value}</span>
}
