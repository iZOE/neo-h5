import React from 'react'
import PropTypes from 'prop-types'

export const getFullPath = (path, mediaType) =>
  `${process.env.NEXT_PUBLIC_END_POINT_STORAGE}/${path}.${mediaType}`
export default function Picture({ src, webp, png, alt, hFull, ...props }) {
  const tailwWindClassForImg = hFull ? 'core-img h-full' : 'core-img'
  return (
    <picture data-testid="shared-component-picture">
      <source srcSet={webp} type="image/webp" />
      <source srcSet={png} type="image/png" />
      <img className={tailwWindClassForImg} src={webp} alt={alt} {...props} />
    </picture>
  )
}

Picture.propTypes = {
  src: PropTypes.string,
  webp: PropTypes.string.isRequired,
  png: PropTypes.string.isRequired,
}

Picture.defaultProps = {
  src: undefined,
}
