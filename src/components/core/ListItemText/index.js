import React, { useState } from 'react'
import styled from 'styled-components'

export default styled.div
  .withConfig({
    componentId: 'ListItemText',
  })
  .attrs({
    dataTestid: 'shared-component-list-item-text',
    className: 'transition duration-150 ease-in-out list-item-text flex-1',
  })`
  font-size: 14px;
  color: ${props => (props.theme.darkMode ? '#ffffff' : '#333333')};
`
