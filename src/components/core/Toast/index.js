import { useState, useEffect } from 'react'
import { AnimatePresence } from 'framer-motion'
import PropTypes from 'prop-types'

const spring = {
  type: 'spring',
  damping: 20,
  stiffness: 100,
}

export default function Toast({ duration = 3000, type, message, onDismiss }) {
  const [open, setOpen] = useState(true)

  useEffect(() => {
    setTimeout(() => {
      setOpen(false)
      if (onDismiss) {
        setTimeout(() => {
          onDismiss()
        }, 500)
      }
    }, duration)
  }, [])

  return (
    <AnimatePresence>
      {open ? (
        <div
          data-testid="shared-components-toast"
          className="fixed flex left-0 right-0 w-auto text-center z-9999"
          style={{ bottom: '150px' }}
          transition={spring}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1, y: 50 }}
          exit={{ opacity: 0 }}
        >
          <div
            className="toast-content my-0 mx-auto px-3 py-2 bg-gray-500"
            style={{ maxWidth: '80vw', borderRadius: '10px' }}
          >
            <p className="text-body-6 text-white">{message}</p>
          </div>
        </div>
      ) : null}
    </AnimatePresence>
  )
}

Toast.propTypes = {
  type: PropTypes.string,
  message: PropTypes.string.isRequired,
}

Toast.defaultProps = {
  type: 'normal',
}
