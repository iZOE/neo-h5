import ReactDOM from 'react-dom'
import { theme } from 'components/shared/ThemeProvider'
import { ThemeProvider } from 'styled-components'
import Toast from './index'

export default function displayToast({ type = 'normal', message }) {
  const targetEle = document.createElement('toast-template')
  document.body.appendChild(targetEle)
  ReactDOM.render(
    <ThemeProvider theme={theme.config}>
      <Toast
        isOpen
        type={type}
        message={message}
        onDismiss={() => {
          const unmountResult = ReactDOM.unmountComponentAtNode(targetEle)
          if (unmountResult && targetEle.parentNode) {
            targetEle.parentNode.removeChild(targetEle)
          }
        }}
      />
    </ThemeProvider>,
    targetEle,
  )
}
