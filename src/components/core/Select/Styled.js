/* eslint-disable no-confusing-arrow */
import styled from 'styled-components'

export const Wrapper = styled.div.withConfig({
  componentId: 'SharedInput',
})`
  // thanks to https://stackoverflow.com/a/65650953/5599652
  select {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    background: url("data:image/svg+xml;utf8,<svg width='12' height='7' viewBox='0 0 12 7' fill='none' xmlns='http://www.w3.org/2000/svg'>
<path fill-rule='evenodd' clip-rule='evenodd' d='M0.696815 0.989586C0.870381 0.81602 1.19107 0.776736 1.36464 0.950303L6.00012 5.58578L10.6356 0.950303C10.8092 0.776737 11.1299 0.81602 11.3034 0.989586C11.477 1.16315 11.5163 1.48384 11.3427 1.65741L6.00012 6.99999L0.657531 1.65741C0.483965 1.48384 0.523249 1.16315 0.696815 0.989586Z' fill='%2395a3b0'/>
</svg>")
      no-repeat;
    background-position: calc(100% - 0.75rem) center;
    -webkit-padding-end: 18px;
    -moz-padding-end: 18px;
    padding-right: 18px;
    user-select: none;
    -webkit-padding-start: 16px;
    -moz-padding-start: 16px;
    background-color: ${props => props.theme.sharedComponent.input.bg};
    box-sizing: border-box;
    color: ${props => props.theme.sharedComponent.input.color};
    border-radius: 4px;
    &::placeholder {
      color: ${props => props.theme.sharedComponent.input.placeholderColor};
    }
  }
`

export const SelectOutline = styled.div`
  border-radius: 4px;
  border: 1px solid ${props => props.theme.sharedComponent.input.borderColor};
  background-color: ${props => props.theme.sharedComponent.input.bg};
`
