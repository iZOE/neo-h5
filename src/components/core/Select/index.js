import { useContext } from 'react'
import { ThemeContext } from 'styled-components'
import { Wrapper, SelectOutline } from './Styled'

function Select({ options, name, label, ...rest }) {
  const themeContext = useContext(ThemeContext)

  return (
    <Wrapper>
      {label && (
        <label
          className="label block text-body-4 font-semibold mb-1"
          style={{ color: themeContext.sharedComponent.input.label.color }}
          htmlFor={name}
        >
          {label}
        </label>
      )}
      <SelectOutline className="input-outline flex items-center box-content h-11 sm:h-12">
        <select name={name} className="w-full" {...rest}>
          {options.map(o => (
            <option value={o.value}>{o.name}</option>
          ))}
        </select>
      </SelectOutline>
    </Wrapper>
  )
}

export default Select
