import { useState, useMemo } from 'react'
import styled from 'styled-components'
import { AnimatePresence, motion } from 'framer-motion'
import { useIntl } from 'react-intl'

const DropDownList = styled(motion.div)`
  overscroll-behavior: contain;
  display: ${({ display }) => (display ? 'block' : 'none')};
  background: ${({ theme }) => (theme.darkMode ? '#333333' : '#FFF')};
  div {
    border-top: ${({ theme }) =>
      theme.darkMode ? '1px solid #454545' : '1px solid #ececec'};
    margin: 0;
    &:last-child {
      margin-bottom: 0;
    }
  }
`

const Triangle = styled.div`
  &.up {
    border-bottom: 4px solid rgba(149, 163, 176, 1);
  }
  &.down {
    border-top: 4px solid rgba(149, 163, 176, 1);
  }
`

/**
 * Restructured dropDownList data to this
 *
 * ```
 *  dropDownList = [{
 *    id: '',
 *    name: ''
 *  }]
 * ```
 */

function Selector({ onChange, dropDownList = [] }) {
  const intl = useIntl()
  const optionAll = {
    id: 0,
    name: intl.formatMessage({ id: 'all' }),
  }
  const [displayList, setdisplayList] = useState(false)
  const [currentOption, setCurrentOption] = useState(optionAll.name)

  const variants = {
    open: { opacity: 1 },
    closed: { opacity: 0 },
  }

  function onClick() {
    setdisplayList(!displayList)
  }

  function doSearch(options) {
    setdisplayList(!displayList)
    setCurrentOption(options.name)
    onChange({ id: options.id })
  }

  return (
    <AnimatePresence>
      <div
        className="flex items-center mb-0 text-title-7 text-platinum-200"
        onClick={onClick}
      >
        {currentOption}
        <Triangle
          className={`ml-1 border-r-4 border-l-4 border-transparent ${
            displayList ? 'up' : 'down'
          }`}
        />
      </div>

      <DropDownList
        display={displayList}
        className="w-full fixed top-11 sm:top-12 left-0 z-50 overflow-auto"
        animate={displayList ? 'open' : 'closed'}
        variants={variants}
        style={{ maxHeight: '50vh' }}
      >
        <div
          key="all"
          className={`text-platinum-200 ${
            currentOption === optionAll.name && 'text-blue-200 '
          }text-body-4 py-3`}
          onClick={() => doSearch(optionAll)}
        >
          {optionAll.name}
        </div>
        {dropDownList.map(options => (
          <div
            key={options.name}
            className={`text-platinum-200 ${
              currentOption === options.name && 'text-blue-200 '
            }text-body-4 py-3`}
            onClick={() => doSearch(options)}
          >
            {options.name}
          </div>
        ))}
      </DropDownList>

      {displayList && (
        <div
          className="w-full z-10 h-screen absolute top-11 sm:top-12 left-0 bg-black bg-opacity-60"
          onClick={onClick}
        />
      )}
    </AnimatePresence>
  )
}

export default Selector
