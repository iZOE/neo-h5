import React, { useState, useCallback } from 'react'
import DockingMultipleControl from 'components/core/DockingMultipleControl'

function DockingMultipleControlController({
  isRounded = false,
  data,
  textOnCancelButton,
  onDissmiss,
  componentToRender: ComponentToRender,
}) {
  const [isShow, setIsShow] = useState(true)
  const handleDissmiss = useCallback(() => {
    setIsShow(prev => false)
    onDissmiss()
  }, [])

  return (
    <DockingMultipleControl handleDissmiss={handleDissmiss} isShow={isShow} isRounded={isRounded}>
      <ComponentToRender
        data={data}
        textOnCancelButton={textOnCancelButton}
        handleCancel={handleDissmiss}
      />
    </DockingMultipleControl>
  )
}

export default DockingMultipleControlController
