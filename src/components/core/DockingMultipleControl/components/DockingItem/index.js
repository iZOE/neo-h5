import React from 'react'
import Link from 'next/link'

function Item({ name, href, ...rest }) {
  return (
    <div
      className="flex flex-row justify-center items-center py-3 px-2 border-b text-blue-200 border-gray-100 dark:border-gray-500"
      {...rest}
    >
      {href ? (
        <div className="no-underline text-body-4 text-blue-200 not-italic font-semibold">
          <Link href={href} target="_blank" rel="noopener">
            {name}
          </Link>
        </div>
      ) : (
        name
      )}
    </div>
  )
}

export default Item
