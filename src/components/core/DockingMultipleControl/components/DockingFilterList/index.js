import React, { useState } from 'react'
import Button from 'components/core/Button'
import IconTick from 'components/icons/Tick.svg'

function TypeFilter({ data, handleCancel }) {
  const router = data?.router
  const [active, setActive] = useState(data?.activate)
  const handleLink = e => {
    e.preventDefault()
    router.push(`/report/${data.reportType}?type=${active}`)
    handleCancel()
  }

  return (
    <>
      <ul className="flex flex-wrap overflow-y-scroll p-2 mb-2" style={{ maxHeight: '75vh' }}>
        {data?.typeOptions?.map(item => (
          <li
            className="w-1/2"
            key={item.name}
            onClick={e => {
              e.stopPropagation()
              setActive(item.type)
            }}
          >
            <div
              className={`bg-gray-50 dark:bg-purple-700 rounded-md m-1 text-title-8 flex justify-center items-center overflow-hidden ${
                Number(active) === item.type
                  ? 'border-blue-200 border-solid border text-blue-200'
                  : 'text-platinum-300 dark:text-platinum-200 '
              }`}
              style={{ height: '64px' }}
            >
              {item.name}
            </div>
          </li>
        ))}
      </ul>
      <div className="flex justify-around shadow-t-2">
        <div className="flex-1 py-2 pl-2 pr-1">
          <Button variant="secondary" onClick={handleCancel}>
            {data?.btnText.cancel}
          </Button>
        </div>
        <div className="flex-1 py-2 pl-1 pr-2">
          <Button variant="primary" onClick={handleLink}>
            {data?.btnText.confirm}
          </Button>
        </div>
      </div>
    </>
  )
}

export default TypeFilter
