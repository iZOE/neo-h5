import React, { useEffect } from 'react'
import { motion, AnimatePresence } from 'framer-motion'

const DockingMultipleControl = ({ isShow, isRounded = false, handleDissmiss, children }) => {
  useEffect(() => {
    if (isShow) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = 'auto'
    }
  }, [isShow])
  return (
    <section
      className="fixed flex w-screen h-screen bg-opacity-50 bg-black justify-center items-center top-0 left-0 overflow-y-auto"
      style={{ zIndex: '200' }}
      onClick={handleDissmiss}
    >
      <div className="fixed bottom-0 h-0">
        <AnimatePresence>
          {isShow && (
            <motion.div
              data-testid="modalContent-motion"
              animate={{ originX: 0, y: '-100%' }}
              transition={{ duration: 0.5 }}
              exit={{ originX: 0, y: '100%' }}
            >
              <div
                className={`relative w-screen bg-white dark:bg-purple-800 ${
                  isRounded ? 'rounded-t-md' : null
                }`}
                style={{ zIndex: '201' }}
              >
                <div>{children}</div>
              </div>
            </motion.div>
          )}
        </AnimatePresence>
      </div>
    </section>
  )
}

export default DockingMultipleControl
