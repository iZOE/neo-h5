import React from 'react'
import ReactDOM from 'react-dom'
import { theme } from 'components/shared/ThemeProvider'
import { ThemeProvider } from 'styled-components'
import DockingMultipleControlController from './components/DockingMultipleControlController'

export const showDockingMultipleControl = (
  options,
  textOnCancelButton,
  componentToRender,
  isRounded = false,
) => {
  const domNode = document.createElement('docking-multiple-contorl-template')
  document.body.appendChild(domNode)

  ReactDOM.render(
    <ThemeProvider theme={theme.config}>
      <DockingMultipleControlController
        isRounded={isRounded}
        componentToRender={componentToRender}
        data={options}
        textOnCancelButton={textOnCancelButton}
        onDissmiss={() => {
          setTimeout(() => {
            const unmountResult = ReactDOM.unmountComponentAtNode(domNode)
            if (unmountResult && domNode.parentNode) {
              domNode.parentNode.removeChild(domNode)
            }
          }, 520)
        }}
      />
    </ThemeProvider>,
    domNode,
  )
}
