import React, { useState } from 'react'
import { useRouter } from 'next/router'
import style from './switchtab.module.scss'

export default function SwitchTab(props) {
  const router = useRouter()
  const { data } = props
  const [isActive, setIsActive] = useState(true)

  const onClick = (active, path) => {
    setIsActive(active)
    router.push(path)
  }

  return (
    <div className="flex text-center" style={{ width: '192px' }}>
      {data.map((tab, index) => {
        const activeStyle = isActive && tab.path === router.pathname && `${style.tab_active_style}`
        return (
          <div
            className={`${style.tab_style} ${activeStyle} flex-1 h-7 border border-blue-100 text-body-6 leading-7 text-blue-100 overflow-hidden appearance-none`}
            type="button"
            tabsCount={data.length}
            key={index.toString()}
            isActive={isActive && tab.path === router.pathname}
            onClick={() => onClick(true, tab.path, index)}
          >
            {tab.title}
          </div>
        )
      })}
    </div>
  )
}
