import { useState, useCallback } from 'react'
import dynamic from 'next/dynamic'
import * as dayjs from 'dayjs'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import { DayPickerRangeController } from 'react-dates'
import { START_DATE, END_DATE } from 'react-dates/constants'

const IconLineUp = dynamic(() => import('components/icons/LineUp'))
const IconLineDown = dynamic(() => import('components/icons/LineDown'))

export default function DayPickerRangeControllerWrapper({ handleSelectDate }) {
  const [startDate, setStartDate] = useState(null)
  const [endDate, setEndDate] = useState(null)
  const [focus, setFocus] = useState(START_DATE)

  const isOutsideRange = useCallback(
    day => dayjs(day).isAfter(dayjs()) || dayjs(day).isBefore(dayjs().subtract(30, 'days')),
    [],
  )

  const onDatesChange = ({ startDate, endDate }) => {
    setStartDate(startDate)
    setEndDate(endDate)
    startDate && endDate && handleSelectDate(startDate, endDate)
  }
  const onFocusChange = focusedInput => {
    setFocus(focusedInput)
  }

  return (
    <DayPickerRangeController
      startDate={startDate}
      startDateId={START_DATE}
      endDate={endDate}
      endDateId={END_DATE}
      focusedInput={focus}
      onFocusChange={onFocusChange}
      onDatesChange={onDatesChange}
      navPrev={<IconLineUp />}
      navNext={<IconLineDown />}
      navPosition="navPositionBottom"
      dayPickerNavigationInlineStyles={{
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '40px',
        boxShadow: '0px -2px 4px rgba(0, 0, 0, 0.2)',
      }}
      orientation="vertical"
      verticalHeight={window.matchMedia('(max-height: 600px)').matches ? 460 : 540}
      numberOfMonths={2}
      hideKeyboardShortcutsPanel
      isOutsideRange={isOutsideRange}
    />
  )
}
