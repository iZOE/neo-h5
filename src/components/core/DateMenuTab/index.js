import { useState, useEffect, useCallback } from 'react'
import ReactDOM from 'react-dom'
import { FormattedMessage, useIntl } from 'react-intl'
import Menu from 'components/core/Menu'
import * as dayjs from 'dayjs'
import DayPickerRangeControllerWrapper from './DayPickerRangeControllerWrapper'

export default function DateMenuTab({ onChange }) {
  const intl = useIntl()
  const [selectedTab, setSelectedTab] = useState(0)
  const [isShowPicker, setIsShowPicker] = useState(false)

  const options = [
    { id: 0, name: intl.formatMessage({ id: 'date_menu.today' }) },
    { id: 1, name: intl.formatMessage({ id: 'date_menu.day' }, { num: '7' }) },
    { id: 2, name: intl.formatMessage({ id: 'date_menu.day' }, { num: '14' }) },
    { id: 3, name: intl.formatMessage({ id: 'date_menu.customization' }) },
  ]

  const handleSelectDate = (start, end) => {
    // get start date & end date data
    const startUnix = dayjs(start._d).startOf('date').unix()
    const endUnix = dayjs(end._d).hour(23).minute(59).second(59).unix()
    setIsShowPicker(false)
    start && end && handleTabChange(3, startUnix, endUnix) // 先關閉 Datepicker，再show dialog
  }
  const handleShowPicker = () => {
    // 先關閉dialog，再show Datepicker
    setTimeout(() => {
      const el = document.getElementsByTagName('alert-template')
      const unmountResult = ReactDOM.unmountComponentAtNode(el[0])
      if (unmountResult && el[0].parentNode) {
        el[0].parentNode.removeChild(el[0])
      }
    }, 100)

    setIsShowPicker(true)
  }

  const handleTabChange = async (id, start, end) => {
    const message = (await import('components/core/Alert/message')).default
    switch (id) {
      case 0:
        onChange({ start: dayjs().startOf('date').unix() })
        break
      case 1:
        onChange({ start: dayjs().subtract(6, 'day').startOf('date').unix() })
        break
      case 2:
        onChange({ start: dayjs().subtract(13, 'day').startOf('date').unix() })
        break
      case 3:
        const startDateString =
          (start && dayjs.unix(start).format('YYYY-MM-DD')) ||
          intl.formatMessage({ id: 'date_menu.start_date' })
        const endDateString =
          (end && dayjs.unix(end).format('YYYY-MM-DD')) ||
          intl.formatMessage({ id: 'date_menu.end_date' })
        const content = (
          <div
            className="flex mt-3 justify-center bg-gray-50 dark:bg-purple-700 rounded-full h-7"
            onClick={handleShowPicker}
          >
            <input
              type="text"
              name="start date"
              className="bg-transparent text-body-4 text-platinum-200 h-7 text-center"
              value={startDateString}
              readOnly
            />
            <span className="py-1">~</span>
            <input
              type="text"
              name="end date"
              className="bg-transparent text-body-4 text-platinum-200 h-7 text-center"
              value={endDateString}
              readOnly
            />
          </div>
        )
        message({
          title: intl.formatMessage({ id: 'date_menu.select_date' }),
          content,
          okText: intl.formatMessage({ id: 'confirm' }),
          cancelText: intl.formatMessage({ id: 'cancel' }),
          onCancel: () => {},
          onOk: () => {
            start && end ? onChange({ start, end }) : {}
          },
        })
        break
      default:
        onChange({ start: dayjs().startOf('date').unix() })
    }
    setSelectedTab(id)
  }
  return (
    <>
      <Menu
        data={options}
        selected={selectedTab}
        onTypeChange={id => handleTabChange(id)}
        isFitWidth
      />
      {isShowPicker ? (
        <div
          className="absolute h-screen w-full"
          style={{ background: 'rgba(0,0,0,0.3)', zIndex: '999' }}
        >
          <div
            className="fixed left-1/2"
            style={{ top: '8%', width: '95%', zIndex: '1000', transform: 'translateX(-50%)' }}
          >
            <DayPickerRangeControllerWrapper handleSelectDate={handleSelectDate} />
          </div>
        </div>
      ) : null}
    </>
  )
}
