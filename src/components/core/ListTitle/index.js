import styled from 'styled-components'

export default styled.div
  .withConfig({
    componentId: 'ListTitle',
  })
  .attrs({ className: 'py-2 px-3' })`
  background: ${props => (props.theme.darkMode ? '#2A2A38' : '#ffffff')};
  height: 32px;
  color: #95a3b0;
  font-size: 12px;
  margin-bottom: 1px;
`
