const NoData = ({ ...props }) => {
  return (
    <div
      {...props}
      className="w-full h-full text-white dark:text:gray-purple-100 bg-gray-100 dark:bg-purple-200">
      NO IMAGE AVAILABLE
    </div>
  )
}

export default NoData
