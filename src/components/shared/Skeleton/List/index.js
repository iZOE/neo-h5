const SkeletonList = ({ q = 6, ...restprops }) => (
  <div {...restprops}>
    {Array(q).fill(1).map((el, index) => (
      <div className="border-b last:border-none border-gray-100 dark:border-gray-500 py-3" key={index.toString()}>
        <div className="animate-pulse flex">
          <div className="flex-1 space-y-2 ">
            <div className="rounded bg-gray-200 dark:bg-purple-700 h-4 w-1/2" />
            <div className="rounded bg-gray-200 dark:bg-purple-700 h-4 w-5/6" />
          </div>
        </div>
      </div>
    ))}
  </div>
)

export default SkeletonList
