const SkeletonDetail = ({ q = 1, ...restprops }) => (
  <div {...restprops}>
    {Array(q).fill(1).map((el, index) => (
      <div className="mb-3" key={index.toString()}>
        <div className="animate-pulse flex flex-col">
          <div className="flex-1 mb-4">
            <div className="rounded bg-gray-200 dark:bg-purple-700 w-full" style={{ height: '80px' }} />
          </div>
          <div className="flex-1 space-y-2">
            <div className="rounded bg-gray-200 dark:bg-purple-700 h-4 w-5/6" />
            <div className="rounded bg-gray-200 dark:bg-purple-700 h-4 w-1/2" />
          </div>
        </div>
      </div>
    ))}
  </div>
)

export default SkeletonDetail
