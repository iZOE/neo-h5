import { useMemo, useEffect } from 'react'
import { motion } from 'framer-motion'
import { useRecoilValue } from 'recoil'
import { pageTransitionState } from 'atoms/pageTransitionState'

// direction: >, <, -
export default function PageTransition({ children }) {
  const { mode, direction } = useRecoilValue(pageTransitionState)

  let initX = 0
  if (direction === '>') {
    initX = -20
  } else if (direction === '<') {
    initX = 20
  }

  return (
    <motion.div
      style={{ height: '-webkit-fill-available' }}
      initial={{ opacity: 0, x: initX }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ opacity: 0 }}
    >
      {children}
    </motion.div>
  )
}
