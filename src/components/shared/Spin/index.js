import dynamic from 'next/dynamic'

const IconSpin = dynamic(() => import('components/icons/Spin'))

const Spin = () => {
  return (
    <div className="animate-spin">
      <IconSpin />
    </div>
  )
}

export default Spin