import { css } from 'styled-components'

export const fontFace = css`
  @font-face {
    font-family: PingFangSC, sans-serif;
    font-weight: 400;
    src: local('PingFang SC Regular');
    unicode-range: U+4E00-9FFF;
  }

  @font-face {
    font-family: PingFangSC, sans-serif;
    font-weight: 600;
    src: local('PingFang SC Medium');
    unicode-range: U+4E00-9FFF;
  }
`

export const fontRegular = css`
  font-family: PingFangSC, Roboto, sans-serif;
  font-weight: 400;
`

export const fontBold = css`
  font-family: PingFangSC, Roboto, sans-serif;
  font-weight: 600;
`
