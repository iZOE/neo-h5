import { createGlobalStyle } from 'styled-components'
import { fontFace, fontRegular } from './fonts'

export default createGlobalStyle`
  ${fontFace};
  html, body, #__next{
    height: fill-available;
  }
  body {
    ${fontRegular};
    background-color: ${({ theme }) => theme.background};
    margin: 0;
    -webkit-font-smotth: antialiased;
    -webkit-tap-highlight-color: transparent;
    overscroll-behavior: contain;
  }
  input, button{
    outline: none;
    &:-internal-autofill-selected {
      background-color: transparent;
      color: ${({ theme }) => (theme.darkMode ? '#fff' : '#000000')};
    }

    &:-webkit-autofill,
    &:-webkit-autofill:hover,
    &:-webkit-autofill:focus,
    &:-webkit-autofill:active {
      transition: background-color 5000s ease-in-out 0s;
      -webkit-text-fill-color: ${({ theme }) => (theme.darkMode ? '#fff' : '#000000')};
    }
  }
  *:focus {
    // tailwind outline focus有問題，只能先加入important..
    outline: none !important;
  }
`
