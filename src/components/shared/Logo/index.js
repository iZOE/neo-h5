import Picture from '../../core/Picture'

const Logo = () => {
  const mediaPath = `/shared/${process.env.NEXT_PUBLIC_AGENT_CODE}/logo`

  return (
    <Picture
      png={`${mediaPath}.png`}
      webp={`${mediaPath}.webp`}
      alt="logo"
    />
  )
}

export default Logo
