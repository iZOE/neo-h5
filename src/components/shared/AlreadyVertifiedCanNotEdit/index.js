import React from 'react'
import Link from 'next/link'
import { FormattedMessage } from 'react-intl'
import Button from 'components/core/Button'

function AlreadyVertifiedCanNotEdit({ alreadyVertifiedSubject, hiddenCodeContent }) {
  return (
    <>
      <div className="text-platinum-300 dark:text-platinum-200 text-body-2 mb-2 mt-4">
        <FormattedMessage id="already_vertified1" values={{ kind: alreadyVertifiedSubject }} />
      </div>
      <div className="text-blue-200 text-body-2 mb-4">
        {hiddenCodeContent ? hiddenCodeContent : ''}
      </div>
      <div className="text-platinum-300 dark:text-platinum-200 text-body-2 mb-6">
        <FormattedMessage id="already_vertified2" values={{ kind: alreadyVertifiedSubject }} />
      </div>
      <div className="flex">
        <div className="flex-1">
          <Link href="/service">
            <Button variant="secondary" type="button">
              <FormattedMessage id="cskh" />
            </Button>
          </Link>
        </div>
      </div>
    </>
  )
}

export default AlreadyVertifiedCanNotEdit
