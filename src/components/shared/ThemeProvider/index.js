import React, { useState, useEffect } from 'react'
import { ThemeProvider } from 'styled-components'
import { useRecoilValue } from 'recoil'
import { reportSizeState } from '../../../atoms/reportSizeState'
import { darkModeState } from '../../../atoms/darkModeState'
import GlobalStyle from '../GlobalStyles'

const themeColorSet = JSON.parse(process.env.NEXT_PUBLIC_THEME_COLOR_SET)
const lightThemeConfig = themeColorSet ? themeColorSet.light : {}
const darkThemeConfig = themeColorSet ? themeColorSet.dark : {}

export const theme = {}

export const common = {
  breakpoints: ['600px'],
}

const staticLightTheme = {
  ...common,
  darkMode: false,
}

const staticDarkTheme = {
  ...common,
  darkMode: true,
}

export default function ThemeProviderComponents({ children }) {
  const darkMode = useRecoilValue(darkModeState)
  const reportSizeBig = useRecoilValue(reportSizeState)
  const [themeStyleConfig, setThemeStyleConfig] = useState({
    ...staticLightTheme,
    ...lightThemeConfig,
  })

  useEffect(() => {
    if (reportSizeBig === true) {
      document.documentElement.classList.add('big')
    } else {
      document.documentElement.classList.remove('big')
    }
  }, [reportSizeBig])

  useEffect(() => {
    if (darkMode === true) {
      document.documentElement.classList.add('dark')
      setThemeStyleConfig({
        ...staticDarkTheme,
        ...darkThemeConfig,
      })
    } else {
      document.documentElement.classList.remove('dark')
      setThemeStyleConfig({
        ...staticLightTheme,
        ...lightThemeConfig,
      })
    }
  }, [darkMode])

  if (!themeStyleConfig) {
    return null
  }

  theme.config = themeStyleConfig

  return (
    <ThemeProvider theme={themeStyleConfig}>
      <GlobalStyle />
      {children}
    </ThemeProvider>
  )
}
