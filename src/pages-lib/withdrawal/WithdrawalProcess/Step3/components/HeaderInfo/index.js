import { FormattedMessage, useIntl } from 'react-intl'

export default function HeaderInfo({ type, cardInfo }) {
  const { cardNumber, bankName } = cardInfo
  const lastNumber = type === 'bankcard' ? cardNumber.slice(-4) : cardNumber.slice(-5)
  const breakString = soureStr => {
    const rex = /^[\u4E00-\u9FA5]+$/
    const breakPoint = rex.test(soureStr) ? 15 : 30
    return `${soureStr.slice(0, breakPoint)}\n${soureStr.slice(breakPoint)}`
  }
  const infoName = breakString(bankName)
  return (
    <div className="flex justify-between items-center border-solid border-b border-gray-100 dark:border-gray-500 pb-3">
      <div className="flex-1 font-bold text-platinum-300 dark:text-white">
        <FormattedMessage id={`step3.header.${type}_title`} />
      </div>
      <div className="flex-2 text-right text-platinum-200">
        <div className="text-body-6 whitespace-pre">{infoName}</div>
        <div className="text-body-8">
          <FormattedMessage id={`step3.header.${type}_number`} values={{ num: lastNumber }} />
        </div>
      </div>
    </div>
  )
}
