import { FormattedMessage, useIntl } from 'react-intl'
import Link from 'next/link'

export default function WarnNote({ type }) {
  return (
    <div className="text-platinum-200 text-body-4 pt-3">
      <div>
        <FormattedMessage id="step3.warn.title" />
      </div>
      <ol className="list-decimal pl-5">
        <li className="my-1">
          <span>
            <FormattedMessage id="step3.warn.item1" />
          </span>
        </li>
        <li className="my-1">
          <span>
            <FormattedMessage id="step3.warn.item2" />
          </span>
        </li>
        <li className="my-1">
          <span>
            <FormattedMessage id="step3.warn.item3" />
          </span>
        </li>
        <li className="my-1">
          <span>
            <FormattedMessage id={`step3.warn.item4_${type}`} />
          </span>
        </li>
        <li className="my-1">
          <span>
            <FormattedMessage
              id="step3.warn.item5"
              values={{
                br: <br />,
                cskh: (
                  <Link href="/service">
                    <span className="no-underline text-body-4 text-blue-200 not-italic font-semibold">
                      <FormattedMessage id="step3.cskh" />
                    </span>
                  </Link>
                ),
              }}
            />
          </span>
        </li>
      </ol>
    </div>
  )
}
