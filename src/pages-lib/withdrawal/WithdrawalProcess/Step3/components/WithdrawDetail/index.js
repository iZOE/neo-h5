import { useCallback } from 'react'
import dynamic from 'next/dynamic'
import { FormattedMessage, useIntl } from 'react-intl'

const Currency = dynamic(() => import('components/core/Currency'))
const IconHelp = dynamic(() => import('components/icons/Help.js'))
const IconLineRight = dynamic(() => import('components/icons/LineRight'))

export default function WithdrawDetail({
  type,
  requestAmount,
  turnoverInfo,
  cardInfo,
  hasTurnoverDetail,
  showTurnoverDetail,
}) {
  const intl = useIntl()
  const actuallyAmount = turnoverInfo?.actuallyAmount

  const handleShowModal = useCallback(async fee => {
    const message = (await import('components/core/Alert/message')).default
    let content = ''
    if (fee === 'administration_fee') {
      content = intl.formatMessage(
        { id: 'step3.note.administration_fee' },
        { rate: turnoverInfo?.administrationRate },
      )
    }
    if (fee === 'promotional_deduction') {
      content = intl.formatMessage({ id: 'step3.note.promotional_deduction' })
    }
    if (fee === 'handling_fee') {
      content = intl.formatMessage({ id: 'step3.note.handling_fee' })
    }
    message({
      content,
    })
  }, [])

  return (
    <div className="py-3 border-solid border-b border-gray-100 dark:border-gray-500 font-semibold">
      <div className="flex justify-between text-platinum-100 dark:text-platinum-200 h-7 items-center">
        <div>
          <FormattedMessage id="step3.application_amount" />
        </div>
        <div>
          <Currency value={requestAmount} intClassName="text-body-4" floatClassName="text-body-6" />
        </div>
      </div>
      <div className="flex justify-between text-platinum-100 dark:text-platinum-200 h-8 items-center">
        <div className="text-body-6 inline-flex">
          -<FormattedMessage id="step3.administration_fee" />
          <div onClick={() => handleShowModal('administration_fee')}>
            <IconHelp w="14" h="14" fillColor="#ccc" vw="14" vh="14" />
          </div>
        </div>
        <div>
          <Currency value={turnoverInfo?.administrationFee} />
        </div>
      </div>
      <div className="flex justify-between text-platinum-100 dark:text-platinum-200 h-8 items-center">
        <div className="text-body-6 inline-flex">
          -<FormattedMessage id="step3.promotional_deduction" />
          <div onClick={() => handleShowModal('promotional_deduction')}>
            <IconHelp w="14" h="14" fillColor="#ccc" vw="14" vh="14" />
          </div>
        </div>
        <div>
          <Currency value={turnoverInfo?.preferAmount} />
        </div>
      </div>
      <div className="flex justify-between text-platinum-100 dark:text-platinum-200 h-8 items-center">
        <div className="text-body-6 inline-flex">
          -<FormattedMessage id="step3.handling_fee" />
          <div onClick={() => handleShowModal('handling_fee')}>
            <IconHelp w="14" h="14" fillColor="#ccc" vw="14" vh="14" />
          </div>
        </div>
        <div>
          <Currency value={turnoverInfo?.withdrawalFee} />
        </div>
      </div>
      <div className="flex justify-between text-platinum-300 dark:text-white h-7 items-center">
        <div>
          =<FormattedMessage id="step3.real_amount" />
        </div>
        <div>
          <Currency
            value={actuallyAmount}
            intClassName="text-body-4"
            floatClassName="text-body-6"
          />
        </div>
      </div>
      {type === 'cryptoWallet' ? (
        <div className="text-right text-gray-400">
          ≈{cardInfo?.currencyName}{' '}
          <Currency
            value={actuallyAmount ? actuallyAmount / cardInfo?.exchangeRate : 0}
            intClassName="text-body-4"
            floatClassName="text-body-6"
          />
        </div>
      ) : null}
      {hasTurnoverDetail && (
        <div className="mt-1 text-right text-body-6 text-blue-200" onClick={showTurnoverDetail}>
          <FormattedMessage id="step3.check_detail" />
          <div className="inline-block align-top">
            <IconLineRight width="14" height="14" fillColor="text-blue-200" />
          </div>
        </div>
      )}
    </div>
  )
}
