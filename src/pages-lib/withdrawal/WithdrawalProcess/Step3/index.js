import { useState, useCallback } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import useStep, { useStepData } from 'hooks/useStep'
import fetcher from 'libs/fetcher'
import HeaderInfo from './components/HeaderInfo'
import WithdrawDetail from './components/WithdrawDetail'
import WarnNote from './components/WarnNote'
import TurnoverDetail from '../TurnoverDetail'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Button = dynamic(() => import('components/core/Button'))

function Step3({ security, withdrawInit }) {
  const intl = useIntl()
  const router = useRouter()
  const { gotoNext, gotoStep } = useStep()
  const stepData = useStepData()
  const [isLoading, setIsLoading] = useState(false)
  const [isShowDetail, setIsShowDetail] = useState(false)
  const { cardInfo, fundPassword, requestAmount, type, turnover } = stepData

  const { turnOverInfo: turnoverInfo, records } = turnover && turnover

  const handleShowTurnoverDetail = useCallback(status => setIsShowDetail(status), [])

  const handleWithdrawSubmit = useCallback(async () => {
    setIsLoading(true)
    const message = (await import('components/core/Alert/message')).default
    const fundTransferSMSLimitAmount = withdrawInit?.fundTransferSMSLimitAmount
    const actuallyAmount = turnoverInfo?.actuallyAmount
    const bankcardData = {
      fundPassword,
      requestAmount,
      memberBankId: cardInfo?.cardId,
      bankBranchName: cardInfo?.bankBranchName,
    }
    const cryptoWalletData = {
      fundPassword,
      requestAmount,
      cryptoCurrencyId: cardInfo?.cardId,
      currencyId: cardInfo?.currencyId,
    }
    const data = type === 'bankcard' ? bankcardData : cryptoWalletData
    if (fundTransferSMSLimitAmount > actuallyAmount) {
      try {
        const url = type === 'bankcard' ? 'v1/api/Withdrawal' : 'v1/api/withdrawal/CryptoCurrency'
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}${url}`,
          method: 'post',
          withToken: true,
          data,
        })
        if (res?.data?.code === '0000') {
          message({
            content: intl.formatMessage({ id: 'step3.withdraw_success' }),
            okText: intl.formatMessage({ id: 'ok' }),
            onOk: () => {
              router.push('/user')
            },
            closable: false,
          })
          setIsLoading(false)
        } else {
          message({
            content: res?.data?.responseMessage,
            okText: intl.formatMessage({ id: 'ok' }),
            onOk: () => {},
            closable: false,
          })
          setIsLoading(false)
        }
      } catch (error) {
        message({
          content: error?.data?.responseMessage,
          okText: intl.formatMessage({ id: 'ok' }),
          onOk: () => {},
          closable: false,
        })
        setIsLoading(false)
      }
    } else if (security?.contactVerified < 2) {
      gotoNext({ submitData: data })
    } else {
      const sms = {
        contactNumber: security?.phone,
        expiredTime: null,
        lockDuration: null,
      }
      gotoStep(5, { submitData: data, sms })
    }
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={intl.formatMessage({ id: 'step3.title' })}
      />
      <Container withNavigationBar noPadding>
        <div className="bg-white dark:bg-purple-800 p-3 mt-2">
          <HeaderInfo type={type} cardInfo={cardInfo} />
          <WithdrawDetail
            type={type}
            requestAmount={requestAmount}
            turnoverInfo={turnoverInfo}
            cardInfo={cardInfo}
            hasTurnoverDetail={!!records?.length}
            showTurnoverDetail={() => handleShowTurnoverDetail(true)}
          />
          <WarnNote type={type} />
        </div>
        
        <div className="px-3">
          <Button
            variant="primary"
            text={intl.formatMessage({ id: 'step3.confirm' })}
            isLoading={isLoading}
            onClick={handleWithdrawSubmit}
          />
        </div>
      </Container>
      {isShowDetail && turnover && (
        <TurnoverDetail onClose={() => handleShowTurnoverDetail(false)} data={turnover} />
      )}
    </>
  )
}

export default Step3
