import { useState, useMemo } from 'react'
import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'

const NavBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))
const MainArea = dynamic(() => import('./components/MainArea'))
const Tab = dynamic(() => import('./components/Tab'))

const TurnoverDetail = ({ onClose, data }) => {
  const { turnOverInfo: turnoverInfo, records } = data && data
  const [activedTab, setActivedTab] = useState(null)

  const { details } = useMemo(() => {
    if (records) {
      const res = Object.values(records.reduce((target, { date, ...restProps }) => {
        target[date] = target[date] || { date, detail: [] }
        target[date].detail.push(restProps)
        return target
      }, Object.create(null)))

      setActivedTab(res?.[0]?.date)

      return {
        details: res,
      }
    }

    return {
      details: null,
    }
  }, [records])

  return (
    <div className="bg-gray-50 dark:bg-purple-900 fixed inset-0 z-9999 overflow-y-scroll overflow-x-hidden">
      <NavBar
        fixed
        left={<IconBack />}
        onLeftClick={onClose}
        title={<FormattedMessage id="turnover_detail.title" />}
      />
      <Container withNavigationBar>
        {!details ? <Skeleton /> : (
          <div className="p-3 -mx-3">
            <MainArea
              duration={turnoverInfo?.lastSettlementTime}
              amount={turnoverInfo?.totalValidBetAmount}
            />
            {activedTab && (
              <Tab
                data={details}
                activedTab={activedTab}
                onSelect={(key) => setActivedTab(key)}
              />
            )}
          </div>
        )}
      </Container>
    </div>
  )
}

export default TurnoverDetail
