import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'
import * as dayjs from 'dayjs'

const DATE = {
  XH: 'YYYY-MM-DD HH:mm:ss',
  VT: 'DD-MM-YYYY HH:mm:ss',
}

const FORMATE = (process.env.NEXT_PUBLIC_AGENT_CODE === 'STA01') ? DATE.XH : DATE.VT

const Currency = dynamic(() => import('components/core/Currency'))

const MainArea = ({ duration, amount }) => (
  <div className="pt-1 mb-4 text-platinum-200 text-body-6">
    <div className="mb-1">
      <FormattedMessage id="turnover_detail.main_area.duration" />
      <span className="font-semibold">
        {dayjs.unix(duration).format(FORMATE)}
      </span>
    </div>
    <div className="mb-2">
      <FormattedMessage id="turnover_detail.main_area.note" />
    </div>
    <>
      <FormattedMessage id="turnover_detail.main_area.amount" />
      <span className="font-semibold">
        <Currency value={amount} intClassName="text-body-6" />
      </span>
    </>
  </div>
)

export default MainArea
