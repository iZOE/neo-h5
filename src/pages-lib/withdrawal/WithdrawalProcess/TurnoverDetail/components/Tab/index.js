import dynamic from 'next/dynamic'

const TabOption = dynamic(() => import('./components/TabOption'))
const TabContent = dynamic(() => import('./components/TabContent'))

const Tab = ({ data, onSelect, activedTab }) => (
  data?.map(({ date, detail }) => (
    <>
      <TabOption
        key={date}
        index={date}
        text={date}
        onSelect={onSelect}
      />
      {(activedTab === date) && (
        <TabContent
          data={detail}
        />
      )}
    </>
  ))
)

export default Tab
