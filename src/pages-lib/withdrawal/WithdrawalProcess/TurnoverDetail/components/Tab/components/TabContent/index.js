import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'
import { TURNOVER_STATUS } from 'constants/withdrawal'

const Currency = dynamic(() => import('components/core/Currency'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))
const ListItem = dynamic(() => import('./components/ListItem'))

const TabContent = ({ data: d }) => (
  <div className="-mx-3">
    {!d ? <Skeleton /> : d.map((item) => (
      <div
        key={item.id}
        className="px-3 bg-gray-50 dark:bg-purple-900 border-b-8 last:border-none border-gray-100 dark:border-gray-500">
        <ListItem
          label={<FormattedMessage id="turnover_detail.field.id" />}
          value={item.id}
        />
        <ListItem
          label={<FormattedMessage id="turnover_detail.field.type" />}
          value={item.auditTypeText}
        />
        <ListItem
          label={<FormattedMessage id="turnover_detail.field.item" />}
          value={item.financialText}
        />
        <ListItem
          label={<FormattedMessage id="turnover_detail.field.amount" />}
          value={<Currency value={item.amount} intClassName="text-body-6" />}
        />
        <ListItem
          label={<FormattedMessage id="turnover_detail.field.multiple" />}
          value={item.betMultiple}
        />
        <ListItem
          label={<FormattedMessage id="turnover_detail.field.turnover_amount" />}
          value={<Currency value={item.shortBagAmount} intClassName="text-body-6" />}
        />
        <ListItem
          label={<FormattedMessage id="turnover_detail.field.status" />}
          value={<span className={`${(item.auditStatus === TURNOVER_STATUS.FAIL) ? 'text-red' : 'text-current'}`}>{item.auditStatusText}</span>}
        />
      </div>
    ))}
  </div>
)

export default TabContent
