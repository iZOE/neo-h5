import dynamic from 'next/dynamic'

const IconArrowDown = dynamic(() => import('components/icons/ArrowDown'))

const TabOption = ({ onSelect, index, text }) => (
  <div
    onClick={() => onSelect(index)}
    className="flex bg-white dark:bg-purple-800 text-title-8 text-gray-600 dark:text-white py-3 px-4 -mx-3 border-b border-gray-100 dark:border-gray-500">
    <div className="flex-1">
      {text}
    </div>
    <div className="flex-none">
      <IconArrowDown w={20} h={20} vw={16} vh={16} />
    </div>
  </div>
)

export default TabOption
