const ListItem = ({ label, value }) => (
  <div className="flex border-b last:border-none border-gray-100 dark:border-gray-500 text-body-6 py-3">
    <div className="flex-1 text-platinum-200">
      {label}
    </div>
    <div className="flex-1 text-right text-platinum-300 dark:text-platinum-100">
      {value}
    </div>
  </div>
)

export default ListItem
