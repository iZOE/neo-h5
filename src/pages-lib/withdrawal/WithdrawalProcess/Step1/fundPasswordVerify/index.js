import { useEffect, useCallback, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import useStep, { useStepData } from 'hooks/useStep'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const IconDelete = dynamic(() => import('components/icons/Delete.svg'))

export default function FundPasswordVerify({ haveFundPassword }) {
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const { gotoNext } = useStep()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const { control, handleSubmit, errors, setValue, formState } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const { isDirty, isValid } = formState

  useEffect(() => {
    if (!haveFundPassword) {
      handleFundPassword()
    }
  }, [])

  const handleFundPassword = useCallback(async () => {
    const message = (await import('components/core/Alert/message')).default
    message({
      title: intl.formatMessage({
        id: 'step1.fundPassword_verify.dialog.fundPassword_empty.title',
      }),
      okText: intl.formatMessage({
        id: 'step1.fundPassword_verify.dialog.fundPassword_empty.okText',
      }),
      cancelText: intl.formatMessage({
        id: 'step1.fundPassword_verify.dialog.fundPassword_empty.cancelText',
      }),
      onCancel: () => {
        router.push('/user')
      },
      onOk: () => {
        router.push('/user/setting/info/fund-password')
      },
    })
  }, [])

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default
    setIsSubmitting(prev => true)
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/funds/verify`,
        method: 'post',
        withToken: true,
        data: {
          password: formData.fundpassword,
        },
      })

      setIsSubmitting(prev => false)

      if (res?.data.code === '0000') {
        gotoNext({
          fundPassword: formData.fundpassword,
        })
      }
    } catch (err) {
      let isLockFundPassword = false
      const availableCount = err?.data?.data?.availableCount

      if (availableCount > 0 && !isLockFundPassword) {
        message({
          title: intl.formatMessage(
            { id: 'step1.fundPassword_verify.dialog.fail.title' },
            { availableCount },
          ),
          okText: intl.formatMessage({ id: 'step1.fundPassword_verify.dialog.fail.okText' }),
        })
      } else {
        isLockFundPassword = true
        message({
          title: intl.formatMessage({ id: 'step1.fundPassword_verify.dialog.lock.title' }),
          okText: intl.formatMessage({ id: 'step1.fundPassword_verify.dialog.lock.okText' }),
          onOk: () => {
            router.push('/service')
          },
        })
      }
    }
    setIsSubmitting(false)
  }, [])

  const handleInputClear = () => {
    setValue('fundpassword', '', {
      shouldValidate: true,
      shouldDirty: true,
    })
  }

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={intl.formatMessage({ id: 'step1.fundPassword_verify.title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mt-4 mb-6">
            <Controller
              name="fundpassword"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: intl.formatMessage({
                    id: 'step1.fundPassword_verify.form.validate.message.required',
                  }),
                },
                maxLength: 6,
                minLength: 6,
              }}
              render={({ onChange, value }) => (
                <div className="relative">
                  <Input
                    placeholder={intl.formatMessage({
                      id: 'step1.fundPassword_verify.placeholder',
                    })}
                    onChange={onChange}
                    type="password"
                    value={value}
                  />
                  {value && (
                    <div
                      className="flex justify-center items-center absolute top-0 right-11 w-10 h-full"
                      onClick={handleInputClear}
                    >
                      <IconDelete />
                    </div>
                  )}
                </div>
              )}
              error={
                errors.fundpassword &&
                intl.formatMessage({
                  id: 'step1.fundPassword_verify.form.validate.message.error',
                })
              }
            />
          </div>
          <Button
            variant="primary"
            isLoading={isSubmitting}
            type="submit"
            disabled={isSubmitting || !isDirty || !isValid}
          >
            {intl.formatMessage({ id: 'step1.fundPassword_verify.submit' })}
          </Button>
          <div className="flex mt-4 text-body-6 text-platinum-200 dark:text-platinum-300">
            <div className="flex-1">
              <FormattedMessage id="step1.fundPassword_verify.hint" />
              <Link href="/service">
                <span className="no-underline text-blue-200 not-italic font-semibold">
                  <FormattedMessage id="step1.fundPassword_verify.cskh" />
                </span>
              </Link>
            </div>
          </div>
        </form>
      </Container>
    </>
  )
}
