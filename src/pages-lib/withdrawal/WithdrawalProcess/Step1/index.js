import { useState, useEffect, useCallback } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { FormattedMessage, useIntl } from 'react-intl'
import FundPasswordVerify from './fundPasswordVerify'

const Button = dynamic(() => import('components/core/Button'))

function Step1({ security, meBank, withdrawInit }) {
  const intl = useIntl()
  const router = useRouter()
  const [isPass, setIsPass] = useState(false)
  const availableServices = withdrawInit?.availableServices
  const haveFundPassword = security?.fundPassword
  const bankCards = meBank?.bankCards
  const cryptoCurrencies = meBank?.cryptoCurrencies

  useEffect(() => {
    if (availableServices.indexOf('clf') === -1) {
      handleShowMessage('1')
    } else if (!haveFundPassword) {
      handleShowMessage('2')
    } else if (bankCards.length <= 0 && cryptoCurrencies.length <= 0) {
      handleShowMessage('3')
    } else if (!isPass) {
      setIsPass(true)
    }
  }, [isPass])

  const handleShowMessage = useCallback(async type => {
    const message = (await import('components/core/Alert/message')).default
    switch (type) {
      case '1':
        message({
          title: intl.formatMessage({ id: 'step1.dialog.withdrawal_service.title' }),
          content: intl.formatMessage({ id: 'step1.dialog.withdrawal_service.content' }),
          okText: intl.formatMessage({ id: 'step1.dialog.withdrawal_service.okText' }),
          cancelText: intl.formatMessage({ id: 'step1.dialog.withdrawal_service.cancelText' }),
          onCancel: () => {
            router.push('/user')
          },
          onOk: () => {
            router.push('/service')
          },
          closable: false,
        })
        break
      case '2':
        message({
          title: intl.formatMessage({ id: 'step1.dialog.fundPassword_empty.title' }),
          okText: intl.formatMessage({ id: 'step1.dialog.fundPassword_empty.okText' }),
          cancelText: intl.formatMessage({ id: 'step1.dialog.fundPassword_empty.cancelText' }),
          onCancel: () => {
            router.push('/user')
          },
          onOk: () => {
            router.push('/user/setting/info/fund-password')
          },
          closable: false,
        })
        break
      case '3':
        message({
          title: intl.formatMessage({ id: 'step1.dialog.meBank_empty.title' }),
          content: intl.formatMessage({ id: 'step1.dialog.meBank_empty.content' }),
          okText: intl.formatMessage({ id: 'step1.dialog.meBank_empty.okText' }),
          cancelText: intl.formatMessage({ id: 'step1.dialog.meBank_empty.cancelText' }),
          onCancel: () => {
            router.push('/user')
          },
          onOk: () => {
            router.push('/withdraw-manage/banks')
          },
          closable: false,
        })
        break
      default:
    }
  })

  return <>{isPass ? <FundPasswordVerify haveFundPassword={haveFundPassword} /> : null}</>
}

export default Step1
