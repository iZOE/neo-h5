const TabOption = ({ onSelect, activedTab, index, text }) => {
  const style = (index === activedTab) ? 'text-blue-200 border-blue-200' : 'text-platinum-200'

  return (
    <div
      onClick={() => onSelect(index)}
      className={`flex-1 text-center text-body-6 py-3 border-b-2 ${style}`}>
      {text}
    </div>
  )
}

export default TabOption
