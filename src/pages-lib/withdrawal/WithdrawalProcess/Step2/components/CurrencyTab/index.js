import dynamic from 'next/dynamic'

const TabOption = dynamic(() => import('./components/TabOption'))

const CurrencyTab = ({ activedTab, onSelectTab, exchangeRates }) => (
  <div className="flex mb-4 -mx-3 -mt-4">
    {exchangeRates && exchangeRates.map(({ currencyId, currencyName }, key) => (
      <TabOption
        activedTab={activedTab}
        index={currencyId}
        text={currencyName}
        key={key.toString()}
        onSelect={onSelectTab} />
    ))}
  </div>
)

export default CurrencyTab
