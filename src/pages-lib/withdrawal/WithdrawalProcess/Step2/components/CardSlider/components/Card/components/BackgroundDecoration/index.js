import dynamic from 'next/dynamic'

const Picture = dynamic(() => import('components/core/Picture'))

const BackgroundDecoration = ({ isCreditCardTab }) => {
  const mediaPath = `/pages/withdrawal/bg/${isCreditCardTab ? 'credit_card' : 'crypto_currency'}`

  return (
    <Picture
      className="rounded-md"
      webp={`${mediaPath}.webp`}
      png={`${mediaPath}.png`}
    />
  )
}

export default BackgroundDecoration
