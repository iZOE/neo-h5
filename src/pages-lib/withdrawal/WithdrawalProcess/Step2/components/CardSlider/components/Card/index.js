import dynamic from 'next/dynamic'

const BackgroundDecoration = dynamic(() => import('./components/BackgroundDecoration'))

const BG_COLOR = [
  '270deg, #6CE8E8 0%, #24A1A8 100%',
  '270deg, #EF96CB 0%, #DD66B8 100%',
  '270deg, #CC8DD8 0%, #7762B2 100%',
  '270deg, #FFB36F 0%, #E78E81 100%',
]

const Card = ({ bankName, cardNumber, accountName, index, isCreditCardTab }) => (
  <div className="mx-1 px-1 pb-5">
    <div
      className="rounded-md relative shadow-b-3"
      style={{ background: `linear-gradient(${BG_COLOR[(index % BG_COLOR.length)]})` }}>

      <BackgroundDecoration isCreditCardTab={isCreditCardTab} />
      <div className="m-5 text-white absolute inset-0">
        <div className="text-title-7 font-semibold mb-3">
          {bankName}
        </div>

        <div className="text-body-6 h-8 mb-6">
          {cardNumber}
        </div>

        <div className="text-title-8 font-semibold">
          {accountName}
        </div>
      </div>
    </div>
  </div>
)

export default Card
