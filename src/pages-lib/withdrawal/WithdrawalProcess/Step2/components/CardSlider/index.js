import dynamic from 'next/dynamic'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

const Skeleton = dynamic(() => import('components/shared/Skeleton/Detail'))
const Card = dynamic(() => import('./components/Card'))

const CardSlider = ({ list, isCreditCardTab, onChange }) => {
  const SETTINGS = {
    adaptiveHeight: false,
    arrows: false,
    autoplay: false,
    centerMode: true,
    centerPadding: '16px',
    dots: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    pauseOnHover: false,
    afterChange: current => { onChange(current) },
  }

  return (
    !list ? <Skeleton /> : (<Slider
      className="-mx-3"
      {...SETTINGS}>
      {list.map((item, index) => (
        <Card
          key={`card_${index.toString()}`}
          index={index}
          bankName={item.bankName}
          cardNumber={item.cardNumber}
          accountName={item.accountName}
          isCreditCardTab={isCreditCardTab}
        />
      ))}
    </Slider>)
  )
}

export default CardSlider
