import { useIntl } from 'react-intl'
import dynamic from 'next/dynamic'

const Input = dynamic(() => import('components/core/Input'))
const Currency = dynamic(() => import('components/core/Currency'))

const CryptoCurrencyAmountInput = ({ currencyName, exchangeRate, onChange, value, error }) => {
  const intl = useIntl()

  return (
    <div className="relative">
      <Input
        height="h-auto"
        padding="px-4 pt-3 pb-9"
        text="text-title-6 font-semobold text-right"
        type="number"
        onChange={onChange}
        value={value}
        placeholder={intl.formatMessage({ id: 'step2.field.amount.placeholder' })}
        error={error}
      />
      <span className="absolute top-10 right-4
        font-semibold text-body-4 text-gray-400 dark:text-purple-100">
        &#x2248; {currencyName} <Currency value={value ? value / exchangeRate : 0} intClassName="text-body-4" />
      </span>
    </div>
  )
}

export default CryptoCurrencyAmountInput
