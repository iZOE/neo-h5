import { useCallback, useMemo, useState } from 'react'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import { useForm, Controller } from 'react-hook-form'
import useStep, { useStepData } from 'hooks/useStep'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { TURNOVER_TYPE, TURNOVER_SUCCESS_ALERT_TYPE, TURNOVER_STATUS } from 'constants/withdrawal'

const Input = dynamic(() => import('components/core/Input'))
const Button = dynamic(() => import('components/core/Button'))
const CryptoCurrencyAmountInput = dynamic(() => import('./components/CryptoCurrencyAmountInput'))
const TurnoverFailModal = dynamic(() => import('../TurnoverFailModal'))
const TurnoverDetail = dynamic(() => import('../../../TurnoverDetail'))

const DEFAULT_VALUE = null

const Form = ({ balance, max, min, isCreditCardTab, exchangeRateInfo, times, cardInfo }) => {
  const intl = useIntl()
  const router = useRouter()
  const { gotoNext } = useStep()
  const stepData = useStepData()

  const [isTurnoverDetailShown, setIsTurnoverDetailShown] = useState(false)
  const [turnoverDetail, setTurnoverDetail] = useState(null)
  const onToggleTurnoverDetail = useCallback(() => {
    setIsTurnoverDetailShown(prevState => !prevState)
  }, [])

  const [isTurnoverFailModalShown, setIsTurnoverFailModalShown] = useState(false)

  const [isSubmitting, setIsSubmitting] = useState(false)

  const { minAmount } = useMemo(
    () => ({
      minAmount: isCreditCardTab ? min : exchangeRateInfo?.minWithdrawAmount,
    }),
    [isCreditCardTab, min, exchangeRateInfo],
  )

  const validateAmount = value => {
    const v = parseInt(value, 10)
    if (v > balance) {
      return intl.formatMessage({
        id: 'step2.field.amount.validator.insufficient_balance',
      })
    }

    if (v > max || v < minAmount) {
      return intl.formatMessage({
        id: 'step2.field.amount.validator.out_of_range',
      })
    }

    return true
  }

  const handleShowDialog = useCallback(
    async type => {
      const message = (await import('components/core/Alert/message')).default

      message({
        closable: false,
        content: intl.formatMessage({ id: `step2.dialog.${type}_maximum` }),
        cancelText: intl.formatMessage({ id: 'step2.action.confirm' }),
        okText: intl.formatMessage({ id: 'step2.action.contact_cs' }),
        onCancel: () => {
          router.push('/withdrawal')
        },
        onOk: () => {
          router.push('/service')
        },
      })
    },
    [intl, router],
  )

  const handleTurnoverAction = (d, amount) => {
    const { turnOverInfo: turnoverInfo, records } = d && d
    const isTimesValid = times > 0
    const isAmountValid = turnoverInfo?.actuallyAmount > 0
    const isMaximumValid = !turnoverInfo?.isOverDailyMax

    if (turnoverInfo?.status === TURNOVER_STATUS.SUCCESS) {
      if (!isTimesValid && !isAmountValid) {
        return handleShowDialog(TURNOVER_SUCCESS_ALERT_TYPE.TIMES)
      }

      if (isAmountValid && !isMaximumValid) {
        return handleShowDialog(TURNOVER_SUCCESS_ALERT_TYPE.AMOUNT)
      }

      if (isMaximumValid) {
        const details = {
          turnoverDetail: [...records],
          duration: turnoverInfo?.lastSettlementTime,
          amount: turnoverInfo?.totalValidBetAmount,
        }

        gotoNext({
          ...stepData,
          requestAmount: amount,
          type: isCreditCardTab ? 'bankcard' : 'cryptoWallet',
          cardInfo: {
            ...cardInfo,
            currencyName: isCreditCardTab ? null : exchangeRateInfo?.currencyName,
            currencyId: isCreditCardTab ? null : exchangeRateInfo?.currencyId,
            exchangeRate: isCreditCardTab ? null : exchangeRateInfo?.exchangeRate,
          },
          turnoverDetail: details,
          turnover: d && d,
        })
      }
    } else if (turnoverInfo?.status === TURNOVER_STATUS.FAIL) {
      setIsTurnoverFailModalShown(true)
    }
  }

  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
    reset,
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const resetField = () => {
    reset({
      amount: DEFAULT_VALUE,
    })
  }

  const onSubmit = async formData => {
    setIsSubmitting(true)
    const type = isCreditCardTab ? 'credit card' : 'crypto currency'

    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/Turnover`,
        method: 'post',
        data: {
          ...formData,
          type: TURNOVER_TYPE.WITHDRAWAL,
        },
        withToken: true,
      })
      console.log(`[${type}] get turnover result success`, res?.data?.data)
      setTurnoverDetail(res?.data?.data)
      handleTurnoverAction(res?.data?.data, formData.amount)
    } catch (err) {
      console.error(`[${type}] get turnover resule fail`, err)
    } finally {
      resetField()
      setIsSubmitting(false)
    }
  }

  const handleTurnoverDetailModal = () => {
    setIsTurnoverFailModalShown(false)
    setIsTurnoverDetailShown(true)
  }

  return (
    <>
      <form className="mb-4" onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name="amount"
          control={control}
          defaultValue={DEFAULT_VALUE}
          rules={{
            validate: validateAmount,
            required: true,
          }}
          render={({ onChange, value }) =>
            isCreditCardTab ? (
              <Input
                placeholder={intl.formatMessage({ id: 'step2.field.amount.placeholder' })}
                type="number"
                onChange={onChange}
                value={value}
                style={{ textAlign: 'right' }}
                error={errors?.amount?.message}
              />
            ) : (
              <CryptoCurrencyAmountInput
                currencyName={exchangeRateInfo?.currencyName}
                exchangeRate={exchangeRateInfo?.exchangeRate}
                onChange={onChange}
                value={value}
                error={errors?.amount?.message}
              />
            )
          }
        />
        <div className="mt-4">
          <Button
            disabled={!isDirty || !isValid || isSubmitting}
            isLoading={isSubmitting}
            variant="primary"
            type="submit"
          >
            {intl.formatMessage({ id: 'step2.action.next' })}
          </Button>
        </div>
      </form>
      {isTurnoverFailModalShown && turnoverDetail && (
        <TurnoverFailModal
          isOpen={isTurnoverFailModalShown}
          data={turnoverDetail}
          handleTurnoverDetailModal={handleTurnoverDetailModal}
          onClose={() => setIsTurnoverFailModalShown(false)}
        />
      )}
      {isTurnoverDetailShown && turnoverDetail && (
        <TurnoverDetail onClose={onToggleTurnoverDetail} data={turnoverDetail} />
      )}
    </>
  )
}

export default Form
