import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'
import Dialog from 'components/core/Dialog'

const IconArrowRight = dynamic(() => import('components/icons/ArrowRight'))

const TurnoverFailModal = ({ isOpen, data, handleTurnoverDetailModal, onClose }) => {
  const router = useRouter()
  const { turnOverInfo: turnoverInfo, records } = data && data

  return (
    <Dialog
      cancelText={<FormattedMessage id="step2.action.contact_cs" />}
      isOpen={isOpen}
      okText={<FormattedMessage id="step2.action.play_more" />}
      onCancel={() => { router.push('/service') }}
      onClose={onClose}
      onOk={() => { router.push('/') }}>
      <div className="text-center font-semibold text-title-7 text-platinum-200 mb-6">
        <FormattedMessage id="step2.dialog.turnover_fail.title" />
      </div>
      <div className="px-4 mb-3">
        <div className="bg-gray-200 rounded h-1 relative mb-2">
          <div
            className="bg-blue-200 rounded h-1 absolute left-0 top-0"
            style={{ width: `${(turnoverInfo?.totalValidBetAmount / turnoverInfo?.totalDiscountValidBetAmount) * 100}%` }}
          />
        </div>

        <div className="text-center text-body-6 text-platinum-300 dark:text-platinum-200">
          <FormattedMessage
            id="step2.dialog.turnover_fail.content"
            values={{
              amount: (
                <span className="text-blue-200 text-body-1 font-semibold">
                  {Math.ceil(turnoverInfo?.shortBagAmount)}
                </span>
              ),
            }}
          />
        </div>
      </div>

      {!!records?.length && (
        <div
          onClick={handleTurnoverDetailModal}
          className="text-right text-body-6 text-blue-200">
          <FormattedMessage id="step2.dialog.turnover_fail.note" />
          <span className="inline-block align-middle">
            <IconArrowRight w={16} h={16} />
          </span>
        </div>
      )}
    </Dialog>
  )
}

export default TurnoverFailModal
