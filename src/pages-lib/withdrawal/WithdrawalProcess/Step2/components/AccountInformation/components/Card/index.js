import dynamic from 'next/dynamic'

const IconBalance = dynamic(() => import('../IconBalance'))
const IconTimes = dynamic(() => import('../IconTimes'))

const Card = ({ isBalance, value, text }) => (
  <div className="flex-1 py-4 px-3 bg-gray-50 dark:bg-purple-700 rounded-md overflow-hidden relative font-semibold">
    <h1 className="text-title-8 text-gray-500 dark:text-white mb-1">
      {value}
    </h1>
    <p className="text-body-6 text-platinum-200">
      {text}
    </p>
    <div className="absolute right-0 bottom-0">
      {isBalance ? <IconBalance /> : <IconTimes />}
    </div>
  </div>
)

export default Card
