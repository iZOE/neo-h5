import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'

const Card = dynamic(() => import('./components/Card'))

const AccountInformation = ({ balance, times }) => (
  <div className="flex gap-x-2 mb-4">
    <Card
      isBalance
      value={balance}
      text={<FormattedMessage id="step2.account_info.balance" />}
    />
    <Card
      value={times}
      text={<FormattedMessage id="step2.account_info.times" />}
    />
  </div>
)

export default AccountInformation
