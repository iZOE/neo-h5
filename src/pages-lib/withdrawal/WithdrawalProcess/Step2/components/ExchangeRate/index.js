import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'

const Currency = dynamic(() => import('components/core/Currency'))
const IconRefresh = dynamic(() => import('components/icons/Refresh'))

const ExchangeRate = ({ rate, onClick, isLoading }) => (
  <div className="flex p-3 bg-gray-50 dark:bg-purple-700 rounded">
    <div className="flex-1 text-title-8 text-gray-500 dark:text-white">
      <FormattedMessage id="step2.exchange_rate" />
      <span className="font-semibold">
        {isLoading ? '--' : <Currency value={rate} intClassName="text-title-8" />}
      </span>
    </div>

    <div className="flex-none" onClick={onClick}>
      <div className={`${isLoading && 'animate-spin'}`}>
        <IconRefresh fillColor="text-blue-200" w={20} h={20} vw={16} vh={16} />
      </div>
    </div>
  </div>
)

export default ExchangeRate
