import { FormattedMessage } from 'react-intl'

const ExtraInformation = ({ isCreditCardTab, min, max, fee }) => (
  <div className="text-body-4 text-platinum-200 dark:text-platinum-300">
    <h4><FormattedMessage id="step2.extra_info.title" /></h4>
    <ol className="list-decimal pl-5">
      <li><FormattedMessage id="step2.extra_info.content.1" values={{ min, max }} /></li>
      <li><FormattedMessage id="step2.extra_info.content.2" /></li>
      <li><FormattedMessage id="step2.extra_info.content.3" /></li>
      <li><FormattedMessage id="step2.extra_info.content.4" values={{ fee }} />{isCreditCardTab}</li>
      {!isCreditCardTab && <li><FormattedMessage id="step2.extra_info.content.5" /></li>}
    </ol>
  </div>
)

export default ExtraInformation
