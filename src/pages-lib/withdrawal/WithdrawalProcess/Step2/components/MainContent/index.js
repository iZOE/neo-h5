import { useEffect, useMemo, useState, useCallback } from 'react'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'

const Currency = dynamic(() => import('components/core/Currency'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/Detail'))
const CurrencyTab = dynamic(() => import('../CurrencyTab'))
const ExchangeRate = dynamic(() => import('../ExchangeRate'))
const CardSlider = dynamic(() => import('../CardSlider'))
const AccountInformation = dynamic(() => import('../AccountInformation'))
const Form = dynamic(() => import('../Form'))
const ExtraInformation = dynamic(() => import('../ExtraInformation'))

const MainContent = ({ accountInfo: ac, cardList, isCreditCardTab }) => {
  const [exchangeRateInfo, setExchangeRateInfo] = useState(null)
  const [activedCard, setActivedCard] = useState(0)
  const [activedCurrencyTab, setActivedCurrencyTab] = useState(exchangeRates?.[0]?.currencyId)

  const { list } = useMemo(() => {
    if (cardList) {
      if (!isCreditCardTab) {
        const cryptoCurrencyList = cardList.map(item => ({
          bankName: item.nickName,
          cardNumber: item.address,
          accountName: item.protocol,
          cardId: item.id,
        }))

        return { list: cryptoCurrencyList }
      }

      const creditCardList = cardList.filter(item => !!item.withdrawalEnabled)
      return { list: creditCardList }
    }
  }, [cardList, isCreditCardTab])

  const delayRefresh = () => {
    setIsValidToRefresh(prevState => false)
    setTimeout(() => {
      setIsValidToRefresh(prevState => true)
    }, [60000])
  }

  const getExchangeRate = useCallback(async () => {
    const res = await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/mebank/CryptoCurrencyExchangeRate`,
      method: 'get',
      withToken: true,
    })
    delayRefresh()
    setExchangeRateInfo(prevState => res?.data?.data)
  }, [])

  useEffect(() => {
    if (!isCreditCardTab && !exchangeRateInfo) {
      getExchangeRate()
    }
  })

  const { exchangeRates } = useMemo(() => {
    if (!isCreditCardTab && exchangeRateInfo && list) {
      const protocol = list[activedCard].accountName
      const target = exchangeRateInfo.find(({ protocolName }) => protocolName === protocol)
      setActivedCurrencyTab(target.exchangeRates?.[0]?.currencyId)

      return { exchangeRates: target.exchangeRates }
    }
    return { exchangeRates: null }
  }, [exchangeRateInfo, list, activedCard, isCreditCardTab])

  const [isValidToRefresh, setIsValidToRefresh] = useState(true)
  const [isLoading, setIsLoading] = useState(false)

  const handleUpdateExchangeRate = useCallback(async () => {
    if (isValidToRefresh) {
      setIsLoading(prevState => true)
      delayRefresh()

      getExchangeRate()
      setIsLoading(prevState => false)
    } else {
      setIsLoading(prevState => true)
      setTimeout(() => {
        setIsLoading(prevState => false)
      }, [3000])
    }
  }, [isValidToRefresh, getExchangeRate])

  return (
    <>
      {!list ? (
        <Skeleton />
      ) : (
        <CardSlider list={list} isCreditCardTab={isCreditCardTab} onChange={setActivedCard} />
      )}

      <div className="bg-white dark:bg-purple-800 -mx-3 px-3 py-4">
        {!isCreditCardTab && (
          <CurrencyTab
            activedTab={activedCurrencyTab}
            onSelectTab={k => setActivedCurrencyTab(k)}
            exchangeRates={exchangeRates}
          />
        )}
        {ac && (
          <>
            <AccountInformation
              balance={<Currency value={ac.accountBalance} intClassName="text-title-8" />}
              times={ac.withdrawalCount}
            />
            {!isCreditCardTab && (
              <ExchangeRate
                onClick={handleUpdateExchangeRate}
                rate={exchangeRates?.[0]?.exchangeRate}
                isLoading={isLoading}
              />
            )}
            <Form
              balance={Math.floor(ac.accountBalance)}
              max={ac.maxWithdrawalAmount}
              min={ac.minWithdrawAmount}
              isCreditCardTab={isCreditCardTab}
              exchangeRateInfo={!isCreditCardTab && exchangeRates?.[0]}
              times={ac.withdrawalCount}
              cardInfo={list[activedCard]}
            />
            <ExtraInformation
              isCreditCardTab={isCreditCardTab}
              fee={<Currency value={ac.fundTransferSMSLimitAmount} intClassName="text-body-4" />}
              max={<Currency value={ac.maxWithdrawalAmount} intClassName="text-body-4" />}
              min={
                <Currency
                  value={
                    isCreditCardTab ? ac.minWithdrawAmount : exchangeRates?.[0]?.minWithdrawAmount
                  }
                  intClassName="text-body-4"
                />
              }
            />
          </>
        )}
      </div>
    </>
  )
}

export default MainContent
