import TabOption from './components/TabOption'

const AccountTab = ({ activedTab, onSelectTab, options }) => (
  <div className="flex bg-white dark:bg-purple-700 rounded-full p-1 mt-6 mb-4">
    {options?.map(({ key, text }) => (
      <TabOption
        activedKey={activedTab}
        index={key}
        key={key}
        onSelect={onSelectTab}
        text={text}
      />
    ))}
  </div>
)

export default AccountTab
