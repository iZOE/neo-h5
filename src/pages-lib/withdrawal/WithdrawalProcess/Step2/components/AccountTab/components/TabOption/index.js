const TabOption = ({ onSelect, children, ...props }) => {
  const { index, text, activedKey } = { ...props }
  const style = (index === activedKey) ? 'bg-platinum-300 text-white' : 'bg-transparent text-platinum-200'

  return (
    <div
      onClick={() => onSelect(index)}
      className={`flex-1 text-center text-body-6 font-semibold p-2 rounded-full transition-all delay-150 duration-300 ease-linear ${style}`}>
      {text}
    </div>
  )
}

export default TabOption
