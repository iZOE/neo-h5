import { useEffect, useState, useMemo } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import { WITHDRAWAL_TYPE } from 'constants/withdrawal'

const IconBack = dynamic(() => import('components/icons/Back'))
const NavBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/Detail'))
const MainContent = dynamic(() => import('./components/MainContent'))
const AccountTab = dynamic(() => import('./components/AccountTab'))

const Step2 = ({ withdrawInit, meBank }) => {
  const router = useRouter()

  const TAB_OPTIONS = [
    {
      key: WITHDRAWAL_TYPE.CREDIT_CARD,
      text: <FormattedMessage id="step2.tab.header.credit_card" />,
    },
    {
      key: WITHDRAWAL_TYPE.CRYPTO_CURRENCY,
      text: <FormattedMessage id="step2.tab.header.crypto_currency" />,
    },
  ]

  const [activedTab, setActivedTab] = useState(WITHDRAWAL_TYPE.CREDIT_CARD)

  useEffect(() => {
    if (!meBank?.bankCards?.length) {
      setActivedTab(WITHDRAWAL_TYPE.CRYPTO_CURRENCY)
    }
  }, [meBank])

  const { cardList, tabOptions } = useMemo(() => {
    if (meBank) {
      const { bankCards, cryptoCurrencies } = meBank
      let data = []
      if (bankCards?.length && cryptoCurrencies?.length) {
        data = [...TAB_OPTIONS]
      } else if (bankCards?.length && !cryptoCurrencies?.length) {
        data = [TAB_OPTIONS?.[0]]
      } else if (!bankCards?.length && cryptoCurrencies?.length) {
        data = [TAB_OPTIONS?.[1]]
      }
      return {
        tabOptions: data,
        cardList:
          activedTab === WITHDRAWAL_TYPE.CREDIT_CARD ? meBank.bankCards : meBank.cryptoCurrencies,
      }
    }
  }, [activedTab, meBank])

  return (
    <>
      <NavBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={<FormattedMessage id="step2.title" />}
      />
      <Container withNavigationBar>
        {!(tabOptions && withdrawInit && cardList) ? (
          <Skeleton q={2} />
        ) : (
          <>
            <AccountTab
              activedTab={activedTab}
              onSelectTab={key => setActivedTab(key)}
              options={tabOptions}
            />
            <MainContent
              isCreditCardTab={activedTab === WITHDRAWAL_TYPE.CREDIT_CARD}
              accountInfo={withdrawInit}
              cardList={cardList}
            />
          </>
        )}
      </Container>
    </>
  )
}

export default Step2
