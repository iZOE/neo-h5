import { useEffect, useCallback, useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import { useStepView } from '../../hooks/useStep'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/Detail'))

const Step1Component = dynamic(() => import('./WithdrawalProcess/Step1')) // 前置檢查
const Step2Component = dynamic(() => import('./WithdrawalProcess/Step2')) // 申請頁
const Step3Component = dynamic(() => import('./WithdrawalProcess/Step3')) // 結果頁
const Step4Component = dynamic(() => import('./WithdrawalProcess/Step4')) // 手機綁定
const Step5Component = dynamic(() => import('./WithdrawalProcess/Step5')) // 簡訊驗證

const STEP_VIEW = {
  1: Step1Component,
  2: Step2Component,
  3: Step3Component,
  4: Step4Component,
  5: Step5Component,
}

const MIN_STEP = 1
const MAX_STEP = 5

function WithdrawalProcess() {
  const intl = useIntl()
  const router = useRouter()
  const [withdrawInitData, setWithdrawInitData] = useState(null)

  const fetchWithdrawalData = async () => {
    const meRes = await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
      withToken: true,
    })
    const securityRes = await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security`,
      withToken: true,
    })
    const meBankRes = await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/MeBank`,
      withToken: true,
    })
    return {
      me: meRes.data.data,
      security: securityRes.data.data,
      meBank: meBankRes.data.data,
    }
  }

  const { data, error, isValidating } = useSWR('withdrawalData', fetchWithdrawalData, {
    revalidateOnFocus: false,
  })

  useEffect(() => {
    async function fetchWithdrawalInit() {
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/withdrawal/initial`,
          withToken: true,
        })
        setWithdrawInitData(res?.data?.data)
      } catch (error) {
        handleInitialError(error)
      }
    }
    fetchWithdrawalInit()
  }, [])

  const userData = data?.me
  const securityData = data?.security
  const withdrawInit = withdrawInitData
  const meBank = data?.meBank

  const handleInitialError = useCallback(
    async error => {
      const message = (await import('components/core/Alert/message')).default
      if (error?.data?.code === '1076') {
        message({
          content: error?.data?.responseMessage,
          okText: intl.formatMessage({
            id: 'step1.fundPassword_verify.dialog.fundPassword_empty.okText',
          }),
          cancelText: intl.formatMessage({
            id: 'step1.fundPassword_verify.dialog.fundPassword_empty.cancelText',
          }),
          onCancel: () => {
            router.push('/user')
          },
          onOk: () => {
            router.push('/user/setting/info/fund-password')
          },
          closable: false,
        })
      } else {
        message({
          content: error?.data?.responseMessage,
          okText: intl.formatMessage({ id: 'step1.dialog.initial_error.back' }),
          onOk: () => {
            router.back()
          },
          closable: false,
        })
      }
    },
    [error],
  )

  const ViewComponent = useStepView(STEP_VIEW, MIN_STEP, MAX_STEP)

  if (isValidating || !userData || !securityData || !withdrawInit || !meBank) {
    return (
      <>
        <NavigationBar fixed left={<IconBack />} onLeftClick={() => router.back()} title="" />
        <Container withNavigationBar>
          <Skeleton className="p-3" />
        </Container>
      </>
    )
  }
  return (
    <ViewComponent
      userData={userData}
      security={securityData}
      withdrawInit={withdrawInit}
      meBank={meBank}
    />
  )
}

WithdrawalProcess.protect = true

export default WithdrawalProcess
