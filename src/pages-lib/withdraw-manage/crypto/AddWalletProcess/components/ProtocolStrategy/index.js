import dynamic from 'next/dynamic'
import style from './ProtocolStrategy.module.scss'

const IconERC20 = dynamic(() => import('components/icons/ProtocolERC20'))
const IconTRC20 = dynamic(() => import('components/icons/ProtocolTRC20'))
const IconOmni = dynamic(() => import('components/icons/ProtocolOmni'))
const IconProtocolChecked = dynamic(() => import('components/icons/ProtocolChecked'))

export const PROTOCOL_VALIDATE_MAP = {
  ERC20: addr => addr.startsWith('0x'),
  TRC20: addr => addr.startsWith('T'),
  Omni: addr => addr.startsWith('1') || addr.startsWith('3'),
}

const ERC20 = ({ register, nameToBind, keyProtocolId, protocolValidator }) => (
  <div className={style.protocolItemWrapper} key={keyProtocolId}>
    <input
      type="radio"
      id="ERC20"
      value={`ERC20,${keyProtocolId}`}
      name={nameToBind}
      ref={register({ validate: protocolValidator })}
    />
    <label htmlFor="ERC20" className="bg-white dark:bg-purple-800">
      <div>
        <IconProtocolChecked />
      </div>
      <IconERC20 />
      <span className="text-green mt-1">ERC20</span>
    </label>
  </div>
)

const TRC20 = ({ register, nameToBind, keyProtocolId, protocolValidator }) => (
  <div className={style.protocolItemWrapper} key={keyProtocolId}>
    <input
      type="radio"
      id="TRC20"
      value={`TRC20,${keyProtocolId}`}
      name={nameToBind}
      ref={register({ validate: protocolValidator })}
    />
    <label htmlFor="TRC20" className="bg-white dark:bg-purple-800">
      <div>
        <IconProtocolChecked />
      </div>
      <IconTRC20 />
      <span className="text-orange mt-1">TRC20</span>
    </label>
  </div>
)

const Omni = ({ register, nameToBind, keyProtocolId, protocolValidator }) => (
  <div className={style.protocolItemWrapper} key={keyProtocolId}>
    <input
      type="radio"
      id="Omni"
      value={`Omni,${keyProtocolId}`}
      name={nameToBind}
      ref={register({ validate: protocolValidator })}
    />
    <label htmlFor="Omni" className="bg-white dark:bg-purple-800">
      <div>
        <IconProtocolChecked />
      </div>
      <IconOmni />
      <span className="text-platinum-300 mt-1">OMNI</span>
    </label>
  </div>
)

const ProtocolsStrategy = {
  ERC20,
  TRC20,
  Omni,
}

export default ProtocolsStrategy
