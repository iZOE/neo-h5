import { useEffect, useCallback, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import useStep, { useStepData } from 'hooks/useStep'
import showError from 'libs/showError'
import { useHandleError } from 'hooks/useHandleError'
import ProtocolsStrategy, { PROTOCOL_VALIDATE_MAP } from '../components/ProtocolStrategy'
import style from './ProtocolWrapper.module.scss'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

const walletAddrErrorMsg = {
  required: <FormattedMessage id="validate.wallet_address.required" />,
  format: <FormattedMessage id="validate.wallet_address.format" />,
}

export default function CryptoWalletAddStep1({ meBankData }) {
  const intl = useIntl()
  const router = useRouter()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    protocol: protocolFromStepData,
    protocolId: protocolIdFromStepData,
    walletAddress: walletAddressFromStepData,
  } = useStepData() || {} // 有可能從step2按上一頁回來 但作為第一步按進來時又會是null
  const {
    control,
    watch,
    register,
    handleSubmit,
    errors,
    getValues,
    setValue,
    trigger,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues: {
      protocol: '', // 協定字串 如TRC20, Omni + protocolId  交由下方useEffect那裡去做出初始給值
      walletAddress: walletAddressFromStepData || '',
    },
  })
  const watchProtocol = watch('protocol', '')
  const { gotoNext } = useStep()
  const walletAddreassValidator = {
    required: value => !!value,
    format: value => {
      const nowChoosedProtocol = getValues('protocol')?.split(',')[0]
      const result = nowChoosedProtocol && PROTOCOL_VALIDATE_MAP[nowChoosedProtocol](value)
      trigger('protocol')
      return result
    },
  }

  const protocolValidator = {
    required: value => !!value,
    format: async value => {
      const walletAddress = getValues('walletAddress')
      const nowChoosedProtocol = value.split(',')[0]
      const result = PROTOCOL_VALIDATE_MAP[nowChoosedProtocol](walletAddress)
      return result
    },
  }

  const { data: protocolData, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/mebank/CryptoCurrencyProtocol`,
    async url => {
      const res2 = await fetcher({
        url,
        withToken: true,
      })
      const _protocol = res2?.data?.data
      return _protocol
    },
  )

  useEffect(() => {
    if (protocolFromStepData) {
      setValue('protocol', `${protocolFromStepData},${protocolIdFromStepData}`, {
        shouldValidate: true,
      })
    } else if (protocolData?.length >= 1) {
      setValue('protocol', `${protocolData[0]?.protocolName},${protocolData[0]?.protocolId}`, {
        shouldValidate: true,
      })
    }
  }, [protocolData, protocolFromStepData, protocolIdFromStepData])

  useEffect(() => {
    if (watchProtocol) {
      setValue('walletAddress', getValues('walletAddress'), {
        shouldValidate: true,
      })
      setValue('protocol', watchProtocol, {
        shouldValidate: true,
      })
    }
  }, [watchProtocol])

  useHandleError(error)

  const onSubmit = useCallback(async formData => {
    setIsSubmitting(prev => true)
    try {
      const { protocol, walletAddress } = formData

      const [protocolName, protocolId] = protocol.split(',')
      // 檢查虛擬錢包地址是否重複
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/mebank/IsCryptoCurrencyAddressDuplicate/${walletAddress}`,
        withToken: true,
      })

      if (res?.data?.code === '0000') {
        gotoNext({ protocol: protocolName, protocolId, walletAddress })
      } else {
        throw new Error(res?.data?.responseMessage)
      }
    } catch (err) {
      setIsSubmitting(prev => false)
      showError(err)
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    const walletAddreass = getValues('walletAddreass')

    if (!walletAddreass) {
      router.push('/withdraw-manage/crypto')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'fund_password.abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/withdraw-manage/crypto')
        },
      })
    }
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'bar_title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex flex-col mt-4">
            <div className="text-title-8 font-bold text-platinum-300 mb-1">
              <FormattedMessage id="wallet_protocol" />
            </div>
            {protocolData ? (
              <div className={`flex justify-between items-start ${style.protocolListWrapper}`}>
                {protocolData?.map(d => {
                  const ProtocolComp = ProtocolsStrategy[d.protocolName]
                  return (
                    ProtocolComp && (
                      <ProtocolComp
                        key={d?.protocolId}
                        keyProtocolId={d?.protocolId}
                        nameToBind="protocol"
                        register={register}
                        protocolValidator={protocolValidator}
                      />
                    )
                  )
                })}
              </div>
            ) : (
              <Skeleton q={1} />
            )}
          </div>
          <div className="mt-4" />
          <div className="flex flex-col">
            <div className="text-title-8 font-bold text-platinum-300 mb-1">
              <FormattedMessage id="wallet_address" />
            </div>
            <Controller
              name="walletAddress"
              control={control}
              rules={{ validate: walletAddreassValidator }}
              render={({ onChange, value }) => (
                <Input
                  placeholder={intl.formatMessage({
                    id: 'placeholder.please_enter_wallet_address',
                  })}
                  onChange={onChange}
                  width={1}
                  type="text"
                  value={value}
                />
              )}
            />
          </div>
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {walletAddrErrorMsg[errors?.walletAddress?.type]}
            </div>
          )}
          <div className="h-6" />
          <div className="flex">
            <div className="flex-1 mr-2">
              <Link href="/service">
                <Button variant="secondary" type="button">
                  <FormattedMessage id="CSKH" />
                </Button>
              </Link>
            </div>
            <div className="flex-1">
              <Button
                variant="primary"
                type="submit"
                disabled={!isDirty || !isValid || isSubmitting}
                isLoading={isSubmitting}
              >
                {intl.formatMessage({ id: 'next_step' })}
              </Button>
            </div>
          </div>
        </form>
        <div className="text-platinum-200 dark:text-platinum-300 text-body-4 mt-4">
          <FormattedMessage id="warm_warning" />
          <ol className="list-decimal ml-4">
            <li>
              <FormattedMessage id="warm_warning_item1" />
            </li>
            <li>
              <FormattedMessage id="warm_warning_item2" />
            </li>
          </ol>
        </div>
      </Container>
    </>
  )
}
