import React, { useState, useCallback } from 'react'
import WithdrawCard from 'components/core/WithdrawCard'
import { useIntl } from 'react-intl'
import fetcher from 'libs/fetcher'
import showError from 'libs/showError'
import dynamic from 'next/dynamic'

const AddWalletCard = dynamic(() => import('../AddWalletCard'))

function WalletList({ wallets }) {
  const [defaultWalletIdx, setDefaultWalletIdx] = useState(0)
  const intl = useIntl()

  // 切換預設錢包
  const onCardClick = useCallback(
    async idx => {
      if (idx === defaultWalletIdx) {
        return
      }
      const toBeDefaultWalletId = wallets[idx].id
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/mebank/CryptoCurrency?cryptoCurrencyId=${toBeDefaultWalletId}`,
          method: 'put',
          withToken: true,
        })
        res?.data?.code === '0000' && setDefaultWalletIdx(prev => idx)
      } catch (error) {
        showError(error, intl)
      }
    },
    [wallets, defaultWalletIdx],
  )

  return (
    <div className="flex flex-col">
      {wallets?.map((w, idx) => (
        <WithdrawCard
          key={w.id}
          data={w}
          checked={defaultWalletIdx === idx}
          onCardClick={() => onCardClick(idx)}
        />
      ))}
      {wallets?.length <= 3 && <AddWalletCard />}
    </div>
  )
}

WalletList.propTypes = {}

export default WalletList
