import style from 'components/core/WithdrawCard/withdrawcard.module.scss'
import { FormattedMessage } from 'react-intl'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import style2 from './AddWalletCard.module.scss'

const IconCrypto = dynamic(() => import('components/icons/Crypto'))
const IconNextStep = dynamic(() => import('components/icons/NextStep'))

function AddWalletCard(props) {
  return (
    <Link href="/withdraw-manage/crypto/add-wallet">
      <div
        className={`${style.cardSize} ${style2.addWallet} text-blue-200 bg-white dark:bg-purple-700 shadow-b-1`}
      >
        <div className="flex flex-1 justify-between pl-4 pr-4">
          <div className="flex text-body-4">
            <IconCrypto />
            <div className="ml-2">
              <FormattedMessage id="add_wallet" />
            </div>
          </div>
          <div>
            <IconNextStep />
          </div>
        </div>
      </div>
    </Link>
  )
}

export default AddWalletCard
