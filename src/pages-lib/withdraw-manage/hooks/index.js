import useSWR from 'swr'
import fetcher from 'libs/fetcher'

export const useMeBank = () => {
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/MeBank`,
    async url => {
      const res1 = await fetcher({
        url,
        withToken: true,
      })
      return res1?.data?.data
    },
    {
      revalidateOnFocus: false,
    },
  )

  return [data, error]
}
