import { WithdrawType } from '../constant'
import { createUSDTCryptoWallet, createNewWithdrawBankcardAccount } from '../api'

export const showCreateWithdrawAccountSuccess = async (title, content, okText, onOKClick) => {
  const message = (await import('components/core/Alert/message')).default
  message({
    title,
    content,
    okText,
    onOk: onOKClick,
    closable: false,
  })
}

export const bindBankcardOrCryptoWallet = async (type, stepData, password, validationCode) => {
  if (type === WithdrawType.CRYPTO) {
    const { protocol, walletNickname, walletAddress, protocolId } = stepData

    const [data, error] = await createUSDTCryptoWallet({
      address: walletAddress,
      protocolType: protocol,
      protocolId,
      nickName: walletNickname,
      fundPassword: password || '',
      validationCode: validationCode || '',
    })

    return [data, error]
  }
  if (type === WithdrawType.BANKS) {
    const { accountName, cardNumber, bankTypeId } = stepData

    const [data, error] = await createNewWithdrawBankcardAccount({
      cardNumber,
      bankTypeId,
      accountName,
      fundPassword: password || '',
      validationCode: validationCode || '',
    })
    return [data, error]
  }

  return [null, new Error(`wraong type:${type}`)]
}
