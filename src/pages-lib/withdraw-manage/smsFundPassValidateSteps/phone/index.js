import { useCallback, useState, useEffect, useMemo } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import showError from 'libs/showError'
import useStep, { useStepData } from 'hooks/useStep'

const Input = dynamic(() => import('components/core/Input'))
const Select = dynamic(() => import('components/core/Select'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const PHONE_NUMBER_FORM_FIELD_NAME = 'ContactNumber'

// length驗證越南 手機號 10位 / 中國 11位
const phoneValidator = {
  required: value => !!value,
  length: value => {
    const phoneNumberLength = process.env.NEXT_PUBLIC_AGENT_CODE === 'VT999' ? 10 : 11
    if (value.length !== phoneNumberLength) {
      return false
    }
    return true
  },
}

const contactNumberErrorMsg = {
  required: <FormattedMessage id="phone.validate.ContactNumber.required" />,
  length: <FormattedMessage id="phone.validate.ContactNumber.length" />,
}
const OPTIONS = JSON.parse(process.env.NEXT_PUBLIC_PHONE_COUNTRY_CODE_LIST)

const isShouldDisabled = (isSubmitting, watchContactNumber, isDirty, isValid) => {
  if (isValid) {
    return false
  }

  return isSubmitting || !watchContactNumber || !isDirty || !isValid
}

export default function PhoneSetting({ type }) {
  // 沒綁定過手機 才來這個step, 輸入完手機後進到下個step, 在那裡等簡訊傳來mappingCode
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    getValues,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const watchContactNumber = watch(PHONE_NUMBER_FORM_FIELD_NAME, '')

  const { gotoNext, gotoStep } = useStep()
  const { phone } = stepData

  useEffect(() => {
    setValue(PHONE_NUMBER_FORM_FIELD_NAME, phone, { shouldValidate: true })
  }, [phone])

  const onSubmit = useCallback(async formData => {
    const _contactNumber = formData[PHONE_NUMBER_FORM_FIELD_NAME]
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/contact?ContactNumber=${_contactNumber}`,
        method: 'put',
        withToken: true,
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        gotoNext({
          contactNumber: res.data.data.contactNumber,
          expiredTime: res.data.data.expiredTime,
          lockDuration: res.data.data.lockDuration,
        })
      }
    } catch (error) {
      setIsSubmitting(prev => false)
      showError(error)
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    gotoStep(2)
  }, [])

  const clearValue = useCallback(() => {
    setValue(PHONE_NUMBER_FORM_FIELD_NAME, '', { shouldValidate: true })
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'phone.step1.title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mt-4" />
          <Controller
            name="locale"
            control={control}
            render={({ onChange, value }) => (
              <Select
                label={intl.formatMessage({ id: 'phone.select_label' })}
                options={OPTIONS}
                name="country"
                onChange={onChange}
              />
            )}
          />
          <div className="mt-4" />
          <div className="mt-4">
            <Controller
              name={PHONE_NUMBER_FORM_FIELD_NAME}
              control={control}
              rules={{ validate: phoneValidator }}
              render={({ onChange, value }) => (
                <Input
                  label={intl.formatMessage({ id: 'phone.phone_label' })}
                  placeholder={intl.formatMessage({ id: 'phone.step1.placeholder' })}
                  onChange={onChange}
                  width={1}
                  type="number"
                  value={value}
                  withDelete
                  clearValue={clearValue}
                />
              )}
            />
          </div>
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {contactNumberErrorMsg[errors?.ContactNumber?.type]}
            </div>
          )}
          <div className="h-6" />
          <div className="flex">
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={isShouldDisabled(isSubmitting, watchContactNumber, isDirty, isValid)}
              >
                {intl.formatMessage({ id: 'phone.step1.submit' })}
              </Button>
            </div>
          </div>
        </form>
      </Container>
    </>
  )
}
