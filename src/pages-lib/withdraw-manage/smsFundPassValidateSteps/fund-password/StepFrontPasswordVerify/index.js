import { useCallback, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import showError from 'libs/showError'
import useStep, { useStepData } from 'hooks/useStep'
import {
  showCreateWithdrawAccountSuccess,
  bindBankcardOrCryptoWallet,
} from 'pages-lib/withdraw-manage/smsFundPassValidateSteps/libs'
import { ON_SUCCESS_BACK_ROUTE_PATH } from '../../constant'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const FUND_PASSWORD_FORM_FIELD_NAME = 'password'

const fundPasswordValidator = {
  required: value => !!value,
  length: value => {
    if (value.length !== 6) {
      return false
    }
    return true
  },
}

const fundPasswordErrorMsg = {
  required: <FormattedMessage id="fund_password.step1.validate.password.required" />,
  length: <FormattedMessage id="fund_password.step1.validate.password.length" />,
}

export default function FundPasswordSettingStep1({ type }) {
  // 之前綁定過資金密碼  在這裡驗證再輸入
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const { gotoStep } = useStep()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    getValues,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const watchFundPassword = watch(FUND_PASSWORD_FORM_FIELD_NAME, '')

  const onSubmit = useCallback(async formData => {
    const password = formData[FUND_PASSWORD_FORM_FIELD_NAME]
    setIsSubmitting(prev => true)
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/funds/verify`,
        method: 'post',
        withToken: true,
        data: { password },
      })
      setIsSubmitting(prev => false)
      if (res?.data?.code === '0000') {
        // 驗證完資金密碼  綁錢包
        const [data, error] = await bindBankcardOrCryptoWallet(type, stepData, password, null)

        if (error) {
          throw error
        }
        if (data?.code === '0000') {
          const title = intl.formatMessage({ id: 'api.add_account_success.title' })
          const content = intl.formatMessage({ id: 'api.add_account_success.content' })
          const okText = intl.formatMessage({ id: 'api.add_account_success.confirm' })

          const onOKClick = () => {
            router.push(ON_SUCCESS_BACK_ROUTE_PATH[type])
          }
          showCreateWithdrawAccountSuccess(title, content, okText, onOKClick)
        } else {
          throw new Error(`set ${type} account error`)
        }
      }
    } catch (error) {
      setIsSubmitting(prev => false)
      showError(error)
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    gotoStep(2)
  }, [])

  const clearValue = useCallback(() => {
    setValue(FUND_PASSWORD_FORM_FIELD_NAME, '')
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'fund_password.step1.title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mt-4">
            <Controller
              name={FUND_PASSWORD_FORM_FIELD_NAME}
              control={control}
              rules={{ validate: fundPasswordValidator }}
              render={({ onChange, value }) => (
                <Input
                  placeholder={intl.formatMessage({
                    id: 'fund_password.step1.password.placeholder',
                  })}
                  onChange={onChange}
                  width={1}
                  type="password"
                  value={value}
                  withDelete
                  clearValue={clearValue}
                />
              )}
            />
          </div>
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {fundPasswordErrorMsg[errors?.password?.type]}
            </div>
          )}
          <div className="h-6" />
          <div className="flex">
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={isSubmitting || !watchFundPassword || !isDirty || !isValid}
              >
                {intl.formatMessage({ id: 'fund_password.step1.submit' })}
              </Button>
            </div>
          </div>
          <div className="flex mt-4 text-body-6 text-platinum-200 dark:text-platinum-300">
            <div className="flex-1">
              <FormattedMessage
                id="fund_password.step1.explanation1"
                values={{
                  br: <br />,
                  cskh: (
                    <Link href="/service">
                      <span className="no-underline text-body-4 text-blue-200 not-italic font-semibold">
                        <FormattedMessage id="fund_password.cskh" />
                      </span>
                    </Link>
                  ),
                }}
              />
            </div>
          </div>
        </form>
      </Container>
    </>
  )
}
