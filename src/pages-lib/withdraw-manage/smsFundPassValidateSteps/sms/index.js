import { useCallback, useState, useMemo, useRef, useEffect } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import showError from 'libs/showError'
import useStep, { useStepData } from 'hooks/useStep'
import {
  showCreateWithdrawAccountSuccess,
  bindBankcardOrCryptoWallet,
} from 'pages-lib/withdraw-manage/smsFundPassValidateSteps/libs'
import ExpiredTime from './components/ExpiredTime'
import GetCodeButton from './components/GetCodeButton'
import hiddenThePhone from './libs'
import { ON_SUCCESS_BACK_ROUTE_PATH } from '../constant'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const MAPPING_CODE_FORM_FIELD_NAME = 'MappingCode'

const mappingCodeValidator = {
  required: value => !!value,
  length: value => {
    if (value.length !== 4) {
      return false
    }
    return true
  },
}

const mappingCodeErrorMsg = {
  required: <FormattedMessage id="phone.validate.MappingCode.required" />,
  length: <FormattedMessage id="phone.validate.MappingCode.length" />,
}

export default function PhoneSettingStep2({ type }) {
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const { gotoStep } = useStep()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isMappingCodeDisabled, setIsMappingCodeDisabled] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const resetHolder = useRef({})

  const { contactNumber, expiredTime, lockDuration, phone } = stepData

  const onGetCodeClick = async phoneNumber => {
    setIsSubmitting(prev => true)
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/contact?ContactNumber=${phoneNumber}`,
        method: 'put',
        withToken: true,
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        const { resetExpireTime, resetLockDuration } = resetHolder.current
        resetExpireTime && resetExpireTime(res.data?.data?.expiredTime)
        resetExpireTime && setIsMappingCodeDisabled(prev => false)
        resetLockDuration && resetLockDuration(res.data?.data?.lockDuration)
      }
    } catch (error) {
      setIsSubmitting(prev => false)
      showError(error)
    }
  }

  useEffect(() => {
    if (!contactNumber || !expiredTime || !lockDuration) {
      // 代表綁過手機，直接進來了這一步
      onGetCodeClick(phone)
    }
  }, [])

  const cachedHiddenPhone = useMemo(() => hiddenThePhone(contactNumber), [contactNumber])

  const watchMappingCode = watch(MAPPING_CODE_FORM_FIELD_NAME, '')

  const onSubmit = useCallback(async formData => {
    setIsSubmitting(prev => true)
    try {
      // 做綁錢包的動作
      const _mappingCode = formData?.MappingCode

      const [data, error] = await bindBankcardOrCryptoWallet(type, stepData, null, _mappingCode)
      console.log('sms', data, error)
      if (error) {
        throw error
      }
      if (data?.code === '0000') {
        const title = intl.formatMessage({ id: 'api.add_account_success.title' })
        const content = intl.formatMessage({ id: 'api.add_account_success.content' })
        const okText = intl.formatMessage({ id: 'api.add_account_success.confirm' })
        const onOKClick = () => {
          router.push(ON_SUCCESS_BACK_ROUTE_PATH[type])
        }
        console.log('things', title, content, okText)
        showCreateWithdrawAccountSuccess(title, content, okText, onOKClick)
      } else {
        throw new Error(`set ${type} account error`)
      }
    } catch (error) {
      setIsSubmitting(prev => false)
      showError(error)
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    gotoStep(2)
  }, [])

  const clearValue = useCallback(() => {
    setValue(MAPPING_CODE_FORM_FIELD_NAME, '')
  }, [])

  const whenMappingCodeExpired = useCallback(() => {
    setIsMappingCodeDisabled(prev => true)
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'phone.step2.title' })}
      />
      <Container withNavigationBar>
        <div className="mt-4 text-platinum-300 text-body-4">
          {intl.formatMessage({ id: 'phone.mapping_code_sent' })}
        </div>
        <div className="mt-2 mb-4 text-blue-200 text-body-4">{cachedHiddenPhone}</div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            name={MAPPING_CODE_FORM_FIELD_NAME}
            control={control}
            rules={{ validate: mappingCodeValidator }}
            render={({ onChange, value }) => (
              <Input
                placeholder={intl.formatMessage({ id: 'phone.step2.placeholder' })}
                onChange={onChange}
                width={1}
                disabled={isMappingCodeDisabled}
                type="number"
                value={value}
                withDelete
                clearValue={clearValue}
              />
            )}
          />
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {mappingCodeErrorMsg[errors?.MappingCode?.type]}
            </div>
          )}
          <div className="h-1" />
          <ExpiredTime
            sec={expiredTime}
            collector={resetHolder}
            whenMappingCodeExpired={whenMappingCodeExpired}
          />
          <div className="flex mt-6">
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={isSubmitting || !watchMappingCode || !isDirty || !isValid}
              >
                {intl.formatMessage({ id: 'phone.step2.submit' })}
              </Button>
            </div>
          </div>
        </form>
        <div className="flex justify-center mt-6">
          <GetCodeButton
            isSubmitting={isSubmitting}
            sec={lockDuration}
            onClick={() => onGetCodeClick(contactNumber || phone)}
            collector={resetHolder}
          />
        </div>
      </Container>
    </>
  )
}
