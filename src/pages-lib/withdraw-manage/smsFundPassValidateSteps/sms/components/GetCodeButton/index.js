import { useCallback, useEffect } from 'react'
import { useIntl } from 'react-intl'
import useCountdown from 'hooks/useCountdown'

export default function GetCodeButton({ isSubmitting, sec, onClick, collector }) {
  const intl = useIntl()
  const [lockDurationCountDown, resetLockDuration] = useCountdown(sec)

  const handleClick = useCallback(() => {
    onClick()
  }, [])

  useEffect(() => {
    collector.current.resetLockDuration = resetLockDuration
  }, [resetLockDuration])

  if (isSubmitting) {
    return (
      <div className="text-body-6 text-platinum-300">
        {intl.formatMessage({ id: 'phone.get_code_button' })}
      </div>
    )
  }

  return (
    <>
      {lockDurationCountDown < 1 ? (
        <button
          className="re-send-code text-blue-200 text-body-6 bg-transparent border-none font-semibold"
          onPointerUp={handleClick}
          type="button"
        >
          {intl.formatMessage({ id: 'phone.get_code_button' })}
        </button>
      ) : (
        <div className="text-body-6 text-platinum-300">
          {intl.formatMessage({ id: 'phone.get_code_button' })}({lockDurationCountDown}s)
        </div>
      )}
    </>
  )
}
