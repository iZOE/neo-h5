import fetcher from 'libs/fetcher'

/**
 *
 * @param {*} body :
      address,
      protocolType,
      protocolId,
      nickName,
      fundPassword,
      validationCode,
 * @returns Array<Promise<Data>, Error>
 */

export const createUSDTCryptoWallet = async body => {
  try {
    const res = await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/mebank/CryptoCurrency`,
      withToken: true,
      method: 'post',
      data: body,
    })
    // 這API的結構好像跟其他的比起來  data好像少一層
    return [res?.data, null]
  } catch (err) {
    return [null, err]
  }
}

export const createNewWithdrawBankcardAccount = async body => {
  try {
    const res = await fetcher({
      url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/MeBank`,
      withToken: true,
      method: 'post',
      data: body,
    })
    // 這API的結構好像跟其他的比起來  data好像少一層
    return [res?.data, null]
  } catch (err) {
    return [null, err]
  }
}
