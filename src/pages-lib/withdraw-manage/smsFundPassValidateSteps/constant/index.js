export const WithdrawType = {
  BANKS: 'BANKS',
  CRYPTO: 'CRYPTO',
}

export const ON_SUCCESS_BACK_ROUTE_PATH = {
  BANKS: '/withdraw-manage/banks',
  CRYPTO: '/withdraw-manage/crypto',
}
