import dynamic from 'next/dynamic'
import { useStepView } from 'hooks/useStep'
import { useHandleError } from 'hooks/useHandleError'
import { useMeBank } from 'pages-lib/withdraw-manage/hooks'
import { WithdrawType } from '../../smsFundPassValidateSteps/constant'

const Step1 = dynamic(() => import('./steps/Step1'))
const Step2 = dynamic(() => import('./steps/Step2'))
const PhoneBindStep = dynamic(() =>
  import('pages-lib/withdraw-manage/smsFundPassValidateSteps/phone'),
)
const SMSValidateStep = dynamic(() =>
  import('pages-lib/withdraw-manage/smsFundPassValidateSteps/sms'),
)
const FundPassVerifyStep = dynamic(() =>
  import(
    'pages-lib/withdraw-manage/smsFundPassValidateSteps/fund-password/StepFrontPasswordVerify'
  ),
)
const FundPassSettingStep = dynamic(() =>
  import(
    'pages-lib/withdraw-manage/smsFundPassValidateSteps/fund-password/StepBackPasswordSetting'
  ),
)

const MIN_STEP = 1
const MAX_STEP = 6

const STEP_VIEW = {
  1: Step1,
  2: Step2,
  // 下面這四步 只有 3跟4有點順序關係，但是也有可以直接進入4的時候，但5跟6之間不存在先做完5再進6的情境，這裡跟user/setting綁定手機, 綁定資金密碼那裡
  // 業務邏輯其實不相同
  3: PhoneBindStep,
  4: SMSValidateStep,
  5: FundPassVerifyStep,
  6: FundPassSettingStep,
}

function AddWalletProcess() {
  const ViewComponent = useStepView(STEP_VIEW, MIN_STEP, MAX_STEP)
  const [data, error] = useMeBank()
  useHandleError(error)
  return <ViewComponent type={WithdrawType.BANKS} meBankData={data} />
}

export default AddWalletProcess
