import { useEffect, useCallback, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import useStep, { useStepData } from 'hooks/useStep'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const IconHelp = dynamic(() => import('components/icons/Help'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

const CARD_NUMBER_LENGTH_VALIDATE_MAP = {
  STA01: numSize => numSize === 16 || numSize === 18 || numSize === 19, // 中國 銀行卡 16, 18, 19碼
  VT999: numSize => numSize >= 2 && numSize <= 20, // 越南 銀行卡 2~20碼
}

const cardNumberErrorMsg = {
  required: <FormattedMessage id="validate.card_number.required" />,
  format: <FormattedMessage id="validate.card_number.format" />,
}

const cardNumberValidator = {
  required: value => !!value,
  length: value =>
    CARD_NUMBER_LENGTH_VALIDATE_MAP[process.env.NEXT_PUBLIC_AGENT_CODE](value.length),
}

const accountNameValidator = {
  required: value => !!value,
}

export default function BankcardAddStep1({ meBankData }) {
  const intl = useIntl()
  const router = useRouter()

  const { gotoNext } = useStep()
  const { accountName: accountNameFromStepData, cardNumber: cardNumberFromStepData } =
    useStepData() || {} // 有可能從step2按上一頁回來 但作為第一步按進來時又會是null

  const accountName = accountNameFromStepData || meBankData?.accountName

  const {
    control,
    handleSubmit,
    errors,
    getValues,
    setValue,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues: {
      accountName: accountName || '',
      cardNumber: cardNumberFromStepData || '',
    },
  })

  useEffect(() => {
    setValue('accountName', accountName, { shouldValidate: true })
  }, [accountName])

  const onSubmit = useCallback(async formData => {
    const { accountName, cardNumber } = formData
    gotoNext({ accountName, cardNumber })
  }, [])

  const handleClickNavBack = useCallback(() => {
    router.push('/withdraw-manage/banks')
  }, [])

  const onHelpInfoIconClick = async () => {
    const message = (await import('components/core/Alert/message')).default
    message({
      content: intl.formatMessage({ id: 'no_change_account_name' }),
      closable: true,
    })
  }

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'bar_title' })}
        right={
          <div onClick={onHelpInfoIconClick} className="mr-3">
            <IconHelp
              fillColor="text-platinum-300 dark:text-platinum-100"
              w={24}
              h={24}
              vw={16}
              vh={16}
            />
          </div>
        }
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex flex-col mt-4">
            <div className="text-title-8 font-bold text-platinum-300 mb-1">
              <FormattedMessage id="accountName" />
            </div>
            {meBankData ? (
              <Controller
                name="accountName"
                control={control}
                rules={{ validate: accountNameValidator }}
                render={({ onChange, value }) => (
                  <Input
                    placeholder={intl.formatMessage({
                      id: 'placeholder.accountName',
                    })}
                    readOnly={!!accountName}
                    onChange={onChange}
                    width={1}
                    type="text"
                    value={value}
                  />
                )}
              />
            ) : (
              <Skeleton q={1} />
            )}
          </div>
          <div className="mt-8 flex flex-col">
            <div className="text-title-8 font-bold text-platinum-300 mb-1">
              <FormattedMessage id="card_number" />
            </div>
            <Controller
              name="cardNumber"
              control={control}
              rules={{ validate: cardNumberValidator }}
              render={({ onChange, value }) => (
                <Input
                  placeholder={intl.formatMessage({
                    id: 'placeholder.card_number',
                  })}
                  onChange={onChange}
                  width={1}
                  type="number"
                  value={value}
                />
              )}
            />
          </div>
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {cardNumberErrorMsg[errors?.cardNumber?.type]}
            </div>
          )}
          <div className="h-6" />
          <div className="flex">
            <div className="flex-1  mr-2">
              <Link href="/service">
                <Button variant="secondary" type="button">
                  <FormattedMessage id="CSKH" />
                </Button>
              </Link>
            </div>
            <div className="flex-1">
              <Button variant="primary" type="submit" disabled={!isDirty || !isValid}>
                {intl.formatMessage({ id: 'next_step' })}
              </Button>
            </div>
          </div>
        </form>
        <div className="text-platinum-200 dark:text-platinum-300 text-body-4 mt-4">
          <FormattedMessage id="warm_warning" />
          <ol className="list-decimal ml-4">
            <li>
              <FormattedMessage id="warm_warning_item1" />
            </li>
            <li>
              <FormattedMessage id="warm_warning_item2" />
            </li>
            <li>
              <FormattedMessage id="warm_warning_item3" />
            </li>
          </ol>
        </div>
      </Container>
    </>
  )
}
