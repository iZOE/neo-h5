import { useCallback, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import useStep, { useStepData } from 'hooks/useStep'
import Link from 'next/link'
import showError from 'libs/showError'
import useSWR from 'swr'
import { useHandleError } from 'hooks/useHandleError'

const Select = dynamic(() => import('components/core/Select'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const walletNicknameErrorMsg = {
  required: <FormattedMessage id="validate.wallet_nickname.required" />,
  length: <FormattedMessage id="validate.wallet_nickname.length" />,
  format: <FormattedMessage id="validate.wallet_nickname.format" />,
}

const bankTypeIdValidator = {
  required: value => !!value,
}

export default function BankcardAddStep2({ meBankData }) {
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    setValue,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues: {
      bankTypeId: '',
    },
  })

  const { data: bankOptions, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/mebank/referencce/bank-type`,
    async url => {
      const res1 = await fetcher({
        url,
        withToken: true,
      })
      const data = res1?.data?.data
      // 把各家銀行卡號碼格式變成一個數字
      // const mergePrefixId = Object.keys(data).map(d => ({
      //   ...data[d],
      //   prefixId: d,
      // }))
      // const groupData = _groupBy(mergePrefixId, 'id')
      // const bankData = {}
      // Object.keys(groupData).forEach(d => {
      //   if (!bankData[d]) {
      //     bankData[d] = {
      //       name: groupData[d][0].name,
      //       bankid: groupData[d][0].id,
      //       prefixIds: [],
      //     }
      //   }
      //   bankData[d].prefixIds = groupData[d].map(x => x.prefixId)
      // })
      const bankOptions = data.map(b => ({ value: `${b.id}`, name: b.name }))
      bankOptions.unshift({
        value: '',
        name: intl.formatMessage({ id: 'open_account_bank_please_select' }),
      })
      return bankOptions
    },
    {
      revalidateOnFocus: false,
    },
  )
  useHandleError(error)

  const { gotoStep, gotoPrevious } = useStep()

  const onSubmit = useCallback(async formData => {
    setIsSubmitting(prev => true)
    const { bankTypeId } = formData
    try {
      const res1 = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security`,
        withToken: true,
      })
      const securityData = res1?.data?.data
      const { contactVerified, fundPassword: isFundPassSetted, phone } = securityData
      const isPhoneVerified = contactVerified === 2

      const { bankCards, cryptoCurrencies } = meBankData
      const bCount = bankCards.length
      const wCount = cryptoCurrencies.length
      const { accountName, cardNumber } = stepData
      const body = {
        accountName,
        cardNumber,
        bankTypeId,
        isPhoneVerified,
        phone,
      }

      if (bCount + wCount === 0 && !isFundPassSetted) {
        // 走資金密碼驗證流程 沒設定過資金密碼，所以從資金密碼設定開始
        const message = (await import('components/core/Alert/message')).default
        message({
          content: intl.formatMessage({ id: 'alert_dialog.not_set_fund_password_yet' }),
          okText: intl.formatMessage({ id: 'alert_dialog.go_set_it' }),
          cancelText: intl.formatMessage({ id: 'cancel' }),
          closable: false,
          onCancel: () => {
            setIsSubmitting(prev => false)
          },
          onOk: () => {
            gotoStep(6, body)
          },
        })
      } else if (bCount + wCount === 0 && isFundPassSetted) {
        // 走資金密碼驗證流程 有設定過資金密碼，直接到資金密碼輸入開始
        gotoStep(5, body)
      } else if (bCount + wCount > 0 && !isPhoneVerified) {
        // 大於一張卡片後就走手機流程
        // 沒綁定過手機（無法收簡訊驗證碼），所以先從綁定手機開始
        const message = (await import('components/core/Alert/message')).default
        message({
          title: intl.formatMessage({ id: 'alert_dialog.title' }),
          content: intl.formatMessage({ id: 'alert_dialog.plz_do_phone_validate' }),
          okText: intl.formatMessage({ id: 'alert_dialog.send_code' }),
          cancelText: intl.formatMessage({ id: 'cancel' }),
          closable: false,
          onCancel: () => {
            setIsSubmitting(prev => false)
          },
          onOk: () => {
            gotoStep(3, body)
          },
        })
      } else if (bCount + wCount > 0 && phone && isPhoneVerified) {
        // 綁定過手機，直接用手機號碼發簡訊，所以進 簡訊驗證流程(手機綁定的Step2)
        const message = (await import('components/core/Alert/message')).default
        message({
          title: intl.formatMessage({ id: 'alert_dialog.title' }),
          content: intl.formatMessage({ id: 'alert_dialog.will_send_sms_code' }),
          okText: intl.formatMessage({ id: 'alert_dialog.send_code' }),
          cancelText: intl.formatMessage({ id: 'cancel' }),
          closable: false,
          onCancel: () => {
            setIsSubmitting(prev => false)
          },
          onOk: () => {
            gotoStep(4, body)
          },
        })
      } else {
        throw new Error('something very wrong happened, contact the RD team!!!')
      }
    } catch (err) {
      setIsSubmitting(prev => false)
      showError(err)
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    gotoPrevious()
  }, [])

  const { accountName, cardNumber } = stepData

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'bar_title2' })}
      />
      <Container withNavigationBar>
        <div className="flex flex-col mt-4">
          <div className="text-title-8 font-bold text-platinum-300  mb-1">
            <FormattedMessage id="accountName" />
          </div>
          <div className="mt-1 text-blue-200 text-body-4">{accountName}</div>
        </div>

        <div className="flex flex-col mt-4">
          <div className="text-title-8 font-bold text-platinum-300  mb-1">
            <FormattedMessage id="card_number" />
          </div>
          <div className="mt-1 text-blue-200 text-body-4">{cardNumber}</div>
        </div>

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex flex-col mt-4">
            <div className="text-title-8 font-bold text-platinum-300  mb-1">
              <FormattedMessage id="open_account_bank" />
            </div>
            <div className="mt-1">
              <Controller
                name="bankTypeId"
                control={control}
                rules={{ validate: bankTypeIdValidator }}
                render={({ onChange, value }) => (
                  <Select options={bankOptions || []} name="bankTypeId" onChange={onChange} />
                )}
              />
            </div>
          </div>
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {walletNicknameErrorMsg[errors?.walletNickname?.type]}
            </div>
          )}
          <div className="h-1" />
          <div className="flex mt-6">
            <div className="flex-1  mr-2">
              <Link href="/service">
                <Button variant="secondary" type="button">
                  <FormattedMessage id="CSKH" />
                </Button>
              </Link>
            </div>
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={isSubmitting || !isDirty || !isValid}
              >
                {intl.formatMessage({ id: 'submit' })}
              </Button>
            </div>
          </div>
        </form>
        <div className="text-platinum-200 dark:text-platinum-300 text-body-4 mt-4">
          <FormattedMessage id="warm_warning" />
          <ol className="list-decimal ml-4">
            <li>
              <FormattedMessage id="warm_warning_item1" />
            </li>
            <li>
              <FormattedMessage id="warm_warning_item2" />
            </li>
            <li>
              <FormattedMessage id="warm_warning_item3" />
            </li>
          </ol>
        </div>
      </Container>
    </>
  )
}
