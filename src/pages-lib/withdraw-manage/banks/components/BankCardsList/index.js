import React, { useState, useCallback } from 'react'
import WithdrawCard, { CARD_TYPE } from 'components/core/WithdrawCard'
import { useIntl } from 'react-intl'
import fetcher from 'libs/fetcher'
import showError from 'libs/showError'
import dynamic from 'next/dynamic'

const AddBankAccountCard = dynamic(() => import('../AddBankAccountCard'))

function BankCardList({ bankCards }) {
  const [defaultBankCardIdx, setDefaultBankCardIdx] = useState(0)
  const intl = useIntl()

  // 切換預設銀行卡
  const onCardClick = useCallback(
    async idx => {
      if (idx === defaultBankCardIdx) {
        return
      }
      const toBeDefaultBankCardId = bankCards[idx].cardId
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/MeBank?memberBankId=${toBeDefaultBankCardId}`,
          method: 'put',
          withToken: true,
        })
        res?.data?.code === '0000' && setDefaultBankCardIdx(prev => idx)
      } catch (error) {
        showError(error, intl)
      }
    },
    [bankCards, defaultBankCardIdx],
  )

  return (
    <div className="flex flex-col">
      {bankCards?.map((w, idx) => (
        <WithdrawCard
          type={CARD_TYPE.BANK}
          key={w.cardId}
          data={w}
          checked={defaultBankCardIdx === idx}
          onCardClick={() => onCardClick(idx)}
        />
      ))}
      {bankCards?.length <= 3 && <AddBankAccountCard />}
    </div>
  )
}

BankCardList.propTypes = {}

export default BankCardList
