import style from 'components/core/WithdrawCard/withdrawcard.module.scss'
import { FormattedMessage } from 'react-intl'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import style2 from './AddBankAccountCard.module.scss'

const IconBankCard = dynamic(() => import('components/icons/BankCardAdd'))
const IconNextStep = dynamic(() => import('components/icons/NextStep'))

function AddBankAccountCard(props) {
  return (
    <Link href="/withdraw-manage/banks/add-bankcard">
      <div
        className={`${style.cardSize} ${style2.addBankAccountCard} text-blue-200 bg-white dark:bg-purple-700 shadow-b-1`}
      >
        <div className="flex items-center flex-1 justify-between pl-4 pr-4">
          <div className="flex text-body-4">
            <IconBankCard />
            <div className="ml-2 leading-8">
              <FormattedMessage id="add_bankcard" />
            </div>
          </div>
          <div>
            <IconNextStep />
          </div>
        </div>
      </div>
    </Link>
  )
}

export default AddBankAccountCard
