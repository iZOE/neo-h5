import Dialog from 'components/core/Dialog'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Slider from 'react-slick'
import dynamic from 'next/dynamic'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import * as dayjs from 'dayjs'

const IconAnnounce = dynamic(() => import('./icons/Announce'))

const iconAbsolutePosition = {
  top: '-56px',
  left: 'calc(50% - 88px)',
}

const SliderWrapper = styled.div`
  .slick-dots {
    position: inherit;
    bottom: inherit;
  }
  .slick-dots li {
    width: 10px;
    height: 4px;
    background-color: ${({ theme }) => theme.sharedComponent.switchtab.active.bg};
    border-radius: 3px;
    opacity: 0.2;
  }
  .slick-dots li.slick-active {
    width: 20px;
    opacity: 1;
  }
  .slick-dots li button::before {
    font-size: 0;
  }
`

const settings = {
  adaptiveHeight: false,
  arrows: false,
  autoplay: false,
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  pauseOnHover: false,
}

const additionalStyle = { height: '160px' }
function AnnounceModal({ isOpen, handleClose, data }) {
  return (
    <Dialog isOpen={isOpen} onClose={handleClose} smaller>
      <div className="absolute" style={iconAbsolutePosition}>
        <IconAnnounce />
      </div>
      <SliderWrapper className="mt-11">
        <Slider {...settings}>
          {data?.map(item => (
            <div key={`slider_${item?.id}`}>
              <div
                style={additionalStyle}
                className="overflow-y-auto px-3"
                dangerouslySetInnerHTML={{ __html: item?.content }}
              />
              <div className="text-right mt-6 text-platinum-100 text-body-6">
                {item?.announceDept}
              </div>
              <div className="text-right mb-5 text-platinum-100 text-body-6">
                {dayjs.unix(item?.createTime).format('YYYY/MM/DD')}
              </div>
            </div>
          ))}
        </Slider>
      </SliderWrapper>
    </Dialog>
  )
}

AnnounceModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
}

export default AnnounceModal
