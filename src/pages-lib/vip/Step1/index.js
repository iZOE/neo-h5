import dynamic from 'next/dynamic'
import { useIntl, FormattedMessage } from 'react-intl'
import { Decimal } from 'decimal.js'
import * as dayjs from 'dayjs'
import { numberWithCommas } from 'libs/helper'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import useStep from 'hooks/useStep'
import { darkModeState } from '../../../atoms/darkModeState'
import BackgroundDecoration from '../components/BackgroundDecoration'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

function VipInfo({ vipData, vipLevelData }) {
  const intl = useIntl()
  const router = useRouter()
  const { gotoNext } = useStep()
  const darkMode = useRecoilValue(darkModeState)

  const level = vipData?.levelId > 0 ? vipData?.levelId - 10 : vipData?.levelId
  const nextLevel = vipData?.nextLevelId !== 0 ? vipData?.nextLevelId - 10 : vipData?.nextLevelId
  const restoreLevel =
    vipData?.restoreLevelId > 0 ? vipData?.restoreLevelId - 10 : vipData?.restoreLevelId
  const currentLevelInfo = vipLevelData && vipLevelData[level]
  const nextLevelInfo = vipLevelData && vipLevelData[nextLevel]
  const topLevelInfo = vipLevelData && vipLevelData[10]
  const cumulativeDepositAmount = vipData?.cumulativeDepositAmount
  const thresholdOfDeposit =
    nextLevel !== 0 ? nextLevelInfo?.thresholdOfDeposit : topLevelInfo?.thresholdOfDeposit
  const depositAmountWith =
    cumulativeDepositAmount && thresholdOfDeposit
      ? new Decimal(cumulativeDepositAmount)
          .div(new Decimal(thresholdOfDeposit))
          .mul(new Decimal(100))
          .toNumber()
      : 0
  const cumulativeTurnoverBetAmount = vipData?.cumulativeTurnoverBetAmount
  const thresholdOfTurnoverBet =
    nextLevel !== 0 ? nextLevelInfo?.thresholdOfTurnoverBet : topLevelInfo?.thresholdOfTurnoverBet
  const turnoverBetAmountWidth =
    cumulativeTurnoverBetAmount && thresholdOfTurnoverBet
      ? new Decimal(cumulativeTurnoverBetAmount)
          .div(new Decimal(thresholdOfTurnoverBet))
          .mul(new Decimal(100))
          .toNumber()
      : 0
  const lastUpdate = `${dayjs(new Date()).format('YYYY-MM-DD')} 12:00:00`
  const rightsData = [
    {
      title: intl.formatMessage({ id: 'levelup_bonus' }),
      value: numberWithCommas(currentLevelInfo?.levelUpBonus),
    },
    {
      title: intl.formatMessage({ id: 'birthday_bonus' }),
      value: numberWithCommas(currentLevelInfo?.birthdayBonus),
    },
    {
      title: intl.formatMessage({ id: 'monthly_bonus' }),
      value: numberWithCommas(currentLevelInfo?.monthlyBonus),
    },
    {
      title: intl.formatMessage({ id: 'limit_of_withdrawal_amount' }),
      value: numberWithCommas(currentLevelInfo?.limitOfWithdrawalAmount),
    },
    {
      title: intl.formatMessage({ id: 'limit_of_daily_withdrawal_count' }),
      value: numberWithCommas(currentLevelInfo?.limitOfDailyWithdrawalCount),
    },
    {
      title: intl.formatMessage({ id: 'active_bonus' }),
      value: numberWithCommas(currentLevelInfo?.activeBonus),
    },
    {
      title: intl.formatMessage({ id: 'turnover_multiple' }),
      value: numberWithCommas(currentLevelInfo?.turnoverMultiple),
    },
  ]
  const rightsList = rightsData.map((item, index) => {
    if (item.value && Number(item.value) !== 0) {
      return (
        <div
          className="flex justify-between text-body-6 py-3 border-gray-100 dark:border-gray-500 border-solid border-b last:border-none text-body-6"
          key={`${item.title}`}
        >
          <div className="text-platinum-200">{item.title}</div>
          <div className="text-platinum-300 dark:text-platinum-100">{item.value}</div>
        </div>
      )
    }
  })
  const IconVipLevel = level && dynamic(() => import(`components/icons/vip/Level${level}`))
  let levelupRequireText = intl.formatMessage(
    { id: 'levelup_require_text3' },
    {
      levelUpDepositPercentage: vipData?.levelUpDepositPercentageText,
      levelUpTurnoverBetPercentage: vipData?.levelUpTurnoverBetPercentageText,
      nextLevel,
    },
  )
  if (nextLevel === 0 && vipData?.isDowngrade) {
    levelupRequireText = intl.formatMessage({ id: 'levelup_require_text1' })
  } else if (nextLevel === 0) {
    levelupRequireText = intl.formatMessage({ id: 'levelup_require_text2' })
  }
  const handleToVipRights = () => {
    gotoNext({
      nextLevel: nextLevel !== 0 ? nextLevel : 10,
      vipLevelData,
    })
  }
  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.back()
        }}
        title={intl.formatMessage({ id: 'member_level' })}
      />
      <div
        className="py-4 px-3 mt-11"
        style={{
          height: '224px',
          background: darkMode
            ? 'linear-gradient(179.04deg, #3A3845 0.53%, #20202C 92.9%)'
            : 'linear-gradient(90deg, #50BEFC 0%, #3985EB 100%)',
        }}
      >
        <BackgroundDecoration />
        <div className="text-white ">
          {level > 0 ? (
            <div className="absolute right-3">
              <IconVipLevel w="76" h="76" vw="76" vh="76" />
            </div>
          ) : null}
          <div>
            <FormattedMessage id="my_vip" />
          </div>
          <div className="text-title-4">
            {level > 0 ? `VIP ${level}` : intl.formatMessage({ id: 'new_vip' })}
          </div>
          {vipData?.isDowngrade ? (
            <div className="text-body-6">
              {intl.formatMessage(
                { id: 'recover_text' },
                { restoreTurnover: vipData?.restoreTurnoverBetAmount, restoreLevel },
              )}
            </div>
          ) : null}
        </div>
      </div>
      <div className="relative px-3" style={{ top: '-98px' }}>
        <Button
          variant={darkMode ? 'secondary' : 'tertiary'}
          text={intl.formatMessage({ id: 'levelup_rights' })}
          onClick={handleToVipRights}
        />
      </div>
      <Container noHighScreen>
        <div style={{ transform: 'translateY(-84px)' }}>
          <div className="bg-white dark:bg-purple-800 rounded-md p-4 shadow-b-0">
            <div className="text-title-8 text-blue-200">{levelupRequireText}</div>
            <div className="py-6 border-gray-100 border-solid border-b dark:border-gray-500">
              <div>
                <div className="inline-block text-title-8 text-platinum-300 mr-4">
                  <FormattedMessage id="deposit" />
                </div>
                <div className="inline-block">
                  <span className="text-body-4 text-blue-200">
                    {numberWithCommas(cumulativeDepositAmount)}
                  </span>{' '}
                  <span className="text-body-6 text-platinum-200">
                    / {numberWithCommas(thresholdOfDeposit)}
                  </span>
                </div>
              </div>
              <div className="mt-2">
                <div className="bg-gray-200 dark:bg-purple-200 h-2 rounded-md">
                  <div
                    className="bg-blue-100 h-2 rounded-md"
                    style={{ width: depositAmountWith < 100 ? `${depositAmountWith}%` : '100%' }}
                  />
                </div>
              </div>
            </div>
            <div className="py-6">
              <div>
                <div className="inline-block text-title-8 text-platinum-300 mr-4">
                  <FormattedMessage id="turnover" />
                </div>
                <div className="inline-block">
                  <span className="text-body-4 text-blue-200">
                    {numberWithCommas(cumulativeTurnoverBetAmount)}
                  </span>{' '}
                  <span className="text-body-6 text-platinum-200">
                    / {numberWithCommas(thresholdOfTurnoverBet)}
                  </span>
                </div>
              </div>
              <div className="mt-2">
                <div className="bg-gray-200 dark:bg-purple-200 h-2 rounded-md">
                  <div
                    className="bg-blue-100 h-2 rounded-md"
                    style={{
                      width: turnoverBetAmountWidth < 100 ? `${turnoverBetAmountWidth}%` : '100%',
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="text-platinum-200 text-body-6">
              <div>{intl.formatMessage({ id: 'update_time' }, { lastUpdate })}</div>
              <div>
                <FormattedMessage id="update_time_text" />
              </div>
            </div>
          </div>
          <div className="mt-6 rounded-md shadow-b-0">
            <div className="bg-blue-200 text-white h-9 p-2 rounded-t-md text-center font-semibold leading-4">
              <FormattedMessage id="my_rights" />
            </div>
            <div className="bg-white dark:bg-purple-800 px-3 rounded-b-md">{rightsList}</div>
          </div>
          {currentLevelInfo?.remarks ? (
            <div
              className="text-blue-200 text-body-6 rounded-md mt-6 p-3"
              style={{ background: 'rgba(81, 161, 255, 0.1)' }}
            >
              {currentLevelInfo?.remarks}
            </div>
          ) : null}
          <div className="text-platinum-300 dark:text-platinum-200 text-body-6 mt-6">
            <div>
              {intl.formatMessage(
                { id: 'stay_level_tip1' },
                {
                  thresholdOfStayOnLevel: numberWithCommas(
                    currentLevelInfo?.thresholdOfStayOnLevel,
                  ),
                },
              )}
            </div>
            <div className="mt-2">
              <FormattedMessage id="stay_level_tip2" />
            </div>
          </div>
        </div>
      </Container>
    </>
  )
}

export default VipInfo
