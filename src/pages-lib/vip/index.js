import dynamic from 'next/dynamic'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import { useStepView } from '../../hooks/useStep'

const Step1Component = dynamic(() => import('./Step1'))
const Step2Component = dynamic(() => import('./Step2'))

const STEP_VIEW = {
  1: Step1Component,
  2: Step2Component,
}

const MIN_STEP = 1
const MAX_STEP = 2

function VipSteps() {
  const { data: vipData } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/vip`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
    {
      revalidateOnFocus: false,
    },
  )

  const { data: vipLevelData } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_STORAGE}${process.env.NEXT_PUBLIC_AGENT_VIP_PATH}`,
    async url => {
      const res = await fetcher({ url })
      return res.data
    },
    {
      revalidateOnFocus: false,
    },
  )
  const ViewComponent = useStepView(STEP_VIEW, MIN_STEP, MAX_STEP)
  return <ViewComponent vipData={vipData} vipLevelData={vipLevelData} />
}

VipSteps.protect = true

export default VipSteps
