import Picture from 'components/core/Picture'

const BackgroundDecoration = () => {
  const mediaPath = '/pages/vip/bg'

  return (
    <div className="absolute -z-1 left-0 top-11" style={{ height: '224px' }}>
      <Picture webp={`${mediaPath}.webp`} png={`${mediaPath}.png`} />
    </div>
  )
}

export default BackgroundDecoration
