import { useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { useIntl, FormattedMessage } from 'react-intl'
import { motion } from 'framer-motion'
import { useStepData } from 'hooks/useStep'
import { numberWithCommas } from 'libs/helper'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Menu = dynamic(() => import('components/core/Menu'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const spring = {
  type: 'spring',
  stiffness: 500,
  damping: 30,
}

function VipRights() {
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const { nextLevel, vipLevelData } = stepData
  const [selectedLevel, setSelectedLevel] = useState(nextLevel)
  const [selectedType, setSelectedType] = useState(0)
  const IconVipLevel = selectedLevel && dynamic(() => import(`components/icons/vip/Level${selectedLevel}`))

  const type = [
    { name: intl.formatMessage({ id: 'vip_level_detail' }) },
    { name: intl.formatMessage({ id: 'turnover_concessional' }) },
  ]
  const selectedLevelData = vipLevelData[selectedLevel]
  const levelList = vipLevelData.map((item, index) => ({ id: index, name: item.levelName }))
  const levelRightData = [
    {
      title: intl.formatMessage({ id: 'threshold_of_deposit' }),
      value: numberWithCommas(selectedLevelData?.thresholdOfDeposit),
    },
    {
      title: intl.formatMessage({ id: 'threshold_of_turnover_bet' }),
      value: numberWithCommas(selectedLevelData?.thresholdOfTurnoverBet),
    },
    {
      title: intl.formatMessage({ id: 'min_withdrawal_amount' }),
      value: numberWithCommas(selectedLevelData?.minWithdrawalAmount),
    },
    {
      title: intl.formatMessage({ id: 'withdrawal_fee' }),
      value: numberWithCommas(selectedLevelData?.withdrawalFee),
    },
    {
      title: intl.formatMessage({ id: 'levelup_bonus' }),
      value: numberWithCommas(selectedLevelData?.levelUpBonus),
    },
    {
      title: intl.formatMessage({ id: 'birthday_bonus' }),
      value: numberWithCommas(selectedLevelData?.birthdayBonus),
    },
    {
      title: intl.formatMessage({ id: 'monthly_bonus' }),
      value: numberWithCommas(selectedLevelData?.monthlyBonus),
    },
    {
      title: intl.formatMessage({ id: 'limit_of_withdrawal_amount' }),
      value: numberWithCommas(selectedLevelData?.limitOfWithdrawalAmount),
    },
    {
      title: intl.formatMessage({ id: 'limit_of_daily_withdrawal_count' }),
      value: numberWithCommas(selectedLevelData?.limitOfDailyWithdrawalCount),
    },
    {
      title: intl.formatMessage({ id: 'active_bonus' }),
      value: numberWithCommas(selectedLevelData?.activeBonus),
    },
    {
      title: intl.formatMessage({ id: 'claim_frequency' }),
      value: selectedLevelData?.claimFrequencyText,
    },
    {
      title: intl.formatMessage({ id: 'turnover_multiple' }),
      value: numberWithCommas(selectedLevelData?.turnoverMultiple),
    },
  ]
  const levelRebateData = Object.keys(selectedLevelData.rebateRates).map(key => ({
    title: key,
    value: selectedLevelData.rebateRates[key],
  }))

  const tabVariant = {
    tab1: {},
    tab2: {
      right: 0,
    },
  }
  const listData = selectedType === 0 ? levelRightData : levelRebateData
  const list = listData.map(item => {
    if (item.value && Number(item.value) !== 0) {
      return (
        <div
          className="flex justify-between text-body-6 py-3 border-gray-100 dark:border-gray-500 border-solid border-b last:border-none text-body-6"
          key={`${item.title}`}
        >
          <div className="text-platinum-200">{item.title}</div>
          <div className="text-platinum-300 dark:text-platinum-100">{item.value}</div>
        </div>
      )
    }
  })

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={intl.formatMessage({ id: 'vip_level_detail' })}
      />
      <Menu
        data={levelList}
        selected={selectedLevel}
        onTypeChange={id => {
          setSelectedLevel(id)
        }}
      />
      <Container withNavigationBar>
        <div className="mt-10 pt-6">
          <div className="relative flex w-full box-border rounded-full h-10 text-body-6 bg-white dark:bg-purple-700 mb-6 border-white border-solid border-4 dark:border-purple-700 shadow-b-0">
            <motion.div
              layout
              transition={spring}
              variants={tabVariant}
              animate={selectedType === 0 ? 'tab1' : 'tab2'}
              className="absolute rounded-full h-8 bg-platinum-300 w-1/2"
            />
            {type.map((item, index) => (
              <div key={item.name} style={{ position: 'relative', flex: 1 }}>
                <button
                  type="button"
                  onClick={() => {
                    setSelectedType(index)
                  }}
                  className="absolute top-2/4 left-2/4 w-full border-none bg-transparent"
                  style={{
                    transform: 'translate(-50%,-50%)',
                    color: item.name === type[selectedType].name ? '#fff' : '#95A3B0',
                    lineHeight: '17px',
                  }}
                >
                  {item.name}
                </button>
              </div>
            ))}
          </div>
          <div className="bg-white dark:bg-purple-800 px-3 rounded-md shadow-b-0">
            <>
              {selectedLevel > 0 && (
                <div className="flex justify-between items-center pt-3 pb-2 border-gray-100 border-solid border-b dark:border-gray-500 text-platinum-300 dark:text-platinum-100">
                  <div>
                    <div className="text-title-4 font-semibold">{`VIP ${selectedLevel}`}</div>
                    <div className="text-body-4">{type[selectedType].name}</div>
                  </div>
                  <div>
                    <IconVipLevel w="56" h="56" vw="76" vh="76" />
                  </div>
                </div>
              )}
              {list}
            </>
          </div>
        </div>
        {selectedLevelData?.remarks ? (
          <div
            className="text-blue-200 text-body-6 rounded-md mt-6 p-3"
            style={{ background: 'rgba(81, 161, 255, 0.1)' }}
          >
            {selectedLevelData?.remarks}
          </div>
        ) : null}
        <div className="text-platinum-300 dark:text-platinum-200 text-body-6 py-6">
          <div>
            {intl.formatMessage(
              { id: 'stay_level_tip1' },
              {
                thresholdOfStayOnLevel:
                  selectedLevelData && numberWithCommas(selectedLevelData?.thresholdOfStayOnLevel),
              },
            )}
          </div>
          <div className="mt-2">
            <FormattedMessage id="stay_level_tip2" />
          </div>
        </div>
      </Container>
    </>
  )
}

export default VipRights
