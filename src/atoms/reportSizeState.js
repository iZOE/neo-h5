import { atom } from 'recoil'
import localStorageEffect from './effects/localStorageEffect'

export const reportSizeState = atom({
  key: 'reportSizeState',
  default: false,
  effects_UNSTABLE: [localStorageEffect('report-size')],
})
