import { atom } from 'recoil'
import localStorageEffect from './effects/localStorageEffect'

export const darkModeState = atom({
  key: 'darkModeState',
  default: false,
  effects_UNSTABLE: [localStorageEffect('dark-mode')],
})
