import { atom } from 'recoil'

export const pageTransitionState = atom({
  key: 'pageTransitionState',
  default: {
    mode: 'normal',
    direction: '-',
  },
})
