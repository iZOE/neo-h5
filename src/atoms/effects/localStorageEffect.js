const localStorageEffect = key => ({ setSelf, onSet }) => {
  const savedValue = typeof window !== 'undefined' && window.localStorage.getItem(key)
  if (savedValue != null) {
    setSelf(JSON.parse(savedValue))
  }

  onSet(newValue => {
    if (newValue === null) {
      window.localStorage.removeItem(key)
    } else {
      window.localStorage.setItem(key, JSON.stringify(newValue))
    }
  })
}

export default localStorageEffect
