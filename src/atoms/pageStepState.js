import { atom } from 'recoil'

export const pageStepState = atom({
  key: 'pageStepState',
  default: {
    currentStep: 1,
    data: null,
  },
})
