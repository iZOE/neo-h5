import { atom } from 'recoil'
import localStorageEffect from './effects/localStorageEffect'

export const tokenState = atom({
  key: 'tokenState',
  default: null,
  effects_UNSTABLE: [localStorageEffect('token')],
})
