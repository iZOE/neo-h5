import { FormattedMessage } from 'react-intl'

export const COLUMNS_CONFIG = [
  {
    key: 1,
    item: <FormattedMessage id="game_list.lottery" />,
    value: 100,
  },
  {
    key: 2,
    item: <FormattedMessage id="game_list.video" />,
    value: 10,
  },
  {
    key: 3,
    item: <FormattedMessage id="game_list.chess" />,
    value: 5,
  },
  {
    key: 4,
    item: <FormattedMessage id="game_list.egame" />,
    value: 10,
  },
  {
    key: 5,
    item: <FormattedMessage id="game_list.fish" />,
    value: 10,
  },
  {
    key: 6,
    item: <FormattedMessage id="game_list.esport" />,
    value: 10,
  }
]
