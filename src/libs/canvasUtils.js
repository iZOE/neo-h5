const readFile = file =>
  new Promise(resolve => {
    const reader = new FileReader()
    reader.addEventListener('load', () => resolve(reader.result), false)
    reader.readAsDataURL(file)
  })

const createImage = url =>
  new Promise((resolve, reject) => {
    const image = new Image()
    image.addEventListener('load', () => resolve(image))
    image.addEventListener('error', error => reject(error))
    image.setAttribute('crossOrigin', 'anonymous') // needed to avoid cross-origin issues on CodeSandbox
    image.src = url
  })

export async function getCompressImg(imageFile) {
  const imageSrc = await readFile(imageFile)
  const image = await createImage(imageSrc)
  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')
  canvas.width = 640
  canvas.height = canvas.width * (image.height / image.width)
  ctx.drawImage(image, 0, 0, 640, (640 * image.height) / image.width)
  const imgBase64 = canvas.toDataURL('image/jpeg', 0.5)
  const res = await fetch(imgBase64)
  const blob = await res.blob()
  const result = new File([blob], 'avatar.png', { type: 'image/png' })
  result.url = imgBase64
  return result
}

export default async function getCroppedImg(imageSrc, pixelCrop, rotation = 0) {
  const image = await createImage(imageSrc)
  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')

  canvas.width = 640
  canvas.height = 640

  ctx.drawImage(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, 640, 640)
  const imgBase64 = canvas.toDataURL('image/jpeg', 0.5)
  const res = await fetch(imgBase64)
  const blob = await res.blob()

  const result = new File([blob], 'avatar.png', { type: 'image/png' })
  result.url = imgBase64

  return result
}
