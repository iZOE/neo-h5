import styled, { css } from 'styled-components'

export const backgroundImage = css`
  background-image: ${props => `url(${props.imgSrc})`};
`
