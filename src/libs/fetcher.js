import axios from 'axios'
import btoa from './btoa'

let isAlreadyFetchingAccessToken
let subscribers = []
const NOT_ERROR_CODES = ['0000', '4200', '4301']

function onAccessTokenFetched(accessToken) {
  subscribers = subscribers.filter(callback => callback(accessToken))
}

function getLocalStorageToken() {
  try {
    const localStorageToken = localStorage.getItem('token')
    if (localStorageToken) {
      return JSON.parse(localStorageToken)
    }
    return null
  } catch (error) {
    return null
  }
}

function addSubscriber(callback) {
  subscribers.push(callback)
}

export function refreshToken() {
  return new Promise((resolve, reject) => {
    const tokenObj = getLocalStorageToken()

    if (tokenObj) {
      // eslint-disable-next-line camelcase
      const { refresh_token } = tokenObj
      try {
        const bodyFormData = new FormData()
        bodyFormData.append('refresh_token', refresh_token)
        bodyFormData.append('grant_type', 'refresh_token')
        const bodyFormDataString = new URLSearchParams(bodyFormData).toString()
        const authorizationVal = `${process.env.NEXT_PUBLIC_CLIENT_ID}:${
          process.env.NEXT_PUBLIC_APP_KEY || ''
        }`
        // const res = await
        return fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_OAUTH}Token`,
          method: 'post',
          headers: {
            Authorization: `Basic ${btoa(authorizationVal)}`,
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-Platform': '4',
            'X-Device-ID': '444',
            'X-Device-Model': '444',
          },
          data: bodyFormDataString,
          isRefreshToken: true,
        })
          .then(res => {
            if (res.status === 500) {
              reject()
              // throw Error('REFRESH_TOKEN_FAILED 1')
            } else {
              localStorage.setItem('token', JSON.stringify(res.data))
              resolve(res.data)
            }
          })
          .catch(error => {
            reject(error)
            // throw Error('REFRESH_TOKEN_FAILED 2', error)
          })
      } catch (error) {
        reject(error)
        // throw Error('REFRESH_TOKEN_FAILED 2', error)
      }
    }
  })
}

axios.interceptors.request.use(
  config => config,
  error => Promise.reject(error),
)

axios.interceptors.response.use(
  response => {
    if (
      response.status === 202 ||
      (response.data && (!response.data.code || NOT_ERROR_CODES.indexOf(response.data.code) > -1))
    ) {
      return response
    }
    return Promise.reject(response)
  },
  ({ config, response }) => {
    if (response) {
      const { status } = response
      const { isRefreshToken, withToken } = config
      if (withToken && status === 401 && !isRefreshToken) {
        if (!isAlreadyFetchingAccessToken) {
          isAlreadyFetchingAccessToken = true
          refreshToken()
            .then(res => {
              if (res && res.access_token) {
                isAlreadyFetchingAccessToken = false
                onAccessTokenFetched(res.access_token)
              }
            })
            .catch(err => {
              localStorage.removeItem('token')
              sessionStorage.setItem('requireLogin', true)
            })
        }

        const retryOriginalRequest = new Promise(resolve => {
          addSubscriber(accessToken => {
            const nextConfig = Object.assign(config)
            nextConfig.headers.Authorization = `Bearer ${accessToken}`
            resolve(axios(nextConfig))
          })
        })
        return retryOriginalRequest
      }
      return Promise.reject(response)
    }
    return Promise.reject(response)
  },
)

export default function fetcher({ url, method = 'get', headers, withToken, data, isRefreshToken }) {
  const reqHeaders = {
    ...headers,
    language: process.env.NEXT_PUBLIC_AGENT_LOCALE_LANGUAGE,
  }
  if (withToken) {
    const tokenObj = getLocalStorageToken()
    // 需要token ,but storage無token ,throw error
    if (!tokenObj || !tokenObj.access_token) {
      throw Error('REFRESH_TOKEN_FAILED')
    }
    reqHeaders.Authorization = withToken && tokenObj ? `Bearer ${tokenObj.access_token}` : ''
  }
  return axios({
    method,
    url,
    data,
    headers: reqHeaders,
    isRefreshToken,
    withToken,
  })
}
