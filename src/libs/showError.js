const showError = async (error, intl) => {
  const message = (await import('components/core/Alert/message')).default
  const messages = error?.data?.responseMessage || error?.message || error
  message({
    content: messages,
    okText: intl?.formatMessage({ id: 'confirm' }),
  })
}

export default showError
