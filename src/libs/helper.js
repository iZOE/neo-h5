import { Decimal } from 'decimal.js'

export const numberWithCommas = x => {
  if (typeof x !== 'number') return x
  const parts = new Decimal(x).toFixed(/\./g.test(x.toString()) ? 2 : 0, 1).split('.')
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return parts.join('.')
}
