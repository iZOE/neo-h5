import axios from 'axios'

let isAlreadyFetchingAccessToken
let isRefreshingToken
const subscribers = []

function btoa(str) {
  return Buffer.from(str).toString('base64')
}

const DEFAULT_HEADERS = {
  Authorization: `Basic ${btoa(
    `${process.env.REACT_APP_CLIENT_ID}:${process.env.REACT_APP_APP_KEY || ''}`,
  )}`,
  'Content-Type': 'application/x-www-form-urlencoded',
  'X-Platform': '4',
  'X-Device-ID': '444', // device uuid
  'X-Device-Model': '444', // device type
}

function getLocalStorageToken() {
  try {
    const localStorageToken = localStorage.getItem('token')
    if (localStorageToken) {
      return JSON.parse(localStorageToken)
    }
    return null
  } catch (error) {
    return null
  }
}

async function refreshToken() {
  isRefreshingToken = true
  const tokenObj = getLocalStorageToken()

  if (tokenObj) {
    // eslint-disable-next-line camelcase
    const { refresh_token } = tokenObj
    try {
      console.log('nani!!')
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_OAUTH}Token`,
        method: 'post',
        headers: DEFAULT_HEADERS,
        data: JSON.stringify({
          refresh_token,
          grant_type: 'refresh_token',
        }),
      })
      console.log(' refresh_token res')
      if (res.status === 500) {
        throw new Error('REFRESH_TOKEN_FAILED')
      } else {
        localStorage.setItem('token', res.json())
      }
    } catch (error) {
      throw new Error('REFRESH_TOKEN_FAILED')
    }
  }
}

export default async function fetcher({ url, method = 'get', headers, body, withToken }) {
  const reqHeaders = {
    ...headers,
    language: localStorage.getItem('USER_LANG') || 'zh-CN',
  }
  if (withToken) {
    const tokenObj = getLocalStorageToken()
    // 需要token ,but storage無token ,throw error
    if (!tokenObj || !tokenObj.access_token) {
      throw new Error('REFRESH_TOKEN_FAILED')
    }
    reqHeaders.Authorization = withToken && tokenObj ? `Bearer ${tokenObj.access_token}` : ''
  }
  const res = await fetch(url, {
    method,
    headers: reqHeaders,
    body,
  })

  if (res.status === 401 && withToken) {
    if (!isAlreadyFetchingAccessToken) {
      isAlreadyFetchingAccessToken = true

      // goto refresh token
      try {
        const refreshResponse = await refreshToken()
        console.log('refreshResponse')
        if (refreshResponse.access_token) {
          isAlreadyFetchingAccessToken = false
          const afterRefreshTokenRes = await fetcher({ url, method, headers, body, withToken })
          return afterRefreshTokenRes.json()
        }
      } catch (error) {
        throw error
      }
    }
  }
  return res.json()
}
