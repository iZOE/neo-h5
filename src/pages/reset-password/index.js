import { useEffect } from 'react'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { useStepView } from '../../hooks/useStep'

const Step1Component = dynamic(() => import('./components/Step1'))
const Step2Component = dynamic(() => import('./components/Step2'))
const Step3Component = dynamic(() => import('./components/Step3'))
const Step4Component = dynamic(() => import('./components/Step4'))

const STEP_VIEW = {
  1: Step1Component,
  2: Step2Component,
  3: Step3Component,
  4: Step4Component,
}

const MIN_STEP = 1
const MAX_STEP = 4

function ResetPassword() {
  const ViewComponent = useStepView(STEP_VIEW, MIN_STEP, MAX_STEP)
  return <ViewComponent />
}

export default ResetPassword
