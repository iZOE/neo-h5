import useSWR from 'swr'
import { useCallback } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import useStep from 'hooks/useStep'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Input = dynamic(() => import('components/core/Input'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

export default function Step1() {
  const intl = useIntl()
  const router = useRouter()
  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })
  const { gotoNext } = useStep()
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/sessionkey`,
    url => fetcher({ url }),
    {
      revalidateOnFocus: false,
    },
  )

  const sessionKey = data ? data.data.data : null
  const handleGetAlert = useCallback(async () => {
    const message = (await import('components/core/Alert/message')).default
    message({
      content: intl.formatMessage({ id: 'page1.warning_tip' }),
      okText: intl.formatMessage({ id: 'contact_customer' }),
      onOk: () => {
        router.push('/service')
      },
    })
  }, [])

  const onSubmit = useCallback(
    async formData => {
      const body = {
        accountId: formData.accountId,
        agentCode: process.env.NEXT_PUBLIC_AGENT_CODE,
        sessionKey,
      }
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/RecoveryPassword`,
          method: 'post',
          data: body,
        })
        if (res.data.code === '0000') {
          gotoNext({
            sessionKey,
            ...res.data.data,
          })
        } else {
          handleGetAlert()
        }
      } catch (err) {
        handleGetAlert()
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [sessionKey],
  )

  if (error) {
    return <div>404</div>
  }

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            name="accountId"
            control={control}
            rules={{ required: true }}
            render={({ onChange, value }) => (
              <Input
                label={intl.formatMessage({ id: 'page1.input.accountId.label' })}
                placeholder={intl.formatMessage({ id: 'page1.input.accountId.placeholder' })}
                onChange={onChange}
                width={1}
                value={value}
              />
            )}
          />
          <div className="h-6" />
          <p className="text-body-4 text-platinum-300 mb-2">
            {intl.formatMessage({ id: 'page1.tips.line_1' })}
          </p>
          <p className="text-body-4 text-platinum-300">
            {intl.formatMessage({ id: 'page1.tips.line_2' })}
          </p>
          <div className="h-6" />
          <div className="flex">
            <div className="flex-1">
              <Button variant="secondary" type="submit" onClick={() => router.push('/service')}>
                {intl.formatMessage({ id: 'contact_customer' })}
              </Button>
            </div>
            <div className="w-3" />
            <div className="flex-1">
              <Button variant="primary" type="submit" disabled={!isDirty || !isValid}>
                {intl.formatMessage({ id: 'common.next' })}
              </Button>
            </div>
          </div>
        </form>
      </Container>
    </>
  )
}
