import useStep, { useStepData } from 'hooks/useStep'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import NavigationBar from 'components/core/NavigationBar'
import Container from 'components/core/Container'
import Button from 'components/core/Button'
import IconBack from 'components/icons/Back.svg'

export default function Step2() {
  const intl = useIntl()
  const router = useRouter()
  const { gotoNext, gotoPrevious } = useStep()
  const stepData = useStepData()
  if (!stepData) {
    return null
  }

  const { enableSMS, contactNumber } = stepData
  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => gotoPrevious()}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar>
        <div className="py-6">
          <p className="text-body-4 text-platinum-300">
            {intl.formatMessage({ id: 'page2.tips' })}
          </p>
        </div>
        <div className="flex">
          <div className="flex-1">
            <Button variant="secondary" type="button" onClick={() => router.push('/service')}>
              {intl.formatMessage({ id: 'contact_customer' })}
            </Button>
          </div>
          {enableSMS && contactNumber && (
            <>
              <div className="w-3" />
              <div className="flex-1">
                <Button variant="secondary" onClick={() => gotoNext()} type="button">
                  {intl.formatMessage({ id: 'page2.mail_validate' })}
                </Button>
              </div>
            </>
          )}
        </div>
      </Container>
    </>
  )
}
