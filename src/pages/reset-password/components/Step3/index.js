import { useState, useCallback, useEffect } from 'react'
import useStep, { useStepData } from 'hooks/useStep'
import { useForm, Controller } from 'react-hook-form'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import * as dayjs from 'dayjs'
import IconBack from 'components/icons/Back.svg'
import fetcher from 'libs/fetcher'
import GetCodeButton from './components/GetCodeButton'

const Input = dynamic(() => import('components/core/Input'))
const Button = dynamic(() => import('components/core/Button'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))

export default function Step3() {
  const router = useRouter()
  const intl = useIntl()
  const { gotoNext, gotoPrevious } = useStep()
  const stepData = useStepData()
  const [sendResData, setSendResData] = useState()

  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
  })

  // 發送簡訊驗證碼
  const handleSendCode = useCallback(async () => {
    const message = (await import('components/core/Alert/message')).default
    const toast = (await import('components/core/Toast/toast')).default
    try {
      const { sessionKey, contactNumber } = stepData
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/RecoveryPassword/SMS`,
        method: 'post',
        data: {
          phoneNo: contactNumber,
          sessionKey,
        },
      })
      const finalCountDown =
        parseInt(res.data.data.expiredUtcTime, 10) - parseInt(dayjs().unix(), 10) - 1
      console.log('ok', finalCountDown)
      setSendResData({
        finalCountDown,
        ...res.data,
      })
      toast({
        message: intl.formatMessage({
          id: 'page3.sended_message',
        }),
      })
    } catch (err) {
      const finalCountDown =
        parseInt(err?.data?.data?.expiredUtcTime, 10) - parseInt(dayjs().unix(), 10) - 1
      setSendResData({
        finalCountDown,
      })
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({
          id: 'contact_customer',
        }),
        onOk: () => {
          router.push('/service')
        },
      })
    }
  }, [])

  // 輸入簡訊驗證碼
  const onSubmit = useCallback(async formData => {
    const { validationCode } = formData
    const { contactNumber, sessionKey } = stepData
    const message = (await import('components/core/Alert/message')).default
    const toast = (await import('components/core/Toast/toast')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/RecoveryPassword/SMSCode`,
        method: 'post',
        data: {
          sessionKey,
          validationCode,
        },
      })
      toast({
        message: intl.formatMessage({
          id: 'page3.toast',
        }),
      })
      gotoNext(res.data.data)
    } catch (err) {
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  useEffect(() => {
    handleSendCode()
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => gotoPrevious()}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar>
        <div className="my-6">
          {sendResData?.code === '0000' && (
            <div className="mb-4" mb={16}>
              <div className="text-body-4 text-platinum-300">
                {intl.formatMessage({
                  id: 'page3.sended_message',
                })}
              </div>
              <div className="text-body-6 text-blue-200">{stepData?.contactNumber}</div>
            </div>
          )}
          <form onSubmit={handleSubmit(onSubmit)}>
            <Controller
              name="validationCode"
              control={control}
              rules={{ required: true }}
              render={({ onChange, value }) => (
                <Input
                  placeholder={intl.formatMessage({
                    id: 'page3.input.validation_code.placeholder',
                  })}
                  inputmode="tel"
                  onChange={onChange}
                  width={1}
                  value={value}
                  error={
                    errors.validationCode &&
                    intl.formatMessage({
                      id: 'page3.input.validation_code.placeholder',
                    })
                  }
                />
              )}
            />
            <div className="mt-6">
              <Button variant="primary" type="submit" disabled={!isDirty || !isValid}>
                {intl.formatMessage({ id: 'common.next' })}
              </Button>
            </div>
          </form>
        </div>
        {!Number.isNaN(sendResData?.finalCountDown) && (
          <div className="text-center">
            <GetCodeButton
              key={sendResData?.finalCountDown}
              sec={sendResData?.finalCountDown}
              onClick={handleSendCode}
            />
          </div>
        )}
      </Container>
    </>
  )
}
