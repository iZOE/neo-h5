import { useState, useEffect, useCallback } from 'react'
import styled from 'styled-components'
import { useIntl } from 'react-intl'

export default function GetCodeButton({ sec, onClick }) {
  const intl = useIntl()
  const [countdown, setCountdown] = useState(sec)

  useEffect(() => {
    if (sec) {
      const timer = setTimeout(() => {
        if (countdown > 0) {
          setCountdown(countdown - 1)
        }
      }, 1000)

      return () => {
        clearTimeout(timer)
      }
    }
  }, [sec, countdown])

  const handleClick = useCallback(() => {
    onClick()
  }, [])

  return (
    <>
      {countdown < 1 ? (
        <button
          className="re-send-code text-blue-200 text-body-6 bg-transparent border-none font-semibold"
          onPointerUp={handleClick}
          type="button"
        >
          {intl.formatMessage({ id: 'page3.get_code_button' })}
        </button>
      ) : (
        <div className="text-body-6 text-blue-200">
          {intl.formatMessage({ id: 'page3.get_code_button' })}({countdown}s)
        </div>
      )}
    </>
  )
}
