import useStep, { useStepData } from 'hooks/useStep'
import { useForm, Controller } from 'react-hook-form'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import Input from 'components/core/Input'
import NavigationBar from 'components/core/NavigationBar'
import Container from 'components/core/Container'
import Button from 'components/core/Button'
import IconBack from 'components/icons/Back.svg'
import fetcher from 'libs/fetcher'

export default function Step4() {
  const router = useRouter()
  const intl = useIntl()
  const {
    control,
    handleSubmit,
    errors,
    getValues,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })
  const { gotoPrevious } = useStep()
  const stepData = useStepData()

  const onSubmit = async formData => {
    const message = (await import('components/core/Alert/message')).default
    if (stepData) {
      try {
        await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/login/${stepData?.accountId}?resetToken=${stepData?.tokenContent}`,
          method: 'put',
          data: {
            setPassword: formData.password,
          },
        })

        message({
          content: intl.formatMessage({ id: 'page4.dialog.success' }),
          okText: intl.formatMessage({ id: 'login' }),
          onOk: () => {
            router.push('/login')
          },
        })
      } catch (err) {
        message({
          content: err?.data?.responseMessage,
          okText: intl.formatMessage({ id: 'page4.title' }),
          onOK: () => {},
        })
      }
    } else {
      message({
        content: intl.formatMessage({ id: 'page4.initial_error' }),
        okText: intl.formatMessage({ id: 'page4.title' }),
        onOK: () => {},
      })
    }
  }

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => gotoPrevious()}
        title={intl.formatMessage({ id: 'page4.title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mt-[6px]">
            <Controller
              name="password"
              control={control}
              rules={{
                required: intl.formatMessage({ id: 'page4.tips' }),
                validate: value =>
                  value === getValues('password_confirm') ||
                  intl.formatMessage({ id: 'page4.input.password_confirm.rules.confirm' }),
              }}
              render={({ onChange, value }) => (
                <Input
                  label={intl.formatMessage({ id: 'page4.input.password.label' })}
                  type="password"
                  onChange={onChange}
                  value={value}
                  error={errors?.password?.message}
                />
              )}
            />
          </div>
          <div className="mt-4">
            <Controller
              name="password_confirm"
              control={control}
              rules={{
                required: intl.formatMessage({ id: 'page4.tips' }),
                validate: value =>
                  value === getValues('password') ||
                  intl.formatMessage({ id: 'page4.input.password_confirm.rules.confirm' }),
              }}
              render={({ onChange, value }) => (
                <Input
                  label={intl.formatMessage({ id: 'page4.input.password_confirm.label' })}
                  type="password"
                  onChange={onChange}
                  value={value}
                  error={errors?.password_confirm?.message}
                />
              )}
            />
          </div>
          <div className="my-6">
            <div className="text-body-4 text-platinum-300">
              {intl.formatMessage({ id: 'page4.tips' })}
            </div>
          </div>
          <Button variant="primary" type="submit" disabled={!isDirty || !isValid}>
            {intl.formatMessage({ id: 'page4.button.finish' })}
          </Button>
        </form>
      </Container>
    </>
  )
}
