import { useIntl, FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import Link from 'next/link'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))

const SSC = dynamic(() => import('./components/SSC'))
const Ex5 = dynamic(() => import('./components/Ex5'))
const Fc3d = dynamic(() => import('./components/Fc3d'))
const Pl3 = dynamic(() => import('./components/Pl3'))
const Pk10 = dynamic(() => import('./components/Pk10'))
const Xyft = dynamic(() => import('./components/Xyft'))
const Lucky28 = dynamic(() => import('./components/Lucky28'))
const Mark6 = dynamic(() => import('./components/Mark6'))
const K3 = dynamic(() => import('./components/K3'))
const Dicecup = dynamic(() => import('./components/Dicecup'))

const IconBack = dynamic(() => import('components/icons/Back.svg'))
const CustomerService = dynamic(() => import('components/icons/CustomerService'))

const lottery_introduction_data = {
    SSC: <SSC />,
    Ex5: <Ex5 />,
    Fc3d: <Fc3d />,
    Pl3: <Pl3 />,
    Pk10: <Pk10 />,
    Xyft: <Xyft />,
    Lucky28: <Lucky28 />,
    Mark6: <Mark6 />,
    K3: <K3 />,
    Dicecup: <Dicecup />
}

function Question({ title, question, answer }) {
    const intl = useIntl()
    const router = useRouter()

    return (
        <>
            <NavigationBar
                fixed
                left={<IconBack />}
                right={<CustomerService />}
                onLeftClick={() => router.push(`/support/q/${title}`)}
                onRightClick={() => router.push('/service')}
                title={intl.formatMessage({ id: `title_${title}` })}
            />
            <div className="p-3 bg-white dark:bg-purple-800 text-gray-500 dark:text-white" style={{ marginTop: '52px' }}>
                {title !== 'lottery_introduction' ? (
                    <>
                        <div className="mb-2 text-title-8">
                            <FormattedMessage id={`${question}`} />
                        </div>
                        <div className="text-body-6">
                            <FormattedMessage id={`${answer}`} values={{ br: <br /> }} />
                        </div>
                    </>) : <>{lottery_introduction_data[answer]}</>
                }
            </div>
            <Link href="/support">
                <div className="px-3 py-8 w-full text-body-4 text-platinum-200 text-center">
                    <div className="px-4 py-3 border border-platinum-200 rounded-full">
                        <FormattedMessage id="goback" />
                    </div>
                </div>
            </Link>
        </>
    )
}

export async function getStaticPaths() {
    const data = (await import('constants/qa')).default
    const paths = []
    data.forEach(d => d.problem.forEach(p => {
        paths.push({
            params: {
                title: d.title,
                question: p.question,
            },
        })
    }))
    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const data = (await import('constants/qa')).default
    const { title, question } = params
    const getProblem = data.filter(d => d.title === title)[0]
    const getQuestion = getProblem.problem.filter(d => d.question === question)[0]
    return {
        props: {
            title,
            question: getQuestion.question,
            answer: getQuestion.answer
        },
    }
}

export default Question
