import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const Fc3d = () => {
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.fc3d`} />
			</span>

			<table className={`${style.table_style}`} type="fc3d">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.fc3d" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>21：15</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage
								id={`period_per_day`}
								values={{ number: '1' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage id={`period`} values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.betting_endtime" /></td>
						<td>20：00</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="fast3d">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.fast3d" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>21：15</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id={`period`}
								values={{ number: '288' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default Fc3d
