import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const Lucky28 = () => {
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.lucky28`} />
			</span>
			<table className={`${style.table_style}`} type="lucky28">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.lucky28" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>21:30</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage
								id={`period_per_day`}
								values={{ number: '1' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td><FormattedMessage id={`period`} values={{ number: '1' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.betting_endtime" /></td>
						<td>21:20</td>
					</tr>
				</tbody>
			</table>
			<span>
				<FormattedMessage id={`introduction_lucky28_list`} values={{ br: <br /> }} />
			</span>
		</div>
	)
}

export default Lucky28
