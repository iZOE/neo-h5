import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const Pk10 = () => {
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.pk10`} />
			</span>

			<table className={`${style.table_style}`} type="pk10">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.pk10" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：30～23：50</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '20' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage id={`period`} values={{ number: '44' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:20</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>23:40</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="f1racing">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.f1racing" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：01：15～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`second`} values={{ number: '75' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id={`period`}
								values={{ number: '1152' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="f2racing">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.f2racing" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：02：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '2' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id={`period`}
								values={{ number: '720' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="f3racing">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.f2racing" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：05：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id={`period`}
								values={{ number: '288' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="twracing">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.twracing" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>07：05：00～23：55：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '203' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="actracing">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.actracing" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：00：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`second`} values={{ number: '160' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							{'527 ~ '}
							<FormattedMessage
								id="period"
								values={{ number: '530' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
				</tbody>
			</table>

		</div>
	)
}

export default Pk10
