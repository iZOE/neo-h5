import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const SSC = () => {
	const zodiac = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id="intro.ssc" />
			</span>

			<table className={`${style.zodiac_table_style}`} type="zodiac">
				<tbody>
					<tr>
						{zodiac.map((z, index) => (
							<td key={`head_${index}.toString()`}>{z}</td>
						))}
					</tr>
					<tr>
						{zodiac.map((z, index) => (
							<td key={`body_${index}.toString()`}>
								<FormattedMessage id={`zodiac.${z}`} />
							</td>
						))}
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="cqc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.cqc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>07：30～23：50</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td><FormattedMessage id="minute" values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>50</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>07:28</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>23:48</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="tjc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.tjc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>09：20～23：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td><FormattedMessage id="minute" values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>42</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:13</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>22:53</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="xjc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.xjc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>10：20～02：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td><FormattedMessage id="minute" values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>48</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>10:18</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>01:58</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="txffc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.txffc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：01：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td><FormattedMessage id="minute" values={{ number: '1' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '1440' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="lottery1">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.lottery1" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：01：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="minute" values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '1440' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="lottery2">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.lottery2" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id="minute" values={{ number: '2' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '720' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '1440' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="lottery5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.lottery5" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：05：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="minute" values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '288' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="btcffc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.btcffc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：01：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="minute" values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '1440' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="wxffc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.wxffc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：01：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="minute" values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '1440' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="jnd30s">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.jnd30s" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：00：30～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="second" values={{ number: '30' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '2880' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="tg60">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.tg60" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：01：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="minute" values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '1440' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="twjc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.twjc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>07：05：00～23：55：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="minute" values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id="period"
								values={{ number: '203' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id="bet_anytime" />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="actjc">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.actjc" />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>00：00：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>
							<FormattedMessage id="second" values={{ number: '160' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							{'527 ~ '}
							<FormattedMessage
								id="period"
								values={{ number: '530' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default SSC
