import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const mark6List = [
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	[14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
	[24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
	[37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47]
]

const Mark6 = () => {
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.mark6`} />
			</span>

			<table className={`${style.table_style}`} type="mark6">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.mark6" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>21：30</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage
								id={`period_per_week`}
								values={{ number: '3' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.betting_endtime" /></td>
						<td>21：25</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="fmark6">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.fmark6" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：05：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			{mark6List.map((data) =>
				Array.isArray(data) ? (
					<section key={data} className="my-4">
						{data.map((text) => (
							<div key={text}>
								<FormattedMessage id={`intro.mark6_list.text${text}`} />
							</div>
						))}
					</section>
				) : (
					<div key={data}>
						<FormattedMessage id={`intro.mark6_list.text${data}`} />
					</div>
				)
			)}
		</div>
	)
}

export default Mark6
