import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const Ex5 = () => {
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.ex5`} />
			</span>

			<table className={`${style.table_style}`} type="sd11x5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id={`lottery_code.sd11x5`} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：01～23：01</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td><FormattedMessage id={`minute`} values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>43</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>08:59</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>22:59</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="jx11x5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id={`lottery_code.jx11x5`} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：30～23：10</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td><FormattedMessage id={`minute`} values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>42</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:28</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>23:08</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="gd11x5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id={`lottery_code.gd11x5`} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：30～23：10</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td><FormattedMessage id={`minute`} values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>42</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:28</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>23:08</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="sh11x5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id={`lottery_code.sh11x5`} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：20～00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td><FormattedMessage id={`minute`} values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>45</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:18</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>23:58</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="js11x5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id={`lottery_code.js11x5`} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>08：45～22：05</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td><FormattedMessage id={`minute`} values={{ number: '20' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>41</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>08:43</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>22:03</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="tw11x5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id={`lottery_code.tw11x5`} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>07：05～23：55</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td><FormattedMessage id={`minute`} values={{ number: '5' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>203</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>07:04:40</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>23:54:40</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="act11x5">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id={`lottery_code.act11x5`} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：00～24：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td><FormattedMessage id={`second`} values={{ number: '160' }} /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							{'527 ~ '}
							<FormattedMessage
								id="period"
								values={{ number: '530' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default Ex5
