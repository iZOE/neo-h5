import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const Xyft = () => {
  return (
    <div className="text-body-6 text-gray-500 dark:text-white">
      <span>
        <FormattedMessage id={`intro.xyft`} />
      </span>

      <table className={`${style.table_style}`} type="xyft">
        <tbody>
          <tr>
            <td><FormattedMessage id="columns.game_type" /></td>
            <td><FormattedMessage id="lottery_code.xyft" /></td>
          </tr>
          <tr>
            <td><FormattedMessage id="columns.lottery_time" /></td>
            <td>13：09 ~ 04：04</td>
          </tr>
          <tr>
            <td><FormattedMessage id="columns.period_interval" /></td>
            <td>
              <FormattedMessage id={`minute`} values={{ number: '5' }} />
            </td>
          </tr>
          <tr>
            <td><FormattedMessage id="columns.total_period" /></td>
            <td>
              <FormattedMessage
                id={`period`}
                values={{ number: '180' }}
              />
            </td>
          </tr>
          <tr>
            <td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
            <td>13:08</td>
          </tr>
          <tr>
            <td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
            <td>04:03</td>
          </tr>
        </tbody>
      </table>

    </div>
  )
}

export default Xyft
