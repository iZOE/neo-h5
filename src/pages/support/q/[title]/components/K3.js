import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const K3 = () => {
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.k3`} />
			</span>

			<table className={`${style.table_style}`} type="gxk3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.gxk3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：30～22：30</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '20' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>40</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:27</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>22:27</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="bjk3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.bjk3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：20～23：40</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '20' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>44</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:17</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>23:37</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="jsk3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.jsk3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>08：50～22：40</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '20' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>41</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>08:47</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>22:07</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="hubk3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.hubk3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>09：20～22：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '20' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>39</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>09:18</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>21:58</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="quick3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.quick3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：01：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id={`period`}
								values={{ number: '1440' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="hquick3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.hquick3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：05：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id={`period`}
								values={{ number: '288' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="twk3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.twk3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>07：05：00～23：55：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '5' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage
								id={`period`}
								values={{ number: '203' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>

			<table className={`${style.table_style}`} type="actk3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.actk3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：00：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`second`} values={{ number: '160' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							{'527 ~ '}
							<FormattedMessage
								id="period"
								values={{ number: '530' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.first_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.last_period_betting_endtime" /></td>
						<td>-</td>
					</tr>
				</tbody>
			</table>

		</div>
	)
}

export default K3
