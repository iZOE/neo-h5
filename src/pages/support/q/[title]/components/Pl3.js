import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const Pl3 = () => {

	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.pl3`} />
			</span>
			<table className={`${style.table_style}`} type="pl3">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.pl3" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>20：30</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage
								id={`period_per_day`}
								values={{ number: '1' }}
							/>
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.total_period" /></td>
						<td>
							<FormattedMessage id={`period`} values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.betting_endtime" /></td>
						<td>19：55</td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default Pl3
