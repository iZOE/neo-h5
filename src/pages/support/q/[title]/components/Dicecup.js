import { FormattedMessage } from 'react-intl'
import style from './table.module.scss'

const Dicecup = () => {
	return (
		<div className="text-body-6 text-gray-500 dark:text-white">
			<span>
				<FormattedMessage id={`intro.dicecup`} values={{ br: <br /> }} />
			</span>
			<table className={`${style.table_style}`} type="dicecup">
				<tbody>
					<tr>
						<td><FormattedMessage id="columns.game_type" /></td>
						<td><FormattedMessage id="lottery_code.dicecup" /></td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.lottery_time" /></td>
						<td>00：01：00～24：00：00</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.period_interval" /></td>
						<td>
							<FormattedMessage id={`minute`} values={{ number: '1' }} />
						</td>
					</tr>
					<tr>
						<td><FormattedMessage id="columns.betting_endtime" /></td>
						<td>
							<FormattedMessage id={`bet_anytime`} />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	)
}

export default Dicecup
