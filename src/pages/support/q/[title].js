import { useIntl, FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import Link from 'next/link'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const CustomerService = dynamic(() => import('components/icons/CustomerService'))
const ListItem = dynamic(() => import('components/core/ListItem'))
const ListItemText = dynamic(() => import('components/core/ListItemText'))

function Title({ title, questions }) {
    const intl = useIntl()
    const router = useRouter()
    return (
        <>
            <NavigationBar
                fixed
                left={<IconBack />}
                right={<CustomerService />}
                onLeftClick={() => router.push('/support')}
                onRightClick={() => router.push('/service')}
                title={intl.formatMessage({ id: `title_${title}` })}
            />
            <div className="mt-[52px]">
                {questions[0].map((item, index) => (
                    <Link
                        href={`/support/q/${title}/${item.question}`}
                    >
                        <ListItem
                            useAllow
                            height="auto"
                            key={`questions_${index.toString()}`}
                        >
                            <ListItemText>
                                <FormattedMessage id={`${item.question}`} />
                            </ListItemText>
                        </ListItem>
                    </Link>
                ))}
            </div>
            <Link href="/support">
                <div className="px-3 py-8 w-full text-body-4 text-platinum-200 text-center">
                    <div className="px-4 py-3 border border-platinum-200 rounded-full">
                        <FormattedMessage id="goback" />
                    </div>
                </div>
            </Link>
        </>
    )
}

export async function getStaticPaths() {
    const data = (await import('constants/qa')).default
    const paths = data.map((t) => ({
        params: { title: t.title },
    }))
    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const data = (await import('constants/qa')).default
    const questions = data
        .filter(x => x.title === params.title)
        .map(x => x.problem)
    return {
        props: {
            title: params.title,
            questions,
        },
    }
}

export default Title
