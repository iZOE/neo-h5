import { useEffect, useState } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import Link from 'next/link'
import styled from 'styled-components'
import SUPPORT_ITEM from 'constants/qa'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const ListItem = dynamic(() => import('components/core/ListItem'))
const ListItemText = dynamic(() => import('components/core/ListItemText'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const CustomerService = dynamic(() => import('components/icons/CustomerService.svg'))

const ListWrapper = styled.div`
  & button:nth-child(4n) {
    margin-bottom: 8px;
  }
`

const CommonListWrapper = styled.div.attrs({
  className: 'flex my-2 items-start bg-white dark:bg-purple-800 text-center',
})`
  & div {
    display: inherit;
    margin: 0;
    padding: 0;
    text-align: center;
  }
  & button {
    padding: 15px 0;
    height: inherit;
  }
  & .icon {
    display: inline-block;
  }
  & svg {
    width: 48px;
    height: 48px;
  }
  & button::after {
    display: none;
  }
`

function Support() {
  const router = useRouter()
  const intl = useIntl()
  const [randomIndex, setRandomIndex] = useState()

  useEffect(() => {
    const arr = [];
    while (arr.length < 4) {
      const r = Math.floor(Math.random() * (SUPPORT_ITEM.length)) + 1;
      if (arr.indexOf(r) === -1) arr.push(r);
    }
    setRandomIndex(arr)
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        right={<CustomerService />}
        onLeftClick={() => router.push('/user/setting')}
        onRightClick={() => router.push('/service')}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar noPadding>
        <CommonListWrapper>
          {randomIndex && randomIndex.map((i) => {
            const item = SUPPORT_ITEM[i - 1];
            return (
              <Link
                key={item.title}
                href={item.title === 'deposit_guide' ? '/support/deposit-guide' : `/support/q/${item.title}`}
              >
                <ListItem icon={item.icon}>
                  <ListItemText>
                    <FormattedMessage id={item.title} />
                  </ListItemText>
                </ListItem>
              </Link>
            )
          })}
        </CommonListWrapper>
        <ListWrapper>
          {SUPPORT_ITEM?.map((item, index) => (
            <ListItem
              useAllow
              icon={item.icon}
              key={`support_${index.toString()}`}
            >
              <Link href={item.title === 'deposit_guide' ? '/support/deposit-guide' : `/support/q/${item.title}`}>
                <ListItemText style={{ marginLeft: '16px' }}>
                  <FormattedMessage id={item.title} />
                </ListItemText>
              </Link>
            </ListItem>
          ))}
        </ListWrapper>
        <Link href="/service">
          <div className="px-3 py-8 w-full text-body-4 text-platinum-200 text-center">
            <div className="px-4 py-3 border border-platinum-200 rounded-full">
              <FormattedMessage id="cskh" />
            </div>
          </div>
        </Link>
      </Container>
    </>
  )
}

export default Support
