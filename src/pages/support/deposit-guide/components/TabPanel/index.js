import { FormattedMessage } from 'react-intl'
import Picture from 'components/core/Picture'

const agentCode = process.env.NEXT_PUBLIC_AGENT_CODE

function TabPanel({ depositWay }) {
  let data
  depositWay === 'manual' ? data = [1, 2, 3, 4] : data = [1, 2, 3, 4, 5, 6]

  return (
    <div className="px-4 py-6 bg-white dark:bg-purple-800 text-center text-platinum-200">
      {agentCode && data.map((t, index) =>
        <div key={`depositguide_${index.toString()}`}>
          <p className="mb-1 text-title-8">
            <FormattedMessage id={`${depositWay}.title${t}`} />
          </p>
          <p className="text-body-6">
            <FormattedMessage id={`${depositWay}.text${t}`} values={{ br: <br /> }} />
          </p>
          <div className="mt-2 mb-6">
            <Picture
              src={`/support/depositguide/${agentCode}/${depositWay}/${t}.png`}
              png={`/support/depositguide/${agentCode}/${depositWay}/${t}.png`}
              webp={`/support/depositguide/${agentCode}/${depositWay}/${t}.webp`}
              alt={t}
            />
          </div>
        </div>
      )}
    </div>
  )
}

export default TabPanel
