
import { useIntl, FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import styled from 'styled-components'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Tabs = dynamic(() => import('components/core/Tabs'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const CustomerService = dynamic(() => import('components/icons/CustomerService'))
const TabPanel = dynamic(() => import('./components/TabPanel'))

const tabsGroupStyles = {
  boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.2)',
  zIndex: '1'
}

const Wrapper = styled.div.withConfig({
  componentId: 'deposit-guide-tabs-active',
}).attrs({
  className: 'bg-gray-100 dark:bg-black'
})`
  & .tab-title {
    height: 40px;
    color: #95A3B0;
  }
  & .active {
    background: inherit;
    color: #51A1FF;
    border-bottom: 2px solid #51A1FF;
  }
`

function DepositGuide() {
  const router = useRouter()
  const intl = useIntl()
  const tabData = [
    {
      tabTitle: <FormattedMessage id={'deposit_way.1'} />,
      tabPanel: <TabPanel depositWay="alipay" />,
    },
    {
      tabTitle: <FormattedMessage id={'deposit_way.2'} />,
      tabPanel: <TabPanel depositWay="manual" />,
    },
    {
      tabTitle: <FormattedMessage id={'deposit_way.3'} />,
      tabPanel: <TabPanel depositWay="crypto" />,
    },
  ]

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        right={<CustomerService />}
        onLeftClick={() => router.push('/support')}
        onRightClick={() => router.push('/service')}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar noPadding>
        <Wrapper>
          <Tabs
            data={tabData}
            initActiveIndex={0}
            fontSize="12px"
            lineHeight="40px"
            textAlign
            {...tabsGroupStyles}
          />
        </Wrapper>
      </Container>
    </>
  )
}

export default DepositGuide
