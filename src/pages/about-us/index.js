import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import NavigationBar from 'components/core/NavigationBar'
import Picture from 'components/core/Picture'
import Logo from 'components/shared/Logo'
import BackgroundDecoration from './components/BackgroundDecoration'

const IconBack = dynamic(() => import('components/icons/Back'))

const AboutUs = () => {
  const router = useRouter()
  const version = '1.2.3'
  const mediaPath = `/pages/about-us/${process.env.NEXT_PUBLIC_AGENT_CODE}`

  return (
    <div className="pt-11 sm:pt-12">
      <div className="relative z-10">
        <NavigationBar
          fixed
          left={<IconBack />}
          onLeftClick={() => router.push('/user/setting')}
          title={<FormattedMessage id="title" />}
        />

        <div className="w-full h-full overflow-hidden px-8">
          <div
            onClick={() => router.push('/')}
            className="block h-auto my-8 mx-auto"
            style={{ width: 120 }}>
            <Logo />
          </div>

          <p className="text-body-6 text-platinum-200 mb-12">
            <FormattedMessage id="intro" values={{ br: <br /> }} />
          </p>
          <h4 className="text-body-6 text-platinum-200 text-center mb-2">
            <FormattedMessage id="partner" />
          </h4>

          <Picture
            webp={`${mediaPath}/sponsor.webp`}
            png={`${mediaPath}/sponsor.png`}
            alt="sponsors"
            className="mb-2 mx-auto"
          />

          <span className="text-body-6 text-platinum-200 block text-center font-semibold">
            Ver. {version}
          </span>
        </div>
      </div>
      <BackgroundDecoration mediaPath="/pages/about-us/bg" />
    </div>
  )
}

export default AboutUs
