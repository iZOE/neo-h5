import Picture from 'components/core/Picture'

const BackgroundDecoration = () => {
  const mediaPath = '/pages/about-us/bg'

  return (
    <div className="w-full h-full absolute z--1 inset-0 overflow-hidden bg-blue-50 dark:bg-gray-700">
      <Picture webp={`${mediaPath}.webp`} png={`${mediaPath}.png`} />
    </div>
  )
}

export default BackgroundDecoration
