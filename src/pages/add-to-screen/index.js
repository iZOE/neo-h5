import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import NavigationBar from 'components/core/NavigationBar'
import Container from 'components/core/Container'
import IconBack from 'components/icons/Back.svg'
import BackgroundWrapper from './components/BackgroundWrapper'
import Tutorial from './components/Tutorial'

const Wrapper = styled.div`
  position: relative;
  background-color: #FFFFFF;
  padding: 15px 0;
  text-align: center;
  z-index: 2;
  & .core-img {
    display: inline-block;
    width: 50%;
  }
`

function AddToScreen() {
  const router = useRouter()
  const { device } = router.query

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/download')}
        title={<FormattedMessage id="title" values={{ device }} />}
      />
      <Container withNavigationBar bg="inherit" noPadding>
        <Wrapper>
          <Tutorial device={device.toLowerCase()} />
        </Wrapper>
        <BackgroundWrapper />
      </Container>
    </>
  )
}

export default AddToScreen
