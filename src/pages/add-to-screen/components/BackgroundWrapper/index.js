import { useMemo } from 'react'
import styled from 'styled-components'
import Picture from 'components/core/Picture'

const Wrapper = styled.div`
  & img {
    min-height: 100vh;
    min-height: -webkit-fill-available;
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }
`

function BackgroundWrapper() {
  const { bgWebp, bgPng } = useMemo(() => {
    const webpSource = `/download/${process.env.NEXT_PUBLIC_AGENT_CODE}/bg@2x.webp`
    const pngSource = `/download/${process.env.NEXT_PUBLIC_AGENT_CODE}/bg@2x.png`

    return {
      bgWebp: webpSource,
      bgPng: pngSource,
    }
  }, [])

  if (!process.browser) {
    return null
  }

  return (
    <Wrapper className="fixed inset-0">
      <Picture src={bgPng} png={bgPng} webp={bgWebp} />
    </Wrapper>
  )
}

export default BackgroundWrapper
