/* eslint-disable no-confusing-arrow */
import { useIntl } from 'react-intl'
import dynamic from 'next/dynamic'

const Picture = dynamic(() => import('components/core/Picture'))

const agentCode = process.env.NEXT_PUBLIC_AGENT_CODE

function Tutorial({ device }) {
  const intl = useIntl()
  return [0, 1, 2, 3].map(s =>
    s === 0 ? (
      <section key={s}>
        <div className="text-body-5 text-center text-blue-200 mb-5">
          {intl.formatMessage({ id: `${device}_PWA_tutorial.${s}` }, { br: <br /> })}
        </div>
      </section>
    ) : (
      <section key={s}>
        {agentCode && (
          <Picture
            src={`/addtoscreen/${agentCode}/${device}/${s}.png`}
            png={`/addtoscreen/${agentCode}/${device}/${s}.png`}
            webp={`/addtoscreen/${agentCode}/${device}/${s}.webp`}
            alt={s.title}
          />
        )}
        <div
          className="text-body-5 text-center text-blue-200 mt-4"
          style={{ marginBottom: '65px' }}
        >
          {intl.formatMessage({ id: `${device}_PWA_tutorial.${s}` }, { br: <br /> })}
        </div>
      </section>
    ),
  )
}

export default Tutorial
