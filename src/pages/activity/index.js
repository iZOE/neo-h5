import { useMemo, useState } from 'react'
import { useIntl } from 'react-intl'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import LinkActivity from './components/LinkActivity'

const PageTransition = dynamic(() => import('components/shared/PageTransition'))
const WithTabBar = dynamic(() => import('components/layout/WithTabBar'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const SwitchTab = dynamic(() => import('components/core/SwitchTab'))
const Menu = dynamic(() => import('components/core/Menu'))

function Activity() {
  const intl = useIntl()
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/playinfo/activity/type`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      const items = res?.data?.data
      if (items && items.length > 0) {
        return [
          {
            id: 0,
            name: intl.formatMessage({ id: 'all' }),
            sortOrder: 0,
          },
          ...items,
        ]
      }
      return []
    },
    {
      revalidateOnFocus: false,
    },
  )
  const tabs = useMemo(
    () => [
      { title: intl.formatMessage({ id: 'tabs.activity' }), path: '/activity' },
      { title: intl.formatMessage({ id: 'tabs.noticeboard' }), path: '/notice-board' },
    ],
    [],
  )
  const [currentSelectType, setCurrentSelectType] = useState(0)

  return (
    <PageTransition style={{ height: '-webkit-fill-available' }}>
      <NavigationBar fixed centerComponent={<SwitchTab data={tabs} />} />
      <Menu
        data={data}
        onTypeChange={id => {
          setCurrentSelectType(id)
        }}
      />
      <Container withNavigationBar>
        <div className="pt-12">
          <LinkActivity typeId={currentSelectType} />
        </div>
      </Container>
    </PageTransition>
  )
}

Activity.protect = true
Activity.TabBar = WithTabBar

export default Activity
