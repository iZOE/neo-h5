import dynamic from 'next/dynamic'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'

const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))
const Card = dynamic(() => import('../Card'))
const Empty = dynamic(() => import('../Empty'))

function LinkActivity({ typeId }) {
  const { data: list } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/playinfo/activity`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
  )

  const filterData = typeId !== 0 ? list.filter(t => t.type === typeId) : list

  return (
    <>
      {!list && <Skeleton />}
      {list && !list.length && <Empty />}
      {list &&
        <div style={{ paddingBottom: 60 }}>
          {filterData?.map((d, index) => (
            <Card d={d} key={`list_${index.toString()}`} />
          ))}
        </div>
      }
    </>
  )
}

export default LinkActivity
