/* eslint-disable no-unused-expressions */
import React from 'react'
import { useRouter } from 'next/router'
import Picture, { getFullPath } from '../../../../components/core/Picture'

export default function Card({ d }) {
  const router = useRouter()

  return (
    d.imageUrl && (
      <Picture
        className="w-full h-auto mb-2 rounded-md shadow-b-2"
        onClick={() => {
          d.targetUrl ? router.push(d.targetUrl) : console.log('no URL')
        }}
        src={getFullPath(d.imagePath, 'jpg')}
        webp={getFullPath(d.imagePath, 'webp')}
        png={getFullPath(d.imagePath, 'png')}
        alt={d.name}
      />
    )
  )
}
