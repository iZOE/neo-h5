import { FormattedMessage } from 'react-intl'
import IconNoActivity from 'components/icons/NoActivity'
import EmptyWithIcon from 'components/core/EmptyWithIcon'

function Empty() {
  return (
    <div className="absolute top-0 bottom-0 right-0 left-0 m-auto w-max" style={{ height: '125px' }}>
      <EmptyWithIcon
        icon={<IconNoActivity width="110" height="110" fillColor="#B9C0C7" />}
        text={<FormattedMessage id="emptyMessage" />}
        textColor=""
      />
    </div>
  )
}

export default Empty
