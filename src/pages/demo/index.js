import { useEffect, useState } from 'react'
import Link from 'next/link'
import QRCode from 'qrcode.react'

function Demo() {
  const [mobileconfig, setMobileconfig] = useState()
  useEffect(() => {
    setMobileconfig(`${window.location.origin}/neoh5.mobileconfig`)
  }, [])

  return (
    <div className="h-screen w-full text-platinum-300 flex items-center justify-center">
      <Link href="/neoh5.mobileconfig">
        <div>{mobileconfig && <QRCode value={mobileconfig} />}</div>
      </Link>
    </div>
  )
}

// Demo.protect = true

export default Demo
