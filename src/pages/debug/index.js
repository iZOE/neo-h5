import useSWR from 'swr'
import styled from 'styled-components'

function Debug() {
  return (
    <div>
      <p>TAG: {process.env.NEXT_PUBLIC_TAG}</p>
      <p>END_POINT_PORTAL: {process.env.NEXT_PUBLIC_END_POINT_PORTAL}</p>
      <p>AGENT_LOCALE_LANGUAGE: {process.env.NEXT_PUBLIC_AGENT_LOCALE_LANGUAGE}</p>
      <p>CHECK_API_URL: {process.env.NEXT_PUBLIC_CHECK_API_URL}</p>
      <p>AGENT_CODE: {process.env.NEXT_PUBLIC_AGENT_CODE}</p>
    </div>
  )
}

export default Debug
