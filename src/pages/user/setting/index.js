import { useRouter } from 'next/router'
import { useIntl, FormattedMessage } from 'react-intl'
import { useRecoilState } from 'recoil'
import useSession from 'hooks/useSession'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import { darkModeState } from 'atoms/darkModeState'
import { reportSizeState } from 'atoms/reportSizeState'
import useRouterPushBeforeCheckLogin from 'hooks/useRouterPushBeforeCheckLogin'
import useLocalStorage from 'hooks/useLocalStorage'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Button = dynamic(() => import('components/core/Button'))
const Container = dynamic(() => import('components/core/Container'))
const List = dynamic(() => import('components/core/List'))
const ListItem = dynamic(() => import('components/core/ListItem'))
const ListItemText = dynamic(() => import('components/core/ListItemText'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

export default function UserSetting() {
  const intl = useIntl()
  const router = useRouter()
  const pushBeforeCheck = useRouterPushBeforeCheckLogin()
  const [hasSession] = useSession()
  const [darkMode, setDarkMode] = useRecoilState(darkModeState)
  const [reportSize, setReportSize] = useRecoilState(reportSizeState)
  const [token, setToken, remove] = useLocalStorage('token')
  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/user')}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar noPadding>
        <div className="mt-2">
          <List>
            <ListItem
              useSwitch
              value={darkMode}
              onClick={() => {
                setDarkMode(prev => !prev)
              }}
            >
              <ListItemText>
                <FormattedMessage id="dark_mode" />
              </ListItemText>
            </ListItem>
            <ListItem
              useSwitch
              value={reportSize}
              onClick={() => {
                if (hasSession) {
                  setReportSize(prev => !prev)
                } else {
                  pushBeforeCheck()
                }
              }}
            >
              <ListItemText>
                <FormattedMessage id="font_size_bigger" />
              </ListItemText>
            </ListItem>
          </List>
          <List>
            <Link href="/support">
              <ListItem useAllow>
                <ListItemText>
                  <FormattedMessage id="support" />
                </ListItemText>
              </ListItem>
            </Link>
            <Link href="/service">
              <ListItem useAllow>
                <ListItemText>
                  <FormattedMessage id="feeback" />
                </ListItemText>
              </ListItem>
            </Link>
            <Link href="/about-us">
              <ListItem useAllow>
                <ListItemText>
                  <FormattedMessage id="about_us" />
                </ListItemText>
              </ListItem>
            </Link>
          </List>
        </div>
      </Container>
    </>
  )
}
