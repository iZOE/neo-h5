import { useCallback, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useIntl, FormattedMessage } from 'react-intl'
import { useForm, Controller } from 'react-hook-form'
import useSWR from 'swr'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import Bound from './components/Bound'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const emailValidator = {
  required: true,
  pattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
}

function UserSettingInfoEmail() {
  const intl = useIntl()
  const router = useRouter()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const { data: userData, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
    {
      revalidateOnFocus: false,
    },
  )
  const { data: security, error: securityError } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security`,
    url => fetcher({ url, withToken: true }),
    {
      revalidateOnFocus: false,
    },
  )

  const securityData = security?.data?.data

  const watchEmail = watch()

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default
    try {
      setIsSubmitting(true)
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/email?email=${formData.email}`,
        method: 'put',
        withToken: true,
      })
      if (res.data.code === '0000') {
        message({
          content: intl.formatMessage({ id: 'dialog.success' }),
          okText: intl.formatMessage({ id: 'confirm' }),
          closable: false,
          onOk: () => {
            router.push('/user/setting/info')
          },
        })
      } else {
        setIsSubmitting(false)
        alert('no ok')
      }
    } catch (err) {
      setIsSubmitting(false)
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  const clearValue = useCallback(() => {
    setValue('email', '')
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={async () => {
          if (isDirty) {
            const message = (await import('components/core/Alert/message')).default
            message({
              content: intl.formatMessage({ id: 'dialog.back_alert.content' }),
              okText: intl.formatMessage({ id: 'confirm' }),
              cancelText: intl.formatMessage({ id: 'cancel' }),
              onOk: () => {
                router.push('/user/setting/info')
              },
            })
          } else {
            router.push('/user/setting/info')
          }
        }}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar>
        {userData ? (
          <>
            {userData.email && securityData?.emailVerified === 2 ? (
              <Bound email={userData.maskEmail} />
            ) : (
              <div className="mt-4">
                <form onSubmit={handleSubmit(onSubmit)}>
                  <Controller
                    defaultValue={userData.email}
                    name="email"
                    control={control}
                    rules={emailValidator}
                    render={({ onChange, value }) => (
                      <Input
                        type="email"
                        placeholder={intl.formatMessage({ id: 'input.placeholder' })}
                        onChange={onChange}
                        width={1}
                        value={value}
                        withDelete
                        clearValue={clearValue}
                      />
                    )}
                  />
                  {errors?.email?.type === 'pattern' && (
                    <div className="text-red mt-1 text-body-8">
                      <FormattedMessage id="input.validate" />
                    </div>
                  )}
                  <div className="h-6" />
                  <div className="flex">
                    <div className="flex-1">
                      <Button
                        variant="primary"
                        type="submit"
                        isLoading={isSubmitting}
                        disabled={isSubmitting || !isValid || watchEmail.email === ''}
                      >
                        {intl.formatMessage({ id: 'button' })}
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            )}
          </>
        ) : (
          <div>loading</div>
        )}
      </Container>
    </>
  )
}

UserSettingInfoEmail.protect = true

export default UserSettingInfoEmail
