import dynamic from 'next/dynamic'
import Link from 'next/link'
import { FormattedMessage } from 'react-intl'

const Button = dynamic(() => import('components/core/Button'))

export default function Bound({ email }) {
  return (
    <div className="mt-4">
      <p className="text-platinum-300 text-body-4">
        <FormattedMessage id="bound_page.tip_1" />
      </p>
      <p className="mt-2 text-blue-200 text-body-4">{email || '--'}</p>
      <p className="mt-2 text-platinum-300 text-body-4">
        <FormattedMessage id="bound_page.tip_2" />
      </p>
      <div className="mt-6">
        <Link href="/service">
          <Button variant="secondary">
            <FormattedMessage id="contact_customer" />
          </Button>
        </Link>
      </div>
    </div>
  )
}
