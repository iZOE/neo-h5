const hiddenThePhone = originPhoneNumber => {
  if (!originPhoneNumber) {
    return ''
  }

  const strPhone = `${originPhoneNumber}`

  const lastIdx = strPhone.length - 1
  const showNumberIdx = lastIdx - 5 // 隱碼規則：僅顯示後五碼
  const qqArr = Array.from(strPhone)
  return qqArr.reduce((acc, curr, idx) => {
    if (idx > showNumberIdx) {
      acc += curr
      return acc
    }
    return (acc += '*')
  }, '')
}

export default hiddenThePhone
