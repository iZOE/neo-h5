import { useEffect } from 'react'
import { useIntl } from 'react-intl'
import useCountdown from 'hooks/useCountdown'

const toMMSS = secs => {
  let mm = Math.floor(secs / 60)
  let ss = secs % 60

  if (mm < 10) {
    mm = `0${mm}`
  }

  if (ss < 10) {
    ss = `0${ss}`
  }

  return `${mm}:${ss}`
}

export default function ExpiredTime({ sec, collector, whenMappingCodeExpired }) {
  const intl = useIntl()
  const [expireTimeCountDown, resetExpireTime] = useCountdown(sec)

  useEffect(() => {
    collector.current.resetExpireTime = resetExpireTime
  }, [resetExpireTime])

  if (expireTimeCountDown < 1) {
    whenMappingCodeExpired()
  }

  return (
    <>
      {expireTimeCountDown < 1 ? (
        <div className="text-body-6 text-platinum-300">
          {intl.formatMessage({ id: 'mapping_code_is_expired' })}
        </div>
      ) : (
        <div className="text-body-6 text-platinum-300">
          {intl.formatMessage(
            { id: 'mapping_code_will_expire' },
            { mmss: toMMSS(expireTimeCountDown) },
          )}
        </div>
      )}
    </>
  )
}
