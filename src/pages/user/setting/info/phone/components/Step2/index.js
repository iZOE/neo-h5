import { useCallback, useState, useMemo, useRef } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { useStepData } from 'hooks/useStep'
import ExpiredTime from './components/ExpiredTime'
import GetCodeButton from './components/GetCodeButton'
import hiddenThePhone from '../../libs'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const MAPPING_CODE_FORM_FIELD_NAME = 'MappingCode'

const mappingCodeValidator = {
  required: value => !!value,
  length: value => {
    if (value.length !== 4) {
      return false
    }
    return true
  },
}

const mappingCodeErrorMsg = {
  required: <FormattedMessage id="validate.MappingCode.required" />,
  length: <FormattedMessage id="validate.MappingCode.length" />,
}

export default function PhoneSettingStep2() {
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isMappingCodeDisabled, setIsMappingCodeDisabled] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const resetHolder = useRef({})

  const { contactNumber, expiredTime, lockDuration } = stepData

  const cachedHiddenPhone = useMemo(() => hiddenThePhone(contactNumber), [contactNumber])

  const watchMappingCode = watch(MAPPING_CODE_FORM_FIELD_NAME, '')

  const onGetCodeClick = async () => {
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/contact?ContactNumber=${contactNumber}`,
        method: 'put',
        withToken: true,
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        const { resetExpireTime, resetLockDuration } = resetHolder.current
        resetExpireTime && resetExpireTime(res.data?.data?.expiredTime)
        resetExpireTime && setIsMappingCodeDisabled(prev => false)
        resetLockDuration && resetLockDuration(res.data?.data?.lockDuration)
      }
    } catch (error) {
      console.log('error', error)
      setIsSubmitting(prev => false)
      const messages = error?.data?.responseMessage

      message({
        content: messages,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }

  const onSubmit = useCallback(async formData => {
    const _mappingCode = formData.MappingCode
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/confirmContact?MappingCode=${_mappingCode}`,
        method: 'put',
        withToken: true,
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        message({
          content: intl.formatMessage({ id: 'edit_success' }),
          okText: intl.formatMessage({ id: 'confirm' }),
          onOk: () => {
            router.push('/user/setting/info')
          },
          closable: false,
        })
      }
    } catch (error) {
      setIsSubmitting(prev => false)
      const messages = error?.data?.responseMessage

      message({
        content: messages,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    if (!isDirty) {
      router.push('/user/setting/info')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/user/setting/info')
        },
      })
    }
  }, [isDirty])

  const clearValue = useCallback(() => {
    setValue(MAPPING_CODE_FORM_FIELD_NAME, '')
  }, [])

  const whenMappingCodeExpired = useCallback(() => {
    setIsMappingCodeDisabled(prev => true)
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'step2.title' })}
      />
      <Container withNavigationBar>
        <div className="mt-4 text-platinum-300 text-body-4">
          {intl.formatMessage({ id: 'mapping_code_sent' })}
        </div>
        <div className="mt-2 mb-4 text-blue-200 text-body-4">{cachedHiddenPhone}</div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            name={MAPPING_CODE_FORM_FIELD_NAME}
            control={control}
            rules={{ validate: mappingCodeValidator }}
            render={({ onChange, value }) => (
              <Input
                placeholder={intl.formatMessage({ id: 'step2.placeholder' })}
                onChange={onChange}
                width={1}
                disabled={isMappingCodeDisabled}
                type="number"
                value={value}
                withDelete
                clearValue={clearValue}
              />
            )}
          />
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {mappingCodeErrorMsg[errors?.MappingCode?.type]}
            </div>
          )}
          <div className="h-1" />
          <ExpiredTime
            sec={expiredTime}
            collector={resetHolder}
            whenMappingCodeExpired={whenMappingCodeExpired}
          />
          <div className="flex mt-6">
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={isSubmitting || !watchMappingCode || !isDirty || !isValid}
              >
                {intl.formatMessage({ id: 'step2.submit' })}
              </Button>
            </div>
          </div>
        </form>
        <div className="flex justify-center mt-6">
          <GetCodeButton
            isSubmitting={isSubmitting}
            sec={lockDuration}
            onClick={onGetCodeClick}
            collector={resetHolder}
          />
        </div>
      </Container>
    </>
  )
}
