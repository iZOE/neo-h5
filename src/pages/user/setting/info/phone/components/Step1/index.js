import { useCallback, useState, useEffect, useMemo } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import useStep from 'hooks/useStep'
import hiddenThePhone from '../../libs'

const Input = dynamic(() => import('components/core/Input'))
const Select = dynamic(() => import('components/core/Select'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const AlreadyVertifiedCanNotEdit = dynamic(() =>
  import('components/shared/AlreadyVertifiedCanNotEdit'),
)

const PHONE_NUMBER_FORM_FIELD_NAME = 'ContactNumber'

// length驗證越南 手機號 10位 / 中國 11位
const phoneValidator = {
  required: value => !!value,
  length: value => {
    const phoneNumberLength = process.env.NEXT_PUBLIC_AGENT_CODE === 'VT999' ? 10 : 11
    if (value.length !== phoneNumberLength) {
      return false
    }
    return true
  },
}

const contactNumberErrorMsg = {
  required: <FormattedMessage id="validate.ContactNumber.required" />,
  length: <FormattedMessage id="validate.ContactNumber.length" />,
}
const OPTIONS = JSON.parse(process.env.NEXT_PUBLIC_PHONE_COUNTRY_CODE_LIST)

const isShouldDisabled = (isSubmitting, watchContactNumber, isDirty, isValid) => {
  if (isValid) {
    return false
  }

  return isSubmitting || !watchContactNumber || !isDirty || !isValid
}

export default function PhoneSettingStep1() {
  const intl = useIntl()
  const router = useRouter()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    getValues,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const watchContactNumber = watch(PHONE_NUMBER_FORM_FIELD_NAME, '')

  const { gotoNext } = useStep()
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security`,
    url => fetcher({ url, withToken: true }),
    {
      revalidateOnFocus: false,
    },
  )

  const securityData = data?.data?.data

  const [isPhoneVerified, setIsPhoneVerified] = useState(false)

  useEffect(() => {
    if (securityData?.contactVerified === 2) {
      setIsPhoneVerified(prev => true)
    }
  }, [securityData?.contactVerified])

  useEffect(() => {
    setValue(PHONE_NUMBER_FORM_FIELD_NAME, securityData?.phone, { shouldValidate: true })
  }, [securityData?.phone])

  const onSubmit = useCallback(async formData => {
    const _contactNumber = formData[PHONE_NUMBER_FORM_FIELD_NAME]
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/contact?ContactNumber=${_contactNumber}`,
        method: 'put',
        withToken: true,
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        gotoNext({
          contactNumber: res.data.data.contactNumber,
          expiredTime: res.data.data.expiredTime,
          lockDuration: res.data.data.lockDuration,
        })
      }
    } catch (error) {
      console.log('error', error)
      setIsSubmitting(prev => false)
      const messages = error?.data?.responseMessage

      message({
        content: messages,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    if (!isDirty) {
      router.push('/user/setting/info')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/user/setting/info')
        },
      })
    }
  }, [isDirty])

  const clearValue = useCallback(() => {
    setValue(PHONE_NUMBER_FORM_FIELD_NAME, '', { shouldValidate: true })
  }, [])

  const cachedHiddenPhone = useMemo(() => hiddenThePhone(securityData?.phone), [
    securityData?.phone,
  ])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'step1.title' })}
      />
      <Container withNavigationBar>
        {isPhoneVerified ? (
          <AlreadyVertifiedCanNotEdit
            alreadyVertifiedSubject={intl.formatMessage({ id: 'verified_kind_name' })}
            hiddenCodeContent={cachedHiddenPhone}
          />
        ) : (
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="mt-4" />
            <Controller
              name="locale"
              control={control}
              render={({ onChange, value }) => (
                <Select
                  label={intl.formatMessage({ id: 'select_label' })}
                  options={OPTIONS}
                  name="country"
                  onChange={onChange}
                />
              )}
            />
            <div className="mt-4" />
            <div className="mt-4">
              <Controller
                name={PHONE_NUMBER_FORM_FIELD_NAME}
                control={control}
                rules={{ validate: phoneValidator }}
                render={({ onChange, value }) => (
                  <Input
                    label={intl.formatMessage({ id: 'phone_label' })}
                    placeholder={intl.formatMessage({ id: 'step1.placeholder' })}
                    onChange={onChange}
                    width={1}
                    type="number"
                    value={value}
                    withDelete
                    clearValue={clearValue}
                  />
                )}
              />
            </div>
            {errors && (
              <div className="text-red mt-1 text-body-8">
                {contactNumberErrorMsg[errors?.ContactNumber?.type]}
              </div>
            )}
            <div className="h-6" />
            <div className="flex">
              <div className="flex-1">
                <Button
                  variant="primary"
                  isLoading={isSubmitting}
                  type="submit"
                  disabled={isShouldDisabled(isSubmitting, watchContactNumber, isDirty, isValid)}
                >
                  {intl.formatMessage({ id: 'step1.submit' })}
                </Button>
              </div>
            </div>
          </form>
        )}
      </Container>
    </>
  )
}
