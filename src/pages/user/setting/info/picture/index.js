import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { useStepView } from '../../../../../hooks/useStep'

const Step1Component = dynamic(() => import('./components/Step1'))
const Step2Component = dynamic(() => import('./components/Step2'))

const STEP_VIEW = {
  1: Step1Component,
  2: Step2Component,
}

const MIN_STEP = 1
const MAX_STEP = 2

function SetPicture({ data }) {
  const ViewComponent = useStepView(STEP_VIEW, MIN_STEP, MAX_STEP)
  return <ViewComponent avatarData={data} />
}
export async function getStaticProps() {
  const res = await fetcher({
    url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/web/account/icon?agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`,
  })
  const { data } = res?.data
  data.pop()
  return {
    props: {
      data,
    },
    revalidate: 1,
  }
}

export default SetPicture
