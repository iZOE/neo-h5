import styled from 'styled-components'
import { useState, useCallback } from 'react'
import { useStepData } from 'hooks/useStep'
import { useIntl } from 'react-intl'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import fetcher from 'libs/fetcher'
import getCroppedImg from 'libs/canvasUtils'
import Cropper from 'react-easy-crop'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

export default function Step2() {
  const intl = useIntl()
  const router = useRouter()
  const [crop, setCrop] = useState({ x: 0, y: 0 })
  const [zoom, setZoom] = useState(1)
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels)
  }, [])

  const stepData = useStepData()
  if (!stepData) {
    return null
  }

  const { imageSrc } = stepData

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const handleImageUpload = useCallback(async () => {
    setIsLoading(true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const croppedAvatar = await getCroppedImg(imageSrc, croppedAreaPixels)
      const avatarData = { originalUpload: croppedAvatar, fileAfterPrefix: croppedAvatar }
      const formData = new FormData()
      const keys = Object.keys(avatarData)
      for (let i = 0; i < keys.length; i += 1) {
        const key = keys[i]
        formData.append(key, avatarData[key])
      }
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/avatar`,
        method: 'post',
        withToken: true,
        data: formData,
      })
      if (res?.data?.code === '0000') {
        message({
          content: intl.formatMessage({ id: 'setting_success' }),
          okText: intl.formatMessage({ id: 'ok' }),
          onOk: () => {
            router.push('/user/setting/info')
          },
          closable: false,
        })
        setIsLoading(false)
      } else {
        message({
          content: res?.data?.responseMessage,
          okText: intl.formatMessage({ id: 'ok' }),
          onOk: () => {},
          closable: false,
        })
        setIsLoading(false)
      }
    } catch (error) {
      message({
        content: error?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'ok' }),
        onOk: () => {},
        closable: false,
      })
      setIsLoading(false)
    }
  }, [imageSrc, croppedAreaPixels])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={intl.formatMessage({ id: 'crop_avatar' })}
      />
      <Container withNavigationBar>
        <div className="absolute w-full inset-x-0" style={{ height: '320px' }}>
          <Cropper
            image={imageSrc}
            crop={crop}
            zoom={zoom}
            aspect={1 / 1}
            cropShape="round"
            showGrid={false}
            cropSize={{ width: 240, height: 240 }}
            onCropChange={setCrop}
            onCropComplete={onCropComplete}
            onZoomChange={setZoom}
          />
        </div>
        <div className="relative" style={{ marginTop: '344px' }}>
          <Button
            variant="primary"
            text={intl.formatMessage({ id: 'ok' })}
            isLoading={isLoading}
            onClick={handleImageUpload}
          />
        </div>
      </Container>
    </>
  )
}
