import styled, { useTheme } from 'styled-components'
import useSWR from 'swr'
import { useState, useEffect, useCallback } from 'react'
import { useIntl } from 'react-intl'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import fetcher from 'libs/fetcher'
import useStep from 'hooks/useStep'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const Picture = dynamic(() => import('components/core/Picture'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const IconCheck = dynamic(() => import('components/icons/Check.svg'))
const IconCamera = dynamic(() => import('components/icons/Camera'))

function readFile(file) {
  return new Promise(resolve => {
    const reader = new FileReader()
    reader.addEventListener('load', () => resolve(reader.result), false)
    reader.readAsDataURL(file)
  })
}

function Avatar({ avatarSrc, onclick }) {
  return (
    <div
      className="relative inline-block m-3 rounded-full overflow-hidden cursor-pointer"
      style={{ width: '64px', height: '64px' }}
      onClick={onclick}
    >
      <Picture src={avatarSrc} webp={avatarSrc} png={avatarSrc} />
    </div>
  )
}
export default function Step1({ avatarData }) {
  const intl = useIntl()
  const router = useRouter()
  const theme = useTheme()
  const { gotoNext } = useStep()
  const [selectedAvatar, setSelectedAvatar] = useState({ id: 9999, url: null })
  const [isLoading, setIsLoading] = useState(false)
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
    {
      revalidateOnFocus: false,
    },
  )
  if (error) {
    alert('error')
  }
  const userAvatarUrl = data?.avatorOriginal

  useEffect(() => {
    const originAvatar = avatarData.filter(item => userAvatarUrl === item.url)
    if (originAvatar.length > 0) {
      setSelectedAvatar(originAvatar[0])
    } else {
      setSelectedAvatar({ id: 999, url: userAvatarUrl })
    }
  }, [userAvatarUrl])

  const handleSelectAvatar = useCallback(id => {
    setSelectedAvatar(avatarData[id])
  }, [])

  const handleOnSetAvatar = useCallback(async () => {
    setIsLoading(true)
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/avatarUrl?id=${selectedAvatar.id}`,
        method: 'put',
        withToken: true,
      })
      if (res.data.code === '0000') {
        setIsLoading(false)
        router.push('/user/setting/info')
      }
    } catch (err) {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'ok' }),
        onOk: () => { },
        closable: false,
      })
      setIsLoading(false)
    }
  }, [selectedAvatar])

  const onFileChange = useCallback(async e => {
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0]
      const imageDataUrl = await readFile(file)
      gotoNext({
        imageSrc: imageDataUrl,
      })
    }
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/user/setting/info')}
        title={intl.formatMessage({ id: 'select_avatar' })}
      />
      <Container withNavigationBar>
        <div
          className="relative my-3 mx-auto text-center"
          style={{ maxWidth: '300px', minWidth: '255px', height: '88px' }}
        >
          <Avatar avatarSrc={selectedAvatar?.url} />
        </div>
        <div
          className="absolute inset-x-0"
          style={{ background: theme.darkMode ? '#2A2A38' : '#fff' }}
        >
          <div className="relative my-0 mx-auto py-3 px-0 text-center" style={{ width: '264px' }}>
            {avatarData.map(item => (
              <div
                key={item.id}
                className="relative inline-block"
                onClick={selectedAvatar.id === item.id ? null : () => handleSelectAvatar(item.id)}
              >
                <Avatar key={`avatar${item.id}`} avatarSrc={item.url} />
                {selectedAvatar.id === item.id ? (
                  <div className="inline-block absolute left-0 w-16 h-16 rounded-full m-3 py-5 px-4 bg-black bg-opacity-30">
                    <IconCheck />
                  </div>
                ) : null}
              </div>
            ))}
            <div className="inline-block m-3">
              <label htmlFor="photo-input">
                <div
                  className="rounded-full border-2 border-solid"
                  style={{ borderColor: '#95A3B0', padding: '6px' }}
                >
                  <IconCamera width="48" height="48" fillColor="#95A3B0" />
                </div>
              </label>
              <input
                id="photo-input"
                className="hidden"
                type="file"
                accept="image/*"
                onChange={onFileChange}
              />
            </div>
          </div>
        </div>
        <div className="relative" style={{ marginTop: '330px' }}>
          <Button
            variant="primary"
            text={intl.formatMessage({ id: 'ok' })}
            onClick={handleOnSetAvatar}
            isLoading={isLoading}
            disabled={selectedAvatar.id === 999}
          />
        </div>
      </Container>
    </>
  )
}
