import dynamic from 'next/dynamic'
import { useStepView } from 'hooks/useStep'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'

const Step1Component = dynamic(() => import('./components/Step1'))
const Step2Component = dynamic(() => import('./components/Step2'))

const STEP_VIEW = {
  1: Step1Component,
  2: Step2Component,
}

const MIN_STEP = 1
const MAX_STEP = 2

const handleAPIError = async error => {
  const message = (await import('components/core/Alert/message')).default
  message({
    content: error?.data?.responseMessage,
  })
}

export default function SetFundsPassword() {
  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security`,
    url => fetcher({ url, withToken: true }),
    {
      revalidateOnFocus: false,
    },
  )

  if (error) {
    handleAPIError(error)
  }

  const securityData = data?.data?.data

  const isFundPasswordSetted = !!securityData?.fundPassword

  const ViewComponent = useStepView(STEP_VIEW, isFundPasswordSetted ? MIN_STEP : MAX_STEP, MAX_STEP)

  if (!securityData) {
    return <div>loading</div> // loading
  }
  return <ViewComponent />
}
