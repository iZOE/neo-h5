import { useCallback, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import useStep from 'hooks/useStep'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const FUND_PASSWORD_FORM_FIELD_NAME = 'password'

const fundPasswordValidator = {
  required: value => !!value,
  length: value => {
    if (value.length !== 6) {
      return false
    }
    return true
  },
}

const fundPasswordErrorMsg = {
  required: <FormattedMessage id="step1.validate.password.required" />,
  length: <FormattedMessage id="step1.validate.password.length" />,
}

export default function FundPasswordSettingStep1() {
  const intl = useIntl()
  const router = useRouter()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    getValues,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const watchFundPassword = watch(FUND_PASSWORD_FORM_FIELD_NAME, '')

  const { gotoNext } = useStep()

  const onSubmit = useCallback(async formData => {
    const password = formData[FUND_PASSWORD_FORM_FIELD_NAME]
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/funds/verify`,
        method: 'post',
        withToken: true,
        data: { password },
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        gotoNext({
          tokenContent: res.data.data.tokenContent,
        })
      }
    } catch (error) {
      console.log('error', error)
      setIsSubmitting(prev => false)
      const messages = error?.data?.responseMessage

      message({
        content: messages,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    if (!isDirty) {
      router.push('/user/setting/info')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/user/setting/info')
        },
      })
    }
  }, [isDirty])

  const clearValue = useCallback(() => {
    setValue(FUND_PASSWORD_FORM_FIELD_NAME, '')
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'step1.title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mt-4">
            <Controller
              name={FUND_PASSWORD_FORM_FIELD_NAME}
              control={control}
              rules={{ validate: fundPasswordValidator }}
              render={({ onChange, value }) => (
                <Input
                  placeholder={intl.formatMessage({ id: 'step1.password.placeholder' })}
                  onChange={onChange}
                  width={1}
                  type="password"
                  value={value}
                  withDelete
                  clearValue={clearValue}
                />
              )}
            />
          </div>
          {errors && (
            <div className="text-red mt-1 text-body-8">
              {fundPasswordErrorMsg[errors?.password?.type]}
            </div>
          )}
          <div className="h-6" />
          <div className="flex">
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={isSubmitting || !watchFundPassword || !isDirty || !isValid}
              >
                {intl.formatMessage({ id: 'step1.submit' })}
              </Button>
            </div>
          </div>
          <div className="flex mt-4 text-body-6 text-platinum-300 dark:text-platinum-200">
            <div className="flex-1">
              <FormattedMessage
                id="step1.explanation1"
                values={{
                  br: <br />,
                  cskh: (
                    <Link href="/service">
                      <span className="no-underline text-body-4 text-blue-200 not-italic font-semibold">
                        <FormattedMessage id="cskh" />
                      </span>
                    </Link>
                  ),
                }}
              />
            </div>
          </div>
        </form>
      </Container>
    </>
  )
}
