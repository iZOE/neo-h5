import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl, FormattedMessage } from 'react-intl'
import Link from 'next/link'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import styled from 'styled-components'
import dynamic from 'next/dynamic'
import * as dayjs from 'dayjs'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const List = dynamic(() => import('components/core/List'))
const ListTitle = dynamic(() => import('components/core/ListTitle'))
const ListItem = dynamic(() => import('components/core/ListItem'))
const ListItemText = dynamic(() => import('components/core/ListItemText'))
const Picture = dynamic(() => import('components/core/Picture'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const IconEdit = dynamic(() => import('components/icons/Edit.svg'))

const GrayTip = styled.span`
  color: #95a3b0;
`

const RedTip = styled.span`
  color: #ee5650;
`

const fetchMeAndSecurity = async () => {
  const meRes = await fetcher({
    url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    withToken: true,
  })
  const securityRes = await fetcher({
    url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security`,
    withToken: true,
  })
  return { me: meRes.data.data, security: securityRes.data.data }
}

function UserSetting() {
  const intl = useIntl()
  const router = useRouter()

  const { data, error } = useSWR('meAndSecurity', fetchMeAndSecurity, {
    revalidateOnFocus: false,
  })

  useEffect(async () => {
    if (error) {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: error?.status,
        okText: intl.formatMessage({ id: 'close' }),
      })
    }
  }, [error])

  const userData = data?.me
  const securityData = data?.security

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/user')}
        title={<FormattedMessage id="member_center" />}
      />
      <Container withNavigationBar noPadding>
        <div className="flex items-center justify-center py-4 my-2 bg-white dark:bg-purple-800 transition duration-150 ease-in-out">
          <Link href="/user/setting/info/picture">
            <div className="relative flex">
              <button
                type="button"
                className="relative overflow-hidden rounded-full"
                style={{ width: '64px', height: '64px' }}
              >
                <Picture
                  src={userData?.avatarIcon}
                  webp={userData?.avatarIcon}
                  png={userData?.avatarIcon}
                />
              </button>
              <div className="absolute right-0 bottom-0">
                <IconEdit />
              </div>
            </div>
          </Link>
        </div>
        <List>
          <ListTitle>
            <FormattedMessage id="user_data" />
          </ListTitle>
          <Link href="/user/setting/info/nickname">
            <ListItem useAllow allowTips={<GrayTip>{userData?.nickName}</GrayTip>}>
              <ListItemText>
                <FormattedMessage id="nickname" />
              </ListItemText>
            </ListItem>
          </Link>
          <Link href="/user/setting/info/phone">
            <ListItem
              useAllow
              allowTips={
                userData?.tel ? (
                  <GrayTip>
                    <FormattedMessage id="edit" />
                  </GrayTip>
                ) : (
                  <RedTip>
                    <FormattedMessage id="unset" />
                  </RedTip>
                )
              }
            >
              <ListItemText>
                <FormattedMessage id="phone_number" />
              </ListItemText>
            </ListItem>
          </Link>
          <Link href="/user/setting/info/email">
            <ListItem
              useAllow
              allowTips={
                userData?.email ? (
                  <GrayTip>
                    <FormattedMessage id="edit" />
                  </GrayTip>
                ) : (
                  <RedTip>
                    <FormattedMessage id="unset" />
                  </RedTip>
                )
              }
            >
              <ListItemText>
                <FormattedMessage id="mail" />
              </ListItemText>
            </ListItem>
          </Link>
          <Link href="/user/setting/info/birthday">
            <ListItem
              useAllow
              allowTips={
                userData?.birthday ? (
                  <GrayTip>
                    <FormattedMessage id="edit" />
                  </GrayTip>
                ) : (
                  <RedTip>
                    <FormattedMessage id="unset" />
                  </RedTip>
                )
              }
              onClick={async e => {
                if (userData?.birthday) {
                  e.preventDefault()
                  const message = (await import('components/core/Alert/message')).default
                  message({
                    title: intl.formatMessage({ id: 'dialog.birthday.title' }),
                    content: intl.formatMessage(
                      { id: 'dialog.birthday.content' },
                      {
                        birthday: dayjs(userData.birthday).format('YYYY-MM-DD'),
                        br: <br />,
                      },
                    ),
                    okText: intl.formatMessage({ id: 'close' }),
                  })
                }
              }}
            >
              <ListItemText>
                <FormattedMessage id="birthday" />
              </ListItemText>
            </ListItem>
          </Link>
          {JSON.parse(process.env.NEXT_PUBLIC_ENABLE_USER_QQ_CONFIG) && (
            <Link href="/user/setting/info/qq">
              <ListItem
                useAllow
                allowTips={
                  userData?.qq ? (
                    <GrayTip>
                      <FormattedMessage id="edit" />
                    </GrayTip>
                  ) : (
                    <RedTip>
                      <FormattedMessage id="unset" />
                    </RedTip>
                  )
                }
              >
                <ListItemText>
                  <FormattedMessage id="qq" />
                </ListItemText>
              </ListItem>
            </Link>
          )}
          {JSON.parse(process.env.NEXT_PUBLIC_ENABLE_USER_WECHAT_CONFIG) && (
            <Link href="/user/setting/info/wechat">
              <ListItem
                useAllow
                allowTips={
                  userData?.weChat ? (
                    <GrayTip>
                      <FormattedMessage id="edit" />
                    </GrayTip>
                  ) : (
                    <RedTip>
                      <FormattedMessage id="unset" />
                    </RedTip>
                  )
                }
              >
                <ListItemText>
                  <FormattedMessage id="wechat" />
                </ListItemText>
              </ListItem>
            </Link>
          )}
        </List>
        <List>
          <ListTitle>
            <FormattedMessage id="security_info" />
          </ListTitle>
          <Link href="/user/setting/info/login-password">
            <ListItem
              useAllow
              allowTips={
                <GrayTip>
                  <FormattedMessage id="edit" />
                </GrayTip>
              }
            >
              <ListItemText>
                <FormattedMessage id="login_password" />
              </ListItemText>
            </ListItem>
          </Link>
          <Link href="/user/setting/info/fund-password">
            <ListItem
              useAllow
              allowTips={
                securityData?.fundPassword ? (
                  <GrayTip>
                    <FormattedMessage id="edit" />
                  </GrayTip>
                ) : (
                  <RedTip>
                    <FormattedMessage id="unset" />
                  </RedTip>
                )
              }
            >
              <ListItemText>
                <FormattedMessage id="fund_password" />
              </ListItemText>
            </ListItem>
          </Link>
        </List>
      </Container>
    </>
  )
}

UserSetting.protect = true

export default UserSetting
