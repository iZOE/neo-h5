import { useCallback, useState, useEffect } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const nicknameValidator = {
  required: value => !!value,
  length: value => {
    if (value.length > 14) {
      return false
    }
    return true
  },
}

const nicknameErrorMsg = {
  required: <FormattedMessage id="validate.nick_name.required" />,
  length: <FormattedMessage id="validate.nick_name.length" />,
}

const isShouldDisabled = (isSubmitting, isValid, watchNickname, isDirty) => {
  if (isValid) {
    return false
  }
  return isSubmitting || !watchNickname || !isDirty || !isValid
}

export default function NickNameSetting() {
  const intl = useIntl()
  const router = useRouter()
  const [isSubmitting, setIsSubmitting] = useState(false)

  const { data: userData, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
    {
      revalidateOnFocus: false,
    },
  )

  const {
    control,
    handleSubmit,
    errors,
    getValues,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })
  const watchNickname = watch('nickName', '')
  const onSubmit = useCallback(async formData => {
    const body = {
      nickName: formData.nickName,
    }
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
        method: 'put',
        data: body,
        withToken: true,
      })
      if (res.data.code === '0000') {
        message({
          content: intl.formatMessage({ id: 'edit_success' }),
          okText: intl.formatMessage({ id: 'confirm' }),
          onOk: () => {
            router.push('/user/setting/info')
          },
          closable: false,
        })

        setIsSubmitting(prev => false)
      } else {
        const messages = error?.data?.responseMessage
        message({
          title: 'ERROR',
          content: messages,
          okText: intl.formatMessage({ id: 'confirm' }),
        })
        setIsSubmitting(prev => false)
      }
    } catch (error) {
      setIsSubmitting(prev => false)
      const messages = error?.data?.responseMessage

      message({
        content: messages,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    const nickName = getValues('nickName')

    if (!nickName) {
      router.push('/user/setting/info')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/user/setting/info')
        },
      })
    }
  }, [])

  const clearValue = useCallback(() => {
    setValue('nickName', '', { shouldValidate: true })
  }, [])

  useEffect(() => {
    if (userData) {
      setValue('nickName', userData.nickName, { shouldValidate: true })
    }
  }, [userData])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar>
        <div className="mt-4">
          <form onSubmit={handleSubmit(onSubmit)}>
            <Controller
              name="nickName"
              control={control}
              rules={{ validate: nicknameValidator }}
              render={({ onChange, value }) => (
                <Input
                  placeholder={intl.formatMessage({ id: 'placeholder' })}
                  onChange={onChange}
                  width={1}
                  value={value}
                  withDelete
                  clearValue={clearValue}
                />
              )}
            />
            {errors && (
              <div className="text-red mt-1 text-body-8">
                {nicknameErrorMsg[errors?.nickName?.type]}
              </div>
            )}
            <div className="flex mt-6">
              <div className="flex-1">
                <Button
                  isLoading={isSubmitting}
                  variant="primary"
                  type="submit"
                  disabled={isShouldDisabled(isSubmitting, isValid, watchNickname, isDirty)}
                >
                  {intl.formatMessage({ id: 'submit' })}
                </Button>
              </div>
            </div>
          </form>
          <div className="text-platinum-300 dark:text-platinum-200 text-body-4 mt-4">
            {intl.formatMessage(
              { id: 'tips' },
              {
                br: <br />,
              },
            )}
          </div>
        </div>
      </Container>
    </>
  )
}
