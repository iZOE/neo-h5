import { useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl, FormattedMessage } from 'react-intl'
import { useForm, Controller } from 'react-hook-form'
import * as dayjs from 'dayjs'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'

const MAX_DATE = dayjs().add(-14, 'year').format('YYYY-MM-DD')
const MIN_DATE = dayjs().add(-100, 'year').format('YYYY-MM-DD')

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Input = dynamic(() => import('components/core/Input'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

function UserSettingInfoBirthday() {
  const intl = useIntl()
  const router = useRouter()
  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    url => fetcher({ url, withToken: true }),
    {
      revalidateOnFocus: false,
    },
  )

  const userData = data?.data?.data

  useEffect(() => {
    if (userData?.birthday) {
      router.push('/user/setting/info')
    }
  }, [userData])

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/birthday?birthday=${formData.birthday}`,
        method: 'put',
        withToken: true,
      })
      if (res.data.code === '0000') {
        message({
          title: intl.formatMessage({ id: 'dialog.success.title' }),
          content: intl.formatMessage(
            { id: 'dialog.success.content' },
            { birthday: formData.birthday },
          ),
          okText: intl.formatMessage({ id: 'close' }),
          onOk: () => {
            router.push('/user/setting/info')
          },
        })
      } else {
        alert('no ok')
      }
    } catch (err) {
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={async () => {
          if (isDirty) {
            const message = (await import('components/core/Alert/message')).default
            message({
              content: intl.formatMessage({ id: 'dialog.back_alert.content' }),
              okText: intl.formatMessage({ id: 'confirm' }),
              cancelText: intl.formatMessage({ id: 'cancel' }),
              onOk: () => {
                router.push('/user/setting/info')
              },
            })
          } else {
            router.push('/user/setting/info')
          }
        }}
        title={intl.formatMessage({ id: 'title' })}
      />
      {!userData?.birthday && (
        <Container withNavigationBar>
          <div className="mt-4">
            <form onSubmit={handleSubmit(onSubmit)}>
              <Controller
                name="birthday"
                control={control}
                rules={{ required: true, min: MIN_DATE, max: MAX_DATE }}
                render={({ onChange, value }) => (
                  <Input
                    defaultValue="1980-01-01"
                    label={intl.formatMessage({ id: 'title' })}
                    placeholder={intl.formatMessage({ id: 'input.placeholder' })}
                    type="date"
                    max={MAX_DATE}
                    min={MIN_DATE}
                    onChange={onChange}
                    width={1}
                    value={value}
                    error={
                      errors.birthday &&
                      intl.formatMessage({ id: 'input.error' }, { min: MIN_DATE, max: MAX_DATE })
                    }
                  />
                )}
              />
              <div className="h-6" />
              <div>
                <Button variant="primary" type="submit" disabled={!isDirty || !isValid}>
                  <FormattedMessage id="bind" />
                </Button>
              </div>
              <div className="h-6" />
              <div className="text-body-4 text-platinum-200">
                {intl.formatMessage(
                  { id: 'tips' },
                  {
                    br: <br />,
                  },
                )}
              </div>
            </form>
          </div>
        </Container>
      )}
    </>
  )
}

UserSettingInfoBirthday.protect = true

export default UserSettingInfoBirthday
