import { useCallback, useState, useEffect, useMemo } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const AlreadyVertifiedCanNotEdit = dynamic(() =>
  import('components/shared/AlreadyVertifiedCanNotEdit'),
)

const qqValidator = {
  required: value => !!value,
  length: value => {
    if (value.length < 4) {
      return false
    }
    if (value.length > 10) {
      return false
    }
    return true
  },
}

const qqErrorMsg = {
  required: <FormattedMessage id="validate.qq.required" />,
  length: <FormattedMessage id="validate.qq.length" />,
}

const hiddenQQCode = originQQCode => {
  if (!originQQCode) {
    return ''
  }

  const strQQCode = `${originQQCode}`

  const lastIdx = strQQCode.length - 1
  const qqArr = Array.from(strQQCode)
  return qqArr.reduce((acc, curr, idx) => {
    if (idx === 0 || idx === lastIdx) {
      acc += curr
      return acc
    }
    return (acc += '*')
  }, '')
}

export default function QQSetting({ isQQOpen }) {
  const intl = useIntl()
  const router = useRouter()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    watch,
    getValues,
    setValue,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const watchQQ = watch('qq', '')

  const { data: userData, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
    {
      revalidateOnFocus: false,
    },
  )

  const [qqIsVerified, setIsQQVerified] = useState(false)

  useEffect(() => {
    if (userData) {
      if (userData.qqIsVerified) {
        setIsQQVerified(prev => true)
      } else {
        setValue('qq', userData.qq)
      }
    }
  }, [userData])

  const onSubmit = useCallback(async formData => {
    const body = {
      qq: formData.qq,
    }
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
        method: 'put',
        data: body,
        withToken: true,
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        message({
          content: intl.formatMessage({ id: 'edit_success' }),
          okText: intl.formatMessage({ id: 'confirm' }),
          onOk: () => {
            router.push('/user/setting/info')
          },
          closable: false,
        })
      }
    } catch (error) {
      setIsSubmitting(prev => false)
      const messages = error?.data?.responseMessage

      message({
        content: messages,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    const qq = getValues('qq')

    if (!qq) {
      router.push('/user/setting/info')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/user/setting/info')
        },
      })
    }
  }, [])

  const clearValue = useCallback(() => {
    setValue('qq', '')
  }, [])

  const cachedHiddenQQ = useMemo(() => hiddenQQCode(userData?.qq), [userData?.qq])
  return isQQOpen ? (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar>
        <div className="mt-4">
          {qqIsVerified ? (
            <AlreadyVertifiedCanNotEdit
              alreadyVertifiedSubject={intl.formatMessage({ id: 'verified_kind_name' })}
              hiddenCodeContent={cachedHiddenQQ}
            />
          ) : (
            <form onSubmit={handleSubmit(onSubmit)}>
              <Controller
                name="qq"
                control={control}
                rules={{ validate: qqValidator }}
                render={({ onChange, value }) => (
                  <Input
                    placeholder={intl.formatMessage({ id: 'placeholder' })}
                    onChange={onChange}
                    width={1}
                    type="number"
                    value={value}
                    withDelete
                    clearValue={clearValue}
                  />
                )}
              />
              {errors && (
                <div className="text-red mt-1 text-body-8">{qqErrorMsg[errors?.qq?.type]}</div>
              )}
              <div className="h-6" />
              <div className="flex">
                <div className="flex-1">
                  <Button
                    variant="primary"
                    isLoading={isSubmitting}
                    type="submit"
                    disabled={isSubmitting || !watchQQ || !isDirty || !isValid}
                  >
                    {intl.formatMessage({ id: 'submit' })}
                  </Button>
                </div>
              </div>
            </form>
          )}
        </div>
      </Container>
    </>
  ) : null
}

export async function getStaticProps() {
  return {
    props: {
      isQQOpen: JSON.parse(process.env.NEXT_PUBLIC_ENABLE_USER_QQ_CONFIG),
    },
  }
}
