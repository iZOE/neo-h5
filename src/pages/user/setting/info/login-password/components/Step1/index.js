import { useCallback, useState, useEffect, useMemo } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import Link from 'next/link'
import fetcher from 'libs/fetcher'
import useStep from 'hooks/useStep'
import useLocalStorage from 'hooks/useLocalStorage'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const LOGIN_PASSWORD_FORM_FIELD_NAME = 'password'

const loginPasswordValidator = {
  required: value => !!value,
  length: value => {
    if (value.length < 6 || value.length > 16) {
      return false
    }
    return true
  },
}

const loginPasswordErrorMsg = {
  required: <FormattedMessage id="validate.password.required" />,
  length: <FormattedMessage id="validate.password.length" />,
}

export default function LoginPasswordSettingStep1() {
  const intl = useIntl()
  const router = useRouter()
  const [storedNoShowAnnounceModal, setStoredNoShowAnnounceModal] = useLocalStorage(
    'noShowAnnounceModal',
  )

  const [token, setToken, removeToken] = useLocalStorage('token')
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    setValue,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const watchPassword = watch(LOGIN_PASSWORD_FORM_FIELD_NAME, '')

  const { gotoNext } = useStep()

  const onSubmit = useCallback(async formData => {
    const password = formData[LOGIN_PASSWORD_FORM_FIELD_NAME]
    setIsSubmitting(prev => true)
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/login/verify`,
        method: 'post',
        withToken: true,
        data: { password },
      })
      setIsSubmitting(prev => false)
      if (res.data.code === '0000') {
        gotoNext({
          tokenContent: res.data.data.tokenContent,
        })
      }
    } catch (error) {
      if (error?.data?.data?.availableCount > 0) {
        const messages = error?.data?.responseMessage
        message({
          content: messages,
          okText: intl.formatMessage({ id: 'confirm' }),
        })
      } else {
        removeToken()
        setStoredNoShowAnnounceModal(false)
        message({
          title: intl.formatMessage({ id: 'step1.error.account_logout' }),
          content: intl.formatMessage({ id: 'step1.error.content' }),
          okText: intl.formatMessage({ id: 'confirm' }),
          onOk: () => {
            router.push('/login')
          },
          closable: false,
        })
      }
      setIsSubmitting(prev => false)
    }
  }, [])

  const handleClickNavBack = useCallback(async () => {
    if (!isDirty) {
      router.push('/user/setting/info')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/user/setting/info')
        },
      })
    }
  }, [isDirty])

  const clearValue = useCallback(() => {
    setValue(LOGIN_PASSWORD_FORM_FIELD_NAME, '')
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'step1.title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="mt-4">
            <Controller
              name={LOGIN_PASSWORD_FORM_FIELD_NAME}
              control={control}
              rules={{ validate: loginPasswordValidator }}
              render={({ onChange, value }) => (
                <Input
                  placeholder={intl.formatMessage({ id: 'step1.password.placeholder' })}
                  onChange={onChange}
                  width={1}
                  type="password"
                  value={value}
                  withDelete
                  clearValue={clearValue}
                />
              )}
            />
          </div>
          {/* {errors && (
            <div className="text-red mt-1 text-body-8">
              {loginPasswordErrorMsg[errors?.password?.type]}
            </div>
          )} */}
          <div className="h-6" />
          <div className="flex">
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={isSubmitting || !watchPassword || !isDirty || !isValid}
              >
                {intl.formatMessage({ id: 'step1.submit' })}
              </Button>
            </div>
          </div>
          <div className="mt-4 text-body-6 text-platinum-300 dark:text-platinum-200">
            <FormattedMessage id="step1.explanation1" />
          </div>

          <Link href="/reset-password">
            <div className="mt-6 text-blue-200 text-body-4 flex justify-center cursor-pointer">
              <FormattedMessage id="step1.forgotPassword" />
            </div>
          </Link>
        </form>
      </Container>
    </>
  )
}
