import { useCallback, useState, useMemo } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { FormattedMessage, useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import useSWR from 'swr'
import { useStepData } from 'hooks/useStep'

const Input = dynamic(() => import('components/core/Input'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const Button = dynamic(() => import('components/core/Button'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

const SET_PASSWORD_FORM_FIELD_NAME = 'setPassword'
const CONFIRM_SET_PASSWORD_FORM_FIELD_NAME = 'confirmSetPassword'

const setPasswordErrorMsg = {
  required: <FormattedMessage id="step2.validate.required" />,
  length: <FormattedMessage id="step2.validate.length" />,
  validate: <FormattedMessage id="step2.validate.not_same" />,
}

const handleAPIError = async error => {
  const message = (await import('components/core/Alert/message')).default
  message({
    content: error?.data?.responseMessage,
  })
}

export default function LoginPasswordSettingStep2() {
  const intl = useIntl()
  const router = useRouter()
  const stepData = useStepData()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const {
    control,
    handleSubmit,
    errors,
    setValue,
    getValues,
    watch,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })
  const { tokenContent } = stepData

  const { data, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    url => fetcher({ url, withToken: true }),
    {
      revalidateOnFocus: false,
    },
  )

  if (error) {
    handleAPIError(error)
  }

  const userAccount = data?.data?.data?.account

  const watchSetPassword = watch(SET_PASSWORD_FORM_FIELD_NAME, '')
  const watchConfirmSetPassword = watch(CONFIRM_SET_PASSWORD_FORM_FIELD_NAME, '')

  const onSubmit = useCallback(
    async formData => {
      const setPassword = formData.setPassword
      setIsSubmitting(prev => true)
      const message = (await import('components/core/Alert/message')).default
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/security/login/${userAccount}?resetToken=${tokenContent}`,
          method: 'put',
          withToken: true,
          data: { setPassword },
        })
        setIsSubmitting(prev => false)
        if (res.data.code === '0000') {
          message({
            content: intl.formatMessage({ id: 'edit_success' }),
            okText: intl.formatMessage({ id: 'confirm' }),
            onOk: () => {
              router.push('/user/setting/info')
            },
            closable: false,
          })
        }
      } catch (error) {
        setIsSubmitting(prev => false)
        const messages = error?.data?.responseMessage

        message({
          content: messages,
          okText: intl.formatMessage({ id: 'confirm' }),
        })
      }
    },
    [tokenContent, userAccount],
  )

  const handleClickNavBack = useCallback(async () => {
    if (!isDirty) {
      router.push('/user/setting/info')
    } else {
      const message = (await import('components/core/Alert/message')).default
      message({
        content: intl.formatMessage({ id: 'abort_editing' }),
        okText: intl.formatMessage({ id: 'confirm' }),
        cancelText: intl.formatMessage({ id: 'cancel' }),
        onOk: () => {
          router.push('/user/setting/info')
        },
      })
    }
  }, [isDirty])

  const clearValue = useCallback(() => {
    setValue(SET_PASSWORD_FORM_FIELD_NAME, '')
  }, [])

  const clearValue2 = useCallback(() => {
    setValue(CONFIRM_SET_PASSWORD_FORM_FIELD_NAME, '')
  }, [])

  const setPasswordRules = {
    required: value => !!value,
    length: value => {
      if (value.length < 6 || value.length > 16) {
        return false
      }
      return true
    },
  }

  const confirmPasswordRules = {
    required: value => !!value,
    length: value => {
      if (value.length < 6 || value.length > 16) {
        return false
      }
      return true
    },
    validate: value => value === getValues(SET_PASSWORD_FORM_FIELD_NAME),
  }

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={handleClickNavBack}
        title={intl.formatMessage({ id: 'step2.title' })}
      />
      <Container withNavigationBar>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            name={SET_PASSWORD_FORM_FIELD_NAME}
            control={control}
            rules={{ validate: setPasswordRules }}
            render={({ onChange, value }) => (
              <div className="mt-4">
                <Input
                  label={intl.formatMessage({ id: 'step2.setPassword.label' })}
                  placeholder={intl.formatMessage({ id: 'step2.setPassword.placeholder' })}
                  onChange={onChange}
                  width={1}
                  type="password"
                  value={value}
                  withDelete
                  clearValue={clearValue}
                />
              </div>
            )}
          />
          {errors?.setPassword && (
            <div className="text-red mt-1 text-body-8">
              {setPasswordErrorMsg[errors?.setPassword?.type]}
            </div>
          )}
          <div className="h-1" />
          <Controller
            name={CONFIRM_SET_PASSWORD_FORM_FIELD_NAME}
            control={control}
            rules={{ validate: confirmPasswordRules }}
            render={({ onChange, value }) => (
              <div className="mt-4">
                <Input
                  label={intl.formatMessage({ id: 'step2.confirmSetPassword.label' })}
                  placeholder={intl.formatMessage({ id: 'step2.confirmSetPassword.placeholder' })}
                  onChange={onChange}
                  width={1}
                  type="password"
                  value={value}
                  withDelete
                  clearValue={clearValue2}
                />
              </div>
            )}
          />
          {errors?.confirmSetPassword && (
            <div className="text-red mt-1 text-body-8">
              {setPasswordErrorMsg[errors?.confirmSetPassword?.type]}
            </div>
          )}
          <div className="flex mt-6">
            <div className="flex-1">
              <Button
                variant="primary"
                isLoading={isSubmitting}
                type="submit"
                disabled={
                  isSubmitting ||
                  !watchSetPassword ||
                  !watchConfirmSetPassword ||
                  !isDirty ||
                  !isValid
                }
              >
                {intl.formatMessage({ id: 'step2.submit' })}
              </Button>
            </div>
          </div>
        </form>
        <div className="mt-4 text-body-6 text-platinum-300 dark:text-platinum-200">
          <FormattedMessage id="step2.explanation1" />
        </div>
        <Link href="/reset-password">
          <div className="mt-6 text-blue-200 text-body-4 flex justify-center cursor-pointer">
            <FormattedMessage id="step1.forgotPassword" />
          </div>
        </Link>
      </Container>
    </>
  )
}
