import styled from 'styled-components'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'
import useRouterPushBeforeCheckLogin from 'hooks/useRouterPushBeforeCheckLogin'

const IconRecharge = dynamic(() => import('components/icons/Recharge.svg'))
const IconTransfer = dynamic(() => import('components/icons/Transfer.svg'))
const IconVIP = dynamic(() => import('components/icons/VIP.svg'))
const IconWithdraw = dynamic(() => import('components/icons/Withdraw.svg'))

const MENU = [
  {
    icon: <IconRecharge />,
    name: <FormattedMessage id="recharge" />,
    url: '/deposit',
  },
  {
    icon: <IconWithdraw />,
    name: <FormattedMessage id="withdraw" />,
    url: '/withdrawal',
  },
  {
    icon: <IconTransfer />,
    name: <FormattedMessage id="transfer" />,
    url: '/transfer',
  },
  {
    icon: <IconVIP />,
    name: <FormattedMessage id="vip" />,
    url: '/vip',
  },
]

const Name = styled.div`
  font-size: 14px;
  line-height: 8px;
  font-weight: 600;
  color: #ffffff;
`

export default function QuickLink() {
  const pushBeforeCheck = useRouterPushBeforeCheckLogin()

  return (
    <div className="relative flex items-center my-0 mx-auto max-w-screen-sm px-3 h-12 z-10">
      <div className="flex w-full justify-between quick-link h-12">
        {MENU.map(menu => (
          <button
            key={menu.url}
            className="text-center"
            type="button"
            onClick={() => {
              pushBeforeCheck(menu.url)
            }}
          >
            <div className="inline-flex">{menu.icon}</div>
            <Name>{menu.name}</Name>
          </button>
        ))}
      </div>
    </div>
  )
}
