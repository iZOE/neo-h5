/* eslint-disable react/style-prop-object */
import styled from 'styled-components'
import { FormattedMessage } from 'react-intl'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import useRouterPushBeforeCheckLogin from 'hooks/useRouterPushBeforeCheckLogin'
import useUserData from 'hooks/useUserData'

const Picture = dynamic(() => import('components/core/Picture'))
const Currency = dynamic(() => import('components/core/Currency'))
const IconCustomerService = dynamic(() => import('components/icons/CustomerService'))
const IconAvatar = dynamic(() => import('components/icons/Avatar'))

const UserDataWrapper = styled.div
  .withConfig({
    componentId: 'UserDataComponent',
  })
  .attrs({
    className: 'relative flex my-0 mx-auto max-w-screen-sm pb-4 z-10',
  })`
  color: #ffffff;
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  .head {
    width: 64px;
    height: 64px;
    margin-right: 8px;
    background: #b9c0c7;
  }
  .no-login {
    font-size: 14px;
  }
  .login {
    font-weight: normal;
    .account,
    .money {
      font-size: 14px;
      .value {
        font-weight: 600;
      }
    }
    .vip {
      font-size: 12px;
      line-height: 14px;
      display: inline-block;
      background: linear-gradient(179.49deg, #b8ccdf 0.26%, #a0afbe 99.38%);
      border-radius: 4px;
      height: 14px;
      padding: 0 4px;
    }
  }
  .icon {
    path {
      fill: #ffffff;
    }
  }
`

export default function UserData() {
  const { userData, balance } = useUserData()
  const pushBeforeCheck = useRouterPushBeforeCheckLogin()

  return (
    <UserDataWrapper>
      <div>
        <div className="head rounded-full overflow-hidden">
          {userData ? (
            <Link href="/user/setting/info/picture">
              <div className="border border-white rounded-full overflow-hidden">
                <Picture webp={userData.avatarIcon} png={userData.avatarIcon} />
              </div>
            </Link>
          ) : (
            <Link href="/login">
              <div onClick={event => {
                event.preventDefault()
                pushBeforeCheck('/login')
              }}>
                <IconAvatar w="64" h="64" vw="64" vh="64" />
              </div>
            </Link>
          )}
        </div>
      </div>
      <div className="flex-1 flex items-center">
        {userData ? (
          <div className="login">
            <p className="account">{userData.account}</p>
            <p className="money">
              <FormattedMessage id="money" />
              <span> : </span>
              <span className="value">
                <Currency value={balance} />
              </span>
            </p>
            <Link href="/vip">
              <p className="vip">VIP{userData.levelName}</p>
            </Link>
          </div>
        ) : (
          <Link href="/login">
            <div
              className="no-login"
              onClick={event => {
                event.preventDefault()
                pushBeforeCheck('/login')
              }}
            >
              <p>
                <FormattedMessage id="user_data.no_session.welcome" />
              </p>
              <p>
                <FormattedMessage id="user_data.no_session.please_login" />
              </p>
            </div>
          </Link>
        )}
      </div>
      <div className="cs">
        <Link href="/service">
          <button
            type="button"
            className="h-8 w-8 flex items-center justify-center"
            style={{ marginTop: '-8px' }}
          >
            <IconCustomerService className="icon" width={24} height={24} fillColor="#FFFFFF" />
          </button>
        </Link>
      </div>
    </UserDataWrapper>
  )
}
