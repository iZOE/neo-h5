import { useEffect } from 'react'
import { useRouter } from 'next/router'
import styled from 'styled-components'

const Wrapper = styled.button.withConfig({
  componentId: 'MenuGroupMenuButton',
})`
  .menu-button-text {
    font-size: 14px;
    line-height: 16px;
    color: ${props => (props.theme.darkMode ? '#ffffff' : '#454545')};
  }
  .menu-button-icon {
    svg {
      path {
        fill: #51a1ff;
      }
    }
  }
`

export default function MenuButton({ icon, text, url, onClick }) {
  const router = useRouter()

  useEffect(() => {
    router.prefetch(url)
  }, [])

  return (
    <Wrapper className="w-1/2 flex items-center h-11" onClick={onClick} alt="text" type="button">
      <div className="menu-button-icon w-8 mr-1">{icon}</div>
      <div className="menu-button-text flex-1 text-left">{text}</div>
    </Wrapper>
  )
}
