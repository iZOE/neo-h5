/* eslint-disable no-confusing-arrow */
import { useEffect, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { useRecoilValue } from 'recoil'
import { useOpenCustomerService } from 'hooks/useOpenCustomerService'
import useRouterPushBeforeCheckLogin from 'hooks/useRouterPushBeforeCheckLogin'
import { darkModeState } from 'atoms/darkModeState'
import MenuButton from './components/MenuButton'

const IconDepositHistory = dynamic(() => import('components/icons/DepositHistory'))
const IconWithdrawHistory = dynamic(() => import('components/icons/WithdrawHistory'))
const IconBetHistory = dynamic(() => import('components/icons/BetHistory'))
const IconTransferHistory = dynamic(() => import('components/icons/TransferHistory'))
const IconAccChangeHistory = dynamic(() => import('components/icons/AccChangeHistory'))
const IconBankCard = dynamic(() => import('components/icons/UserBankCard'))
const IconPromote = dynamic(() => import('components/icons/Promote'))
const IconSetting = dynamic(() => import('components/icons/Setting'))
const IconOtherContact = dynamic(() => import('components/icons/OtherContact'))
const IconPersonalInformation = dynamic(() => import('components/icons/PersonalInformation'))
const Picture = dynamic(() => import('components/core/Picture'))

const IS_AGENT_VT = process.env.NEXT_PUBLIC_AGENT_CODE === 'VT999'

const MENU_BASIC = [
  {
    icon: <IconDepositHistory width={32} height={32} />,
    text: <FormattedMessage id="report_deposit" />,
    url: '/report/deposit',
    needCheckSession: true,
    order: 1,
  },
  {
    icon: <IconWithdrawHistory width={32} height={32} />,
    text: <FormattedMessage id="report_withdraw" />,
    url: '/report/withdraw',
    needCheckSession: true,
    order: 2,
  },
  {
    icon: <IconBetHistory width={32} height={32} />,
    text: <FormattedMessage id="report_bet" />,
    url: '/report/bet',
    needCheckSession: true,
    order: 3,
  },
  {
    icon: <IconTransferHistory width={32} height={32} />,
    text: <FormattedMessage id="report_transfer" />,
    url: '/report/transfer',
    needCheckSession: true,
    order: 4,
  },
  {
    icon: <IconAccChangeHistory width={32} height={32} />,
    text: <FormattedMessage id="report_financial" />,
    url: '/report/financial',
    needCheckSession: true,
    order: 5,
  },
  {
    icon: <IconBankCard width={32} height={32} />,
    text: <FormattedMessage id="withdraw_manage" />,
    url: '/withdraw-manage/banks',
    needCheckSession: true,
    order: 6,
  },
]

const MENU_PROMO = [
  {
    icon: <IconPromote width={32} height={32} />,
    text: <FormattedMessage id="promo" />,
    url: '/promo',
    needCheckSession: true,
    order: 7,
  },
]

const MENU_SETTING = [
  {
    icon: <IconPersonalInformation width={32} height={32} />,
    text: <FormattedMessage id="setting_info" />,
    url: '/user/setting/info',
    needCheckSession: true,
    order: 8,
  },
  {
    icon: <IconSetting width={32} height={32} />,
    text: <FormattedMessage id="setting" />,
    url: '/user/setting',
    order: 9,
  },
]

const MENU_LIST = IS_AGENT_VT
  ? [...MENU_BASIC, ...MENU_PROMO, ...MENU_SETTING]
  : [...MENU_BASIC, ...MENU_SETTING]

export default function MenuGroup() {
  const router = useRouter()
  const pushBeforeCheck = useRouterPushBeforeCheckLogin()
  const openCustomerService = useOpenCustomerService()
  const darkMode = useRecoilValue(darkModeState)
  const [bgUrl, setBgUrl] = useState()

  useEffect(() => {
    if (darkMode) {
      setBgUrl('/pages/user/dark-bg')
    } else {
      setBgUrl('/pages/user/bg')
    }
  }, [darkMode])

  return (
    <div
      className="menu-group relative overflow-hidden rounded-md bg-white dark:bg-purple-700 shadow-b-0"
      style={{ height: '280px' }}
    >
      <div className="absolute z-10">
        <div className="title pt-3 px-3 pb-2 text-title-7 text-platinum-200">
          <FormattedMessage id="member_center" />
        </div>
        <div className="underline my-0 mx-3 border-b border-solid border-gray-100 dark:border-gray-500" />
        <div className="items flex flex-wrap m-1 px-2 pb-3">
          {MENU_LIST.map(menu => (
            <MenuButton
              key={menu.order}
              icon={menu.icon}
              text={menu.text}
              onClick={() =>
                menu.needCheckSession ? pushBeforeCheck(menu.url) : router.push(menu.url)
              }
            />
          ))}
          <MenuButton
            icon={<IconOtherContact width={32} height={32} />}
            text={<FormattedMessage id="another_contact" />}
            onClick={openCustomerService}
          />
        </div>
      </div>
      <div className="z-0 absolute inset-0">
        {bgUrl && <Picture webp={`${bgUrl}.webp`} png={`${bgUrl}.png`} />}
      </div>
    </div>
  )
}
