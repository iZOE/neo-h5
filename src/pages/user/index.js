/* eslint-disable no-confusing-arrow */
import { useEffect, useState } from 'react'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import Link from 'next/link'
import styled from 'styled-components'
import dynamic from 'next/dynamic'
import { darkModeState } from 'atoms/darkModeState'
import { useRecoilValue } from 'recoil'
import useLocalStorage from 'hooks/useLocalStorage'
import WithTabBar from 'components/layout/WithTabBar'
import useSession from 'hooks/useSession'
import UserData from './components/UserData'
import MenuGroup from './components/MenuGroup'
import QuickLink from './components/QuickLink'

const PageTransition = dynamic(() => import('components/shared/PageTransition'))
const Button = dynamic(() => import('components/core/Button'))
const Container = dynamic(() => import('components/core/Container'))
const Picture = dynamic(() => import('components/core/Picture'))

const Wrapper = styled.div`
  .user-data {
    height: 208px;
    background: ${props =>
      props.theme.darkMode
        ? 'linear-gradient(179.04deg, #3A3845 0.53%, #20202C 92.9%)'
        : 'linear-gradient(90deg, #778BDD 0%, #5FDCD9 100.36%)'};
  }
  .user-menu-title {
    border-radius-left-top: 10px;
    border-radius-right-top: 10px;
  }
  .main-button {
    button {
      background: ${props => (props.theme.darkMode ? '#353340' : '#ffffff')};
      .shared-component-button-text {
        font-weight: 600;
        color: #1d8aeb;
      }
    }
  }
  .invisibleArea {
    position: absolute;
    z-index: 99;
    width: 100vw;
    height: 100vh;
  }
`

function User() {
  const intl = useIntl()
  const router = useRouter()
  const darkMode = useRecoilValue(darkModeState)
  const [storedNoShowAnnounceModal, setStoredNoShowAnnounceModal] = useLocalStorage(
    'noShowAnnounceModal',
  )
  const [bgUrl, setBgUrl] = useState()
  // invisibleArea是用來防止回上一頁後連點的誤操作
  const [invisible, setInvisible] = useState(true)
  const [hasSession, clear] = useSession()
  const { token } = router.query
  useEffect(() => {
    setTimeout(() => {
      setInvisible(false)
    }, 1500)
  }, [])
  useEffect(() => {
    if (token) {
      localStorage.setItem(
        'token',
        JSON.stringify({
          access_token: token,
          token_type: 'bearer',
        }),
      )
      router.push('/')
    }
  }, [token])
  useEffect(() => {
    if (darkMode) {
      setBgUrl('/pages/user/gradient_bg_dark')
    } else {
      setBgUrl('/pages/user/gradient_bg_light')
    }
  }, [darkMode])

  return (
    <PageTransition style={{ height: '-webkit-fill-available' }}>
      <Wrapper>
        {invisible && <div className="invisibleArea" />}
        <div className="user-data px-3 pt-6">
          <UserData />
          <QuickLink />
          {bgUrl && (
            <div className="absolute inset-0 z-0">
              <Picture png={`${bgUrl}.png`} webp={`${bgUrl}.webp`} />
            </div>
          )}
        </div>
        <Container className="user-menu" noHighScreen>
          <div className="pb-6 transform -translate-y-12">
            <MenuGroup />
            <div className="main-button mt-2 rounded-full shadow-b-0">
              <Link href="/download">
                <Button text={intl.formatMessage({ id: 'download' })} />
              </Link>
            </div>
            <div className="main-button mt-2 rounded-full shadow-b-0">
              {hasSession ? (
                <Button
                  text={intl.formatMessage({ id: 'logout' })}
                  onClick={() => {
                    clear()
                    // window.location.reload()
                  }}
                />
              ) : (
                <Link href="/login">
                  <Button
                    text={intl.formatMessage({ id: 'login' })}
                    onClick={() => {
                      setStoredNoShowAnnounceModal(false)
                    }}
                  />
                </Link>
              )}
            </div>
          </div>
        </Container>
      </Wrapper>
    </PageTransition>
  )
}

User.TabBar = WithTabBar

export default User
