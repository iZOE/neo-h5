import { useEffect } from 'react'
import { AnimateSharedLayout } from 'framer-motion'
import useSelectLoginAndSignupLayout from 'hooks/useSelectLoginAndSignupLayout'
import { NEXT_PUBLIC_SCAFFOLD_LOGINANDREGISTER_LAYOUT } from 'constants/processEnvVariables'
import FormA from './components/form/TemplateA'

function Login({ loginAndSignupLayout }) {
    const LayoutComponent = useSelectLoginAndSignupLayout({
        type: loginAndSignupLayout,
    })

    useEffect(() => {
        const isLogout = sessionStorage.getItem('requireLogin')
        if (isLogout) {
            sessionStorage.removeItem('requireLogin')
            window.location.reload()
        }
    })

    return (
        <LayoutComponent>
            <FormA />
        </LayoutComponent>
    )
}

export async function getStaticProps() {
    return {
        props: {
            loginAndSignupLayout: process.env[NEXT_PUBLIC_SCAFFOLD_LOGINANDREGISTER_LAYOUT],
        },
    }
}

Login.Layout = AnimateSharedLayout

export default Login
