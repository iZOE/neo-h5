import React, { useState, useEffect, useCallback } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useForm } from 'react-hook-form'
import { useIntl } from 'react-intl'
import { useSetRecoilState } from 'recoil'
import fetcher from 'libs/fetcher'
import Lock from 'components/icons/Lock.svg'
import Union from 'components/icons/Union.svg'
import useLocalStorage from 'hooks/useLocalStorage'
import { tokenState } from 'atoms/tokenState'
import btoa from '../../../../libs/btoa'
import Input, { INPUT_TYPE } from '../../../../layouts/LoginAndSignup/TemplateA/components/Input'
import Switch from '../../../../layouts/LoginAndSignup/TemplateA/components/Switch'
import SubmitButton from '../../../../layouts/LoginAndSignup/TemplateA/components/SubmitButton'
import style from './templateA.module.scss'

const checkLoginInput = str => {
  const regex = new RegExp('(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9]{6,16}')
  return regex.test(str) && str.length < 17
}

function TemplateA() {
  const intl = useIntl()
  const router = useRouter()
  const setToken = useSetRecoilState(tokenState)
  const [storedRememberMe, setStoredRememberMe] = useLocalStorage('remember_me')
  const [rememberMeChecked, setRememberMeChecked] = useState(storedRememberMe?.rememberMe || false)
  const [submitLoading, setSubmitLoading] = useState(false)
  const { register, handleSubmit, errors, getValues, formState } = useForm({
    mode: 'onChange',
    defaultValues: {
      accountId: rememberMeChecked && storedRememberMe ? storedRememberMe.accountId : '',
      password: '',
    },
  })
  const { isValid } = formState

  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (storedRememberMe === undefined) {
        setStoredRememberMe(JSON.stringify({ rememberMe: false, accountId: '' }))
        setRememberMeChecked(false)
      }
    }
  }, [])

  const handleOnSwitch = useCallback(() => {
    setRememberMeChecked(!rememberMeChecked)
  }, [rememberMeChecked])

  const onSubmit = useCallback(
    async formData => {
      const message = (await import('components/core/Alert/message')).default
      try {
        const { accountId } = formData
        const { password } = formData
        const isAccountValid = checkLoginInput(accountId)
        const isPasswordValid = checkLoginInput(password)
        const isKeepLogin30Days = rememberMeChecked ? 1 : 0
        if (!isAccountValid || !isPasswordValid) {
          message({
            title: intl.formatMessage({ id: 'form.error.inputError.title' }),
            content: intl.formatMessage({ id: 'form.error.inputError.content' }),
            okText: intl.formatMessage({ id: 'form.ok' }),
          })
        } else {
          setSubmitLoading(true)
          const bodyFormData = new FormData()
          bodyFormData.append('grant_type', 'password')
          bodyFormData.append('scope', 'BasicInfo')
          bodyFormData.append('agentcode', process.env.NEXT_PUBLIC_AGENT_CODE)
          bodyFormData.append('username', accountId)
          bodyFormData.append('password', password)
          bodyFormData.append('isKeepLogin30Days', isKeepLogin30Days)
          const bodyFormDataString = new URLSearchParams(bodyFormData).toString()
          const res = await fetcher({
            url: `${process.env.NEXT_PUBLIC_END_POINT_OAUTH}Token`,
            method: 'post',
            data: bodyFormDataString,
            headers: {
              Authorization: `Basic ${btoa(
                `${process.env.NEXT_PUBLIC_CLIENT_ID}:${process.env.NEXT_PUBLIC_APP_KEY || ''}`,
              )}`,
              'Content-Type': 'application/x-www-form-urlencoded',
              'X-Platform': '4',
              'X-Device-ID': '444', // device uuid
              'X-Device-Model': '444', // device type
            },
            withToken: false,
            isRefreshToken: true,
          })
          if (res.status === 200) {
            setToken(res.data)
            setStoredRememberMe(
              JSON.stringify({
                rememberMe: rememberMeChecked,
                accountId: isKeepLogin30Days ? accountId : '',
              }),
            )
            setSubmitLoading(false)
            router.push('/')
          }
        }
      } catch (error) {
        const errorCode = error?.data?.error
        let title = null
        let content = null
        if (errorCode === '8' || errorCode === '23') {
          message({
            content: intl.formatMessage({ id: 'form.error.lockedError.content' }),
            okText: intl.formatMessage({ id: 'form.contactCustomerService' }),
            cancelText: intl.formatMessage({ id: 'form.cancel' }),
            onOk: () => router.push('/service'),
          })
        } else {
          switch (errorCode) {
            case '6':
              content = intl.formatMessage({ id: 'form.error.notFoundError.content' })
              break
            case '7':
              content = intl.formatMessage({ id: 'form.error.notActiveError.content' })
              break
            case '13':
              title = intl.formatMessage({
                id: 'form.error.accountLockedFiveMinuteError.title',
              })
              content = `${intl.formatMessage(
                {
                  id: 'form.error.accountLockedFiveMinuteError.content',
                },
                { second: Math.round(error?.data?.unlockTimeRemains) },
              )}`
              break
            case '10':
            case '11':
            case '12':
            case '16':
            case '17':
            case '18':
              title = intl.formatMessage({ id: 'form.error.inputError.title' })
              content = intl.formatMessage({ id: 'form.error.inputError.content' })
              break
            default:
              content = intl.formatMessage(
                { id: 'form.error.serverError.content' },
                { code: errorCode },
              )
          }
          message({
            title,
            content,
            okText: intl.formatMessage({ id: 'form.ok' }),
          })
        }
        setSubmitLoading(false)
      }
    },
    [rememberMeChecked],
  )
  return (
    <div className="my-6 sm:my-12">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-4 sm:mb-7">
          <Input
            name="accountId"
            type={INPUT_TYPE.TEXT}
            placeholder={intl.formatMessage({ id: 'form.input.accountId.placeholder' })}
            propsRef={register({ required: true })}
            iconComponent={Union}
          />
        </div>
        <div className="mb-4 sm:mb-7">
          <Input
            name="password"
            type={INPUT_TYPE.PASSWORD}
            placeholder={intl.formatMessage({ id: 'form.input.password.placeholder' })}
            propsRef={register({ required: true })}
            iconComponent={Lock}
          />
        </div>
        <div className={`${style.text_link_area} flex justify-between items-center mt-6 mb-10`}>
          <Link href="/reset-password">
            <p className="text-blue-200 text-body-6 font-semibold">
              {intl.formatMessage({ id: 'form.forgetPassword' })}
            </p>
          </Link>
          <Switch
            label={intl.formatMessage({ id: 'form.switch.label' })}
            checked={rememberMeChecked}
            onClick={handleOnSwitch}
          />
        </div>
        <SubmitButton
          name={intl.formatMessage({ id: 'form.login' })}
          isLoading={submitLoading}
          disabled={!isValid || submitLoading}
        />
      </form>
    </div>
  )
}

export default TemplateA
