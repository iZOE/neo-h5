import { useState } from 'react'
import useSWR from 'swr'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { useRouter } from 'next/router'
import useGameProviderData from 'hooks/useGameProviderData'
import { FormattedMessage } from 'react-intl'
import NotOpen from '../components/NotOpen'
import GameList from '../components/GameList'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconCustomerService = dynamic(() =>
  import('components/icons/CustomerService')
)
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Container = dynamic(() => import('components/core/Container'))

export function ThirdPartyGame() {
  const router = useRouter()
  const [showAll, setShowAll] = useState(true)

  const { gameProviderId, gameProviderTypeId } = router.query
  const { gameProviderData } = useGameProviderData(gameProviderId)

  const { data: gameList } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/${gameProviderId}/game-lobby/${gameProviderTypeId}/game`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    }
  )

  const tabActiveStyle = 'text-blue-100 border-b-2 border-blue-100'

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/')}
        title={gameProviderData?.data?.remark}
        right={
          <>
            <button
              type="button"
              className="mr-3"
              onClick={() => router.push('/service')}
            >
              <IconCustomerService />
            </button>
          </>
        }
      />
      <Container withNavigationBar noPadding>
        <div
          className="flex text-center text-body-7 text-platinum-200 bg-gray-100"
          style={{ boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.2)' }}
        >
          <div
            onClick={() => setShowAll(true)}
            className={`w-1/2 py-3 ${showAll && tabActiveStyle}`}
          >
            <FormattedMessage id="all" />({gameList?.length})
          </div>
          <div
            onClick={() => setShowAll(false)}
            className={`w-1/2 py-3 ${!showAll && tabActiveStyle}`}
          >
            <FormattedMessage id="hot" />
            (0)
          </div>
        </div>
        {showAll ? <GameList /> : <NotOpen />}
      </Container>
    </>
  )
}

export default ThirdPartyGame
