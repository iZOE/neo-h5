import { useState, useEffect } from 'react'
import useSWR from 'swr'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { useRouter } from 'next/router'
import useGameProviderData from 'hooks/useGameProviderData'
import useWindowSize from 'hooks/useWindowSize'
import TransferDialog from './components/TransferDialog'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconArrowDown = dynamic(() => import('components/icons/ArrowDown.svg'))
const IconFullScreen = dynamic(() => import('components/icons/FullScreen.svg'))
const IconTransferHistory = dynamic(() => import('components/icons/TransferHistory.svg'))
const IconOpenAnotherBrowser = dynamic(() => import('components/icons/OpenAnotherBrowser.svg'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Container = dynamic(() => import('components/core/Container'))

export function ThirdPartyGame() {
  const router = useRouter()
  const { gameProviderId, gameProviderTypeId, gameType } = router.query
  const [hide, setHide] = useState(false)
  const [isOpen, setIsOpen] = useState(false)
  const [loadTime, setLoadTime] = useState()
  const [loaded, setLoaded] = useState(false)
  const { gameProviderData } = useGameProviderData(gameProviderId)
  const { width, height } = useWindowSize()

  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/${gameProviderId}/login`,
    async url => {
      const res = await fetcher({
        url,
        withToken: true,
        method: 'post',
        data: {
          gameProviderTypeId,
          gameType,
          deviceType: 2,
        },
      })
      setLoadTime(Date.now())
      return res?.data?.data
    },
  )

  const thirdPartyUrl = data?.loginUrl

  const onLoad = () => {
    setLoadTime(Date.now() - loadTime)
    setLoaded(true)
  }

  function CollapseButton() {
    return (
      <>
        {hide && (
          <div
            style={{ width: '42px', marginLeft: '-21px' }}
            className="h-7 top-0 fixed rounded-b left-1/2 z-9999 text-center bg-platinum-200 bg-opacity-10 flex justify-center items-center"
            onClick={() => setHide(!hide)}
          >
            <IconArrowDown />
          </div>
        )}
      </>
    )
  }

  useEffect(() => {
    // iframe讀取完 就把資料給newRelic
    if (loaded && thirdPartyUrl) {
      if (typeof window.newrelic === 'object') {
        window.newrelic.setCustomAttribute(
          'loginTimeToThirdParty',
          `${thirdPartyUrl} (${String(loadTime)}ms)`,
        )
      }
    }
  }, [loaded])

  const divStyle = !hide ? 'pt-11 w-full h-full' : 'w-full h-full'

  return (
    <div style={{ height }}>
      <TransferDialog isOpen={isOpen} closeDialog={() => setIsOpen(false)} />
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/')}
        title={gameProviderData?.data?.remark}
        hide={hide}
        right={
          <>
            <button
              className="w-9 flex justify-center"
              type="button"
              onClick={() => setIsOpen(true)}
            >
              <IconTransferHistory />
            </button>
            <button
              className="w-9 flex justify-center"
              type="button"
              onClick={() => setHide(!hide)}
            >
              <IconFullScreen />
            </button>
            <button
              className="w-9 flex justify-center"
              type="button"
              onClick={() => window.open(thirdPartyUrl)}
            >
              <IconOpenAnotherBrowser />
            </button>
          </>
        }
      />
      <CollapseButton />
      <div className={divStyle}>
        {thirdPartyUrl && (
          <iframe title="iframe" className="w-full h-full" src={thirdPartyUrl} onLoad={onLoad} />
        )}
      </div>
    </div>
  )
}

ThirdPartyGame.protect = true

export default ThirdPartyGame
