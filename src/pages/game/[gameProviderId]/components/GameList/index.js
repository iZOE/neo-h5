import { useState } from 'react'
import useSWR from 'swr'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import Link from 'next/link'
import Empty from '../Empty'

const IconSearch = dynamic(() => import('components/icons/Search.svg'))

export function ThirdPartyGame() {
  const router = useRouter()
  const [keyWord, setKeyWord] = useState('')
  const intl = useIntl()
  const { gameProviderId, gameProviderTypeId } = router.query

  const { data: gameList } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/${gameProviderId}/game-lobby/${gameProviderTypeId}/game`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    }
  )

  if (!gameList) return null

  const gameListResult = gameList?.filter(
    g => g.gameDescription.toLowerCase().indexOf(keyWord.toLowerCase()) >= 0
  )

  return (
    <>
      <div className="mt-4 mx-3 border border-gray-300 rounded text-body-4 px-4 py-3 bg-white relative">
        <input
          type="text"
          className="w-full h-5"
          placeholder={intl.formatMessage({ id: 'plz_enter_search_name' })}
          onChange={e => setKeyWord(e.target.value)}
        />
        <span className="absolute right-2">
          <IconSearch />
        </span>
      </div>
      <div className="flex flex-wrap">
        {gameListResult.length > 0 ? (
          gameListResult.map(game => (
            <Link
              href={`/game/${gameProviderId}?gameProviderTypeId=${gameProviderTypeId}&gameType=${game.gameType}`}
            >
              <div className="py-2 px-1 w-1/2">
                <div className="p-2 bg-white rounded shadow">
                  <img
                    src={game.gameImgUrl}
                    onError={e =>
                      (e.target.src = '/pages/game/defaultImage.png')
                    }
                    alt={game.gameDescription}
                    style={{ height: '155px' }}
                    className="object-cover m-auto"
                  />
                  <p className="pt-2 text-body-5 text-platinum-200">
                    {game.gameDescription}
                  </p>
                </div>
              </div>
            </Link>
          ))
        ) : (
          <Empty />
        )}
      </div>
    </>
  )
}

export default ThirdPartyGame
