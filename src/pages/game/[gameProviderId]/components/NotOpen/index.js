import { FormattedMessage } from 'react-intl'
import IconNotOpen from 'components/icons/NotOpen'
import EmptyWithIcon from 'components/core/EmptyWithIcon'

function Empty() {
  return (
    <div
      className="absolute top-0 bottom-0 right-0 left-0 m-auto w-max"
      style={{ height: '125px' }}
    >
      <EmptyWithIcon
        icon={<IconNotOpen width="110" height="110" fillColor="#B9C0C7" />}
        text={<FormattedMessage id="not_open" />}
      />
    </div>
  )
}

export default Empty
