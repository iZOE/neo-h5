function InputButton({ value, onClick, activeAmount }) {
  return (
    <button
      type="button"
      className={`flex-1 p-2 border rounded-full px-[10px] text-body-6 ${
        activeAmount === value
          ? ' border-blue-200 text-blue-200'
          : 'border-platinum-200'
      }`}
      onClick={() => onClick(value)}
    >
      {value}
    </button>
  )
}

export default InputButton
