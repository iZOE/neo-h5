import { useState, useCallback, useMemo } from 'react'
import useSWR, { trigger } from 'swr'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { useIntl, FormattedMessage } from 'react-intl'
import { useForm, Controller } from 'react-hook-form'
import { useRouter } from 'next/router'
import useGameProviderData from 'hooks/useGameProviderData'
import useWalletData from 'hooks/useWalletData'
import Wallet from './Wallet'
import InputButton from './InputButton'

const Dialog = dynamic(() => import('components/core/Dialog'))
const Title = dynamic(() => import('components/core/Dialog/components/Title'))
const Button = dynamic(() => import('components/core/Button'))
const IconArrow = dynamic(() => import('components/icons/Arrow.svg'))
const Input = dynamic(() => import('components/core/Input'))

const MAIN_WALLET_ID = 1
const AMOUNT_LIST = [100, 300, 500, 1000, 'Max']

export function TransferDialog({ isOpen, closeDialog }) {
  const router = useRouter()
  const [activeAmount, setActiveAmount] = useState(null)
  const { gameProviderId } = router.query
  const { gameProviderData } = useGameProviderData(gameProviderId)
  const intl = useIntl()
  const { wallet } = useWalletData(gameProviderId)
  const { wallet: centerWallet } = useWalletData(1)
  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
    setValue,
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const { data: walletList } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/wallet`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data?.gameProviderMoney
    },
  )

  const { userAmount, targetAmount } = useMemo(() => {
    if (walletList && gameProviderId) {
      const main = walletList.find(({ gameProviderId: idx }) => idx === MAIN_WALLET_ID)
      const target = walletList.find(({ gameProviderId: idx }) => idx === parseInt(gameProviderId, 10))
      return {
        userAmount: parseInt(main?.money, 10),
        targetAmount: target?.money,
      }
    }

    return {
      userAmount: 0,
      targetAmount: 0,
    }
  }, [walletList, gameProviderId])

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/${gameProviderId}/charge`,
        method: 'post',
        withToken: true,
        data: {
          money: formData.money,
        },
      })
      if (res.data.code === '0000') {
        message({
          content: intl.formatMessage({ id: 'dialog.success' }),
          okText: intl.formatMessage({ id: 'close' }),
          onOk: () => {
            closeDialog()
          },
        })
        console.log('trigger')
        trigger(`${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/wallet`)
      } else {
        alert('no ok')
      }
    } catch (err) {
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'confirm' }),
        onOk: () => {
          closeDialog()
        },
      })
    }
  }, [])

  function updateMoney(value) {
    setActiveAmount(value)
    setValue(
      'money',
      value === 'Max' ? Math.floor(userAmount) : value,
      {
        shouldValidate: true,
        shouldDirty: true,
      }
    )
  }

  return (
    <Dialog isOpen={isOpen} onClose={closeDialog}>
      <Title><FormattedMessage id="transfer" /></Title>
      <div className="flex gap-x-2 justify-between items-center">
        <Wallet
          title={<FormattedMessage id="center_wallet" />}
          money={userAmount}
        />
        <IconArrow />
        <Wallet
          title={gameProviderData?.data?.remark}
          money={targetAmount}
          transferIn
        />
      </div>
      <div className="flex gap-x-2 justify-between py-4">
        {AMOUNT_LIST.map(amount => (
          <InputButton
            key={amount}
            activeAmount={activeAmount}
            value={amount}
            onClick={updateMoney}
          />
        ))}
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name="money"
          control={control}
          rules={{
            required: {
              value: true,
              message: intl.formatMessage({
                id: 'form.validate.message.required',
              }),
            },
            validate: value =>
              value <= Math.floor(userAmount) ||
              intl.formatMessage({
                id: 'form.validate.message.balance_insufficient',
              }),
          }}
          render={({ onChange, value }) => (
            <Input
              label={intl.formatMessage({
                id: 'form.input.money.label',
              })}
              placeholder={intl.formatMessage({
                id: 'form.input.money.placeholder',
              })}
              type="number"
              onChange={onChange}
              width={1}
              min={0}
              value={value}
              error={errors.money && errors.money.message}
            />
          )}
        />
        <div className="mt-3">
          <Button
            variant="primary"
            type="submit"
            disabled={!isDirty || !isValid}
          >
            <FormattedMessage id="transfer_now" />
          </Button>
        </div>
      </form>
    </Dialog>
  )
}
export default TransferDialog
