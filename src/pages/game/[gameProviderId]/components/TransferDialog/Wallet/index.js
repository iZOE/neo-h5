import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'

const Currency = dynamic(() => import('components/core/Currency'))
const Picture = dynamic(() => import('components/core/Picture'))

function Wallet({ transferIn, title, money }) {
  return (
    <div className="flex-1 bg-gray-50 dark:bg-gray-500 p-3 relative rounded-md">
      <div className="text-body-6 text-platinum-200">
        {transferIn ? (
          <FormattedMessage id="transfer_in" />
        ) : (
          <FormattedMessage id="transfer_out" />
        )}
      </div>
      <div className="text-title-8 text-platinum-300 dark:text-white">
        {title}
      </div>
      <div className="text-body-6 text-blue-200">
        <Currency value={money} />
      </div>
      <div className="w-8 absolute right-0 bottom-0 rounded-br-md overflow-hidden">
        <Picture
          png={
            transferIn
              ? '/pages/game/transferIn.png'
              : '/pages/game/transferOut.png'
          }
          webp={
            transferIn
              ? '/pages/game/transferIn.webp'
              : '/pages/game/transferOut.webp'
          }
          alt="logo"
        />
      </div>
    </div>
  )
}

export default Wallet
