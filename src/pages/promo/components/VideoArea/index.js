import { FormattedMessage } from 'react-intl'
import NoData from 'components/shared/NoData'
import AreaTitle from '../AreaTitle'

const VideoArea = () => {
  const videoPath = process.env.NEXT_PUBLIC_PROMOTION_VIDEO_URL
  const agentCode = process.env.NEXT_PUBLIC_AGENT_CODE

  let posterUrl
  switch (agentCode) {
    case 'VT999':
      posterUrl = 'pages/promo/video/poster_vn.png'
      break
    case 'STA01':
      posterUrl = 'pages/promo/video/poster_cn.png'
      break
    default:
      posterUrl = 'pages/promo/video/poster_vn.png'
      break
  }

  return (
    <div className="mb-6 px-3">
      <AreaTitle>
        <FormattedMessage id="video.title" />
      </AreaTitle>
      {videoPath ? (
        <video width="100%" height="100%" poster={posterUrl} controls>
          <source src={videoPath} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      ) : (
        <NoData />
      )}
    </div>
  )
}

export default VideoArea
