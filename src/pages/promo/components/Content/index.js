const Content = ({ children, ...props }) => {
  return (
    <p
      {...props}
      className="text-body-4 text-platinum-300 dark:text-platinum-200 mb-2 last:mb-0">
      {children}
    </p>
  )
}

export default Content