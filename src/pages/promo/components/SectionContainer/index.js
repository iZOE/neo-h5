const SectionContainer = ({ children }) => {
  return (
    <div className="mb-4 last:mb-0">
      {children}
    </div>
  )
}

export default SectionContainer
