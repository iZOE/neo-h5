const AreaContainer = ({ children }) => {
  return (
    <div
      className="bg-white dark:bg-purple-800 mx-3 mb-6 last:mb-0 p-4 rounded-md relative z-1">
      {children}
    </div>
  )
}

export default AreaContainer
