const Title = ({ children }) => {
  return (
    <h4
      className="title-7 font-semibold text-platinum-200 dark:text-platinum-300 mb-2">
      {children}
    </h4>
  )
}

export default Title