const AreaTitle = ({ children }) => {
  return (
    <h3
      className="title-7 font-semibold text-platinum-200 dark:text-platinum-300 mb-2 text-center">
      { children}
    </h3>
  )
}

export default AreaTitle