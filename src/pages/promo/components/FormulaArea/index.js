import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'
import AreaContainer from '../AreaContainer'
import AreaTitle from '../AreaTitle'
import SectionContainer from '../SectionContainer'
import Content from '../Content'

const BackgroundDecoration = dynamic(() => import('./components/BackgroundDecoration'))
const GameListDropdown = dynamic(() => import('./components/GameListDropdown'))

const FormulaArea = () => (
  <div className="mb-6">
    <AreaTitle>
      <FormattedMessage id="formula.title" />
    </AreaTitle>
    <AreaContainer>
      <div className="relative z-10">
        <SectionContainer>
          <Content>
            <FormattedMessage id="formula.section.1" values={{ br: <br /> }} />
          </Content>
        </SectionContainer>
        <SectionContainer>
          <div className="bg-gray-100 dark:bg-purple-200 py-2 px-4 rounded text-body-6 text-platinum-300 dark:text-platinum-200">
            <FormattedMessage id="formula.section.formula" values={{ br: <br /> }} />
          </div>
        </SectionContainer>
        <SectionContainer>
          <Content>
            <FormattedMessage id="formula.section.2" values={{ br: <br /> }} />
          </Content>
        </SectionContainer>
        <SectionContainer>
          <GameListDropdown />
        </SectionContainer>
      </div>
      <BackgroundDecoration />
    </AreaContainer>
  </div>
)

export default FormulaArea
