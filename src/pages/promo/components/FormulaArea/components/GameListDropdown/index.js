import dynamic from 'next/dynamic'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'

const IconArrowDown = dynamic(() => import('components/icons/ArrowDown'))

const GameListDropdown = () => {
  const router = useRouter()
  const intl = useIntl()

  const OPTIONS = [
    {
      value: '',
      text: intl.formatMessage({ id: 'game_list.default' }),
    },
    {
      value: 'lottery',
      text: intl.formatMessage({ id: 'game_list.lottery' }),
    },
    {
      value: 'video',
      text: intl.formatMessage({ id: 'game_list.video' }),
    },
    {
      value: 'chess',
      text: intl.formatMessage({ id: 'game_list.chess' }),
    },
    {
      value: 'egame',
      text: intl.formatMessage({ id: 'game_list.egame' }),
    },
    {
      value: 'fish',
      text: intl.formatMessage({ id: 'game_list.fish' }),
    },
    {
      value: 'esport',
      text: intl.formatMessage({ id: 'game_list.esport' }),
    },
  ]

  const onChange = (event) => {
    const gameType = event.target.value

    if (gameType) {
      router.push(`/promo/trial?type=${event.target.value}`)
    }
  }

  return (
    <div className="relative">
      <select
        onChange={onChange}
        className="w-full h-11 rounded-full bg-transparent border border-platinum-200 text-platinum-200 px-5 appearance-none">
        {OPTIONS.map(({ value, text }, index) => {
          return (
            <option
              key={index}
              value={value}>
              {text}
            </option>
          )
        })}
      </select>
      <div className="absolute h-full right-5 top-0 flex items-center">
        <IconArrowDown w="16" h="16" vw="16" vh="vh" />
      </div>
    </div>
  )
}

export default GameListDropdown
