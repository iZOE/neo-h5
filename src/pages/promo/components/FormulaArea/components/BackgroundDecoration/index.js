import Picture from 'components/core/Picture'

const BackgroundDecoration = () => {
  const mediaPath = 'pages/promo/formula/bg'

  return (
    <div className="w-full h-full absolute -z-1 inset-0">
      <Picture webp={`${mediaPath}.webp`} png={`${mediaPath}.png`} />
    </div>
  )
}

export default BackgroundDecoration