import { useRouter } from 'next/router'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import Slider from 'react-slick'
import Picture, { getFullPath } from 'components/core/Picture'
import NoData from 'components/shared/NoData'
import { PLATFORM_TYPE } from 'constants/index'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import DotContainer from './components/DotContainer'
import Dot from './components/Dot'

const SETTINGS = {
  adaptiveHeight: false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 5000,
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  pauseOnHover: false,
  appendDots: dots => (<DotContainer>{dots}</DotContainer>),
  customPaging: i => (<Dot key={i} />),
}

const StyledSlider = ({ d }) => {
  const router = useRouter()

  return (
    <div
      style={{ height: '224px' }}>
      <Picture
        className="h-full"
        onClick={() => {
          d.targetUrl ? router.push(d.targetUrl) : console.log('no URL')
        }}
        webp={getFullPath(d.imagePath, 'webp')}
        png={getFullPath(d.imagePath, 'png')}
      />
    </div>
  )
}

const SliderArea = () => {
  const { data: sliders } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/slides?platform=${PLATFORM_TYPE.H5}`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    }
  )

  return sliders ? (
    <Slider
      className="relative z-10 w-full h-full"
      {...SETTINGS}>
      {sliders.map((item, index) => (
        <StyledSlider
          d={item}
          key={`slide_${index.toString()}`}
        />
      ))}
    </Slider>
  ) : (
    <NoData style={{ height: '224px' }} />
  )
}

export default SliderArea
