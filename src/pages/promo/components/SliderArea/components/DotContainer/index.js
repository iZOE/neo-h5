const DotContainer = ({ children }) => {
  return (
    <ul className="absolute z-20 inset-0 w-4 flex flex-col justify-center items-center">
      {children}
    </ul>
  )
}

export default DotContainer