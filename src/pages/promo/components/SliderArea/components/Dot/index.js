const Dot = () => {
  return (
    <div className="w-1 h-1 mb-1 border border-platinum-200 box-border rounded-full" />
  )
}

export default Dot