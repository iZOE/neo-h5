import { FormattedMessage } from 'react-intl'
import Currency from 'components/core/Currency'
import Picture from 'components/core/Picture'
import style from '../../../promo.module.scss'

const MemberInfo = ({ data }) => {
  return data ? (
    <div className="flex items-center pb-4 mb-4 border-b border-gray-100 dark:border-gray-500">
      <div className={`${style.memberAvatar}`}>
        <Picture
          className="rounded-full border border-white"
          src={data.avatarUrl}
          png={data.avatarUrl}
          webp={data.avatarUrl}
        />
      </div>
      <div className="flex-1 text-platinum-300 dark:text-platinum-200">
        <p className="text-body-5 mb-1">
          {data.nickname}
        </p>
        <p className="text-title-8 font-semibold mb-1">
          <FormattedMessage id="member.info.income.today" />
          <Currency value={data.todayIncome} />
        </p>
        <p className="text-title-8 font-semibold">
          <FormattedMessage id="member.info.income.total" />
          <Currency value={data.totalIncome} />
        </p>
      </div>
    </div>
  ) : <span>--</span>
}

export default MemberInfo