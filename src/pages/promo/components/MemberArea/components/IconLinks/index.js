import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'
import NoData from 'components/shared/NoData'

const IconCustomerService = dynamic(() => import('components/icons/CustomerServicePromo'))
const IconDownload = dynamic(() => import('components/icons/Download'))
const IconShare = dynamic(() => import('components/icons/Share'))

const ICON_STYLE = {
  WIDTH: 32,
  HEIGHT: 32,
  COLOR: 'text-blue-100',
}

const IconLinks = () => {
  const router = useRouter()

  const ICONS = [
    {
      icon: <IconCustomerService
        w={ICON_STYLE.WIDTH}
        h={ICON_STYLE.HEIGHT}
        vw={ICON_STYLE.WIDTH}
        vh={ICON_STYLE.HEIGHT}
        fillColor={ICON_STYLE.COLOR}
      />,
      text: <FormattedMessage id="member.icons.customer_service" />,
      action: () => router.push('/service'),
    },
    {
      icon: <IconDownload
        w={ICON_STYLE.WIDTH}
        h={ICON_STYLE.HEIGHT}
        vw={ICON_STYLE.WIDTH}
        vh={ICON_STYLE.HEIGHT}
        fillColor={ICON_STYLE.COLOR}
      />,
      text: <FormattedMessage id="member.icons.download" />,
      action: () => router.push('/download'),
    },
    {
      icon: <IconShare
        w={ICON_STYLE.WIDTH}
        h={ICON_STYLE.HEIGHT}
        vw={ICON_STYLE.WIDTH}
        vh={ICON_STYLE.HEIGHT}
        fillColor={ICON_STYLE.COLOR}
      />,
      text: <FormattedMessage id="member.icons.share" />,
      action: () => router.push('/promo/share'),
    }
  ]

  return (
    <div className="flex mb-4">
      {
        ICONS ? (
          ICONS.map(({ icon, text, action }, index) => {
            return (
              <div className="flex-1 text-center" onClick={action} key={index}>
                {icon}
                <h4 className="text-title-8 font-semibold text-platinum-200 dark:text-platinum-300 mt-1">
                  {text}
                </h4>
              </div>
            )
          })
        ) : <NoData />
      }
    </div>
  )
}

export default IconLinks
