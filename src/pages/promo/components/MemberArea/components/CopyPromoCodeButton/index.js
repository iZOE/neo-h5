import { useCallback } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import Button from 'components/core/Button'

const CopyPromoCodeButton = ({ data: code }) => {
  const intl = useIntl()

  const handleCopyPromoCode = useCallback(async () => {
    const toast = (await import('components/core/Toast/toast')).default

    let textarea = document.createElement('textarea');
    textarea.textContent = code;
    document.body.appendChild(textarea);

    let selection = document.getSelection();
    let range = document.createRange();

    range.selectNode(textarea);
    selection.removeAllRanges();
    selection.addRange(range);

    document.execCommand('copy')

    toast({
      message: intl.formatMessage({ id: 'member.copy_promo_code.result.success' })
    })

    selection.removeAllRanges();
    document.body.removeChild(textarea);
  }, [])

  return (
    <Button
      variant="primary"
      onClick={handleCopyPromoCode}>
      <FormattedMessage id="member.copy_promo_code.action" />
      <span className="ml-2 font-semibold">{code}</span>
    </Button>
  )
}

export default CopyPromoCodeButton