import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import AreaContainer from '../AreaContainer'

const MemberInfo = dynamic(() => import('./MemberInfo'))
const IconLinks = dynamic(() => import('./components/IconLinks'))
const CopyPromoCodeButton = dynamic(() => import('./components/CopyPromoCodeButton'))

const MemberArea = () => {
  const { data: userInfo } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/referral`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    }
  )

  return (
    <div className="relative z-20 mb-6 -mt-8">
      <AreaContainer>
        {userInfo && (
          <>
            <MemberInfo data={userInfo} />
            <IconLinks />
            <CopyPromoCodeButton data={userInfo.referralCode} />
          </>
        )}
      </AreaContainer>
    </div>

  )
}

export default MemberArea
