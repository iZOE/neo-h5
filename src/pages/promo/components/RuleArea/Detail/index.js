import { FormattedMessage } from 'react-intl'
import SectionContainer from '../../SectionContainer'
import Title from '../../Title'
import Content from '../../Content'

const Detail = () => (
  <>
    <SectionContainer>
      <Title>
        <FormattedMessage id="rule.detail.section1.title" />
      </Title>
      <Content>
        <FormattedMessage id="rule.detail.section1.content" />
      </Content>
    </SectionContainer>

    <SectionContainer>
      <Title>
        <FormattedMessage id="rule.detail.section2.title" />
      </Title>
      <Content>
        <FormattedMessage id="rule.detail.section2.content" />
      </Content>
    </SectionContainer>

    <SectionContainer>
      <Title>
        <FormattedMessage id="rule.detail.section3.title" />
      </Title>
      <Content>
        <FormattedMessage id="rule.detail.section3.content" />
      </Content>
    </SectionContainer>
  </>
)

export default Detail
