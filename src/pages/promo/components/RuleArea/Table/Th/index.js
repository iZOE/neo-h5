const Th = ({ children }) => {
  return (
    <th className="py-4 first:rounded-tl-md last:rounded-tr-md border-r last:border-none border-white dark:border-purple-800">
      { children}
    </th>
  )
}

export default Th