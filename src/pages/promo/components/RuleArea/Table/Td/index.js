const Td = ({ children, ...props }) => {
  return (
    <td
      {...props}
      className="py-4 border-t border-r last:border-r-0 border-platinum-300 dark:border-platinum-200">
      {children}
    </td>
  )
}

export default Td
