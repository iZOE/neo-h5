const Title = ({ children }) => {
  return (
    <span className="text-white text-title-8">
      { children}
    </span>
  )
}

export default Title