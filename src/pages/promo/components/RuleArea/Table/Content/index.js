const Content = ({ children }) => {
  return (
    <span className="text-platinum-300 dark:text-platinum-200 text-body-6">
      { children}
    </span>
  )
}

export default Content