import { FormattedMessage } from 'react-intl'
import { COLUMNS_CONFIG } from 'libs/data-table/pages/promo/rule'
import Th from './Th'
import Td from './Td'
import Title from './Title'
import Content from './Content'

const Table = () => (
  <div className="rounded-md border border-platinum-300 dark:border-platinum-200 mb-4">
    <table className="w-full text-center border-separate table-fixed" style={{ borderSpacing: 0 }}>
      <thead>
        <tr className="bg-platinum-300 dark:bg-platinum-200">
          <Th className="w-1/2">
            <Title>
              <FormattedMessage id="rule.table.head.column1" />
            </Title>
          </Th>
          <Th className="w-1/2">
            <Title>
              <FormattedMessage id="rule.table.head.column2" values={{ br: <br /> }} />
            </Title>
          </Th>
        </tr>
      </thead>
      <tbody className="bg-transparent">
        {COLUMNS_CONFIG.map(({ item, value, key }) => (
          <tr key={key}>
            <Td>
              <Content>{item}</Content>
            </Td>
            <Td>
              <Content>{value}</Content>
            </Td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
)

export default Table
