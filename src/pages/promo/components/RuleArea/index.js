import { FormattedMessage } from 'react-intl'
import AreaContainer from '../AreaContainer'
import AreaTitle from '../AreaTitle'
import Table from './Table'
import Detail from './Detail'

const RuleArea = () => {
  return (
    <div className="pb-4">
      <AreaTitle>
        <FormattedMessage id="rule.title" />
      </AreaTitle>
      <AreaContainer>
        <Table />
        <Detail />
      </AreaContainer>
    </div>
  )
}

export default RuleArea
