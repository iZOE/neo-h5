import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'

const NavBar = dynamic(() => import('../../components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back'))
const SliderArea = dynamic(() => import('./components/SliderArea'))
const MemberArea = dynamic(() => import('./components/MemberArea'))
const VideoArea = dynamic(() => import('./components/VideoArea'))
const FormulaArea = dynamic(() => import('./components/FormulaArea'))
const RuleArea = dynamic(() => import('./components/RuleArea'))

const Promotion = () => {
  const router = useRouter()

  return (
    <div className="pt-11 sm:pt-12 bg-gray-50 dark:bg-purple-900">
      <NavBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/user')}
        title={<FormattedMessage id="title" />}
      />
      <SliderArea />
      <MemberArea />
      <VideoArea />
      <FormulaArea />
      <RuleArea />
    </div>
  )
}

export default Promotion
