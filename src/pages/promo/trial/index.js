import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'

const NavBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back'))
const IconCustomerService = dynamic(() => import('components/icons/CustomerService'))
const Container = dynamic(() => import('components/core/Container'))

const Trial = () => {
  const router = useRouter()
  const { type } = router.query

  const [url, setUrl] = useState(null)

  useEffect(() => {
    if (window.location.origin) {
      let url = window.location.origin
      const needRelaced = url.indexOf('-02')
      url = needRelaced ? url.replace('-02', '') : url

      // production的環境因為網址的關係 用取代的方法不行 所以改成寫死
      let domain
      if (url.indexOf('vt999vt') !== -1) {
        domain = 'http://www.vt999.club'
      } else if (url.indexOf('xh-6666') !== -1) {
        domain = 'http://www.xh6609.com'
      } else {
        const needConvert = url.indexOf('m.')
        domain = !!(needConvert > 0) ? url.replace('m.', 'www.') : url
      }
      const path = `/event/index.html#/smesheet/${process.env.NEXT_PUBLIC_AGENT_LOCALE_LANGUAGE}/`

      setUrl(domain + path + type)
    }
  }, [])

  return (
    <>
      <NavBar
        fixed
        left={<IconBack />}
        right={<IconCustomerService />}
        onLeftClick={() => router.push('/promo')}
        onRightClick={() => router.push('/service')}
        title={
          <>
            <FormattedMessage id={`game_list.${type}`} />
            <FormattedMessage id="title" />
          </>
        }
      />
      <Container withNavigationBar noPadding>
        <iframe src={url} width="100%" height="100%" />
      </Container>
    </>
  )
}

export default Trial
