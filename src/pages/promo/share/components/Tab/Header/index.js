import { FormattedMessage } from 'react-intl'

const TAB_HEADER = [
  {
    title: <FormattedMessage id="tab.title1" />,
  },
  {
    title: <FormattedMessage id="tab.title2" />,
  },
  {
    title: <FormattedMessage id="tab.title3" />,
  },
]

const TabHeader = ({ activeTab, onSelectTab }) => (
  <div
    className="flex bg-gray-100 dark:bg-black text-center shadow-b-2">
    {(TAB_HEADER && typeof activeTab === 'number') && TAB_HEADER.map(({ title }, index) => {
      const isActiveTab = index === activeTab
      const textColor = isActiveTab ? 'blue-100' : 'platinum-200'
      const borderColor = isActiveTab ? 'blue-100' : 'transparent'

      return (
        <div
          key={index.toString}
          onClick={() => { onSelectTab(index) }}
          className={`flex-1 py-3 text-body-6 text-${textColor} active:text-platinum-250 border-b-2 border-${borderColor}`}>
          {title}
        </div>
      )
    })}
  </div>
)

export default TabHeader
