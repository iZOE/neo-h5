import { useCallback } from 'react'
import { useIntl, FormattedMessage } from 'react-intl'
import html2canvas from 'html2canvas'
import Button from 'components/core/Button'

const saveScreenshot = canvas => {
  const fileName = 'image'
  const link = document.createElement('a')
  link.download = `${fileName}.png`
  canvas.toBlob(blob => {
    link.href = URL.createObjectURL(blob)
    link.click()
  })
}

const CaptureScreenButton = () => {
  const intl = useIntl()

  const captureScreen = useCallback(async () => {
    const toast = (await import('components/core/Toast/toast')).default

    const ele = document.querySelector('#capture')

    window.pageYOffset = 0
    document.documentElement.scrollTop = 0
    document.body.scrollTop = 0

    html2canvas(ele, {
      useCORS: true,
    })
      .then(canvas => {
        const ctx = canvas.getContext('2d')

        const pageImage = new Image()
        ctx.imageSmoothingEnabled = false
        ctx.drawImage(pageImage, 0, 0)
        saveScreenshot(canvas)
      })
      .then(() => {
        toast({
          message: intl.formatMessage({ id: 'screenshot.result.success' }),
        })
      })
      .catch(err => console.error(err))
  }, [])

  return (
    <div className="my-4">
      <Button variant="primary" onClick={captureScreen}>
        <FormattedMessage id="screenshot.action" />
      </Button>
    </div>
  )
}

export default CaptureScreenButton
