import { useEffect, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'
import QRCode from 'qrcode.react'
import Picture, { getFullPath } from 'components/core/Picture'
import CopyButton from 'components/core/CopyButton'

const NoData = dynamic(() => import('components/shared/NoData'))
const CaptureScreenButton = dynamic(() => import('./components/CaptureScreenButton'))

const Content = ({ data: d, activeTab }) => {
  const [url, setUrl] = useState(null)

  useEffect(() => {
    if (window) {
      setUrl(window.location.origin)
    }
  }, [])

  return d && url ? (
    <div className="px-3 pb-3">
      <div className="pt-6 px-6 mb-8 relative">
        <div id="capture">
          <Picture
            src={getFullPath(d.allPromotionalImagePaths.h5[activeTab], 'png')}
            png={getFullPath(d.allPromotionalImagePaths.h5[activeTab], 'png')}
            webp={getFullPath(d.allPromotionalImagePaths.h5[activeTab], 'webp')}
          />
          <div className="absolute left-0 bottom-12 w-full">
            <QRCode
              className="mx-auto border-8 border-white"
              style={{ width: '152px', height: '152px' }}
              value={`${url}/signup?referralCode=${d.referralCode}`}
            />
          </div>
        </div>
      </div>
      <CopyButton
        content={`${url}/signup?referralCode=${d.referralCode}`}
        buttonText={<FormattedMessage id="copy_button.action" />}
      />
      <CaptureScreenButton />
    </div>
  ) : (
    <NoData />
  )
}

export default Content
