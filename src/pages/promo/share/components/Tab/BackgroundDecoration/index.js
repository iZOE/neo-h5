import Picture from 'components/core/Picture'

const BackgroundDecoration = () => {
  const mediaPath = '/pages/promo/share/bg'

  return (
    <div className="w-full h-full pt-10 absolute -z-1 inset-0 overflow-hidden bg-blue-50 dark:bg-purple-900">
      <Picture webp={`${mediaPath}.webp`} png={`${mediaPath}.png`} />
    </div>
  )
}

export default BackgroundDecoration
