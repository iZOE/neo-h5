import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import { useState } from 'react'
import dynamic from 'next/dynamic'

const Header = dynamic(() => import('./Header'))
const Content = dynamic(() => import('./Content'))
const BackgroundDecoration = dynamic(() => import('./BackgroundDecoration'))

const Tab = () => {
  const [currentTab, setCurrentTab] = useState(0)

  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/referral`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
  )

  return (
    <>
      <div className="relative z-10">
        <Header
          activeTab={currentTab}
          onSelectTab={setCurrentTab}
        />
        <Content data={data} activeTab={currentTab} />
      </div>
      <BackgroundDecoration />
    </>
  )
}

export default Tab
