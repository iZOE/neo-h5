import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'

const NavBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back'))
const Container = dynamic(() => import('components/core/Container'))
const Tab = dynamic(() => import('./components/Tab'))

const Share = () => {
  const router = useRouter()

  return (
    <>
      <NavBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/promo')}
        title={<FormattedMessage id="title" />}
      />
      <Container withNavigationBar noPadding>
        <Tab />
      </Container>
    </>
  )
}

export default Share
