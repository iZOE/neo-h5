import { useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl, FormattedMessage } from 'react-intl'
import { useForm, Controller } from 'react-hook-form'
import * as dayjs from 'dayjs'
import useSWR from 'swr'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

function UserSettingInfoFundPassword() {
  const intl = useIntl()
  const router = useRouter()
  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const { data: userData, error } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
    {
      revalidateOnFocus: false,
    },
  )

  useEffect(() => {
    if (userData?.birthday) {
      router.push('/user/setting/info')
    }
  }, [userData])

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/me/birthday?birthday=${formData.birthday}`,
        method: 'put',
        withToken: true,
      })
      if (res.data.code === '0000') {
        message({
          title: intl.formatMessage({ id: 'dialog.success.title' }),
          content: intl.formatMessage(
            { id: 'dialog.success.content' },
            { birthday: formData.birthday },
          ),
          okText: intl.formatMessage({ id: 'close' }),
          onOk: () => {
            router.push('/user/setting/info')
          },
        })
      } else {
        alert('no ok')
      }
    } catch (err) {
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.push('/user')
        }}
        title="登錄密碼"
      />
      <div className="flex justify-center items-center h-screen">
        <div className="text-center">
          <p className="text-body-1">施工中</p>
        </div>
      </div>
    </>
  )
}

UserSettingInfoFundPassword.protect = true

export default UserSettingInfoFundPassword
