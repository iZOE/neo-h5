import { useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl, FormattedMessage } from 'react-intl'
import { useForm, Controller } from 'react-hook-form'
import useSWR from 'swr'
import dynamic from 'next/dynamic'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

function ReportWithdraw() {
  const intl = useIntl()
  const router = useRouter()
  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default
  }, [])

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.push('/user')
        }}
        title="轉賬紀錄"
      />
      <div className="flex justify-center items-center h-screen">
        <div className="text-center">
          <p className="text-body-1 text-platinum-200">施工中</p>
        </div>
      </div>
    </>
  )
}

ReportWithdraw.protect = true

export default ReportWithdraw
