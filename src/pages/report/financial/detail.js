import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import * as dayjs from 'dayjs'
import fetcher from 'libs/fetcher'
import { FormattedMessage } from 'react-intl'
import Currency from 'components/core/Currency'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const TableCell = dynamic(() => import('components/core/TableCell'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

const STATUS_COLOR = {
  positive: {
    text: 'text-red',
  },
  negative: {
    text: 'text-platinum-200',
  },
}

function FinancialDetail() {
  const router = useRouter()
  const { id } = router.query

  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/report/financial/${id}`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
  )

  const amountStatus = data?.amount > 0 ? 'positive' : 'negative'

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={<FormattedMessage id="financial_detail" />}
      />
      <Container withNavigationBar>
        {data ? (
          <>
            <TableCell label={<FormattedMessage id="order_number" />} content={data?.orderNumber} />
            <TableCell
              label={<FormattedMessage id="time" />}
              content={dayjs.unix(data?.createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
            />
            <TableCell label={<FormattedMessage id="type" />} content={data?.financialName} />
            <TableCell
              label={<FormattedMessage id="before_balance" />}
              content={<Currency value={data?.beforeBalance} />}
            />
            <TableCell
              label={<FormattedMessage id="change_balance" />}
              content={
                <div className={STATUS_COLOR[amountStatus].text}>
                  {data?.amount >= 0 ? '+' : null}
                  <Currency value={data?.amount} />
                </div>
              }
            />
            <TableCell
              label={<FormattedMessage id="after_balance" />}
              content={<Currency value={data?.afterBalance} />}
            />
            <TableCell label={<FormattedMessage id="remarks" />} content={data?.description} />
          </>
        ) : (
          <Skeleton />
        )}
      </Container>
    </>
  )
}

export default FinancialDetail
