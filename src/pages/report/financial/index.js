import { useState, useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import * as dayjs from 'dayjs'
import { useSWRInfinite } from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import { showDockingMultipleControl } from 'components/core/DockingMultipleControl/openDocking'
import DockingFilter from 'components/core/DockingMultipleControl/components/DockingFilterList'
import EmptyList from './components/EmptyList'
import Group from './components/Group'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const FetchMoreButton = dynamic(() => import('components/core/FetchMoreButton'))
const DateMenuTab = dynamic(() => import('components/core/DateMenuTab'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function ReportFinancial() {
  const intl = useIntl()
  const router = useRouter()
  const { type } = router.query
  const now = dayjs()
  const [state, setState] = useState({
    start: now.startOf('d').unix(),
    end: now.endOf('d').unix(),
    status: type || 4095,
  })
  useEffect(() => {
    setState(prevState => ({
      ...prevState,
      status: type,
    }))
  }, [type])
  const { start, end, status } = state

  const { data: financialData, isValidating, size, setSize } = useSWRInfinite(
    index =>
      `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}/v1/api/report/financial/${status}?start=${start}&end=${end}&pageNumber=${index}&pageCapacity=10`,
    url => fetcher({ url, withToken: true }),
  )
  const financialList = financialData && financialData.map(d => d?.data?.data)

  function onChange(payload) {
    setState(prevState => ({
      ...prevState,
      ...payload,
    }))
  }

  const typeOptions = [
    { type: 4095, name: intl.formatMessage({ id: 'financial_type.4095' }) },
    { type: 1, name: intl.formatMessage({ id: 'financial_type.1' }) },
    { type: 2, name: intl.formatMessage({ id: 'financial_type.2' }) },
    { type: 4, name: intl.formatMessage({ id: 'financial_type.4' }) },
    { type: 8, name: intl.formatMessage({ id: 'financial_type.8' }) },
    { type: 16, name: intl.formatMessage({ id: 'financial_type.16' }) },
    { type: 32, name: intl.formatMessage({ id: 'financial_type.32' }) },
    { type: 64, name: intl.formatMessage({ id: 'financial_type.64' }) },
    { type: 128, name: intl.formatMessage({ id: 'financial_type.128' }) },
    { type: 256, name: intl.formatMessage({ id: 'financial_type.256' }) },
    { type: 512, name: intl.formatMessage({ id: 'financial_type.512' }) },
    { type: 1024, name: intl.formatMessage({ id: 'financial_type.1024' }) },
    { type: 2048, name: intl.formatMessage({ id: 'financial_type.2048' }) },
  ]
  const filterData = {
    typeOptions,
    activate: status || 4095,
    router,
    reportType: 'financial',
    btnText: {
      cancel: intl.formatMessage({ id: 'cancel' }),
      confirm: intl.formatMessage({ id: 'confirm' }),
    },
  }

  const handleShowFilter = useCallback(
    () => showDockingMultipleControl(filterData, '', DockingFilter, true),
    [filterData],
  )

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.push('/user')
        }}
        title={intl.formatMessage({ id: 'financial_record' })}
        right={
          <span className="text-body-4 text-platinum-200">
            {intl.formatMessage({ id: 'filter' })}
          </span>
        }
        onRightClick={handleShowFilter}
      />
      <DateMenuTab onChange={onChange} />
      <Container withNavigationBar noPadding>
        {(isValidating || !financialList) && <Skeleton className="p-3" />}
        {!isValidating && financialList && !financialList?.[0]?.container.length && <EmptyList />}
        {!isValidating && financialList && (
          <div className="mt-11">
            {financialList.map((list, index) => (
              <Group items={list.container} key={index.toString()} />
            ))}
            <FetchMoreButton
              onClick={() => setSize(size + 1)}
              currentIndex={size}
              totalCount={financialList?.[0]?.totalCount}
            />
          </div>
        )}
      </Container>
    </>
  )
}

ReportFinancial.protect = true

export default ReportFinancial
