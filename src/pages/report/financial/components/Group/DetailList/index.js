import Link from 'next/link'
import * as dayjs from 'dayjs'
import { FormattedMessage } from 'react-intl'
import Currency from 'components/core/Currency'
import Card from 'components/core/Card'
import CardHeader from 'components/core/CardHeader'

const STATUS_COLOR = {
  positive: {
    border: 'red',
    text: 'text-red',
  },
  negative: {
    border: 'platinum',
    text: 'text-platinum-200',
  },
}

function DetailList({ data }) {
  const {
    amount,
    createTime,
    financialName,
    id,
    lotteryCode,
    orderNumber,
    subType,
    targetName,
    type,
  } = data
  const amountStatus = amount > 0 ? 'positive' : 'negative'

  return (
    <Link href={`/report/financial/detail?id=${id}`}>
      <Card color={STATUS_COLOR[amountStatus].border}>
        <CardHeader
          title={
            <div className={`text-title-7 ${STATUS_COLOR[amountStatus].text}`}>
              {amount >= 0 ? '+' : null}
              <Currency value={amount} />
            </div>
          }
          subTitle={orderNumber}
        />
        <table className="w-full text-body-6 big:text-body-4">
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="type" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">{financialName}</td>
          </tr>
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="time" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">
              {dayjs.unix(createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
            </td>
          </tr>
        </table>
      </Card>
    </Link>
  )
}

export default DetailList
