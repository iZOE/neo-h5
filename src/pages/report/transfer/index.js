import { useState } from 'react'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import * as dayjs from 'dayjs'
import { useSWRInfinite } from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import EmptyList from './components/EmptyList'
import TitleOptions from './components/TitleOptions'
import CardItem from './components/CardItem'

const now = dayjs()

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const FetchMoreButton = dynamic(() => import('components/core/FetchMoreButton'))
const DateMenuTab = dynamic(() => import('components/core/DateMenuTab'))
const DateRangePicker = dynamic(() => import('components/core/DateRangePicker'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function ReportTransfer() {
  const intl = useIntl()
  const router = useRouter()
  const [state, setState] = useState({
    start: now.startOf('d').unix(),
    end: now.endOf('d').unix(),
    type: 0,
  })
  const { start, end, type } = state

  const { data: items, isValidating } = useSWRInfinite(
    index =>
      `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/report/transfer/${type}?start=${start}&end=${end}&pageNumber=${index}&pageCapacity=10&isSortingDesc=true&sortingColumn=createTime`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
  )

  function onChange(payload) {
    setState(prevState => ({
      ...prevState,
      ...payload,
      type: payload.id ? payload.id : 0,
    }))
  }

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.push('/user')
        }}
        centerComponent={<TitleOptions onChange={onChange} />}
      />
      <DateMenuTab onChange={onChange} />
      <Container withNavigationBar noPadding>
        {(isValidating || !items) && <Skeleton className="p-3" />}
        {!isValidating && items && !items[0].container.length && <EmptyList />}
        {!isValidating && items && (
          <div className="mt-11">
            {items.map(group => (
              <div className="group">
                {group.container.map(item => (
                  <CardItem key={item.orderNumber} item={item} />
                ))}
              </div>
            ))}
          </div>
        )}
      </Container>
    </>
  )
}

ReportTransfer.protect = true

export default ReportTransfer
