import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'

const DropDownList = dynamic(() => import('components/core/DropDownList'))

export default function TitleOptions({ onChange }) {
  const { data: options } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/fundtransfer/type`,
    async url => {
      const data = await fetcher({ url, withToken: true })
      const items = data.data.data.map(d => ({
        id: d.fundTransferTypeId,
        name: d.description,
      }))

      return items
    },
  )
  return <DropDownList onChange={onChange} dropDownList={options} />
}
