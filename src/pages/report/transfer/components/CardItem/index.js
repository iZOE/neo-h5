import { memo } from 'react'
import dynamic from 'next/dynamic'
import * as dayjs from 'dayjs'
import Link from 'next/link'
import { FormattedMessage } from 'react-intl'

const Currency = dynamic(() => import('components/core/Currency'))
const Card = dynamic(() => import('components/core/Card'))
const CardHeader = dynamic(() => import('components/core/CardHeader'))

function CardItem({ item }) {
  return (
    <Link href={`/report/transfer/detail?id=${item.orderNumber}`}>
      <Card color={item.amount > 0 && 'red'}>
        <CardHeader
          title={
            <span className={item.amount > 0 ? 'text-red' : 'text-black'}>
              <Currency value={item.amount} />
            </span>
          }
          subTitle={item.orderNumber}
        />
        <table className="w-full text-body-6 big:text-body-4">
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="fields.description" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">{item.transferTypeText}</td>
          </tr>
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="fields.createTime" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">
              {dayjs.unix(item.createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
            </td>
          </tr>
        </table>
      </Card>
    </Link>
  )
}

export default memo(CardItem, () => true)
