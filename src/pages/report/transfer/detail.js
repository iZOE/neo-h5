import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import * as dayjs from 'dayjs'
import fetcher from 'libs/fetcher'
import { FormattedMessage } from 'react-intl'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const TableCell = dynamic(() => import('components/core/TableCell'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Currency = dynamic(() => import('components/core/Currency'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function TransferDetail() {
  const router = useRouter()
  const { id } = router.query

  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/report/transfer/${id}`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
  )

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/report/transfer')}
        title={<FormattedMessage id="title" />}
      />
      <Container withNavigationBar>
        {data ? (
          <>
            <TableCell
              label={<FormattedMessage id="fields.order_number" />}
              content={data?.orderNumber}
            />
            <TableCell
              label={<FormattedMessage id="fields.create_time" />}
              content={
                data?.createTime
                  ? dayjs.unix(data.createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)
                  : ''
              }
            />
            <TableCell
              label={<FormattedMessage id="fields.description" />}
              content={data?.transferTypeText}
            />
            <TableCell
              label={<FormattedMessage id="fields.amount" />}
              content={
                <span className={data?.amount > 0 ? 'text-red' : 'text-black dark:text-white'}>
                  <Currency value={data?.amount} />
                </span>
              }
            />
            <TableCell
              label={<FormattedMessage id="fields.preferential_amount" />}
              content={
                <span
                  className={
                    data?.preferentialAmount > 0 ? 'text-red' : 'text-black dark:text-white'
                  }
                >
                  <Currency value={data?.preferentialAmount} />
                </span>
              }
            />
            <TableCell
              label={<FormattedMessage id="fields.note" />}
              content={data?.description || '--'}
            />
          </>
        ) : (
          <Skeleton />
        )}
      </Container>
    </>
  )
}

export default TransferDetail
