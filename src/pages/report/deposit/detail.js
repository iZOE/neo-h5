import React, { useEffect, useCallback, useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import * as dayjs from 'dayjs'
import fetcher from 'libs/fetcher'
import { getCompressImg } from 'libs/canvasUtils'
import { FormattedMessage } from 'react-intl'
import Currency from 'components/core/Currency'
import Picture from 'components/core/Picture'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const TableCell = dynamic(() => import('components/core/TableCell'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const IconAdd = dynamic(() => import('components/icons/Add.svg'))
const IconDelete = dynamic(() => import('components/icons/Delete.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

const STATUS_COLOR = {
  1: {
    text: 'text-red',
  },
  2: {
    text: 'text-blue-200',
  },
  4: {
    text: 'text-platinum-200',
  },
}

function DepositDetail() {
  const router = useRouter()
  const { id } = router.query
  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/report/deposit/${id}`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
  )
  const [imgUrl, setImgUrl] = useState(data?.imgUrl)

  useEffect(() => {
    setImgUrl(data?.imgUrl)
  }, [data?.imgUrl])

  const handleCompressedUpload = useCallback(async e => {
    const imageFile = e.target.files[0]
    try {
      const compressImg = await getCompressImg(imageFile)
      const formData = new FormData()
      formData.append('image', compressImg)
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/deposit/${id}/upload`,
        method: 'post',
        withToken: true,
        data: formData,
      })
      setImgUrl(res?.data?.data)
    } catch (error) {
      console.log('error', error)
    }
  }, [])

  const handleDeleteImage = useCallback(async () => {
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/deposit/${id}/clear`,
        method: 'delete',
        withToken: true,
      })
      setImgUrl(null)
    } catch (error) {
      console.log('error', error)
    }
  }, [])

  let imageArea = null
  if (imgUrl) {
    imageArea = (
      <div className="flex justify-center py-6">
        <Picture
          className="text-platinum-200 border-platinum-200 border-solid border rounded"
          png={`${process.env.NEXT_PUBLIC_END_POINT_STORAGE}${imgUrl}`}
          webp={`${process.env.NEXT_PUBLIC_END_POINT_STORAGE}${imgUrl}`}
          alt="depositImage"
          style={{ width: '160px' }}
        />
        {data?.canUploadImg ? (
          <span className="h-4 relative -left-5 top-1" onClick={handleDeleteImage}>
            <IconDelete />
          </span>
        ) : null}
      </div>
    )
  } else if (data?.canUploadImg) {
    imageArea = (
      <div className="flex justify-center py-6">
        <label htmlFor="image-input">
          <div
            className="text-platinum-200 border-platinum-200 border-solid border rounded text-center py-4 text-body-8"
            style={{ width: '160px', height: '80px' }}
          >
            <div className="w-6 my-0 mx-auto">
              <IconAdd />
            </div>
            <div className="py-1">
              <FormattedMessage id="upload_image" />
            </div>
          </div>
        </label>
        <input
          id="image-input"
          className="hidden"
          type="file"
          accept="image/*"
          onChange={handleCompressedUpload}
        />
      </div>
    )
  }
  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={<FormattedMessage id="deposit_detail" />}
      />
      <Container withNavigationBar>
        {data ? (
          <>
            <TableCell
              label={<FormattedMessage id="order_number" />}
              content={data?.orderNumber}
              right={
                <span className={`${STATUS_COLOR[data?.status || 4].text}`}>
                  <FormattedMessage id={`deposit_status.${data?.status}`} />
                </span>
              }
            />
            <TableCell
              label={<FormattedMessage id="apply_time" />}
              content={dayjs.unix(data?.createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
            />
            <TableCell
              label={<FormattedMessage id="achieved_time" />}
              content={
                data?.status === 2
                  ? dayjs.unix(data?.dealTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)
                  : '--'
              }
            />
            <TableCell label={<FormattedMessage id="pay_name" />} content={data?.bankName} />
            <TableCell
              label={
                data?.depositWay === 3 ? (
                  <FormattedMessage id="crypto_protocol" />
                ) : (
                  <FormattedMessage id="account_name" />
                )
              }
              content={data?.depositWay === 3 ? data?.cryptoProtocol : data?.accountName}
            />
            <TableCell
              label={
                data?.depositWay === 3 ? (
                  <FormattedMessage id="crypto_address" />
                ) : (
                  <FormattedMessage id="account" />
                )
              }
              content={data?.bankAccount}
            />
            <TableCell
              label={<FormattedMessage id="request_amount" />}
              content={
                <div className="text-red">
                  <Currency value={data?.requestAmount} />
                </div>
              }
            />
            <TableCell
              label={<FormattedMessage id="actual_amount" />}
              content={data?.status === 2 ? <Currency value={data?.amount} /> : '--'}
            />
            <TableCell label={<FormattedMessage id="currency" />} content={data?.currency} />
            <TableCell
              label={<FormattedMessage id="handling_fee" />}
              content={
                <div>
                  <Currency value={data?.memberCommissionFee} />%
                </div>
              }
            />
            <TableCell
              label={<FormattedMessage id="exchange_rate" />}
              content={<Currency value={data?.exchangeRate} />}
            />
            {data?.depositWay === 3 ? (
              <TableCell
                label={<FormattedMessage id="actual_deposit_amount" />}
                content={<Currency value={data?.depositAmount} />}
              />
            ) : (
              <TableCell
                label={<FormattedMessage id="append_message" />}
                content={data?.appendMessage}
              />
            )}
            <TableCell
              label={<FormattedMessage id="remarks" />}
              content={!data?.description ? '--' : data?.description}
            />
            {imageArea}
          </>
        ) : (
          <Skeleton />
        )}
      </Container>
    </>
  )
}

export default DepositDetail
