import { useState, useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import * as dayjs from 'dayjs'
import useSWR, { useSWRInfinite } from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import { showDockingMultipleControl } from 'components/core/DockingMultipleControl/openDocking'
import DockingFilter from 'components/core/DockingMultipleControl/components/DockingFilterList'
import EmptyList from './components/EmptyList'
import Group from './components/Group'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const FetchMoreButton = dynamic(() => import('components/core/FetchMoreButton'))
const DateMenuTab = dynamic(() => import('components/core/DateMenuTab'))
const DateRangePicker = dynamic(() => import('components/core/DateRangePicker'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function ReportDeposit() {
  const intl = useIntl()
  const router = useRouter()
  const { type } = router.query
  const now = dayjs()
  const [state, setState] = useState({
    start: now.startOf('d').unix(),
    end: now.endOf('d').unix(),
    status: type || 7,
  })
  useEffect(() => {
    setState(prevState => ({
      ...prevState,
      status: type || 7,
    }))
  }, [type])
  const { start, end, status } = state

  const { data: depositData, isValidating, size, setSize } = useSWRInfinite(
    index =>
      `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}/v1/api/report/deposit/${status}?start=${start}&end=${end}&pageNumber=${index}&pageCapacity=10&status=${status}`,
    url => fetcher({ url, withToken: true }),
  )
  const depositList = depositData && depositData.map(d => d?.data?.data)

  function onChange(payload) {
    setState(prevState => ({
      ...prevState,
      ...payload,
    }))
  }

  const typeOptions = [
    { type: 7, name: intl.formatMessage({ id: 'deposit_status.7' }) },
    { type: 1, name: intl.formatMessage({ id: 'deposit_status.1' }) },
    { type: 2, name: intl.formatMessage({ id: 'deposit_status.2' }) },
  ]
  const filterData = {
    typeOptions,
    activate: type || 7,
    router,
    reportType: 'deposit',
    btnText: {
      cancel: intl.formatMessage({ id: 'cancel' }),
      confirm: intl.formatMessage({ id: 'confirm' }),
    },
  }

  const handleShowFilter = useCallback(
    () => showDockingMultipleControl(filterData, '', DockingFilter, true),
    [filterData],
  )
  console.log('DDDDD', type, start, end, status)
  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.push('/user')
        }}
        title={intl.formatMessage({ id: 'deposit_record' })}
        right={
          <span className="text-body-4 text-platinum-200">
            {intl.formatMessage({ id: 'filter' })}
          </span>
        }
        onRightClick={handleShowFilter}
      />
      <DateMenuTab onChange={onChange} />
      <Container withNavigationBar noPadding>
        {(isValidating || !depositList) && <Skeleton className="p-3" />}
        {!isValidating && depositList && !depositList?.[0]?.container.length && <EmptyList />}
        {!isValidating && depositList && (
          <div className="mt-11">
            {depositList.map((list, index) => (
              <Group items={list.container} key={index.toString()} />
            ))}
            <FetchMoreButton
              onClick={() => setSize(size + 1)}
              currentIndex={size}
              totalCount={depositList?.[0]?.totalCount}
            />
          </div>
        )}
      </Container>
    </>
  )
}

ReportDeposit.protect = true

export default ReportDeposit
