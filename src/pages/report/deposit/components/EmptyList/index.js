import { FormattedMessage } from 'react-intl'
import Picture from 'components/core/Picture'

function EmptyList() {
  return (
    <div className="text-center relative top-[45vh] mt-[-140px]">
      <div className="w-[124px] m-auto">
        <Picture
          src="/reportImg/noData.png"
          png="/reportImg/noData.png"
          webp="/reportImg/noData.webp"
        />
      </div>

      <div className="text-body-6 text-platinum-100">
        <FormattedMessage id="no_data" />
      </div>
    </div>
  )
}

export default EmptyList
