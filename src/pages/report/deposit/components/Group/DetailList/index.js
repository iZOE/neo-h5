import Link from 'next/link'
import * as dayjs from 'dayjs'
import { FormattedMessage } from 'react-intl'
import Currency from 'components/core/Currency'
import Card from 'components/core/Card'
import CardHeader from 'components/core/CardHeader'

const STATUS_COLOR = {
  1: {
    border: 'red',
    text: 'text-red',
  },
  2: {
    border: 'blue',
    text: 'text-blue-200',
  },
  4: {
    border: 'platinum',
    text: 'text-platinum-200',
  },
}

function DetailList({ data }) {
  const { amount, createTime, dealTime, orderNumber, status } = data

  return (
    <Link href={`/report/deposit/detail?id=${orderNumber}`}>
      <Card color={STATUS_COLOR[status].border}>
        <CardHeader
          title={
            <div className="text-title-7 text-gray-500 dark:text-white">
              <Currency value={amount} />
            </div>
          }
          subTitle={orderNumber}
        />
        <table className="w-full text-body-6 big:text-body-4">
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="apply_time" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">
              {dayjs.unix(createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
            </td>
          </tr>
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="achieved_time" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">
              {status === 2
                ? dayjs.unix(dealTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)
                : '--'}
            </td>
          </tr>
          <tr>
            <td colSpan="2" className={`pt-2 text-right ${STATUS_COLOR[status].text}`}>
              <FormattedMessage id={`deposit_status.${status}`} />
            </td>
          </tr>
        </table>
      </Card>
    </Link>
  )
}

export default DetailList
