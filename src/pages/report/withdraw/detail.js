import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import * as dayjs from 'dayjs'
import { Decimal } from 'decimal.js'
import fetcher from 'libs/fetcher'
import { FormattedMessage, useIntl } from 'react-intl'
import Container from 'components/core/Container'
import Currency from 'components/core/Currency'
import StepFlow from './components/StepFlow'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const TableCell = dynamic(() => import('components/core/TableCell'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function WithdrawDetail() {
  const intl = useIntl()
  const router = useRouter()
  const { id } = router.query

  const { data } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/report/withdrawal/${id}`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
  )

  let actualAmount = '--'
  let actualReceivedAmount = actualAmount
  // 已出款
  if (data?.status === 4) {
    actualAmount = new Decimal(data?.requestAmount)
      .sub(data?.preferentialAmount)
      .sub(data?.withdrawalFee)
      .sub(data?.administrationFee)
      .div(data?.exchangeRate)
    actualReceivedAmount = data?.isVirtual ? (
      <div>
        <Currency value={actualAmount} /> {data?.currencyName}
      </div>
    ) : (
      <Currency value={actualAmount} />
    )
  }
  let ContainerElement = null
  if (!data) {
    ContainerElement = <Skeleton />
  } else {
    ContainerElement =
      data?.status > 2 ? (
        <Container withNavigationBar>
          <TableCell
            label={<FormattedMessage id="order_number" />}
            content={data?.orderNumber}
            right={
              <span className="text-platinum-200">
                <FormattedMessage id={`withdraw_status.${data?.status}`} />
              </span>
            }
          />
          <TableCell label={<FormattedMessage id="biller" />} content={data?.memberNickName} />
          <TableCell
            label={<FormattedMessage id="apply_time" />}
            content={dayjs.unix(data?.createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
          />
          <TableCell
            label={<FormattedMessage id="provide_time" />}
            content={
              data?.status === 4
                ? dayjs.unix(data?.dealTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)
                : '--'
            }
          />
          <TableCell
            label={<FormattedMessage id="request_amount" />}
            content={
              <div className="text-red">
                <Currency value={data?.requestAmount} />
              </div>
            }
          />
          <TableCell
            label={
              data?.isVirtual ? (
                <FormattedMessage id="receive_wallet" />
              ) : (
                <FormattedMessage id="receive_bank" />
              )
            }
            content={data?.isVirtual ? data?.cryptoNickName : data?.bankName}
          />
          {data?.isVirtual ? (
            <TableCell
              label={<FormattedMessage id="crypto_address" />}
              content={data?.cryptoAddress}
            />
          ) : (
            <TableCell
              label={<FormattedMessage id="bankcard_number" />}
              content={data?.cardNumber}
            />
          )}
          <TableCell
            label={<FormattedMessage id="handling_fee" />}
            content={<Currency value={data?.withdrawalFee} />}
          />
          <TableCell
            label={<FormattedMessage id="administration_fee" />}
            content={<Currency value={data?.administrationFee} />}
          />
          <TableCell
            label={<FormattedMessage id="actual_amount" />}
            content={actualReceivedAmount}
          />
          {data?.isVirtual ? (
            <TableCell
              label={<FormattedMessage id="exchange_rate" />}
              content={<Currency value={data?.exchangeRate} />}
            />
          ) : null}
          <TableCell
            label={<FormattedMessage id="remarks" />}
            content={!data?.description ? '--' : data?.description}
          />
        </Container>
      ) : (
        <Container withNavigationBar noPadding>
          <div>
            <div className="bg-blue-200 text-white relative px-3 py-2">
              <span className="text-body-2">
                <Currency value={data?.requestAmount} />
              </span>
              <span className="absolute right-3 top-3 text-body-6">
                <FormattedMessage id={`withdraw_status.${data?.status}`} />
              </span>
            </div>
            <div className="p-3 text-body-6 text-platinum-200">
              <FormattedMessage id="review_stage" />
            </div>
            <StepFlow
              steps={[
                {
                  type: intl.formatMessage({ id: 'apply_withdrawal' }),
                  subItem: dayjs
                    .unix(data?.createTime)
                    .format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT),
                },
                {
                  type: intl.formatMessage({ id: 'withdraw_status.2' }),
                  subItem: dayjs
                    .unix(data?.dealTime)
                    .format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT),
                },
                {
                  type: intl.formatMessage({ id: 'review_completed' }),
                },
              ]}
              currentlyStep={data?.status}
            />
          </div>
        </Container>
      )
  }
  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={<FormattedMessage id="withdraw_detail" />}
      />
      {ContainerElement}
    </>
  )
}

export default WithdrawDetail
