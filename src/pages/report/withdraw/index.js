import { useState, useCallback, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import * as dayjs from 'dayjs'
import { useSWRInfinite } from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import { showDockingMultipleControl } from 'components/core/DockingMultipleControl/openDocking'
import DockingFilter from 'components/core/DockingMultipleControl/components/DockingFilterList'
import EmptyList from './components/EmptyList'
import Group from './components/Group'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const FetchMoreButton = dynamic(() => import('components/core/FetchMoreButton'))
const DateMenuTab = dynamic(() => import('components/core/DateMenuTab'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function ReportWithdraw() {
  const intl = useIntl()
  const router = useRouter()
  const { type } = router.query
  const now = dayjs()
  const [state, setState] = useState({
    start: now.startOf('d').unix(),
    end: now.endOf('d').unix(),
    status: type || 31,
  })

  useEffect(() => {
    setState(prevState => ({
      ...prevState,
      status: type,
    }))
  }, [type])

  const { start, end, status } = state

  const { data: withdrawData, isValidating, size, setSize } = useSWRInfinite(
    index =>
      `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}/v1/api/report/withdrawal/${status}?start=${start}&end=${end}&pageNumber=${index}&pageCapacity=10&status=${status}`,
    url => fetcher({ url, withToken: true }),
  )
  const withdrawList = withdrawData && withdrawData.map(d => d?.data?.data)

  function onChange(payload) {
    setState(prevState => ({
      ...prevState,
      ...payload,
    }))
  }

  const typeOptions = [
    { type: 31, name: intl.formatMessage({ id: 'withdraw_status.31' }) },
    { type: 1, name: intl.formatMessage({ id: 'withdraw_status.1' }) },
    { type: 2, name: intl.formatMessage({ id: 'withdraw_status.2' }) },
    { type: 4, name: intl.formatMessage({ id: 'withdraw_status.4' }) },
    { type: 8, name: intl.formatMessage({ id: 'withdraw_status.8' }) },
    { type: 16, name: intl.formatMessage({ id: 'withdraw_status.16' }) },
  ]
  const filterData = {
    typeOptions,
    activate: status || 31,
    router,
    reportType: 'withdraw',
    btnText: {
      cancel: intl.formatMessage({ id: 'cancel' }),
      confirm: intl.formatMessage({ id: 'confirm' }),
    },
  }

  const handleShowFilter = useCallback(
    () => showDockingMultipleControl(filterData, '', DockingFilter, true),
    [filterData],
  )

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.push('/user')
        }}
        title={intl.formatMessage({ id: 'withdraw_record' })}
        right={
          <span className="text-body-4 text-platinum-200">
            {intl.formatMessage({ id: 'filter' })}
          </span>
        }
        onRightClick={handleShowFilter}
      />
      <DateMenuTab onChange={onChange} />
      <Container withNavigationBar noPadding>
        {(isValidating || !withdrawList) && <Skeleton className="p-3" />}
        {!isValidating && withdrawList && !withdrawList?.[0]?.container.length && <EmptyList />}
        {!isValidating && withdrawList && (
          <div className="mt-11">
            {withdrawList.map((list, index) => (
              <Group items={list.container} key={index.toString()} />
            ))}
            <FetchMoreButton
              onClick={() => setSize(size + 1)}
              currentIndex={size}
              totalCount={withdrawList?.[0]?.totalCount}
            />
          </div>
        )}
      </Container>
    </>
  )
}

ReportWithdraw.protect = true

export default ReportWithdraw
