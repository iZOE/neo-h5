import { memo } from 'react'
import DetailList from './DetailList'

function Group({ items }) {
  return (
    <>
      {items.map((data, index) => (
        <DetailList data={data} key={index.toString()} />
      ))}
    </>
  )
}

export default memo(Group)
