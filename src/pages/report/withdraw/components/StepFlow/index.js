import React from 'react'

function StepFlow(props) {
  const { steps, currentlyStep } = props
  return (
    <>
      <ul>
        {steps.map((step, index) => (
          <li className="pl-3 flex" key={`step_${index.toString()}`}>
            {steps.length > index + 1 ? (
              <div className="relative border-l border-gray-100 left-4 top-4" />
            ) : null}
            {currentlyStep > index ? (
              <>
                <div className="relative border-l border-gray-100 left-4 top-4" />
                <div
                  className="text-body-4 bg-blue-200 text-white rounded-full text-center w-8 h-8 align-top z-10"
                  style={{ padding: '6px' }}
                >
                  {index + 1}
                </div>
                <p className="flex-1 mb-7 pl-2 pb-7 border-gray-100 border-b text-body-2 text-gray-500 dark:text-white">
                  {step.type}
                  <span className="block text-body-6">{step.subItem}</span>
                </p>
              </>
            ) : (
              <>
                <div className="w-8 text-center">
                  <span className="w-3 h-3 rounded-full bg-gray-200 inline-block" />
                </div>
                <p className="flex-1 mb-7 pl-2 pb-7 border-gray-100 border-b text-body-2 text-gray-400 dark:text-white">
                  {step.type}
                </p>
              </>
            )}
          </li>
        ))}
      </ul>
    </>
  )
}

export default StepFlow
