import { useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR, { useSWRInfinite } from 'swr'
import * as dayjs from 'dayjs'
import fetcher from 'libs/fetcher'
import EmptyList from './components/EmptyList'
import Group from './components/Group'
import SubTotal from './components/SubTotal'

const FetchMoreButton = dynamic(() => import('components/core/FetchMoreButton'))
const DateMenuTab = dynamic(() => import('components/core/DateMenuTab'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const DropDownList = dynamic(() => import('components/core/DropDownList'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function Bet() {
  const now = dayjs()
  const router = useRouter()
  const [state, setState] = useState({
    start: now.startOf('d').unix(),
    end: now.endOf('d').unix(),
    gameProviderId: 0,
  })
  const { start, end, gameProviderId } = state

  const { data: gameProviderList, isValidating: isPorviderValidating } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      const items = res?.data?.data.map(d => ({
        id: d.gameProviderId,
        name: d.remark,
      }))
      return items
    },
  )

  const { data: betList, isValidating, size, setSize } = useSWRInfinite(
    index =>
      `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}/v2/api/report/betorder?start=${start}&end=${end}&status=0&gameProviderId=${gameProviderId}&pageNumber=${index}&pageCapacity=10`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
  )

  function onChange(payload) {
    setState(prevState => ({
      ...prevState,
      ...payload,
      gameProviderId: payload.id ?? prevState.gameProviderId,
    }))
  }
  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => {
          router.push('/user')
        }}
        centerComponent={
          !isPorviderValidating &&
          Array.isArray(gameProviderList) && (
            <DropDownList onChange={onChange} dropDownList={gameProviderList} />
          )
        }
      />
      <DateMenuTab onChange={onChange} />
      <Container withNavigationBar noPadding>
        {(isValidating || !betList) && <Skeleton className="p-3" />}
        {!isValidating && betList && !betList[0]?.container.length && <EmptyList />}
        {!isValidating && betList && (
          <div className="mt-10">
            <SubTotal subTotal={betList?.[0]} />
            <>
              {betList.map((list, index) => (
                <Group items={list.container} key={index.toString()} />
              ))}
              <FetchMoreButton
                onClick={() => setSize(size + 1)}
                currentIndex={size}
                totalCount={betList?.[0]?.totalCount}
              />
            </>
          </div>
        )}
      </Container>
    </>
  )
}

Bet.protect = true

export default Bet
