import Link from 'next/link'
import * as dayjs from 'dayjs'
import { FormattedMessage } from 'react-intl'
import Picture from 'components/core/Picture'
import Currency from 'components/core/Currency'
import Card from 'components/core/Card'
import CardHeader from 'components/core/CardHeader'

const STATUS_COLOR = {
  win: {
    border: 'red',
    text: 'text-red',
  },
  loss: {
    border: 'platinum',
    text: 'text-platinum-200',
  },
  noResult: {
    border: 'blue',
    text: 'text-blue-200',
  },
}

function DetailList({ data }) {
  const {
    gameName,
    gameProviderId,
    betNumber,
    betOrderNo,
    ruleName,
    betTime,
    status,
    winLossAmount,
    betAmount,
  } = data
  let winningStatus = winLossAmount > 0 ? 'win' : 'loss'
  if (status === 0) {
    winningStatus = 'noResult'
  }
  return (
    <Link href={`/report/bet/detail?id=${betOrderNo}&gameProviderId=${gameProviderId}`}>
      <Card color={STATUS_COLOR[winningStatus].border}>
        <CardHeader
          title={<div className="text-title-7 text-gray-500 dark:text-white">{gameName}</div>}
          subTitle={<FormattedMessage id="period" values={{ betNumber }} />}
        />
        {winningStatus === 'win' && (
          <div className="absolute right-2 top-8" style={{ width: '60px' }}>
            <Picture src="/reportImg/win.png" png="/reportImg/win.png" webp="/reportImg/win.webp" />
          </div>
        )}
        <table className="w-full text-body-6 big:text-body-4">
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="bet_amount" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">
              <Currency value={betAmount} />
            </td>
          </tr>
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="rule_name" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">{ruleName}</td>
          </tr>
          <tr>
            <td className="pt-2 text-platinum-200">
              <FormattedMessage id="bet_time" />
            </td>
            <td className="pt-2 text-gray-500 dark:text-white">
              {dayjs.unix(betTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
            </td>
          </tr>
          <tr>
            <td colSpan="2" className={`pt-2 text-right ${STATUS_COLOR[winningStatus].text}`}>
              {winningStatus === 'win' && '+'}
              {winningStatus === 'noResult' ? (
                <FormattedMessage id="no_result" />
              ) : (
                <Currency value={winLossAmount} />
              )}
            </td>
          </tr>
        </table>
      </Card>
    </Link>
  )
}

export default DetailList
