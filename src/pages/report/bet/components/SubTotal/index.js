import Currency from 'components/core/Currency'
import { FormattedMessage } from 'react-intl'

function SubTotal({ subTotal }) {
  if (!subTotal) return null
  const { totalCount, totalBetAmount, totalWinLossAmount } = subTotal
  return (
    <div className="py-[12px] px-[10.5px] flex justify-around bg-white dark:bg-gray-700">
      <div>
        <span className="text-platinum-200 text-body-8 mr-[2px]">
          <FormattedMessage id="total_count" />
        </span>
        <span className="text-platinum-200 text-body-6">{totalCount}</span>
      </div>
      <div>
        <span className="text-platinum-200 text-body-8 mr-[2px]">
          <FormattedMessage id="total_bet_amount" />
        </span>
        <span className="text-platinum-200 text-body-6">
          <Currency value={totalBetAmount} />
        </span>
      </div>
      <div>
        <span className="text-platinum-200 text-body-8 mr-[2px]">
          <FormattedMessage id="win_loss" />
        </span>
        <span className="text-red text-body-6">
          <Currency value={totalWinLossAmount} decimalSize="text-body-8" />
        </span>
      </div>
    </div>
  )
}

export default SubTotal
