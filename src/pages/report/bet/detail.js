import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import * as dayjs from 'dayjs'
import fetcher from 'libs/fetcher'
import { FormattedMessage } from 'react-intl'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Container = dynamic(() => import('components/core/Container'))
const TableCell = dynamic(() => import('components/core/TableCell'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

function BetDetail() {
  const router = useRouter()
  const { gameProviderId, id } = router.query

  const {
    data,
  } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/report/betorder/${gameProviderId}/${id}`,
    url => fetcher({ url, withToken: true }),
  )

  if (!data) return null
  const {
    betAmount,
    bonusAmount,
    createTime,
    gameName,
    lotteryMoreInfo,
    orderNumber,
    ruleName,
    validBetAmount,
    winLossAmount,
    status,
  } = data?.data?.data

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.back()}
        title={<FormattedMessage id="bet_detail" />}
      />

      <Container withNavigationBar>
        {data ? (
          <>
            <TableCell
              label={<FormattedMessage id="order_number" />}
              content={orderNumber}
              right={
                <div className={status === 1 ? 'text-blue-100' : 'text-red'}>
                  <FormattedMessage id={`bet_status.${status}`} />
                </div>
              }
            />
            <TableCell
              label={<FormattedMessage id="create_time" />}
              content={dayjs.unix(createTime).format(process.env.NEXT_PUBLIC_TIME_FORMAT_LT)}
            />
            <TableCell label={<FormattedMessage id="game_name" />} content={gameName} />
            <TableCell label={<FormattedMessage id="rule_name" />} content={ruleName} />
            {lotteryMoreInfo && (
              <TableCell
                collapse
                label={<FormattedMessage id="lottery_more_info" />}
                contentComponent={
                  <>
                    <TableCell
                      label={<FormattedMessage id="issue_number" />}
                      content={lotteryMoreInfo.issueNumber}
                    />
                    <TableCell
                      label={<FormattedMessage id="sub_rule" />}
                      content={lotteryMoreInfo.subRule}
                    />
                    <TableCell
                      label={<FormattedMessage id="play_content" />}
                      content={lotteryMoreInfo.playContent}
                    />
                    <TableCell
                      label={<FormattedMessage id="winning_number" />}
                      content={lotteryMoreInfo.winningNumber}
                    />
                    <TableCell
                      label={<FormattedMessage id="odds" />}
                      content={lotteryMoreInfo.odds}
                    />
                    <TableCell
                      label={<FormattedMessage id="bet_count" />}
                      content={lotteryMoreInfo.betCount}
                    />
                    <TableCell
                      label={<FormattedMessage id="times" />}
                      content={lotteryMoreInfo.ruleName}
                    />
                  </>
                }
              />
            )}
            <TableCell label={<FormattedMessage id="bet_amount" />} content={betAmount} />
            <TableCell
              label={<FormattedMessage id="valid_bet_amount" />}
              content={validBetAmount}
            />
            <TableCell label={<FormattedMessage id="bonus_amount" />} content={bonusAmount} />
            <TableCell label={<FormattedMessage id="winLoss_amount" />} content={winLossAmount} />
          </>
        ) : (
          <Skeleton />
        )}
      </Container>
    </>
  )
}

export default BetDetail
