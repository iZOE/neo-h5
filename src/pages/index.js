import { useCallback, useState, useEffect } from 'react'
import WithTabBar from 'components/layout/WithTabBar'
import useSWR, { cache } from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'
import * as dayjs from 'dayjs'
import isBetween from 'dayjs/plugin/isBetween'
import PageTransition from 'components/shared/PageTransition'
import { useHandleError } from 'hooks/useHandleError'
import { NEXT_PUBLIC_SCAFFOLD_LOBBY_LAYOUT } from '../constants/processEnvVariables'

const NO_SHOW_ANNOUNCE_MODAL_STORAGE_KEY = 'noShowAnnounceModal'

dayjs.extend(isBetween)

const A = dynamic(() => import('../layouts/Home/TemplateA'), {
  loading: () => <p>Loading...</p>,
})

const B = dynamic(() => import('../layouts/Home/TemplateB'), {
  loading: () => <p>Loading...</p>,
})

const AnnounceModal = dynamic(() => import('pages-lib/index/components/AnnounceModal'))

const VIEW_MAPPER = {
  A,
  B,
}

const noticeBoardAPIAddr = `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/im/noticeboards?agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`

function Home({ template, sliders, gameList }) {
  const TemplateComponent = VIEW_MAPPER[template]
  const [isAnnounceModalShow, setIsAnnounceModalShow] = useState(false)

  const { data: container, error } = useSWR(
    noticeBoardAPIAddr,
    async url => {
      const res = await fetcher({ url })
      const now = dayjs()
      const post = res?.data?.data?.container
      const postsShouldPo = post.filter(
        a => now.isAfter(dayjs.unix(a.postBeginTime)) || a.pinnedOnTop || a.isMarqueeEnable,
      )

      return postsShouldPo
    },
    {
      revalidateOnFocus: false,
      revalidateOnMount: !cache.has(noticeBoardAPIAddr),
    },
  )

  useHandleError(error)

  const onAnnounceModalClose = () => {
    setIsAnnounceModalShow(false)
  }

  useEffect(() => {
    window.addEventListener('visibilitychange', evt => {
      console.log('on before_unload')
      evt.preventDefault()
      window.localStorage.setItem(NO_SHOW_ANNOUNCE_MODAL_STORAGE_KEY, false)
    })
  }, [])

  useEffect(() => {
    if (
      container?.length > 0 &&
      !JSON.parse(window.localStorage.getItem(NO_SHOW_ANNOUNCE_MODAL_STORAGE_KEY))
    ) {
      window.localStorage.setItem(NO_SHOW_ANNOUNCE_MODAL_STORAGE_KEY, true)
      setIsAnnounceModalShow(prev => true)
    }
  }, [container])

  return (
    <PageTransition style={{ height: '-webkit-fill-available' }}>
      <AnnounceModal
        isOpen={isAnnounceModalShow}
        handleClose={onAnnounceModalClose}
        data={container}
      />
      <TemplateComponent sliders={sliders} gameList={gameList} />
    </PageTransition>
  )
}

export async function getStaticProps() {
  const gameList = (await import('constants/gameList')).default
  const apiUrl = `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/Lobby?platform=4&agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`
  const res = await fetch(apiUrl)
  const { data } = await res.json()
  const now = dayjs()

  const sliders = data.sliders.filter(d =>
    now.isBetween(dayjs.unix(d.startTime), dayjs.unix(d.endTime)),
  )

  return {
    props: {
      template: process.env[NEXT_PUBLIC_SCAFFOLD_LOBBY_LAYOUT],
      sliders,
      gameList,
    },
  }
}

Home.TabBar = WithTabBar

export default Home
