import { AnimateSharedLayout } from 'framer-motion'
import Layout from 'layouts/withdraw-manage'
import BankCardList from 'pages-lib/withdraw-manage/banks/components/BankCardsList'

function Banks(props) {
  return <Layout>{(bankCards, cryptoCurrencies) => <BankCardList bankCards={bankCards} />}</Layout>
}

Banks.Layout = AnimateSharedLayout

export default Banks
