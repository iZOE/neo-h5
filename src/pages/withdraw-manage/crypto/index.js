import { AnimateSharedLayout } from 'framer-motion'
import Layout from 'layouts/withdraw-manage'
import WalletList from 'pages-lib/withdraw-manage/crypto/components/WalletList'

function CryptocurrencyWallet(props) {
  return (
    <Layout>{(bankCards, cryptoCurrencies) => <WalletList wallets={cryptoCurrencies} />}</Layout>
  )
}

CryptocurrencyWallet.Layout = AnimateSharedLayout

export default CryptocurrencyWallet
