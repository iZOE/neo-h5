import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'

const NavBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back'))
const IconCustomerService = dynamic(() =>
  import('components/icons/CustomerService')
)
const Container = dynamic(() => import('components/core/Container'))
const WalletArea = dynamic(() => import('./components/WalletArea'))
const AutoTransferArea = dynamic(() => import('./components/AutoTransferArea'))
const TransferIOArea = dynamic(() => import('./components/TransferIOArea'))

const Transfer = () => {
  const router = useRouter()

  return (
    <>
      <NavBar
        fixed
        left={<IconBack />}
        right={<IconCustomerService />}
        onLeftClick={() => router.back()}
        onRightClick={() => router.push('/service')}
        title={<FormattedMessage id="title" />}
      />
      <Container withNavigationBar noPadding>
        <WalletArea />
        <AutoTransferArea />
        <TransferIOArea />
      </Container>
    </>
  )
}

Transfer.protect = true

export default Transfer
