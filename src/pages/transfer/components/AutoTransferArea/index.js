import { useCallback, useState, useEffect } from 'react'
import { FormattedMessage } from 'react-intl'
import { motion } from 'framer-motion'
import dynamic from 'next/dynamic'
import useLocalStorage from 'hooks/useLocalStorage'
import style from '../../transfer.module.scss'

const spring = {
  type: 'spring',
  stiffness: 700,
  damping: 30,
}

const AreaContainer = dynamic(() => import('../AreaContainer'))

const AutoTransferArea = () => {
  const [switchIsOn, setSwitchIsOn] = useState(true)
  const [storedIsAutoTransfer, setStoredIsAutoTransfer] = useLocalStorage('is_auto_transfer')

  const handleToggleSwitch = useCallback(() => {
    setSwitchIsOn(prevIsOn => !prevIsOn)
  }, [])

  useEffect(() => {
    setStoredIsAutoTransfer(switchIsOn)
  }, [switchIsOn])

  return (
    <AreaContainer>
      <div className="flex items-center px-1">
        <p className="flex-none text-body-4 font-semibold text-platinum-200">
          <FormattedMessage id="area.auto_transfer.switch" />
        </p>
        <span className="flex-grow" />
        <div
          onClick={handleToggleSwitch}
          className={`flex-none inline-grid w-10 h-5 rounded-full ${switchIsOn ? style.activeSwitch : style.inactiveSwitch}`}>
          <motion.span transition={spring} layout className={style.switchHandler} />
        </div>

      </div>
    </AreaContainer>
  )
}

export default AutoTransferArea