const AreaContainer = ({ children }) => {
  return (
    <div className="mt-2 p-3 bg-white dark:bg-purple-800">
      { children}
    </div>
  )
}

export default AreaContainer
