import { useCallback, useState, useMemo, useEffect } from 'react'
import { useIntl } from 'react-intl'
import useSWR, { trigger } from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'

const Skeleton = dynamic(() => import('components/shared/Skeleton/Detail'))
const AreaContainer = dynamic(() => import('../AreaContainer'))
const MainWallet = dynamic(() => import('./components/MainWallet'))
const WithdrawAllButton = dynamic(() => import('./components/WithdrawAllButton'))
const ThirdPartyWallet = dynamic(() => import('./components/ThirdPartyWallet'))

const MAIN_WALLET_ID = 1

const TIME = 60

const WalletArea = () => {
  const intl = useIntl()
  const [countdown, setCountdown] = useState(TIME)
  const [timerStart, setTimerStart] = useState(false)
  const [isSpinShown, setIsSpinShown] = useState(false)
  const [isAmountLoading, setIsAmountLoading] = useState(false)

  const { data: walletList } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/wallet`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data?.gameProviderMoney
    }, {
    shouldRetryOnError: false,
  },
  )

  const { data: thirdPartyList } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
  )

  const { mainWallet, thirdPartyWallet } = useMemo(() => {
    let main = {}
    let thirdParty = []

    if (walletList) {
      main = walletList.find(({ gameProviderId: id }) => id === MAIN_WALLET_ID)

      if (thirdPartyList) {
        const list = walletList.map((item, i) => ({
          ...item,
          ...thirdPartyList[i],
        }))
        thirdParty = list.filter(({ isInternal }) => !isInternal)
      }
    }

    return {
      mainWallet: main,
      thirdPartyWallet: thirdParty,
    }
  }, [walletList, thirdPartyList])

  const { validGameProviderIds } = useMemo(() => {
    const list = []

    if (thirdPartyWallet) {
      thirdPartyWallet.map(item => {
        if (item.money >= 1) {
          list.push(item.gameProviderId)
        }
      })
    }

    return { validGameProviderIds: list }
  }, [thirdPartyWallet])

  useEffect(() => {
    if (!timerStart) {
      return null
    }
    const timer = setInterval(() => {
      setCountdown(count => count - 1)
    }, 1000)
    if (countdown === 0) {
      setTimerStart(false)
      setCountdown(TIME)
    }
    return () => clearInterval(timer)
  }, [timerStart, countdown])

  const showSyncWalletMsg = useCallback(async () => {
    const toast = (await import('components/core/Toast/toast')).default
    toast({
      message: intl.formatMessage({ id: 'shared.message.sync_wallet' }),
    })
  }, [intl])

  const handleSubmit = async () => {
    if (validGameProviderIds.length > 0 && countdown === TIME) {
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/aft/back`,
          method: 'post',
          data: { gameProviderId: validGameProviderIds },
          withToken: true,
        })
        console.log('ressss.', res)
        trigger(
          `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/wallet`
        )
      } catch (err) {
        console.error(err)
      }
    }
  }

  useEffect(() => {
    setIsSpinShown(true)

    const timer = setTimeout(() => {
      setIsSpinShown(false)
      setIsAmountLoading(false)
    }, 1000)

    return () => {
      clearTimeout(timer)
    }
  }, [isAmountLoading])

  const onClick = () => {
    if (!timerStart) {
      setTimerStart(true)
    }
    showSyncWalletMsg()
      .then(() => setIsAmountLoading(true))
      .then(() => handleSubmit())
  }

  return (
    <AreaContainer>
      {walletList ? (
        <>
          <div className="flex justify-evenly mb-3">
            <MainWallet amount={parseInt(mainWallet.money, 10)} />
            <WithdrawAllButton onClick={onClick} />
          </div>

          <div className="grid grid-cols-3 mt-3 pt-2 border-t border-gray-50 dark:border-purple-200">
            {thirdPartyWallet.map(({ money: amount, remark: title }, index) => (
              <ThirdPartyWallet
                key={index.toString()}
                amount={amount}
                title={title}
                isSpinShown={isSpinShown}
              />
            ))}
          </div>
        </>
      ) : <Skeleton />}
    </AreaContainer>
  )
}

export default WalletArea
