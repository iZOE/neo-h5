import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'

const IconWallet = dynamic(() => import('components/icons/Wallet'))
const Currency = dynamic(() => import('components/core/Currency'))

const MainWallet = ({ amount }) => {
  return (
    <div className="flex items-center mx-auto">
      <div className="flex-none mr-2">
        <IconWallet fillColor="text-green" />
      </div>
      <div className="flex-auto text-platinum-200 text-body-6 font-semibold">
        <p>
          <FormattedMessage id="shared.main_wallet" />
        </p>
        <p>
          <Currency value={amount} />
        </p>
      </div>
    </div>
  )
}

export default MainWallet
