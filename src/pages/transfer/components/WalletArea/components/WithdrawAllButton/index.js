import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'

const IconWithdrawAll = dynamic(() => import('components/icons/WithdrawAll'))

const WithdrawAllButton = ({ onClick }) => {
  return (
    <div
      className="flex items-center mx-auto"
      onClick={onClick}>
      <div className="flex-none mr-2">
        <IconWithdrawAll fillColor="text-red" />
      </div>
      <div className="flex-auto text-platinum-200 text-body-6 font-semibold">
        <FormattedMessage id="area.wallet.withdraw_all" />
      </div>
    </div>
  )
}

export default WithdrawAllButton