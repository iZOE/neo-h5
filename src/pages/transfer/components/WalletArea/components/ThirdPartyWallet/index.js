import dynamic from 'next/dynamic'
import style from '../../../../transfer.module.scss'

const Currency = dynamic(() => import('components/core/Currency'))
const Spin = dynamic(() => import('components/shared/Spin'))

const AMOUNT_NOT_FOUND = '-'

const ThirdPartyWallet = ({ amount, title, isSpinShown }) => {
  return (
    <div className={style.thirdPartyWallet}>
      <h4 className="text-platinum-200">
        {title}
      </h4>
      <p className="text-blue-200">
        {isSpinShown ? <Spin /> : (
          (amount === AMOUNT_NOT_FOUND) ? amount : <Currency value={parseInt(amount)} />
        )}
      </p>
      <span className={`bg-gray-50 dark:bg-purple-200 ${style.divider}`} />
    </div>
  )
}

export default ThirdPartyWallet