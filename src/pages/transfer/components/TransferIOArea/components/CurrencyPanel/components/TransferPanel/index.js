import { useCallback, useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { useForm, Controller } from 'react-hook-form'
import { useIntl } from 'react-intl'
import { trigger } from 'swr'

const Input = dynamic(() => import('components/core/Input'))
const Button = dynamic(() => import('components/core/Button'))

const TransferPanel = ({ amount, userAmount, id, isTransferOut }) => {
  const intl = useIntl()
  const [isSubmitting, setIsSubmitting] = useState(false)

  const onToggleIsFormSubmitting = useCallback(() => {
    setIsSubmitting(prevState => !prevState)
  }, [])

  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
    setValue,
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  useEffect(() => {
    if (typeof amount === 'number') {
      setValue('money', amount, { shouldValidate: true, shouldDirty: true })
    }
  }, [amount, setValue])

  const showTransferResult = useCallback(
    async isSuccess => {
      const toast = (await import('components/core/Toast/toast')).default

      toast({
        message: isSuccess
          ? intl.formatMessage({ id: 'shared.message.transfer.success' })
          : intl.formatMessage({ id: 'shared.message.transfer.fail' }),
      })
    },
    [intl],
  )

  const onSubmit = async formData => {
    if (isTransferOut) {
      onToggleIsFormSubmitting()
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/${id}/charge`,
          method: 'post',
          data: formData,
          withToken: true,
        })
        showTransferResult(true)
        console.log('transfer money out success', res)
        trigger(`${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/wallet`)
      } catch (err) {
        showTransferResult(false)
        console.error('transfer money out fail', err)
      } finally {
        onToggleIsFormSubmitting()
      }
    } else {
      onToggleIsFormSubmitting()
      try {
        const res = await fetcher({
          url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/${id}/refund`,
          method: 'post',
          data: formData,
          withToken: true,
        })
        showTransferResult(true)
        console.log('transfer money in success', res)
        trigger(`${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/wallet`)
      } catch (err) {
        showTransferResult(false)
        console.error('transfer money in fail', err)
      } finally {
        onToggleIsFormSubmitting()
      }
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="money"
        control={control}
        rules={{
          required: {
            value: true,
            message: intl.formatMessage({
              id: 'shared.validator.is_required',
            }),
          },
          validate: value =>
            (value && Number(value) > 0 && value <= Math.floor(userAmount)) ||
            intl.formatMessage({
              id: 'shared.validator.amount_is_insufficient',
            }),
        }}
        render={({ onChange, value }) => (
          <Input
            label={intl.formatMessage({ id: 'area.transfer_io.field.amount.label' })}
            placeholder={intl.formatMessage({ id: 'area.transfer_io.field.amount.placeholder' })}
            type="number"
            onChange={onChange}
            value={value}
            style={{ textAlign: 'right' }}
            min={0}
            width={1}
            error={errors.money && errors.money.message}
          />
        )}
      />
      <div
        className="-mx-3 p-3 border-t border-gray-100 dark:border-gray-600 mt-11"
        style={{ borderTopWidth: 2 }}
      >
        <Button
          variant="primary"
          type="submit"
          disabled={!isDirty || !isValid || isSubmitting}
          isLoading={isSubmitting}
        >
          {intl.formatMessage({ id: 'area.transfer_io.action.transfer' })}
        </Button>
      </div>
    </form>
  )
}

export default TransferPanel
