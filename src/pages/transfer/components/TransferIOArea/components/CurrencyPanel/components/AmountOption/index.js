const AmountOption = ({ activedKey, index, text, onSelect }) => {
  const style = (activedKey === index) ? 'border-blue-200 text-blue-200' : 'border-platinum-200 text-platinum-200'

  return (
    <div
      onClick={() => { onSelect(index) }}
      className={`py-3 px-5 rounded-lg border text-center text-body-6 ${style}`}>
      { text}
    </div>
  )
}

export default AmountOption