import { useState, useMemo } from 'react'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'

const AmountOption = dynamic(() => import('./components/AmountOption'))
const TransferPanel = dynamic(() => import('./components/TransferPanel'))

const MAIN_WALLET_ID = 1
const BASIC_OPTIONS = [
  { key: 1, amount: 100, text: '100' },
  { key: 2, amount: 300, text: '300' },
  { key: 3, amount: 500, text: '500' },
  { key: 4, amount: 1000, text: '1000' },
]

const CurrencyPanel = ({ isTransferOut, id }) => {
  const [activedKey, setActivedKey] = useState(null)

  const { data: walletList } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider/wallet`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data?.gameProviderMoney
    },
  )

  const { amountOptions, userAmount } = useMemo(() => {
    let options = [...BASIC_OPTIONS]
    let main = null

    if (walletList) {
      main = isTransferOut
        ? walletList.find(({ gameProviderId: idx }) => idx === MAIN_WALLET_ID)
        : walletList.find(({ gameProviderId: idx }) => idx === Number(id))

      if (main.money) {
        options = [...options, { key: 5, amount: parseInt(main.money, 10), text: 'Max' }]
      }
    }

    return {
      amountOptions: options,
      userAmount: parseInt(main?.money, 10),
    }
  }, [walletList, isTransferOut, id])

  return (
    <>
      <div className="grid grid-cols-5 gap-x-2 mb-4">
        {amountOptions &&
          amountOptions.map(({ key, text }) => (
            <AmountOption
              activedKey={activedKey}
              key={key}
              index={key}
              text={text}
              onSelect={() => setActivedKey(key)}
            />
          ))}
      </div>
      <TransferPanel
        amount={activedKey && amountOptions[activedKey - 1].amount}
        userAmount={userAmount}
        id={id}
        isTransferOut={isTransferOut}
      />
    </>
  )
}

export default CurrencyPanel
