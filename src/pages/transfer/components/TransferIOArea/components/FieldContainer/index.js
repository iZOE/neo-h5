const FieldContainer = ({ label, children }) => {
  return (
    <div className="flex-1 bg-gray-50 dark:bg-purple-700 rounded-md py-2 px-3" style={{ height: 60 }}>
      <h4 className="text-body-6 text-platinum-200">{label}</h4>
      {children}
    </div>
  )
}

export default FieldContainer