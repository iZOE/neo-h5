import dynamic from 'next/dynamic'

const IconTransferIn = dynamic(() => import('components/icons/TransferIn'))
const IconTransferOut = dynamic(() => import('components/icons/TransferOut'))

const ToggleButton = ({ isTransferOut, handleTransferState }) => {
  return (
    <div
      onClick={handleTransferState}
      className="flex-grow-0 w-6">
      {isTransferOut ? <IconTransferOut fillColor="text-blue-200" /> : <IconTransferIn fillColor="text-blue-200" />}
    </div>
  )
}

export default ToggleButton
