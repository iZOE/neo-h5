import { useMemo } from 'react'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import dynamic from 'next/dynamic'

const TriangleDown = dynamic(() => import('components/icons/TriangleDown'))
const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))

const ThirdPartyWalletDropdown = ({ handleSelectId }) => {
  const { data: gameProviders } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/game-provider`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data
    },
  )

  const { thirdPartyGameProviders } = useMemo(() => {
    if (gameProviders?.length) {
      const list = gameProviders.filter(({ isInternal }) => !isInternal)
      return {
        thirdPartyGameProviders: list,
      }
    }

    return {
      thirdPartyGameProviders: null,
    }
  }, [gameProviders])

  return (
    <div className="relative">
      {!thirdPartyGameProviders ? <Skeleton q={1} /> : (
        <select
          onChange={(e) => { handleSelectId(e.target.value) }}
          className="w-full h-5 bg-transparent text-title-8 font-semibold text-platinum-300 dark:text-white appearance-none">
          {thirdPartyGameProviders?.map(({ gameProviderId: id, remark: displayText }, index) => (
            <option
              key={index.toString()}
              value={id}>
              {displayText}
            </option>
          ))}
        </select>
      )}
      <div className="absolute right-0 top-0">
        <TriangleDown w="16" h="16" fillColor="text-platinum-300 dark:text-white" vw="16" vh="16" />
      </div>
    </div>
  )
}

export default ThirdPartyWalletDropdown
