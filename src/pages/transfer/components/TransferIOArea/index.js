import { useCallback, useState } from 'react'
import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'

const AreaContainer = dynamic(() => import('../AreaContainer'))
const FieldContainer = dynamic(() => import('./components/FieldContainer'))
const ToggleButton = dynamic(() => import('./components/ToggleButton'))
const ThirdPartyWalletDropdown = dynamic(() => import('./components/ThirdPartyWalletDropdown'))
const CurrencyPanel = dynamic(() => import('./components/CurrencyPanel'))

const TransferIOArea = () => {
  const [isTransferOut, setIsTransferOut] = useState(true)
  const [selectedGameProviderId, setSelectedGameProviderId] = useState(2)

  const handleTransferState = useCallback(() => setIsTransferOut(prevState => !prevState), [])
  const handleSelectId = (id) => { setSelectedGameProviderId(id) }

  return (
    <AreaContainer>
      <div className="flex gap-x-2 items-center mb-4">
        <FieldContainer label={<FormattedMessage id="area.transfer_io.field.transfer.in" />}>
          <p className="text-title-8 mt-1 font-semibold text-platinum-300 dark:text-white">
            <FormattedMessage id="shared.main_wallet" />
          </p>
        </FieldContainer>
        <ToggleButton
          isTransferOut={isTransferOut}
          handleTransferState={handleTransferState}
        />
        <FieldContainer label={<FormattedMessage id="area.transfer_io.field.transfer.out" />}>
          <ThirdPartyWalletDropdown handleSelectId={handleSelectId} />
        </FieldContainer>
      </div>
      <CurrencyPanel isTransferOut={isTransferOut} id={selectedGameProviderId} />
    </AreaContainer>
  )
}

export default TransferIOArea