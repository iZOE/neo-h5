import { useMemo } from 'react'
import { useIntl } from 'react-intl'
import WithTabBar from 'components/layout/WithTabBar'
import NavigationBar from 'components/core/NavigationBar'
import Container from 'components/core/Container'
import SwitchTab from 'components/core/SwitchTab'
import LinkNoticeBoard from './components/LinkNoticeBoard'

function NoticeBoard() {
  const intl = useIntl()
  const tabs = useMemo(
    () => [
      { title: intl.formatMessage({ id: 'tabs.activity' }), path: '/activity' },
      { title: intl.formatMessage({ id: 'tabs.noticeboard' }), path: '/notice-board' },
    ],
    [],
  )

  return (
    <div className="pt-0">
      <NavigationBar fixed centerComponent={<SwitchTab data={tabs} />} />
      <Container withNavigationBar noPadding className="container">
        <LinkNoticeBoard />
      </Container>
    </div>
  )
}

NoticeBoard.protect = true
NoticeBoard.TabBar = WithTabBar

export default NoticeBoard
