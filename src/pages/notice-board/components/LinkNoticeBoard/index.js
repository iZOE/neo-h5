import dynamic from 'next/dynamic'
import { useMemo } from 'react'
import useSWR from 'swr'
import Link from 'next/link'
import * as dayjs from 'dayjs'
import { useIntl } from 'react-intl'
import fetcher from 'libs/fetcher'
import style from './noticeboard.module.scss'

const Skeleton = dynamic(() => import('components/shared/Skeleton/List'))
const Empty = dynamic(() => import('../Empty'))

function LinkNoticeBoard() {
  const intl = useIntl()

  const { data: list } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/im/noticeboards?agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data?.container
    },
  )

  const lang = process.env.NEXT_PUBLIC_AGENT_LOCALE_LANGUAGE

  const typeName = useMemo(
    () => ({
      1: intl.formatMessage({ id: 'typeName.1' }),
      2: intl.formatMessage({ id: 'typeName.2' }),
      3: intl.formatMessage({ id: 'typeName.3' }),
    }),
    [],
  )

  return (
    <>
      {!list && <Skeleton className="p-3" />}
      {list && !list.length && <Empty />}

      {list && list.map((item, index) => (
        <Link href={`/notice-board/details/${index}`}>
          <div className="flex flex-wrap w-full px-3 py-2 mt-2 bg-white dark:bg-gray-650" key={index.toString()}>
            <div className="relative w-full">
              <div className="flex mb-4 h-10 items-center">
                <div className={`${style.title_style} flex-1 mr-3 text-title-8 font-bold leading-4 text-left overflow-hidden`}>
                  <div className="text-gray-500 dark:text-white">{item.subject}</div>
                </div>
                <div className={`${style[`type_${item.typeID}_style`]} max-h-6 box-border px-2 py-1 rounded text-body-6 text-white text-center whitespace-nowrap overflow-hidden`} style={{ maxWidth: '72px' }}>
                  {typeName[item.typeID]}
                </div>
              </div>
              <div className={`${style.title_style} mb-4 h-10 text-body-4`}>
                <div className="text-gray-500 dark:text-white">{item.textContent}</div>
              </div>
              <div className="flex text-body-6">
                <div className="w-1/2 text-platinum-200 dark:text-white">
                  {item?.postBeginTime
                    ? dayjs
                      .unix(item.postBeginTime)
                      .format(lang === 'zh-CN' ? 'YYYY/MM/DD mm:ss' : 'DD/MM/YYYY mm:ss')
                    : null}
                </div>
                <div className="w-1/2 text-right text-platinum-200 dark:text-white truncate overflow-hidden overflow-ellipsis">
                  {item.announceDept}
                </div>
              </div>

            </div>
          </div>
        </Link>
      ))
      }
    </>
  )
}

export default LinkNoticeBoard
