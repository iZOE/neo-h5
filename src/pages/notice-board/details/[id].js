import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import Link from 'next/link'
import styled, { css } from 'styled-components'
import * as dayjs from 'dayjs'
import fetcher from 'libs/fetcher'

const Skeleton = dynamic(() => import('components/shared/Skeleton/Detail'))
const Container = dynamic(() => import('components/core/Container'))
const IconBack = dynamic(() => import('components/icons/Back'))

const FixedBar = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  width: 100%;
  height: 44px;
  padding-bottom: constant(safe-area-inset-top);
  padding-bottom: env(safe-area-inset-top);
  text-align: ${({ fixPosition }) => fixPosition === 'top' ? 'center' : 'inherit'};
  background-color: ${({ theme }) => theme.sharedComponent.navigationBar.bg};
  box-shadow: ${({ fixPosition }) => fixPosition === 'bottom' ? '0px 2px 4px #000000' : 'none'};
  z-index: 99;
  ${({ fixPosition }) =>
    css`
      ${fixPosition}: 0;
    `}
`

function Detail() {
  const router = useRouter()
  const { id } = router.query
  const lang = process.env.NEXT_PUBLIC_AGENT_LOCALE_LANGUAGE

  const { data: list } = useSWR(
    `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/im/noticeboards?agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res?.data?.data?.container
    },
  )

  return list ? (
    <div className="text-platinum-200">
      <FixedBar className="px-3" fixPosition="top">
        <Link href="/notice-board">
          <div className="flex items-center text-center justify-center w-6 sm:w-12">
            <IconBack />
          </div>
        </Link>
        <div className="w-full text-center truncate overflow-ellipsis overflow-hidden">{list[id].subject}</div>
      </FixedBar>
      <Container withNavigationBar noPadding>
        <div className="block px-3 py-4 overflow-x-hidden" dangerouslySetInnerHTML={{ __html: list[id].content }} />
        <FixedBar className="px-3" fixPosition="bottom">
          <div className="w-1/2">
            <div className="text-body-6">
              {list[id].postBeginTime
                ? dayjs
                  .unix(list[id].postBeginTime)
                  .format(lang === 'zh-CN' ? 'YYYY/MM/DD mm:ss' : 'DD/MM/YYYY mm:ss')
                : null}
            </div>
          </div>
          <div className="w-1/2 text-right">
            <div className="text-body-6 truncate overflow-ellipsis overflow-hidden">{list[id].announceDept}</div>
          </div>
        </FixedBar>
      </Container>
    </div>
  ) : <Skeleton />
}

export default Detail
