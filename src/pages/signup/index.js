import dynamic from 'next/dynamic'
import { AnimateSharedLayout } from 'framer-motion'
import useSelectLoginAndRegisterLayout from '../../hooks/useSelectLoginAndSignupLayout'
import { NEXT_PUBLIC_SCAFFOLD_LOGINANDREGISTER_LAYOUT } from '../../constants/processEnvVariables'

const FormA = dynamic(() => import('layouts/LoginAndSignup/TemplateA/Form'), {
  loading: () => <p>Loading...</p>,
})

const FormB = dynamic(() => import('layouts/LoginAndSignup/TemplateB/Form'), {
  loading: () => <p>Loading...</p>,
})

function Signup({ loginAndSignupLayout }) {
  const LayoutComponent = useSelectLoginAndRegisterLayout({
    type: loginAndSignupLayout,
  })

  const FormComponent = loginAndSignupLayout === 'A' ? FormA : FormB

  return (
    <LayoutComponent>
      <FormComponent />
    </LayoutComponent>
  )
}

export async function getStaticProps() {
  return {
    props: {
      loginAndSignupLayout: process.env[NEXT_PUBLIC_SCAFFOLD_LOGINANDREGISTER_LAYOUT],
    },
  }
}

Signup.Layout = AnimateSharedLayout

export default Signup
