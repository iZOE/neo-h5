import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'
import styled from 'styled-components'
import NavigationBar from 'components/core/NavigationBar'
import Container from 'components/core/Container'
import Tabs from 'components/core/Tabs'
import IconBack from 'components/icons/Back.svg'
import BackgroundWrapper from './components/BackgroundWrapper'
import PrimaryTabPanel from './components/PrimaryTabPanel'
import SecondTabPanel from './components/SecondTabPanel'

const colorSet = JSON.parse(process.env.NEXT_PUBLIC_DOWNLOAD_COLOR_SET)

const tabsGroupStyles = {
  border: '1px solid #51A1FF',
  'border-radius': '20px',
  padding: '4px',
}

const Wrapper = styled.div.withConfig({
  componentId: 'downloadWrapper',
})
  .attrs({
    className: 'relative px-3 bg-white pt-3',
  })`
  & .active {
    background: ${`linear-gradient(90deg, ${colorSet.activeColor.bg[0]} 0%, ${colorSet.activeColor.bg[1]} 100%)`};
  }
  & .tab-title {
    order: 1;
  }
  & .primary-panel {
    order: 0;
  }
  & .second-panel {
    order: 2;
  }
`

const MaskWrapper = styled.div`
  position: fixed;
  top: 0;
  width: 100vw;
  height: fill-available;
  background: rgba(0, 0, 0, 0.5);
  color: #fff;
  overflow: hidden;
  z-index: 99;
  & .tips {
    display: inline-block;
    margin-top: 50px;
    width: 100%;
    font-size: 1.5rem;
    font-weight: 500;
    text-align: center;
  }
  & > span[class^='icon-'] {
    position: absolute;
    top: 15px;
    right: 30px;
    transform: rotate(-45deg);
  }
`

function Download() {
  const intl = useIntl()
  const router = useRouter()
  const [isWeChat, setIsWeChat] = useState(false)
  const [initActiveIndex, setInitActiveIndex] = useState()

  useEffect(() => {
    const checkUserAgent =
      navigator.userAgent.toLowerCase().indexOf('micromessenger') > -1 ||
      typeof navigator.wxuserAgent !== 'undefined'
    setIsWeChat(checkUserAgent)

    const initPlatformActiveIndex = window.navigator.platform === 'iPhone' ? 0 : 1
    setInitActiveIndex(initPlatformActiveIndex)
  }, [])

  const tabData = [
    {
      tabTitle: 'iOS',
      tabPanel: <PrimaryTabPanel device="iOS" />,
      secondTabPanel: <SecondTabPanel device="iOS" />,
    },
    {
      tabTitle: 'Android',
      tabPanel: <PrimaryTabPanel device="Android" />,
      secondTabPanel: <SecondTabPanel device="Android" />,
    },
  ]

  return (
    <>
      <NavigationBar
        fixed
        left={<IconBack />}
        onLeftClick={() => router.push('/')}
        title={intl.formatMessage({ id: 'title' })}
      />
      <Container withNavigationBar bg="inherit" noPadding>
        <Wrapper>
          <Tabs
            initActiveIndex={initActiveIndex}
            data={tabData}
            fontSize="14px"
            lineHeight="32px"
            fontWeight="600"
            textAlign
            borderRadius="20px"
            {...tabsGroupStyles}
          />
          <BackgroundWrapper />
        </Wrapper>
      </Container>
      {isWeChat && (
        <MaskWrapper>
          <div className="tips">{intl.formatMessage({ id: 'is_wechat' }, { br: <br /> })}</div>
        </MaskWrapper>
      )}
    </>
  )
}

export default Download
