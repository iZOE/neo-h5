import styled from 'styled-components'
import { FormattedMessage } from 'react-intl'
import Slider from 'react-slick'
import Picture from 'components/core/Picture'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

const slidersData = {
  STA01: [
    {
      title: <FormattedMessage id="slider_banners.title1" />,
      subTitle: '',
      imageUrl: '/download/STA01/1.png',
    },
    {
      title: <FormattedMessage id="slider_banners.title2" />,
      subTitle: <FormattedMessage id="slider_banners.sub_title_1" />,
      imageUrl: '/download/STA01/2.png',
    },
    {
      title: <FormattedMessage id="slider_banners.title3" />,
      subTitle: <FormattedMessage id="slider_banners.sub_title_2" />,
      imageUrl: '/download/STA01/3.png',
    },
    {
      title: <FormattedMessage id="slider_banners.title4" />,
      subTitle: <FormattedMessage id="slider_banners.sub_title_3" />,
      imageUrl: '/download/STA01/4.png',
    },
  ],
  VT999: [
    {
      title: <FormattedMessage id="slider_banners.title1" />,
      subTitle: '',
      imageUrl: '/download/VT999/1.png',
    },
    {
      title: <FormattedMessage id="slider_banners.title2" />,
      subTitle: '',
      imageUrl: '/download/VT999/2.png',
    },
    {
      title: <FormattedMessage id="slider_banners.title3" />,
      subTitle: '',
      imageUrl: '/download/VT999/3.png',
    },
    {
      title: <FormattedMessage id="slider_banners.title4" />,
      subTitle: <FormattedMessage id="slider_banners.sub_title_4" />,
      imageUrl: '/download/VT999/4.png',
    },
  ],
}

const settings = {
  adaptiveHeight: false,
  arrows: false,
  autoplay: false,
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  pauseOnHover: false,
}

const SliderWrapper = styled.div`
  .slick-dots {
    position: inherit;
    bottom: inherit;
  }
  .slick-dots li {
    width: 10px;
    height: 4px;
    background-color: ${({ theme }) => theme.sharedComponent.switchtab.active.bg};
    border-radius: 3px;
    opacity: 0.2;
  }
  .slick-dots li.slick-active {
    width: 20px;
    opacity: 1;
  }
  .slick-dots li button::before {
    font-size: 0;
  }
`

function SlideImage({ data }) {
  const { imageUrl } = data

  return <Picture src={`${imageUrl}`} png={`${imageUrl}`} webp={`${imageUrl}`} />
}

export default function SlideBanner() {
  return (
    <SliderWrapper className="relative mb-4">
      <Slider {...settings}>
        {slidersData[`${process.env.NEXT_PUBLIC_AGENT_CODE}`].map((item, index) => (
          <div key={`slider_${index.toString()}`}>
            <div
              className="text-blue-200 font-bold"
              style={
                item.subTitle !== ''
                  ? { fontSize: '15px', lineHeight: '18px' }
                  : { fontSize: '21px', lineHeight: '25px' }
              }
            >
              {item.title}
            </div>
            {item.subTitle && (
              <div className="font-normal text-body-7 text-blue-200">{item.subTitle}</div>
            )}
            <SlideImage data={item} />
          </div>
        ))}
      </Slider>
    </SliderWrapper>
  )
}
