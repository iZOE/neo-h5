import styled from 'styled-components'
import Logo from 'components/shared/Logo'
import SlideBanner from '../Slider'

const LogoWrapper = styled.span`
  display: block;
  width: 120px;
  height: auto;
  margin: 0 auto 15px;
`

function TabPanel() {
  return (
    <div className="text-center">
      <LogoWrapper>
        <Logo />
      </LogoWrapper>
      <SlideBanner />
    </div>
  )
}

export default TabPanel
