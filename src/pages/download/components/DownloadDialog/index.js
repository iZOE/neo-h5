import { useRouter } from 'next/router'
import styled from 'styled-components'
import { FormattedMessage } from 'react-intl'
import dynamic from 'next/dynamic'

const Dialog = dynamic(() => import('components/core/Dialog'))

const DialogWrapper = styled(Dialog)`
  .core-dialog-body {
    padding-top: 0;
  }
`

export default function DownloadDialog({ data, isOpen }) {
  const router = useRouter()
  return (
    <DialogWrapper isOpen={isOpen}>
      <div className="sticky top-0 bg-white pb-2 border-b-2 border-gray-100 text-title-7 text-center">
        <FormattedMessage id="spare_download" />
      </div>
      {data.map((item, index) => (
        <div
          className="text-body-5 text-platinum-200 leading-10 border-b border-gray-100"
          onClick={() => router.push(item.link)}
          key={`list_${index.toString()}`}
        >
          {item.name}
        </div>
      ))}
    </DialogWrapper>
  )
}
