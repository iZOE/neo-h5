import { useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import styled from 'styled-components'
import Ios from 'components/icons/Ios.svg'
import Android from 'components/icons/Android.svg'
import Safari from 'components/icons/Safari.svg'
import Chrome from 'components/icons/Chrome.svg'
import DownloadDialog from '../DownloadDialog'

const colorSet = JSON.parse(process.env.NEXT_PUBLIC_DOWNLOAD_COLOR_SET)

const ButtonWrapper = styled.button.attrs({
  className: 'w-full mt-4',
})`
  background: ${`linear-gradient(90deg, ${colorSet.activeColor.bg[0]} 0%, ${colorSet.activeColor.bg[1]} 100%)`};
  color: ${({ theme }) => theme.sharedComponent.button.primary.normal.color};
  border-radius: 20px;
  & svg {
    margin-right: 4px;
  }
`

const VersionWrapper = styled.div.attrs({
  className: 'text-body-6 text-blue-100 mt-4 text-center',
})`
  &:last-child {
    margin-bottom: 20px;
  }
`

function SecondTabPanel({ device }) {
  const router = useRouter()
  const [isOpen, setIsOpen] = useState(false)
  const downloads =
    device === 'iOS'
      ? JSON.parse(process.env.NEXT_PUBLIC_AGENT_VERSIONS_DOWNLOADS_IOS)
      : JSON.parse(process.env.NEXT_PUBLIC_AGENT_VERSIONS_DOWNLOADS_ANDROID)
  const latest =
    device === 'iOS'
      ? process.env.NEXT_PUBLIC_AGENT_VERSIONS_LATEST_IOS
      : process.env.NEXT_PUBLIC_AGENT_VERSIONS_LATEST_ANDROID
  const release =
    device === 'iOS'
      ? process.env.NEXT_PUBLIC_AGENT_VERSIONS_RELEASE_IOS
      : process.env.NEXT_PUBLIC_AGENT_VERSIONS_RELEASE_ANDROID
  const support = device === 'iOS' ? `${device} 10.0` : `${device} 5.0`
  const deviceIcon = device === 'iOS' ? <Ios /> : <Android />
  const brosertIcon = device === 'iOS' ? <Safari /> : <Chrome />

  const handleDialogOpen = e => {
    if (downloads.length > 1) {
      setIsOpen(true)
    } else if (downloads.length === 1) {
      router.push(downloads[0].link)
    } else {
      e.preventDefault()
    }
  }

  return (
    <>
      <ButtonWrapper variant="primary" onClick={() => handleDialogOpen()}>
        <div
          className="flex w-full items-center justify-center leading-[40px] font-semibold"
          style={{ fontSize: '14px' }}
        >
          {deviceIcon}
          <FormattedMessage id="download_button_text" values={{ device }} />
        </div>
      </ButtonWrapper>
      <ButtonWrapper variant="primary">
        <Link
          href={{
            pathname: '/add-to-screen',
            query: { device },
          }}
        >
          <div
            className="flex w-full items-center justify-center leading-[40px] font-semibold"
            style={{ fontSize: '14px' }}
          >
            {brosertIcon} <FormattedMessage id="add_to_mainscreen" />
          </div>
        </Link>
      </ButtonWrapper>
      <VersionWrapper>
        <FormattedMessage id="app_version" values={{ latest, release, support }} />
      </VersionWrapper>
      <DownloadDialog data={downloads} isOpen={isOpen} />
    </>
  )
}

export default SecondTabPanel
