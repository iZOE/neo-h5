import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'
import { motion, AnimatePresence } from 'framer-motion'
import styled from 'styled-components'
import Picture from 'components/core/Picture'

const IconClose = dynamic(() => import('components/icons/Close'))

const MotionContent = styled(motion.div).withConfig({
  componentId: 'NotificationContainer',
}).attrs({
  className: 'flex items-center bg-[white] relative z-8888888',
})`
  box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.25);
`

const StyledButton = styled.button.withConfig({
  componentId: 'StyledButton',
}).attrs({
  className: 'text-white p-2 ml-2'
})`
  background: linear-gradient(90deg, #50befc 0%, #3985eb 100%);
  border-radius: 100px;
  font-size: 10px;
  line-height: 12px;
`

const LobbyNotification = ({ onClose }) => {
  const router = useRouter()

  const mediaPath = `/shared/${process.env.NEXT_PUBLIC_AGENT_CODE}/favicon/favicon`

  return (
    <AnimatePresence>
      <MotionContent
        initial={{ y: 10 }}
        animate={{ y: 0 }}
        exit={{ y: -100 }}
        transition={{ duration: 0.5 }}
      >
        <div className="pl-2" onClick={onClose}>
          <IconClose fillColor="#000" />
        </div>

        <div className="flex-auto flex items-center p-2" onClick={() => router.push('/download')}>
          <Picture
            className="w-6 h-6 mr-3 rounded shadow-b-2"
            png={`${mediaPath}.png`}
            webp={`${mediaPath}.webp`}
          />

          <div className="flex-1 text-gray-500">
            <h4 className="text-title-8 font-semibold">
              <FormattedMessage id="download.title" />
            </h4>
            <p className="text-body-8">
              <FormattedMessage id="download.content" />
            </p>
          </div>

          <StyledButton>
            <FormattedMessage id="download.button" />
          </StyledButton>
        </div>
      </MotionContent>
    </AnimatePresence>
  )
}

export default LobbyNotification
