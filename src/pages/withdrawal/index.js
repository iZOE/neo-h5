import WithdrawalProcess from 'pages-lib/withdrawal'

const Withdrawal = () => <WithdrawalProcess />

Withdrawal.protect = true

export default Withdrawal
