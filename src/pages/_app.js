import React, { useEffect, useState } from 'react'
import { IntlProvider } from 'react-intl'
import { RecoilRoot } from 'recoil'
import Head from 'next/head'
import flattenMessages from 'libs/flattenMessages'
import ThemeProvider from 'components/shared/ThemeProvider'
import ProtectedPage from 'components/layout/ProtectedPage'
import NERUM from './_NERUM'
import Dialog from '../components/core/Dialog'

import 'tailwindcss/tailwind.css'
import '@reach/dialog/styles.css'

function MyApp({ Component, pageProps, messages = {}, lang = 'zh-CN' }) {
  const [requireLogin, setLoginAlert] = useState(false)
  const TabBar = Component.TabBar || React.Fragment
  const ProtectPage = Component.protect ? ProtectedPage : React.Fragment
  const ExtendLayout = Component.Layout || React.Fragment

  const handleOkClick = () => {
    sessionStorage.removeItem('requireLogin')
    window.location.href = '/login'
  }

  const loginPopup = () => {
    const isLogout = sessionStorage.getItem('requireLogin')
    if (isLogout) {
      setLoginAlert(isLogout)
      setTimeout(handleOkClick, 3000)
    }
  }

  useEffect(() => {
    loginPopup()
    let interval = setInterval(loginPopup, 800)

    return () => {
      interval = null
    }
  }, [])

  const agentName = {
    STA01: {
      name: '星辉体育',
    },
    VT999: {
      name: 'VT 999',
    },
  }

  return (
    <>
      <Head>
        <title>{agentName[process.env.NEXT_PUBLIC_AGENT_CODE].name}</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover"
        />
        <meta name="HandheldFriendly" content="true" />
        <meta name="application-name" content="Apollo Neo H5" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <meta
          name="apple-mobile-web-app-title"
          content={`${agentName[process.env.NEXT_PUBLIC_AGENT_CODE].name}`}
        />
        <meta name="description" content="Best Apollo" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="msapplication-TileColor" content="#2B5797" />
        <meta name="msapplication-tap-highlight" content="no" />
        <meta name="theme-color" content="#000000" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href={`/shared/${process.env.NEXT_PUBLIC_AGENT_CODE}/favicon/apple-touch-icon.png`}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href={`/shared/${process.env.NEXT_PUBLIC_AGENT_CODE}/favicon/favicon.png`}
        />
        <link rel="manifest" href="manifest.json" />
      </Head>
      <IntlProvider locale={lang} messages={messages}>
        <RecoilRoot>
          <ThemeProvider>
            <ExtendLayout>
              <ProtectPage>
                <TabBar>
                  <Component {...pageProps} />
                  <Dialog
                    isOpen={requireLogin}
                    okText={messages['loginAlert.ok']}
                    onOk={handleOkClick}
                    closable
                  >
                    <p className="text-body-6 text-platinum-200 text-center">
                      <div>{messages['loginAlert.content']}</div>
                    </p>
                  </Dialog>
                </TabBar>
                <NERUM />
              </ProtectPage>
            </ExtendLayout>
          </ThemeProvider>
        </RecoilRoot>
      </IntlProvider>
    </>
  )
}

MyApp.getInitialProps = async ({ ctx }) => {
  const lang = process.env.NEXT_PUBLIC_AGENT_LOCALE_LANGUAGE || 'zh-CN'
  try {
    const routerPath = ctx.pathname === '/' ? '/home' : ctx.pathname
    const commonMessages = (await import(`../locale/common/${lang}.js`)).default
    const routeMessages = (await import(`../locale${routerPath}/${lang}.js`)).default

    return {
      lang,
      messages: flattenMessages({
        ...commonMessages,
        ...routeMessages,
      }),
    }
  } catch (err) {
    return {
      lang,
      messages: {},
    }
  }
}

export default MyApp
