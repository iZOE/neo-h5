import { useState, useEffect, useMemo } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import useSWR from 'swr'
import { FormattedMessage } from 'react-intl'
import useSession from 'hooks/useSession'
import fetcher from 'libs/fetcher'

const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))

function Service() {
  const [hasToken] = useSession()
  const router = useRouter()
  const [clientHeight, setClientHeight] = useState(0)

  const url = useMemo(
    () =>
      hasToken
        ? 'v2/api/customerservice'
        : `v2/api/customerservice/visitor?agentCode=${process.env.NEXT_PUBLIC_AGENT_CODE}`,
    [hasToken],
  )
  const { data, error } = useSWR(
    [url, hasToken],
    async (url, hasToken) => {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}${url}`,
        withToken: hasToken,
      })
      return res.data.data
    },
    { revalidateOnFocus: false },
  )

  useEffect(() => {
    const contentHeight = Number(document.body.clientHeight - 44)
    setClientHeight(contentHeight)
  }, [])

  return (
    <div className="pt-11 sm:pt-12 overflow-hidden">
      <NavigationBar
        fixed
        left={<IconBack />}
        title={<FormattedMessage id="title" />}
        onLeftClick={() => router.back()}
      />
      {data?.mode === 1 && (
        <iframe
          className="w-full"
          style={{ height: clientHeight }}
          title="service"
          src={data?.config.customerSupportOnlineChatUrl}
        />
      )}
    </div>
  )
}

export default Service
