import dynamic from 'next/dynamic'
import { useStepView } from '../../hooks/useStep'

const Step1Component = dynamic(() => import('./Step1'))
const Step2Component = dynamic(() => import('./Step2'))

const STEP_VIEW = {
  1: Step1Component,
  2: Step2Component,
}

const MIN_STEP = 1
const MAX_STEP = 2

function Deposit() {
  const ViewComponent = useStepView(STEP_VIEW, MIN_STEP, MAX_STEP)
  return <ViewComponent />
}

Deposit.protect = true

export default Deposit
