import { useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { numberWithCommas } from 'libs/helper'
import { useStepData } from 'hooks/useStep'
import { FormattedMessage } from 'react-intl'
import { Decimal } from 'decimal.js'
import CopyButton from 'components/core/CopyButton'

import style from './step.module.scss'

const Container = dynamic(() => import('components/core/Container'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const Alert = dynamic(() => import('components/core/Alert'))
const Currency = dynamic(() => import('components/core/Currency'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const IconCopy = dynamic(() => import('components/icons/Copy.svg'))

const UploadImg = dynamic(() => import('./components/UploadImg'))
const CountDown = dynamic(() => import('./components/CountDown'))

function DepositStep2() {
  const router = useRouter()
  const depositResData = useStepData()

  const {
    depositTypeId,
    orderNumber,
    amount,
    exchangeRate,
    channelName,
    accountName,
    accountNumber,
    cryptoProtocol,
    currency,
    memberCommissionRate,
    isRandomAmount,
    link,
  } = depositResData

  const [isOpen, setIsOpen] = useState(false)
  const actual_deposit_amount = amount * exchangeRate

  let { remarks } = depositResData
  const regex = /\([0-9]+\)/
  remarks = depositResData.remarks.replace(
    regex,
    `${depositResData.amount}`.indexOf('.') >= 1 ? amount : `(${amount}.00)`,
  )

  return (
    <div className="w-full h-screen">
      <NavigationBar
        fixed
        left={<IconBack />}
        title={<FormattedMessage id="title_step2" />}
        right={
          <div className="mr-2 text-title-8 text-platinum-200">
            <FormattedMessage id="back_lobby" />
          </div>
        }
        onLeftClick={() => setIsOpen(!isOpen)}
        onRightClick={() => setIsOpen(!isOpen)}
      />
      <Container withNavigationBar noHighScreen>
        <div className="mt-4 p-4 bg-white dark:bg-purple-800 rounded-md shadow-b-1">
          <div className="w-full mb-4 py-1 bg-platinum-100 dark:bg-purple-200 rounded text-center text-body-8 text-white dark:text-gray-400">
            <FormattedMessage id="steps.order_number" />
            &nbsp;
            {orderNumber}
          </div>
          {(depositTypeId === 'BankPlatform' || depositTypeId === 'CryptoCurrency') && (
            <div className="flex justify-between items-center mb-4 text-body-6">
              <div className="text-platinum-300">
                <FormattedMessage id="steps.channel_name" />
              </div>
              <div className="text-platinum-200">{channelName}</div>
            </div>
          )}
          {depositTypeId === 'BankPlatform' && (
            <>
              <div className="flex justify-between items-center mb-4 text-body-6">
                <div className="text-platinum-300">
                  <FormattedMessage id="steps.account_name" />
                </div>
                <div className="text-platinum-200">
                  {accountName}
                  <div
                    className={`${style.copybutton} inline-block align-middle w-7 h-6 border-gray-200 dark:border-gray-500`}
                  >
                    <CopyButton content={accountName} buttonText={<IconCopy />} />
                  </div>
                </div>
              </div>
              <div className="flex justify-between items-center mb-4 text-body-6">
                <div className="text-platinum-300">
                  <FormattedMessage id="steps.account_number" />
                </div>
                <div className="text-platinum-200">
                  {accountNumber}
                  <div
                    className={`${style.copybutton} inline-block align-middle w-7 h-6 border-gray-200 dark:border-gray-500`}
                  >
                    <CopyButton content={accountNumber} buttonText={<IconCopy />} />
                  </div>
                </div>
              </div>
            </>
          )}
          {depositTypeId === 'CryptoCurrency' && (
            <>
              <div className="flex justify-between items-center mb-4 text-body-6">
                <div className="text-platinum-300">
                  <FormattedMessage id="steps.protocol" />
                </div>
                <div className="text-platinum-200">{cryptoProtocol}</div>
              </div>
              <div className="mb-4 text-body-6">
                <div className="text-platinum-300">
                  <FormattedMessage id="steps.address" />
                </div>
                <div className="flex justify-between items-center mt-1 p-2 bg-gray-50 dark:bg-purple-700 rounded">
                  <div className="text-platinum-200">{accountNumber}</div>
                  <div
                    className={`${style.copybutton} w-10 h-6 border-gray-200 dark:border-gray-500`}
                    style={{ borderLeftWidth: '1px' }}
                  >
                    <CopyButton content={accountNumber} buttonText={<IconCopy />} />
                  </div>
                </div>
              </div>
            </>
          )}
          <div className="flex justify-between items-center mb-4 text-body-6">
            <div className="text-platinum-300">
              <FormattedMessage id="steps.amount" />
            </div>
            <div className="text-red">
              <Currency value={amount} />
            </div>
          </div>
          {depositTypeId === 'BankPlatform' && (
            <div className="flex justify-between items-center mb-4 text-body-6">
              <div className="text-platinum-300">
                <FormattedMessage id="steps.actual_deposit_amount" />
              </div>
              <div className="text-platinum-200">
                <Currency value={actual_deposit_amount} />
              </div>
            </div>
          )}
          {(depositTypeId === 'BankPlatform' || depositTypeId === 'CryptoCurrency') && (
            <div className="flex justify-between items-center mb-4 text-body-6">
              <div className="text-platinum-300">
                <FormattedMessage id="steps.currency" />
              </div>
              <div className="text-platinum-200">{currency}</div>
            </div>
          )}
          <div className="flex justify-between items-center mb-4 text-body-6">
            <div className="text-platinum-300">
              <FormattedMessage id="steps.member_commission_rate" />
            </div>
            <div className="text-platinum-200">
              <Currency value={numberWithCommas(new Decimal(memberCommissionRate).toFixed(2, 1))} />
              %
            </div>
          </div>
          {(depositTypeId === 'BankPlatform' || depositTypeId === 'CryptoCurrency') && (
            <div className="flex justify-between items-center mb-4 text-body-6">
              <div className="text-platinum-300">
                <FormattedMessage id="steps.exchange_rate" />
              </div>
              <div className="text-platinum-200">
                <Currency value={exchangeRate} />
              </div>
            </div>
          )}
          {depositTypeId === 'CryptoCurrency' && (
            <div className="flex justify-between items-center mb-4 text-body-6">
              <div className="text-platinum-300">
                <FormattedMessage id="steps.actual_deposit_amount2" />
              </div>
              <div className="text-platinum-200">
                <Currency
                  value={numberWithCommas(new Decimal(actual_deposit_amount).toFixed(2, 1))}
                />
              </div>
            </div>
          )}
          {depositTypeId === 'BankPlatform' && (
            <div className="mb-4 text-body-6">
              <div className="text-platinum-300">
                <FormattedMessage id="steps.remarks" />
              </div>
              <div className="flex justify-between items-center mt-1 p-2 bg-gray-50 dark:bg-purple-700 rounded">
                <div className="text-platinum-200">{!depositResData?.remarks ? '--' : remarks}</div>
                <div
                  className={`${style.copybutton} w-10 h-6 border-gray-200 dark:border-gray-500`}
                  style={{ borderLeftWidth: '1px' }}
                >
                  <CopyButton content={depositResData?.remarks} buttonText={<IconCopy />} />
                </div>
              </div>
            </div>
          )}
        </div>
        <div className="flex flex-col items-center mt-4 mb-6 p-4 text-body-4 text-platinum-200 bg-white dark:bg-purple-800 rounded-md shadow-b-1">
          <div className="w-full">
            <FormattedMessage id="steps.hint8" /> <span className="text-red">30</span>{' '}
            <FormattedMessage id="steps.hint9" />
            <CountDown />
          </div>
          <UploadImg orderNumber={orderNumber} />
        </div>
        <div>
          <Alert
            isOpen={isOpen}
            title={<FormattedMessage id="steps.alert_title" />}
            content={<FormattedMessage id="steps.alert_text" />}
            cancelText={
              <div className="text-body-6">
                <FormattedMessage id="steps.alert_cancen_text" />
              </div>
            }
            okText={
              <div className="text-body-6">
                <FormattedMessage id="steps.alert_confirm_text" />
              </div>
            }
            onCancel={() => setIsOpen(false)}
            onOk={() => {
              router.push('/user')
            }}
          />
          <button
            type="submit"
            className="w-full py-3 bg-blue-200 text-white text-body-4 rounded-full"
            onClick={() => {
              link && window.open(link)
              setIsOpen(!isOpen)
            }}
          >
            <FormattedMessage id="steps.goto_deposit" />
          </button>
        </div>
        <div className="mt-4 p-4 text-body-8 text-platinum-200 bg-white dark:bg-purple-800 rounded-md shadow-b-1">
          <div size="0.75em" color="aluminum">
            <FormattedMessage id="steps.hint1" />
          </div>
          {depositTypeId === 'CryptoCurrency' ? (
            <ol className="list-decimal pl-4">
              <li>
                <FormattedMessage id="steps.hint10a" />{' '}
                <span className="text-red">{cryptoProtocol}</span> USDT
                <FormattedMessage id="steps.hint10b" />
              </li>
              <li>
                <FormattedMessage id="steps.hint11a" />{' '}
                <span className="text-red">
                  <Currency value={numberWithCommas(new Decimal(amount).toFixed(2, 1))} />
                </span>{' '}
                USDT <FormattedMessage id="steps.hint11b" />
              </li>
              <li>
                <FormattedMessage id="steps.hint12" />
              </li>
              <li>
                <FormattedMessage id="steps.hint13" />
              </li>
            </ol>
          ) : isRandomAmount ? (
            <ol className="list-decimal pl-4">
              <li>
                <FormattedMessage id="steps.hint3" />
                <span> ± 0.00 ~ 0.99</span>
              </li>
              <li>
                <FormattedMessage id="steps.hint4" />
              </li>
              <li>
                <FormattedMessage id="steps.hint5" />
              </li>
            </ol>
          ) : (
            <ol className="list-decimal pl-4">
              <li>
                <FormattedMessage id="steps.hint4" />
              </li>
              <li>
                <FormattedMessage id="steps.hint5" />
              </li>
            </ol>
          )}
        </div>
      </Container>
    </div>
  )
}

export default DepositStep2
