import { useState, useEffect } from 'react'
import { FormattedMessage } from 'react-intl'

function CountDown() {

    const [sec, setSec] = useState(1800)

    useEffect(() => {
        const timer = setInterval(() => {
            setSec(sec => sec - 1)
        }, 1000)
        if (sec === 0) {
            setSec(1800)
        }
        return () => clearInterval(timer)
    }, [sec])

    return (
        <div className="block">
            <FormattedMessage id="steps.timeout" />
            <span className="text-red">{`${`0${Math.floor(sec / 60)}`.slice(
                -2
            )}:${`0${sec % 60}`.slice(-2)}`}</span>
        </div>
    )
}

export default CountDown
