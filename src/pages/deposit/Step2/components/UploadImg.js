import { useState, useCallback } from 'react'
import dynamic from 'next/dynamic'
import { FormattedMessage } from 'react-intl'
import fetcher from 'libs/fetcher'
import { getCompressImg } from 'libs/canvasUtils'

const Picture = dynamic(() => import('components/core/Picture'))
const IconAdd = dynamic(() => import('components/icons/Add.svg'))
const IconDelete = dynamic(() => import('components/icons/Delete.svg'))

function UploadImg({ orderNumber }) {

  const [imgUrl, setImgUrl] = useState(null)

  const handleCompressedUpload = useCallback(async e => {
    const imageFile = e.target.files[0]
    try {
      const compressImg = await getCompressImg(imageFile)
      const formData = new FormData()
      formData.append('image', compressImg)
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/deposit/${orderNumber}/upload`,
        method: 'post',
        withToken: true,
        data: formData,
      })
      setImgUrl(res?.data?.data)
    } catch (error) {
      console.log('error', error)
    }
  }, [])

  const handleDeleteImage = useCallback(async () => {
    try {
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/deposit/${orderNumber}/clear`,
        method: 'delete',
        withToken: true,
      })
      setImgUrl(null)
    } catch (error) {
      console.log('error', error)
    }
  }, [])

  return (
    <>
      {
        imgUrl ? (
          <div className="flex justify-center pt-6" >
            <Picture
              className="text-platinum-200 border-platinum-200 border-solid border rounded"
              png={`${process.env.NEXT_PUBLIC_END_POINT_STORAGE}${imgUrl}`}
              webp={`${process.env.NEXT_PUBLIC_END_POINT_STORAGE}${imgUrl}`}
              alt="depositImage"
              style={{ width: '80px' }}
            />
            <span
              className="h-4 relative -left-5 top-1"
              onClick={handleDeleteImage}
            >
              <IconDelete />
            </span>
          </div>) : (
          <div className="flex mt-4">
            <label htmlFor="image-input">
              <div
                className="text-platinum-200 border-platinum-200 border-solid border rounded text-center py-4 text-body-8"
                style={{ width: '80px', height: '80px' }}
              >
                <div className="w-6 my-0 mx-auto">
                  <IconAdd />
                </div>
                <div className="py-1">
                  <FormattedMessage id="steps.upload_image" />
                </div>
              </div>
            </label>
            <input
              id="image-input"
              className="hidden"
              type="file"
              accept="image/*"
              onChange={handleCompressedUpload}
            />
          </div>)}
    </>)
}

export default UploadImg
