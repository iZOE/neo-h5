import { useState, useCallback } from 'react'
import useStep, { useStepData } from 'hooks/useStep'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { Decimal } from 'decimal.js'
import { useForm, Controller } from 'react-hook-form'
import { useIntl, FormattedMessage } from 'react-intl'

const Currency = dynamic(() => import('components/core/Currency'))
const Button = dynamic(() => import('components/core/Button'))
const GuideVideo = dynamic(() => import('../GuideVideo'))
const IconLineRight = dynamic(() => import('components/icons/LineRight'))
const IconPlay = dynamic(() => import('components/icons/Play.svg'))
const IconRefresh = dynamic(() => import('components/icons/Refresh.js'))
const IconHelp = dynamic(() => import('components/icons/Help.js'))

function CryptoCurrency({
  channelId,
  channelName,
  paymentTypeName,
  display,
  isRandomAmount,
  maxAmount,
  minAmount,
  memberCommissionRate,
  depositAmountSettings,
  filmUrl,
  exchangeRate,
  currency,
  cryptoProtocol,
}) {
  const intl = useIntl()
  const { gotoNext, gotoPrevious } = useStep()
  const { register, handleSubmit, watch, control, formState } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })
  const { isDirty, isValid } = formState
  const [videoIsOpen, setVideoIsOpen] = useState(false)
  const [currentExchangeRate, setCurrentExchangeRate] = useState(exchangeRate)
  const [permissionToGetRate, setPermissionToGetRate] = useState(true)
  const [isAnimate, setIsAnimate] = useState(false)
  const [submitLoading, setSubmitLoading] = useState(false)
  const watchAmount = watch('amount', 0)
  const memberCommissionFee =
    watchAmount > 0
      ? new Decimal(watchAmount).mul(new Decimal(memberCommissionRate)).div(100).toFixed(2, 1)
      : 0
  const arrivalAmount =
    watchAmount > 0
      ? new Decimal(watchAmount).mul(new Decimal(currentExchangeRate)).toFixed(2, 1)
      : 0

  const handleUpdateRate = useCallback(async () => {
    // 管制一分鐘內不可以連續打api
    if (permissionToGetRate) {
      setIsAnimate(true)
      setPermissionToGetRate(false)
      setTimeout(() => {
        setPermissionToGetRate(true)
      }, [60000])

      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/deposit/channels/${channelId}`,
        method: 'get',
        withToken: true,
      })
      setCurrentExchangeRate(res?.data?.data?.exchangeRate)
      setIsAnimate(false)
    } else {
      setIsAnimate(true)
      setTimeout(() => {
        setIsAnimate(false)
      }, [3000])
    }
  }, [permissionToGetRate])

  const handleCryptoDescription = useCallback(async () => {
    let content = ''
    switch (cryptoProtocol) {
      case 'ERC20':
        content = 'hint19a'
        break
      case 'TRC20':
        content = 'hint19b'
        break
      case 'OMNI':
        content = 'hint19c'
        break
      default:
        content = ''
    }
    const message = (await import('components/core/Alert/message')).default
    message({
      content: intl.formatMessage({ id: `steps.${content}` }),
    })
  }, [])

  const handleCurrencyDescription = useCallback(async () => {
    const message = (await import('components/core/Alert/message')).default
    message({
      title: intl.formatMessage({ id: 'steps.hint16' }),
      content: intl.formatMessage({ id: 'steps.hint18' }),
    })
  }, [])

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default
    try {
      setSubmitLoading(true)
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/Deposit?requestWay=Web`,
        method: 'post',
        withToken: true,
        data: {
          channelId,
          requestAmount: Number(formData.amount),
        },
      })
      if (res.data.code === '0000') {
        const cryptoData = res.data.data
        cryptoData.cryptoProtocol = cryptoProtocol
        setSubmitLoading(false)
        gotoNext(cryptoData)
      }
    } catch (err) {
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
    setSubmitLoading(false)
  }, [])
  const loadAnimation = isAnimate ? 'animate-spin' : null
  return (
    <div className="px-3 py-4">
      <div className="flex justify-between items-center mb-2 text-body-8 text-blue-200">
        <div>
          <FormattedMessage id="steps.protocol" />: {cryptoProtocol}
          <span
            className="inline-block ml-1 relative"
            style={{ top: '1px' }}
            onClick={handleCryptoDescription}
          >
            <IconHelp w="14" h="14" fillColor="text-blue-200" vw="14" vh="14" />
          </span>
        </div>
        {filmUrl && (
          <>
            <div className="flex items-center">
              <IconPlay />
              <div className="flex items-center ml-1 h-6" onClick={() => setVideoIsOpen(true)}>
                <FormattedMessage id="video_tutorial" />
              </div>
              <IconLineRight width="16" height="16" fillColor="text-blue-200" />
            </div>
            <GuideVideo filmUrl={filmUrl} isOpen={videoIsOpen} />
          </>
        )}
      </div>
      <div
        className="flex justify-between bg-gray-50 dark:bg-purple-700 rounded"
        style={{ padding: '14px 12px' }}
      >
        <div className="text-body-4 text-gray-500 dark:text-white">
          <FormattedMessage id="steps.current_exchange_rate" />:
          <Currency
            value={currentExchangeRate}
            intClassName="text-body-4"
            floatClassName="text-body-6"
          />
        </div>
        <div className={loadAnimation} onClick={handleUpdateRate}>
          <IconRefresh width="24" height="24" fillColor="#1D8AEB" />
        </div>
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="border-solid border border-gray-300 dark:border-purple-100 rounded mb-1 py-3 px-4">
          <div className="text-gray-500 text-right">
            <Controller
              name="amount"
              control={control}
              defaultValue=""
              rules={{ required: true, min: minAmount, max: maxAmount }}
              render={({ onChange, value }) => (
                <input
                  type="number"
                  value={value}
                  className="w-4/5 text-right text-title-6 bg-transparent text-gray-500 dark:text-white"
                  placeholder={intl.formatMessage({
                    id: 'steps.crypto_placeholder',
                  })}
                  onChange={onChange}
                />
              )}
            />
            <span className="ml-2 text-body-8 text-gray-500 dark:text-white">{currency}</span>
          </div>
          <div className="text-right text-body-4 text-gray-400">
            ≈<Currency value={arrivalAmount} />
            <FormattedMessage id="steps.unit" />
            <span
              className="inline-block relative"
              style={{ top: '2px' }}
              onClick={handleCurrencyDescription}
            >
              <IconHelp width="14" height="14" fillColor="#BBB" vw="14" vh="14" />
            </span>
          </div>
        </div>
        <div className="text-red text-body-8 mt-1 mb-4">
          <div>
            <FormattedMessage id="steps.hint14" />
          </div>
          <div className="mt-2">
            <FormattedMessage id="steps.hint14b" />
          </div>
        </div>
        <div className="text-body-6 text-platinum-200">
          {`${intl.formatMessage({
            id: 'steps.deposit_fee',
          })} `}
          <Currency value={memberCommissionFee} />
        </div>
        <div className="flex h-11 bg-blue-200 text-body-4 text-white text-center items-center justify-center rounded-full">
          <Button
            variant="primary"
            type="submit"
            className="w-full h-full"
            isLoading={submitLoading}
            disabled={!isDirty || !isValid || submitLoading}
          >
            {intl.formatMessage({
              id: 'steps.submit',
            })}
          </Button>
        </div>
      </form>
    </div>
  )
}

export default CryptoCurrency
