import { useState, useCallback } from 'react'
import dynamic from 'next/dynamic'
import { useIntl, FormattedMessage } from 'react-intl'
import CopyButton from 'components/core/CopyButton'

import style from './copybutton.module.scss'

const GuideVideo = dynamic(() => import('../GuideVideo'))
const IconLineRight = dynamic(() => import('components/icons/LineRight'))
const IconPlay = dynamic(() => import('components/icons/Play.svg'))

function HumanCharge({ paymentTypeId, channelName, filmUrl }) {
  const intl = useIntl()
  const [videoIsOpen, setVideoIsOpen] = useState(false)

  return (
    <div className="px-3 py-4">
      <div className="flex justify-between items-center mb-2 text-body-8 text-blue-200">
        <div>{channelName}</div>
        {filmUrl && (
          <>
            <div className="flex items-center">
              <IconPlay />
              <div
                className="flex items-center ml-1 h-6"
                onClick={() => setVideoIsOpen(true)}
              >
                <FormattedMessage id="video_tutorial" />
              </div>
              <IconLineRight width="16" height="16" fillColor="text-blue-200" />
            </div>
            <GuideVideo filmUrl={filmUrl} isOpen={videoIsOpen} />
          </>
        )}
      </div>
      <div className="text-body-4 text-platinum-300">
        {intl.formatMessage({
          id: 'steps.hint6',
        })}{' '}
        {intl.formatMessage({
          id: `steps.humna_charge_payment_type_id_map.${paymentTypeId}`,
        })}
        ：{channelName}
        {intl.formatMessage({
          id: 'steps.hint7',
        })}
      </div>
      <div className={style.copybutton}>
        <CopyButton
          content={channelName}
          buttonText={
            <FormattedMessage
              id="copy_button.action"
              values={{ content: channelName }}
            />
          }
        />
      </div>
    </div>
  )
}

export default HumanCharge
