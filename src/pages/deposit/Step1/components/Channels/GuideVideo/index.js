import dynamic from 'next/dynamic'
const Dialog = dynamic(() => import('components/core/Dialog'))

function GuideVideo({ filmUrl, isOpen }) {
  return (
    <>
      <Dialog isOpen={isOpen}>
        <video width="100%" height="100%" controls>
          <source src={filmUrl} />
          Your browser does not support the video tag.
        </video>
      </Dialog>
    </>
  )
}

export default GuideVideo
