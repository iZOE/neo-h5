import dynamic from 'next/dynamic'
import styled from 'styled-components'
import CryptoCurrency from '../CryptoCurrency'
import AmountField from '../AmountField'
import HumanCharge from '../HumanCharge'

const Tabs = dynamic(() => import('components/core/Tabs'))

function ChannelTab({ activePlatform, activePlatformData }) {
  const { paymentTypeName, channels = [] } = activePlatformData
  const Wrapper = styled.div
    .withConfig({
      componentId: 'deposit-tabs-active',
    })
    .attrs({
      className: 'bg-white dark:bg-purple-800 ',
    })`
  & .tab-title {
    height: 40px;
    color: #95A3B0;
    border-bottom: ${({ theme }) => (theme.darkMode ? '2px solid #47475B' : '2px solid #ECECEC')};
  }
  & .active {
    background: inherit;
    color: #1D8AEB;
    border-bottom: 2px solid #1D8AEB;
  }
`

  const tabData =
    channels &&
    channels.map((item, index) => {
      const cId = index + 1
      let { channelName } = item

      if (channelName.length > 30) {
        channelName = `${channelName.slice(0, 27)}...`
      }
      const { channelTabName } = item
      let tabPanel
      switch (activePlatform) {
        case 28:
        case 29:
          tabPanel = (
            <HumanCharge
              paymentTypeId={activePlatform}
              channelName={channelName}
              filmUrl={activePlatformData.filmUrl}
            />
          )
          break
        case 39:
        case 40:
          tabPanel = (
            <CryptoCurrency
              channelId={item.channelId}
              channelName={channelName}
              paymentTypeName={paymentTypeName}
              display={item.display}
              isRandomAmount={item.isRandomAmount}
              maxAmount={item.maxAmount}
              minAmount={item.minAmount}
              memberCommissionRate={item.memberCommissionRate}
              depositAmountSettings={item.depositAmountSettings}
              filmUrl={activePlatformData.filmUrl}
              exchangeRate={item.exchangeRate}
              currency={item.currency}
              cryptoProtocol={item.cryptoProtocol}
            />
          )
          break
        default:
          tabPanel = (
            <AmountField
              channelId={item.channelId}
              channelName={channelName}
              paymentTypeName={paymentTypeName}
              display={item.display}
              isRandomAmount={item.isRandomAmount}
              maxAmount={item.maxAmount}
              minAmount={item.minAmount}
              memberCommissionRate={item.memberCommissionRate}
              depositAmountSettings={item.depositAmountSettings}
              filmUrl={activePlatformData.filmUrl}
            />
          )
          break
      }
      return {
        id: `${cId}`,
        tabTitle: channelTabName,
        tabPanel,
      }
    })

  return (
    <Wrapper>
      <Tabs
        data={tabData}
        initActiveIndex={0}
        fontSize="12px"
        lineHeight="40px"
        textAlign
      />
    </Wrapper>
  )
}

export default ChannelTab
