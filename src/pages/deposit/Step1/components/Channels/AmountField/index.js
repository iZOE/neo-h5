import { useState, useCallback } from 'react'
import useStep, { useStepData } from 'hooks/useStep'
import dynamic from 'next/dynamic'
import fetcher from 'libs/fetcher'
import { Decimal } from 'decimal.js'
import { useForm, Controller } from 'react-hook-form'
import { useIntl, FormattedMessage } from 'react-intl'

const Currency = dynamic(() => import('components/core/Currency'))
const Input = dynamic(() => import('components/core/Input'))
const Button = dynamic(() => import('components/core/Button'))
const GuideVideo = dynamic(() => import('../GuideVideo'))
const IconLineRight = dynamic(() => import('components/icons/LineRight'))
const IconPlay = dynamic(() => import('components/icons/Play.svg'))

function AmountField({
  channelId,
  channelName,
  display,
  isRandomAmount,
  minAmount,
  maxAmount,
  memberCommissionRate,
  depositAmountSettings,
  filmUrl,
}) {
  const {
    control,
    handleSubmit,
    errors,
    formState: { isDirty, isValid },
    setValue,
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange',
  })

  const { gotoNext, gotoPrevious } = useStep()
  const intl = useIntl()
  const [selectedQuota, setSelectedQuota] = useState(0)
  const [amount, setAmount] = useState(0)
  const [videoIsOpen, setVideoIsOpen] = useState(false)
  const [submitLoading, setSubmitLoading] = useState(false)

  const quota = depositAmountSettings
  const quotaRange = quota.filter(qItem => qItem <= maxAmount && qItem >= minAmount)

  const memberCommissionFee = new Decimal(memberCommissionRate).mul(amount).div(100).toFixed(2, 1)

  const handleSelectedQuota = quota => {
    setAmount(quota)
    setValue('amount', quota, {
      shouldValidate: true,
      shouldDirty: true,
    })
    setSelectedQuota(quota)
  }

  const handleInputFocus = () => {
    setValue('amount', '')
    setAmount(0)
    setSelectedQuota(0)
  }

  const numberWithCommas = (x, y = 0) => {
    const parts = new Decimal(x).toFixed(/\./g.test(x.toString()) ? 2 : y, 1).split('.')
    return parts.join('.')
  }

  const onSubmit = useCallback(async formData => {
    const message = (await import('components/core/Alert/message')).default

    let requestAmount = formData.amount
    if (isRandomAmount) {
      if (formData.amount === minAmount) {
        requestAmount = new Decimal(amount).plus(Math.random()).toFixed(2, 1)
      } else if (formData.amount === maxAmount) {
        requestAmount = new Decimal(formData.amount).minus(Math.random()).toFixed(2, 1)
      } else {
        const isPlus = Math.floor(Math.random() * 2) === 1
        if (isPlus) {
          requestAmount = new Decimal(formData.amount).plus(Math.random()).toFixed(2, 1)
        } else {
          requestAmount = new Decimal(formData.amount).minus(Math.random()).toFixed(2, 1)
        }
      }
      requestAmount = Number(requestAmount)
    }

    try {
      setSubmitLoading(true)
      const res = await fetcher({
        url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v1/api/Deposit?requestWay=Web`,
        method: 'post',
        withToken: true,
        data: {
          channelId,
          requestAmount: Number(requestAmount),
        },
      })
      if (res.data.code === '0000') {
        setSubmitLoading(false)
        gotoNext(res.data.data)
      } else {
        alert('no ok')
      }
    } catch (err) {
      console.log('err', err)
      message({
        content: err?.data?.responseMessage,
        okText: intl.formatMessage({ id: 'confirm' }),
      })
    }
    setSubmitLoading(false)
  }, [])

  return (
    <div className="px-3 py-4">
      {(display || filmUrl) && (
        <div className="flex justify-between items-center mb-2 text-body-8 text-blue-200">
          <div>{channelName}</div>
          {filmUrl && (
            <>
              <div className="flex items-center">
                <IconPlay />
                <div className="flex items-center ml-1 h-6" onClick={() => setVideoIsOpen(true)}>
                  <FormattedMessage id="video_tutorial" />
                </div>
                <IconLineRight width="16" height="16" fillColor="text-blue-200" />
              </div>
              <GuideVideo filmUrl={filmUrl} isOpen={videoIsOpen} />
            </>
          )}
        </div>
      )}
      <div className="grid grid-cols-4 gap-2 mb-4">
        {quotaRange.map((qItem, i) => (
          <div className="h-10 text-center" key={`qItem${i}`}>
            <button
              className={`w-full h-full border rounded-lg ${
                selectedQuota === qItem
                  ? 'text-blue-200 border-blue-200'
                  : 'text-platinum-200 border-platinum-200'
              }`}
              onClick={() => handleSelectedQuota(qItem)}
            >
              <span className="flex items-center justify-center text-body-6">{qItem}</span>
            </button>
          </div>
        ))}
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name="amount"
          control={control}
          rules={{ required: true, min: minAmount, max: maxAmount }}
          rules={{
            validate: value => {
              let errors = null
              const amount = value
              if (!amount || !String(amount).match(/^\d+$/g)) {
                errors = <FormattedMessage id="steps.error1" />
              } else if (amount < minAmount) {
                errors = <FormattedMessage id="steps.error2" values={{ minAmount }} />
              } else if (amount > maxAmount) {
                errors = <FormattedMessage id="steps.error3" values={{ maxAmount }} />
              } else if (String(amount).length > 7) {
                errors = <FormattedMessage id="steps.error4" values={{ maxAmount }} />
              }
              return errors
            },
          }}
          render={({ onChange, value }) => (
            <Input
              placeholder={
                amount > 0
                  ? amount
                  : intl.formatMessage({
                      id: 'steps.placeholder',
                    })
              }
              type="number"
              onChange={onChange}
              onFocus={handleInputFocus}
              width={1}
              value={value}
              error={errors?.amount?.message}
              style={{ textAlign: 'right' }}
            />
          )}
        />

        <div className="py-2">
          {isRandomAmount ? (
            <ol className="pl-4 list-decimal text-red text-body-8">
              <li className="py-1">
                {intl.formatMessage({
                  id: 'steps.hint3',
                })}
                <span> ± 0.00 ~ 0.99</span>
              </li>
              <li>
                {intl.formatMessage({
                  id: 'steps.hint4',
                })}
              </li>
              <li className="py-1">
                {intl.formatMessage({
                  id: 'steps.hint5',
                })}
              </li>
            </ol>
          ) : (
            <div>
              <div className="py-1 text-red text-body-8">
                {intl.formatMessage({
                  id: 'steps.hint1',
                })}
                {intl.formatMessage(
                  {
                    id: 'steps.hint2',
                  },
                  {
                    minAmount: numberWithCommas(minAmount),
                    maxAmount: numberWithCommas(maxAmount),
                  },
                )}
              </div>
              <ol className="pl-4 list-decimal text-red text-body-8">
                <li className="py-1">
                  {intl.formatMessage({
                    id: 'steps.hint4',
                  })}
                </li>
                <li className="py-1">
                  {intl.formatMessage({
                    id: 'steps.hint5',
                  })}
                </li>
              </ol>
            </div>
          )}
        </div>
        <div className="text-body-6 text-platinum-200">
          {`${intl.formatMessage({
            id: 'steps.deposit_fee',
          })} `}
          <Currency value={memberCommissionFee} />
        </div>
        <div className="flex h-11 bg-blue-200 text-body-4 text-white text-center items-center justify-center rounded-full">
          <Button
            variant="primary"
            type="submit"
            className="w-full h-full"
            isLoading={submitLoading}
            disabled={!isDirty || !isValid || submitLoading}
          >
            {intl.formatMessage({
              id: 'steps.submit',
            })}
          </Button>
        </div>
      </form>
    </div>
  )
}

export default AmountField
