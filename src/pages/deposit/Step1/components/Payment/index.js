import { useMemo } from 'react'
import dynamic from 'next/dynamic'

const Selected = dynamic(() => import('components/icons/Selected'))

function Payment({ activePlatform, setActivePlatform, payments }) {
  const tags = useMemo(() => {
    const randomTag = []
    payments?.map(bank => {
      const tagsSize = bank.tags.length
      if (tagsSize > 0) {
        const randomResult = Math.floor(Math.random() * tagsSize)
        randomTag.push(randomResult)
      }
    })
    return randomTag
  }, [payments])

  function BankItem({ bank, index }) {
    const tagItem = bank?.tags[tags[index]]
    const selected = activePlatform === bank?.paymentTypeId
    const { usePic } = bank
    const src =
      usePic && usePic !== 2 ? `/pages/deposit/ic-platform-${usePic}.svg` : ''

    return (
      <button
        type="button"
        onClick={() => setActivePlatform(bank?.paymentTypeId)}
        className={`border px-1 rounded pb-2 overflow-hidden relative ${selected ? 'border-blue-200' : 'border-gray-100 dark:border-purple-200'
          }`}
        style={{ minHeight: '64px', width: 'calc(100% + 1px)' }}
      >
        <div
          className="text-white absolute rounded-bl-md text-center text-body-8 h-4 flex justify-center items-center"
          style={{
            width: '50px',
            top: '-1px',
            right: '-1px',
            background: selected
              ? '#1D8AEB'
              : `#${tagItem?.paymentTypeTagColor}`,
          }}
        >
          {selected ? <Selected w={10} h={8} vw={10} vh={8} /> : tagItem?.paymentTypeTagName}
        </div>
        <div className="text-body-6 text-platinum-200 text-center">
          <div className="py-2">
            {src && <img className="m-auto" alt="logo" src={src} />}
          </div>
          {bank?.paymentTypeName}
        </div>
      </button>
    )
  }

  return (
    <div className="w-full px-3 py-4 my-2 bg-white dark:bg-purple-800 grid grid-cols-3 gap-2">
      {payments?.map((bank, index) => (
        <BankItem bank={bank} index={index} />
      ))}
    </div>
  )
}

export default Payment
