import dynamic from 'next/dynamic'
import useUserData from 'hooks/useUserData'
import { FormattedMessage } from 'react-intl'

const Refresh = dynamic(() => import('components/icons/Refresh.js'))
const Currency = dynamic(() => import('components/core/Currency'))

function UserInfo() {
  const { userData, balance, mutateBalance } = useUserData()

  return (
    <div className="flex w-full px-3 py-2 my-2 bg-white dark:bg-purple-800 items-center">
      <div style={{ width: '64px', height: '64px' }} className="overflow-hidden rounded-full">
        <img src={userData?.avatarIcon} alt="" />
      </div>
      <div className="ml-2 flex-1">
        <div className="text-body-4 text-platinum-200">{userData?.account}</div>
        <div className="flex items-center text-body-4 text-platinum-200">
          <FormattedMessage id="center_balance" />
          <span className="font-bold mr-1">
            <Currency value={balance} />
          </span>
          <button type="button" onClick={() => mutateBalance()}>
            <Refresh />
          </button>
        </div>
      </div>
    </div>
  )
}

export default UserInfo
