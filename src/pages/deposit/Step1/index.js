import { useState, useMemo, useEffect } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { FormattedMessage } from 'react-intl'
import useSWR from 'swr'
import fetcher from 'libs/fetcher'
import useSession from 'hooks/useSession'

const Container = dynamic(() => import('components/core/Container'))
const ChannelTab = dynamic(() => import('./components/Channels/ChannelTab'))
const UserInfo = dynamic(() => import('./components/UserInfo'))
const Payment = dynamic(() => import('./components/Payment'))
const NavigationBar = dynamic(() => import('components/core/NavigationBar'))
const IconBack = dynamic(() => import('components/icons/Back.svg'))
const CustomerService = dynamic(() => import('components/icons/CustomerService'))

function Deposit() {
  const [hasSession] = useSession()
  const router = useRouter()
  const [activePlatform, setActivePlatform] = useState()

  const { data: payments, error } = useSWR(
    hasSession ? `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}v2/api/deposit/channels` : null,
    async url => {
      const res = await fetcher({ url, withToken: true })
      return res.data.data
    },
    {
      revalidateOnFocus: false,
    },
  )

  useEffect(() => {
    if (!activePlatform && payments) {
      setActivePlatform(payments[0].paymentTypeId)
    }
  }, [activePlatform, payments])

  const paymentData = useMemo(() => payments?.find(d => d.paymentTypeId === activePlatform) || [], [
    payments,
    activePlatform,
  ])

  return (
    <div className="w-full h-screen">
      <NavigationBar
        fixed
        left={<IconBack />}
        title={<FormattedMessage id="title" />}
        right={<CustomerService width="24" height="24" />}
        onLeftClick={() => router.back()}
        onRightClick={() => router.push('/service')}
      />
      <Container withNavigationBar noPadding>
        <UserInfo />
        <Payment
          activePlatform={activePlatform}
          setActivePlatform={setActivePlatform}
          payments={payments}
        />
        <ChannelTab activePlatform={activePlatform} activePlatformData={paymentData} />
      </Container>
    </div>
  )
}

export default Deposit
