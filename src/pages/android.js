import { useEffect } from 'react'

function Android() {
  useEffect(() => {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault()
      e.prompt()
    })
  })
  return <div />
}

export default Android
