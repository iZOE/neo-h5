import React, { memo, useCallback, useEffect, useReducer } from 'react'
import { useRouter } from 'next/router'

const Withdraw = memo((props) => {
    const router = useRouter()
    useEffect(() => {
        router.replace('/withdraw/banks?step=1')
    })
    return <div />
})

export default Withdraw