import React, { memo, useCallback, useEffect, useReducer, useState } from 'react'
import { useIntl } from 'react-intl'
import { useRouter } from 'next/router'
import NavigationBar from '../../components/core/NavigationBar'
import Back from '../../components/icons/Back'
import { Context, withdrawInitialState, withdrawReducer } from '../../components/withdraw/reducer/reducer'
import WithdrawSelectCard from '../../components/withdraw/WithdrawSelectCard'
import WithdrawSubmit from '../../components/withdraw/WithdrawSubmit'
import WithdrawPassword from '../../components/withdraw/WithdrawPassword'
import fetcher from '../../libs/fetcher'
import showMessage from '../../components/core/Alert/message'
import * as dayjs from 'dayjs'
import WithdrawDetail from '../../components/withdraw/WithdrawDetail'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import VerifySMS from '../../components/withdraw/VerifySMS'


const Withdraw = memo((props) => {
    const intl = useIntl()
    const router = useRouter()
    const { tab, step } = router.query
    const [withdraw, dispatch] = useReducer(withdrawReducer, withdrawInitialState, undefined)
    const [data, setData] = useState({})
    const handleClickNavBack = useCallback(() => {
        router.back()
    }, [])

    const fetch = async (path, handler) => {
        try {
            return await fetcher({
                url: `${process.env.NEXT_PUBLIC_END_POINT_PORTAL}${path}`,
                withToken: true,
            })
        } catch (e) {
            return { data: { data: handler(e) } }
        }
    }

    const validateWithdrawAccountLength = (cards) => {
        if (cards.bankCards.length + cards.cryptoCurrencies.length === 0) {
            showMessage({
                title: intl.messages['validate.no_card.title'],
                content: intl.messages['validate.no_card.body'],
                okText: intl.messages['validate.no_card.ok'],
                cancelText: intl.messages['validate.no_card.cancel'],
                onOk: () => {
                    router.push('/withdraw-manage/banks')
                },
                onCancel: () => {
                    router.push('/user')
                },
            })
        }
    }

    useEffect(async () => {
        const mappings = {
            banks: {
                list: 'bankCards',
                page: 'crypto',
            },
            crypto: {
                list: 'cryptoCurrencies',
                page: 'banks',
            },
        }
        const { data: { data: initial } } = await fetch('v1/api/withdrawal/initial', (e) => {
            showMessage({
                content: intl.messages['validate.no_password.body'],
                okText: intl.messages['validate.no_password.ok'],
                cancelText: intl.messages['validate.no_password.cancel'],
                onOk: () => {
                    router.push('/user/setting/info/fund-password')
                },
                onCancel: () => {
                    router.push('/user')
                },
            })
        })

        if (!initial) {
            return
        }

        const { data: { data: cards } } = await fetch('v1/api/MeBank')
        validateWithdrawAccountLength(cards)

        if (cards[mappings[tab].list].length === 0) {
            router.replace({
                pathname: `/withdraw/[tab]`,
                query: {
                    tab: mappings[tab].page,
                    step: 1,
                },
            })
        }

        const { data: { data: cryptoRate } } = await fetch('v1/api/MeBank/CryptoCurrencyExchangeRate', (e) => {
            return JSON.parse(sessionStorage.getItem('cryptoRate'))
        })
        sessionStorage.setItem('cryptoRate', JSON.stringify(cryptoRate))
        sessionStorage.setItem('cryptoRateTime', dayjs().unix())

        const { data: { data: protocol } } = await fetch('v1/api/mebank/CryptoCurrencyProtocol')

        let rate = {}
        protocol.forEach(item => {
            const exchange = cryptoRate.find(f => f.protocolName === item.protocolName)
            rate[item.protocolName] = {
                rates: exchange.exchangeRates,
                amount: item.minWithdrawAmount,
                id: item.protocolId,
                name: item.protocolName,
                type: item.protocolType,
            }
        })
        const {
            data: {
                data: {
                    contactVerified,
                    phone
                }
            }
        } = await fetch('v1/api/me/security')

        setData({
            accountBalance: initial,
            cards,
            rate,
            phone: {
                verified: contactVerified === 2,
                phone
            }
        })
    }, [])

    const callback = useCallback((password) => {
        dispatch({
            type: 'INPUT_WITHDRAW_PASSWORD',
            value: password,
        })

        router.push({
            pathname: location.pathname,
            query: {
                step: 2,
            },
        })
    }, [tab])

    const {
        accountBalance,
        cards,
        rate,
        phone
    } = data

    console.info(phone)
    return <Context.Provider value={{ withdraw, dispatch }}>
        <div className="overflow-y-scroll overflow-x-hidden dark:bg-purple-900 h-screen">
            <NavigationBar
                fixed
                left={<Back />}
                onLeftClick={handleClickNavBack}
                title={intl.messages[`title.step${withdraw.step}`]} />

            {
                step === '1' || withdraw.password.length === 0 ?
                    <WithdrawPassword callback={callback} /> :
                    ''
            }
            {
                step === '2' && withdraw.password.length > 0 ?
                    <WithdrawSelectCard accountBalance={accountBalance}
                                        cards={cards}
                                        rate={rate} /> :
                    ''
            }
            {
                step === '3' && withdraw.password.length > 0 ?
                    <WithdrawSubmit /> :
                    ''
            }
            {
                step === '10' && withdraw.password.length > 0 ?
                    <VerifySMS verified={phone.verified} phone={phone.phone} /> :
                    ''
            }
            {
                step === '99' && withdraw.password.length > 0 ?
                    <WithdrawDetail /> :
                    ''
            }
        </div>
    </Context.Provider>
})

export async function getStaticProps(context) {
    return {
        props: {},
    }
}

export async function getStaticPaths() {
    return {
        paths: [
            { params: { tab: 'banks' } },
            { params: { tab: 'crypto' } },
        ],
        fallback: false,
    }
}

export default Withdraw
