export default {
  title: 'Thêm vào màn hình chính',
  android_PWA_tutorial: [
    'Không cần tải xuống, bấm vào chơi liền{br}{br}Sử dụng trình duyệt Chrome',
    'Bấm vào nút menu ở góc trên bên phải',
    'Bấm để thêm vào màn hình chính',
    'Nhập tên{br}{br}nhấn thêm để hoàn thành',
  ],
  ios_PWA_tutorial: [
    'Không cần tải xuống, bấm vào chơi liền{br}{br}Sử dụng trình duyệt Safari',
    'Kéo trang web theo chiều ngược lại để làm thanh menu xuất hiện{br}{br}Nhấn vào nút chia sẻ ở cuối màn hình',
    'Bấm để thêm vào màn hình chính',
    'Nhập tên{br}{br}nhấn thêm để hoàn thành',
  ],
}
