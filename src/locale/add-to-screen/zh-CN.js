export default {
  title: '{device} 添加到主屏幕',
  android_PWA_tutorial: [
    '不须下载，随点即玩{br}{br}以Chrome浏览器为例',
    '轻点右上角菜单按钮',
    '轻点添加到主屏幕',
    '输入名称{br}{br}轻点添加即可完成',
  ],
  ios_PWA_tutorial: [
    '不须下载，随点即玩{br}{br}以Safari浏览器为例',
    '反向卷动网页使菜单栏出现{br}{br}轻点屏幕下方分享按钮',
    '轻点添加到主屏幕',
    '输入名称{br}{br}轻点添加即可完成',
  ],
}
