export default {
  game_list: {
    default: '进行试算',
    lottery: '彩票',
    video: '真人',
    chess: '棋牌',
    egame: '电子',
    fish: '捕鱼',
    esport: '体育',
  },
  title: '試算',
}
