export default {
  formula: {
    section: {
      1: 'A : Bạn{br}B : Những hội viên bạn giới thiệu{br}C : Những hội viên B giới thiệu{br}D : Những hội viên C giới thiệu{br}.{br}.{br}.{br}N : Cứ thế nhân lên',
      2: 'Cách tính hoa hồng : {br}(Tổng cược + 10000) x Hoa hồng ngày x 30%{br}{br}Thu nhập của bạn:{br}- Các hoa hồng của Ｂ x 30%{br}+ Các hoa hồng của C x 30% x 30%{br}+ Các hoa hồng của D x 30% x 30% x 30%{br}.{br}+ Các hoa hồng của N x % hoa hồng cấp N',
      formula: 'Cách tính hoa hồng : {br}(Tổng cược + 10000) x Hoa hồng ngày x 30%',
    },
    title: 'Mô hình sale đa thế hệ',
  },
  game_list: {
    default: 'Tiến hành tính thử',
    lottery: 'Xổ số',
    video: 'Điện tử',
    chess: 'Casino',
    egame: 'Bài',
    fish: 'Bắn cá',
    esport: 'Thể thao',
  },
  member: {
    copy_promo_code: {
      action: 'Mã giới thiệu',
      result: {
        success: 'Sao chép thành công'
      }
    },
    icons: {
      customer_service: 'CSKH độc quyển',
      download: 'Tải APP',
      share: 'Chia sẽ mã QR',
    },
    info: {
      income: {
        today: 'Hôm nay:',
        total: 'Tổng thu nhập:',
      },
    },
  },
  rule: {
    detail: {
      section1: {
        content: 'Đáp : Tất cả các hội viên đều có thể trở thành đại lý của hệ thống chúng tôi , bạn thông qua zalo , facebook chia sẽ mã code hoặc link liên kết , thông qua cách này đăng ký trở thành hội viên thì bạn đã là đại lý của hệ thống, những hội viên được bạn giới thiệu sẽ là cấp dưới của bạn , mạng lưới phát triển vô giới hạn.',
        title: '1. Trở thành đại lý như thế nào ?',
      },
      section2: {
        content: 'Đáp : Tỉ lệ hoa hồng được tính theo tổng cược của nhóm bạn và loại hình game để tính (xem chi tiết tại hướng dẫn mở rộng đại lý đa thế hệ) . Tổng cược là số tiền cá cược thực thế của hội viên (không bao gồm số tiền chưa kết toán),tổng cược không tính thắng thua. Ví dụ : Hội viên A cược ván này 10,000, ván sau thua 5,000 , thì tổng cược của bạn sẽ là 15,000 .',
        title: '2. Tỉ lệ hoa hồng được tính như thế nào ?',
      },
      section3: {
        content: 'Đáp : Mỗi ngày 6:00 sẽ kết toán hoa hồng ngày hôm trước , sau khi kết toán bạn có thể đề xuất xuất khoản được.',
        title: '3. Hoa hồng bao lâu kết toán 1 lần ?',
      },
    },
    table: {
      head: {
        column1: 'Loại hình game',
        column2: 'Hoa hồng ngày{br} / chục triệu',
      },
    },
    title: 'Chế độ % hoa hồng vô hạn',
  },
  title: 'ĐẠI LÝ',
  video: {
    title: 'Hoạt ảnh hướng dẫn mở rộng thị trường Đại lý đa thế hệ'
  },
}