export default {
  copy_button: {
    action: 'Sao chép liên kết.',
    result: {
      success: 'Sao chép thành công',
    },
  },
  screenshot: {
    action: 'Chi tiết',
    result: {
      success: 'Đã lưu hình',
    },
  },
  tab: {
    title1: 'chia sẽ hình 1',
    title2: 'chia sẽ hình 2',
    title3: 'chia sẽ hình 3',
  },
  title: 'Chia sẽ mã QR',
}
