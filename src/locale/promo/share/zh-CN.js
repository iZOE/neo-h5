export default {
  copy_button: {
    action: '复制网址',
    result: {
      success: '复制成功',
    },
  },
  screenshot: {
    action: '保存图片',
    result: {
      success: '已保存图片',
    },
  },
  tab: {
    titl1: '分享图一',
    titl2: '分享图二',
    titl3: '分享图三',
  },
  title: '推广分享',
}
