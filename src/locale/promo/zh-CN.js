export default {
  formula: {
    section: {
      1: 'A：您自己{br}B：您介绍的所有用户{br}C：B 介绍的所有用户{br}D：C 介绍的所有用户{br}.{br}.{br}.{br}N：以此类推',
      2: '您的收益＝{br}所有 B 产生的返佣  × 30％{br}＋所有 C 产生的返佣  × 30％ × 30％{br}＋所有 D 产生的返佣  × 30％ × 30％ × 30％{br}.{br}.{br}.{br}＋所有 N 产生的返佣 × N级返佣比例',
      formula: '返佣计算公式：{br}（投注额 ÷ 10000）× 日结佣金 × 30％.....',
    },
    title: '全民代理推广模式',
  },
  game_list: {
    default: '进行试算',
    lottery: '彩票',
    video: '真人',
    chess: '棋牌',
    egame: '电子',
    fish: '捕鱼',
    esport: '体育',
  },
  member: {
    copy_promo_code: {
      action: '复制推广邀请码',
      result: {
        success: '复制成功'
      }
    },
    icons: {
      customer_service: '专属客服',
      download: '手机客户端下载',
      share: '推广分享',
    },
    info: {
      income: {
        today: '今日收入：',
        total: '总计收入：',
      },
    },
  },
  rule: {
    detail: {
      section1: {
        content: '所有玩家都可以成为我们的官方代理，您通过微信、QQ等方式分享您的二维码或者邀请链接；通过此方法注册的会员即为您的直属代理，其邀请的下级会员皆算您的下线玩家、可无限变裂发展。',
        title: '1. 如何做代理？',
      },
      section2: {
        content: '佣金根据您的团队总流水金额及游戏类型来计算（详情请看代理推广教程）。流水是指某会员消费的实际金额（未结算不计算在内），流水不计算输赢。例如：某会员这把下注赢10,000，下把输5,000,则流水15,000。',
        title: '2. 返佣比例怎么结算？',
      },
      section3: {
        content: '佣金每日06:00结算前日的佣金，结算后即可提现。',
        title: '3. 佣金多久结算？',
      },
    },
    table: {
      head: {
        column1: '游戏类型',
        column2: '日结佣金/万',
      },
    },
    title: '无限代理佣金制度表',
  },
  title: '推广赚钱',
  video: {
    title: '全民代理推广动画教程'
  },
}
