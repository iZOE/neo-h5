export default {
  all: '全部',
  tabs: {
    activity: '活动',
    noticeboard: '公告',
  },
  emptyMessage: '优惠规划中，敬请期待！',
}
