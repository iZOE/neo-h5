export default {
  all: 'Toàn bộ',
  tabs: {
    activity: 'Hoạt động',
    noticeboard: 'Thông báo',
  },
  emptyMessage: 'Trang đang cập nhật khuyến mãi, vui lòng chờ đợi !',
}
