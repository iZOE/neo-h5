export default {
  login: '登录',
  form: {
    input: {
      accountId: {
        placeholder: '请输入您的账户名称',
      },
      password: {
        placeholder: '请输入您的账户密码',
      },
    },
    forgetPassword: '忘记密码',
    switch: {
      label: '记住我',
    },
    login: '登录',
    error: {
      serverError: {
        content: '发生异常，请重新尝试({code})',
      },
      inputError: {
        title: '账号密码错误',
        content: '您输入的账号或密码不正确，请再试一次',
      },
      notFoundError: {
        content: '账户名称错误，请重新输入',
      },
      notActiveError: {
        content: '账户尚未启用',
      },
      lockedError: {
        content: '账户已被锁定，请联系客服人员',
      },
      accountLockedFiveMinuteError: {
        title: '账号已被锁定',
        content: '账号已被锁定，{second} 秒后再试',
      },
    },
    ok: '确定',
    cancel: '取消',
    contactCustomerService: '联系客服',
  },
}
