export default {
  login: 'Đăng nhập',
  form: {
    input: {
      accountId: {
        placeholder: 'Nhập tên tài khoản game',
      },
      password: {
        placeholder: 'Nhập mật khẩu',
      },
    },
    forgetPassword: 'Quên mật khẩu',
    switch: {
      label: 'Ghi nhớ',
    },
    login: 'Đăng nhập',
    error: {
      serverError: {
        content: 'Xảy ra lỗi , vui lòng thử lại({code})',
      },
      inputError: {
        title: 'Mật khẩu không đúng',
        content: 'Tài khoản hoặc mật khẩu không đúng , vui lòng nhập lại',
      },
      notFoundError: {
        content: 'Tên tài khoản không đúng , vui lòng nhập lại',
      },
      notActiveError: {
        content: 'Tài khoản chưa sử dụng',
      },
      lockedError: {
        content: 'Tài khoản đã bị khóa , vui lòng liên hệ CSKH',
      },
      accountLockedFiveMinuteError: {
        title: 'Tài khoản đã tạm khóa',
        content: 'Tài khoản đã tạm khóa, vui lòng thử lại sau {second} giây',
      },
    },
    ok: 'Xác nhận',
    cancel: 'Hủy',
    contactCustomerService: 'Liên hệ CSKH',
  },
}
