export default {
  deposit_record: 'Lịch sử nạp tiền',
  filter: 'Chọn',
  last2weeks: '14 ngày gần nhất',
  no_data: 'Chưa có lịch sử nạp',
  apply_time: 'Thời gian đề xuất',
  achieved_time: 'Thời gian nhận được',
  deposit_status: {
    1: 'Chưa chuyển khoản',
    2: 'Đã lên điểm',
    4: 'Lệnh quá hạn',
    7: 'Toàn bộ',
  },
}
