export default {
  deposit_record: '充值纪录',
  filter: '筛选',
  last2weeks: '近14天',
  no_data: '尚无充值纪录',
  apply_time: '申请时间',
  achieved_time: '到账时间',
  deposit_status: {
    1: '未转账',
    2: '已到账',
    4: '已逾期',
    7: '全部',
  },
}
