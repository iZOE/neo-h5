export default {
  withdraw_record: '提现纪录',
  filter: '筛选',
  last2weeks: '近14天',
  no_data: '尚无提现纪录',
  apply_time: '申请时间',
  withdraw_status: {
    1: '未处理',
    2: '提现处理中',
    4: '已出款',
    8: '已拒绝',
    16: '出款失败',
    31: '全部',
  },
}
