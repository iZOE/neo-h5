export default {
  withdraw_record: 'Lịch sử rút tiền',
  filter: 'Chọn',
  last2weeks: '14 ngày gần nhất',
  no_data: 'Chưa có lịch sử rút tiền',
  apply_time: 'Thời gian đề xuất',
  withdraw_status: {
    1: 'Chưa xử lý',
    2: 'Đang xử lý rút tiền',
    4: 'Đã xuất khoản',
    8: 'Đã từ chối',
    16: 'Xuất khoản thất bại',
    31: 'Toàn bộ',
  },
}
