export default {
  title: 'Lịch sử chuyển điểm',
  fields: {
    order_number: 'Mã đơn',
    create_time: 'Thời gian',
    description: 'Thể loại',
    amount: 'Số tiền chuyển khoản',
    preferential_amount: 'Số tiền thay đổi thực tế',
    note: 'Ghi chú',
  },
}
