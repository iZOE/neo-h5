export default {
  title: '转账纪录详情',
  fields: {
    order_number: '订单编号',
    create_time: '时间',
    description: '类型',
    amount: '转账金额',
    preferential_amount: '實際异动金额',
    note: '备注',
  },
}
