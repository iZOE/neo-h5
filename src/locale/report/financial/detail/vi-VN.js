export default {
  financial_detail: 'Lịch sử giao dịch',
  order_number: 'Mã đơn',
  time: 'Thời gian',
  type: 'Thể loại',
  before_balance: 'Số dư trước giao dịch',
  change_balance: 'Số dư giao dịch',
  after_balance: 'Số dư sau giao dịch',
  remarks: 'Ghi chú',
}
