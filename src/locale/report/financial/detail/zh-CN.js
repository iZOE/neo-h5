export default {
  financial_detail: '账变详情',
  order_number: '订单编号',
  time: '时间',
  type: '类型',
  before_balance: '异动前余额',
  change_balance: '异动金额',
  after_balance: '异动后余额',
  remarks: '备注',
}
