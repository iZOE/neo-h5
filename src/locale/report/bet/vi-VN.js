export default {
  last2weeks: '14 ngày gần nhất',
  no_data: 'Không có lịch sử cược',
  win_loss: 'Lợi nhuận',
  total_bet_amount: 'Mức cược',
  total_count: 'Số khoản',
  period: 'Kỳ thứ {betNumber}',
  bet_amount: 'Số tiền đặt cược',
  rule_name: 'Cách chơi',
  bet_time: 'Thời gian đặt cược',
  no_result: 'Chưa thanh toán',
}
