export default {
  last2weeks: '近14天',
  no_data: '尚无投注纪录',
  win_loss: '盈亏',
  total_bet_amount: '投注额',
  total_count: '筆數',
  period: '第{betNumber}期',
  bet_amount: '金额',
  rule_name: '玩法',
  bet_time: '投注時間',
  no_result: '未結算',
}
