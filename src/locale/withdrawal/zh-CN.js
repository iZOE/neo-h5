export default {
  step1: {
    dialog: {
      initial_error: {
        back: '返回',
      },
      withdrawal_service: {
        title: '请解绑您的提现功能',
        content: '如您需开通此功能，请联系在线客服解除锁定。',
        okText: '联系客服',
        cancelText: '取消',
      },
      fundPassword_empty: {
        title: '尚未设定资金密码',
        okText: '前往设置',
        cancelText: '取消',
      },
      meBank_empty: {
        title: '请先新增提现账户',
        content: '请在提现前新增至少一个提现账户',
        okText: '进行绑定',
        cancelText: '取消',
      },
    },
    fundPassword_verify: {
      title: '验证资金密码',
      placeholder: '请输入6位数资金密码',
      hint: '*为确保您的帐户安全，若忘记资金密码，请直接 ',
      submit: '下一步',
      cskh: '联系客服',
      form: {
        validate: {
          message: {
            required: '必填',
            error: '资金密码必须是6位数字',
          },
        },
      },
      dialog: {
        success: {
          title: '新增提现账户',
          content: '提现账户添加成功',
          okText: '确定',
        },
        fail: {
          title: '资金密码错误，您还有{availableCount}次机会',
          okText: '确定',
        },
        lock: {
          title: '您的资金密码已遭锁定，请联系客服',
          okText: '联系客服',
        },
        fundPassword_empty: {
          title: '尚未设定资金密码',
          okText: '前往设置',
          cancelText: '取消',
        },
      },
    },
  },
  step2: {
    account_info: {
      balance: '账户余额',
      times: '今日免手续费次数',
    },
    action: {
      contact_cs: '联系客服',
      confirm: '确定',
      play_more: '前往游戏',
      next: '下一步',
    },
    dialog: {
      amount_maximum: '提现总额已达上限，若有疑问请联系客服',
      times_maximum: '当日提现次数已达上限，若有疑问请联系客服',
      turnover_fail: {
        content: '尚缺 {amount} 流水，即可进行提现',
        title: '流水未达标准！',
        note: '查看详情',
      },
    },
    exchange_rate: '当前汇率：',
    extra_info: {
      content: {
        1: '单次提现最低{min}，最高{max}。',
        2: '若超过当日免手续费次数，会酌收手续费。每日00:00重新计算。',
        3: '提现时，若未达平台流水要求，会产生相关费用。',
        4: '提现单笔达{fee}(含)以上，需通过手机验证。',
        5: '当前汇率仅供参考。',
      },
      title: '温馨提醒：',
    },
    field: {
      amount: {
        placeholder: '请输入提现金额',
        validator: {
          insufficient_balance: '提现金额不得大于帐户金额',
          out_of_range: '输入金额超出可申请范围',
        },
      },
    },
    tab: {
      header: {
        credit_card: '银行卡',
        crypto_currency: '虚拟币钱包',
      },
    },
    title: '提现申请',
  },
  step3: {
    title: '提现审核结果',
    header: {
      bankcard_title: '賬戶',
      cryptoWallet_title: '虚拟币钱包',
      bankcard_number: '(卡号后四码 {num})',
      cryptoWallet_number: '(钱包地址后5码 {num})',
    },
    application_amount: '申请金额',
    administration_fee: '行政费',
    promotional_deduction: '优惠金额扣除',
    handling_fee: '手续费',
    real_amount: '实际可提领金额',
    check_detail: '审核详情',
    note: {
      administration_fee: '若未达到平台流水要求，平台将收取未達流水的{rate}%行政费。',
      promotional_deduction: '若未达到平台流水要求，平台将扣除優惠金額。',
      handling_fee: '超過每日免手續費提領上限，酌收手續費。',
    },
    cskh: '联系客服',
    warn: {
      title: '温馨提醒：',
      item1: '若超过当日免手续费次数，会酌收手续费。每日 00:00 重新计算。',
      item2: '提现时，若未达到平台流水要求，会产生相关费用。',
      item3: '可点击项目说明按钮，查看审核详情。',
      item4_bankcard:
        '请确认银行卡与取款人姓名一致，提醒您，若取款人与银行卡号无法匹配，将不予出款。',
      item4_cryptoWallet: '审核通过将立即出款，实际到账时间依网络确认速度而定，请耐心等候。',
      item5: '若有任何问题请{cskh}。',
    },
    confirm: '确认提领',
    withdraw_success: '申请成功！进度可至提现纪录查询',
  },
  step4: {
    title: '设置手機號碼',
    submit: '获取验证码',
    placeholder: '请输入手机号码',
    phone_label: '请输入手机号码',
    validate: {
      required: '必须是 10 字的组合',
      length: '必须是 10 字的组合',
    },
  },
  step5: {
    title: '短信认证',
    submit: '完成',
    placeholder: '请输入4位数验证码',
    validate: {
      required: '验证码为必填',
      length: '必须是 4 字的组合',
    },
    mapping_code_sent: '已发送4位数验证码至您的手机',
    mapping_code_will_expire: '{mmss} 后验证码失效',
    mapping_code_is_expired: '验证码已失效',
    get_code_button: '重新发送验证码',
    withdraw_success: '申请成功！进度可至提现纪录查询',
  },
  turnover_detail: {
    field: {
      amount: '金额',
      id: '项目ID',
      item: '项目',
      multiple: '稽核倍数',
      status: '状态',
      turnover_amount: '所需流水',
      type: '类型',
    },
    main_area: {
      amount: '期间有效投注总额(有效流水)：',
      duration: '本次结算起始时间：',
      note: '(上次提现或转账时间)',
    },
    title: '审核详情',
  },
}
