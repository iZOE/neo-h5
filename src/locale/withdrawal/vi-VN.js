export default {
  step1: {
    dialog: {
      initial_error: {
        back: 'Trở về',
      },
      withdrawal_service: {
        title: 'Vui lòng gỡ mặc định chức năng rút tiền',
        content: 'Nếu bạn cần kích hoạt chức năng này, vui lòng liên hệ CSKH để mở khóa',
        okText: 'Liên hệ CSKH',
        cancelText: 'Hủy',
      },
      fundPassword_empty: {
        title: 'Chưa cài đặt mật khẩu quỹ',
        okText: 'Tiến đến đặt cược',
        cancelText: 'Hủy',
      },
      meBank_empty: {
        title: 'Vui lòng thêm tài khoản rút tiền',
        content: 'Trước khi rút tiền , cần thêm tối thiểu 1 tài khoản rút tiền',
        okText: 'Tiến hành mặc định',
        cancelText: 'Hủy',
      },
    },
    fundPassword_verify: {
      title: 'Xác nhận mật khẩu quỹ',
      placeholder: 'Nhập 6 số mật khẩu quỹ',
      hint: '*Vì lý do bảo mật tài khoản , nếu quên mật khẩu quỹ , vui lòng liên hệ ',
      submit: 'Bước tiếp theo',
      cskh: 'CSKH',
      form: {
        validate: {
          message: {
            required: 'Bắt buộc điền',
            error: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
          },
        },
      },
      dialog: {
        success: {
          title: 'Thêm Tài khoản rút tiền',
          content: 'Đã thêm Tài khoản rút tiền thành công',
          okText: 'Xác nhận',
        },
        fail: {
          title: 'Mật khẩu quỹ không chính xác , bạn còn {availableCount} lần cơ hội',
          okText: 'Xác nhận',
        },
        lock: {
          title: 'Mật khẩu quỹ của bạn đã bị khóa, vui lòng liên hệ CSKH',
          okText: 'CSKH',
        },
        fundPassword_empty: {
          title: 'Chưa cài đặt mật khẩu quỹ',
          okText: 'Tiến đến đặt cược',
          cancelText: 'Hủy',
        },
      },
    },
  },
  step2: {
    account_info: {
      balance: 'Số dư ví chính',
      times: 'Số lần miễn phí rút hôm nay',
    },
    action: {
      contact_cs: 'CSKH',
      confirm: 'Xác nhận',
      play_more: 'Tiến đến trò chơi',
      next: 'Bước tiếp theo',
    },
    dialog: {
      amount_maximum: 'Tổng số tiền rút đã đạt giới hạn, liên hệ CSKH nếu có thắc mắc.',
      times_maximum: 'Số lần rút tiền hôm nay đã đạt giới hạn, liên hệ CSKH nếu có thắc mắc.',
      turnover_fail: {
        content: 'Bạn còn thiếu {amount}d tổng cược , mới có thể rút khoản',
        title: 'Tổng cược chưa đạt',
        note: 'Xem chi tiết',
      },
    },
    exchange_rate: 'Tỷ giá hối đoái hiện hành：',
    extra_info: {
      content: {
        1: 'Hạn mức rút tiền thấp nhất {min}, cao nhất {max}.',
        2: 'Nếu vượt số lần miễn phí trong ngày, phí xử lý sẽ được tính. Cập nhật lại mỗi ngày vào lúc 00:00.',
        3: 'Nếu chưa đạt tổng cược hợp lệ, sẽ bị tính phí hành chính.',
        4: 'Rút tiền đạt đến hoặc vượt quá {fee}(bao gồm), cần xác minh số điện thoại.',
        5: 'Tỉ giá hiện tại chỉ mang tính chất tham khảo.',
      },
      title: 'Lưu ý:',
    },
    field: {
      amount: {
        placeholder: 'Nhập số tiền rút',
        validator: {
          insufficient_balance: 'Số tiền rút không được vượt số dư tài khoản',
          out_of_range: 'Số tiền vượt hạn mức',
        },
      },
    },
    tab: {
      header: {
        crypto_currency: 'Vií tiền ảo',
        credit_card: 'Thẻ ngân hàng',
      },
    },
    title: 'Về chúng tôi',
  },
  step3: {
    title: 'Kết quả kiểm duyệt',
    header: {
      bankcard_title: 'Tài khoản',
      cryptoWallet_title: 'Vií tiền ảo',
      bankcard_number: '(4 số cuối trong số tài khoản {num})',
      cryptoWallet_number: '(5 số cuối của liên kết ví tiền {num})',
    },
    application_amount: 'Số tiền đề xuất',
    administration_fee: 'Phí hành chính',
    promotional_deduction: 'Khấu trừ điểm khuyến mãi',
    handling_fee: 'Phí thủ tục',
    real_amount: 'Số tiền rút thực tế',
    check_detail: 'Xem chi tiết',
    note: {
      administration_fee:
        'Nếu vòng cược chưa đủ yêu cầu, hệ thống sẽ thu phí {rate}% phí hành chính.',
      promotional_deduction:
        'Nếu chưa đạt tổng cược yêu cầu , hệ thống sẽ trừ khoản tiền khuyến mãi.',
      handling_fee: 'Nếu vượt số lần miễn phí trong ngày, phí xử lý sẽ được tính.',
    },
    cskh: 'CSKH',
    warn: {
      title: 'Lưu ý:',
      item1:
        'Nếu vượt số lần miễn phí trong ngày, phí xử lý sẽ được tính. Cập nhật lại mỗi ngày vào lúc 00:00.',
      item2: 'Nếu chưa đạt tổng cược hợp lệ, sẽ bị tính phí hành chính.',
      item3: 'Nhấn vào nút biểu tượng kiểm tra để theo dõi chi tiết kiểm duyệt.',
      item4_bankcard:
        'Nếu tên người thụ hưởng không trùng khớp với tên chủ tài khoản ngân hàng, sẽ không được duyệt xuất khoản.',
      item4_cryptoWallet:
        'Sau khi thông qua thẩm định sẽ xuất khoản ngay , thời gian nhận được thực tế dựa vào tốc độ đường truyền mạng , bạn vui lòng chờ trong giây lát.',
      item5: 'Nếu bạn có bất cứ câu hỏi nào, vui lòng liên hệ bộ phận {cskh}。',
    },
    confirm: 'xác nhận rút tiền',
    withdraw_success: 'Đề xuất thành công ! Vui lòng kiểm tra tiến độ tại lịch sử rút tiền',
  },
  step4: {
    title: 'Thay đổi số điện thoại',
    submit: 'Xác nhận',
    placeholder: 'Vui lòng nhập số điện thoại',
    phone_label: 'Vui lòng nhập số điện thoại',
    validate: {
      required: 'Bắt buộc điền số điện thoại',
      length: 'Nhập 10 ký tự chữ',
    },
  },
  step5: {
    title: 'Tin nhắn xác nhận',
    submit: 'Xác nhận',
    placeholder: 'Vui lòng nhập mã xác nhận gồm 4 chữ số',
    validate: {
      required: 'Bắt buộc điền mã xác nhận',
      length: 'Nhập 4 ký tự chữ',
    },
    mapping_code_sent: 'Đã gửi mã xác nhận đến số điện thoại của bạn.',
    mapping_code_will_expire: '{mmss} sau mã xác nhận vô hiệu hóa',
    mapping_code_is_expired: 'Mã xác nhận đã vô hiệu hóa',
    get_code_button: 'Gửi lại mã xác nhận',
    withdraw_success: 'Đề xuất thành công ! Vui lòng kiểm tra tiến độ tại lịch sử rút tiền',
  },
  turnover_detail: {
    field: {
      amount: 'Số tiền',
      id: 'Hạng mục ID',
      item: 'Hạng mục',
      multiple: 'Bội số xét duyệt',
      status: 'Trạng thái',
      turnover_amount: 'Doanh thu yêu cầu',
      type: 'Thể loại',
    },
    main_area: {
      amount: 'Tổng cược hợp lệ trong thời gian：',
      duration: 'Thời gian bắt đầu kết toán kỳ này：',
      note: '(Lần rút khoản gần nhất hoặc thời gian chuyển khoản)',
    },
    title: 'Chi tiết kiểm duyệt',
  },
}
