export default {
  center_balance: 'Số dư：',
  title: 'Nạp tiền',
  title_step2: 'Chi tiết cược xổ số',
  empty_channel: 'Hiện tại không có kênh nạp tiền nào',
  back_lobby: 'Trở về trang chủ',
  steps: {
    account_name: 'Tên tài khoản ngân hàng',
    account_number: 'Số tài khoản ngân hàng',
    address: 'Liên kết ví tiền ảo',
    alert_cancen_text: 'Chưa thanh toán',
    alert_confirm_text: 'Đã thanh toán',
    alert_text: 'Vui lòng hoàn tất thanh toán trong thời gian có hiệu lực của đơn hàng',
    alert_title: 'Nhắc nhở',
    amount: 'Số tiền nạp',
    actual_deposit_amount: 'Thời gian thực tế nhận được tiền',
    actual_deposit_amount2: 'Số tiền lên điểm thực tế',
    channel: 'Kênh',
    channel_name: 'Cách nạp tiền',
    currency: 'Tiền tệ',
    current_exchange_rate: 'Tỷ giá hối đoái hiện hành',
    crypto_placeholder: 'Số tiền điều chỉnh phải là số chẵn',
    deposit_fee: 'Phí nạp tiền',
    error1: 'Số tiền đề xuất không được để trống',
    error2: 'Số tiền nạp không được thấp hơn {minAmount} ₫',
    error3: 'Số tiền nạp không được cao hơn {maxAmount} ₫',
    error4: 'Nhập số chẵn dưới 7 đơn vị số',
    exchange_rate: 'Tỷ giá hối đoái',
    for_reference_only: 'Cung cấp tham khảo',
    goto_deposit: 'Gửi lệnh',
    hint1: 'Lưu ý :',
    hint2: 'Số tiền nạp : thấp nhất {minAmount} ₫ , cao nhất {maxAmount} ₫',
    hint3:
      'Nếu từ Alipay nạp tiền vào thẻ ngân hàng, để tăng tốc hệ thống đối chiếu, số tiền nạp sẽ được điều chỉnh ngẫu nhiên theo số tiền ứng dụng',
    hint4:
      'Vui lòng truy cập Internet Banking để chuyển khoản và điền các nội dung trên vào các ô trống',
    hint5: 'Ghi chú đính kèm phải được điền vào nội dung chuyển khoản của ngân hàng',
    hint6: 'Kính gửi, vui lòng kết bạn với chuyên viên cộng điểm',
    hint7:
      'Nhận tài khoản thanh toán mới nhất , sau khi hoàn thành thanh toán , cần chụp hình ảnh hóa đơn gửi cho CSKH , sau khi xác nhận chính xác tài khoản sẽ được cộng điểm (Kết quả cộng điểm vui lòng đến lịch sử thay đổi số điểm tài khoản để kiểm tra)',
    hint8: 'Đơn hàng đã thành lập , có hiệu lực là',
    hint9: 'Phút , cần nạp tiền trong thời gian sớm nhất',
    hint10a: 'Vui lòng không thanh toán bất kỳ tài sản',
    hint10b: 'nào đến địa chỉ trên, nếu không tài sản sẽ không được truy xuất.',
    hint11a: 'Vui lòng chuyển',
    hint11b:
      '[Không bao gồm phí chuyển khoản] đến địa chỉ trên, nếu không tài khoản không nhận được.',
    hint12:
      'Vui lòng lấy lại địa chỉ ví cho mỗi lần gửi tiền, nếu không, chúng tôi sẽ không chịu trách nhiệm về mọi tổn thất gây ra.',
    hint13: 'Bạn không biết cách nạp tiền? Vui lòng xem hướng dẫn nạp tiền của chúng tôi.',
    hint14: 'Bạn chưa biết cách nạp tiền? Vui lòng quay lại trang trước để xem hướng dẫn.',
    hint14b:
      'Tỷ giá hối đoái sẽ được điều chỉnh theo sự biến động của thị trường quốc tế, chúng tôi có quyền điều chỉnh và thay đổi cuối cùng.',
    hint15: 'Mô tả tiền ảo:',
    hint16: 'USDT là gì ?',
    hint17: 'Sự khác biệt giữa thỏa thuận giao dịch bằng ví điện tử là gì?',
    hint18:
      'USDT được viết tắt là Tether, là một loại tiền tệ được Công ty Tether tung ra dựa trên giá trị ổn định: Đô la Mỹ (USD) token Tether USD (viết tắt là USDT). Giá trị của đơn vị tiền tệ này tương đương với đô la Mỹ, 1USDT = 1 USD . Đơn vị tiền tệ này, là loại tiền tệ ổn định nhất, có giá trị nhất và an toàn nhất, dẫn đầu trong số các loại tiền kỹ thuật số.',
    hint19a:
      'ERC20: USDT do Tether phát hành dựa trên mạng ETH. Nên giao dịch gửi và rút tiền đều diễn ra trên mạng ETH. Và cần phải hao tốn lượng Gas , được gọi là ETH.',
    hint19b:
      'TRC20: USDT do Tether phát hành dựa trên mạng TRON. Nên giao dịch gửi và rút tiền đều diễn ra trên mạng TRON .',
    hint19c:
      'OMNI: USDT do Tether phát hành dựa trên mạng BTC.Nên giao dịch gửi và rút tiền đều diễn ra trên mạng BTC và phải tốn phí chuyển khoản.',
    humna_charge_payment_type_id_map: {
      28: 'QQ',
      29: 'Wechat',
    },
    member_commission_rate: 'Phí thủ tục',
    order_number: 'Mã đơn',
    payment_type_name: 'Phương thức thanh toán',
    placeholder: 'Vui lòng nhập số tiền nạp',
    placeholder1: 'Thấp nhất',
    placeholder2: '，',
    placeholder3: 'Cao nhất',
    protocol: 'Loại tiền giao dịch',
    remarks: 'Ghi chú đính kèm khi nạp tiền',
    submit: 'Thanh toán ngay',
    timeout: 'Khoảng thời gian chờ:',
    unit: '₫',
    upload_image: 'Tải lên hình ảnh',
  },
  to: 'Chuyển',
  video_tutorial: 'Hướng dẫn trực tuyến',
  form: {
    validate: {
      message: {
        required: 'Bắt buộc điền',
      },
    },
  },
  copy_button: {
    action: 'Sao chép số {content}',
    result: {
      success: 'Sao chép thành công',
    },
  },
}
