export default {
  title: 'Tải xuống ứng dụng điện thoại',
  slider_banners: {
    sub_title_4: 'Các môn thể thao thuần túy nhất, sự hưởng thụ cao cấp nhất, tận cùng của sự tinh hoa.',
    title1: 'APP cá cược online đỉnh nhất',
    title2: 'Đại lý đa thế hệ, thu nhập tiền triệu mỗi tháng.',
    title3: 'Hội tụ các sảnh trò chơi đa dạng cho bạn lựa chọn thỏa thích',
    title4: 'Ra mắt giao diện thể thao đẹp mắt, video trực tuyến Full HD , hàng ngàn người trò chuyện nhóm cùng nhau chia sẻ niềm vui',
  },
  is_wechat: 'Vui lòng nhấp vào lựa chọn ngay góc phải trên cùng để tải APP{br}Sử dụng trình duyệt để mở',
  download_button_text: 'Tải xuống {device}',
  add_to_mainscreen: 'Thêm vào màn hình chính',
  app_version: 'Phiên bản mới nhất：{latest}  Ngày cập nhật：{release}  Hỗ trợ hệ thống：{support} Trở lên',
  spare_download: 'Nhấp vào tải xuống',
}
