export default {
  title: '手机客户端下载',
  slider_banners: {
    sub_title_1: '最纯粹的体育 最高级的享受 尽在星辉',
    sub_title_2: '微信 支付宝 网银支付 快捷 东京 QQ支付 银联扫码 网银转账',
    sub_title_3: '持欧洲、亚洲多国博彩监管机构颁发的合法执照',
    title1: '业内顶尖手机投注APP',
    title2: '星辉体育火热上线 精彩视频同步直播 万人聊天共享红单',
    title3: '支持8种主流存款方式',
    title4: '世界博彩联盟权威认证',
  },
  is_wechat: '下载APP请点右上菜单{br}用浏览器打开',
  download_button_text: '下载 {device}',
  add_to_mainscreen: '添加到主屏幕',
  app_version: '最新版本：{latest}  更新日期：{release}  支持系统：{support} 以上',
  spare_download: '备用载点',
}
