export default {
  title: '帮助中心',
  registration_problem: '注册问题',
  recharge_problem: '充值问题',
  game_problem: '游戏问题',
  betting_problem: '投注问题',
  lottery_introduction: '彩种介绍',
  security_problem: '安全问题',
  user_problem: '用户专区',
  agent_problem: '代理专区',
  withdraw_problem: '提现问题',
  crypto_problem: '虚拟货币',
  deposit_guide: '充值教学',
  cskh: '联系客服',
}
