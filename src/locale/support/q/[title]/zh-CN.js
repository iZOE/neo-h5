export default {
  title_registration_problem: '注册问题',
  title_recharge_problem: '充值问题',
  title_game_problem: '游戏问题',
  title_betting_problem: '投注问题',
  title_lottery_introduction: '彩种介绍',
  title_security_problem: '安全问题',
  title_user_problem: '用户专区',
  title_agent_problem: '代理专区',
  title_withdraw_problem: '提现问题',
  title_crypto_problem: '虚拟货币',
  title_deposit_guide: '充值教学',
  cskh: '联系客服',
  goback: '回到帮助中心',
  period: "{number}期",
  period_per_day: "每天{number}期",
  period_per_week: "每周{number}期",
  second: "{number}秒",
  bet_anytime: "7*24随时下注",
  betting_endtime: "投注截止时间",
  last_period_betting_endtime: "最后一期投注截止时间",
  lottery_time: "开奖时间",
  minute: "{number}分钟",
  period_interval: "期数间隔",
  total_period: "总期数",
  introduction_dicecup: "骰宝",
  introduction_ex5: "11选5",
  introduction_fc3d: "福彩3D",
  introduction_k3: "快三",
  introduction_lucky28: "幸运28",
  introduction_mark6: "六合彩",
  introduction_pk10: "PK10",
  introduction_xyft: "幸运飞艇",
  introduction_pl3: "排列三",
  introduction_ssc: "时时彩",
  can_i_cancel_the_bet_order: "还没开奖，可以进行撤单吗？",
  can_protect_the_safety_of_the_platform: "星辉能保障在平台充提的安全吗？",
  first_period_betting_endtime: "第一期投注截止时间",
  game_type: "彩种",
  how_long_to_deposit_account: "提现多久到帐？",
  how_many_games: "星辉有什么彩种？",
  how_to_avoid_account_theft: "如何避免帐户被盗用？",
  how_to_bind_my_account_to_my_email: "要怎么将我的帐号与邮箱绑定？",
  how_to_buy_lottery: "要怎么买彩票？",
  how_to_change_the_password: "要怎么更改密码？",
  how_to_do_when_not_receive_certification_letter: "手机／信箱 收不到 验证码／认证信 怎么办？",
  how_to_do_when_registration_typo: "注册时姓名、银行卡打错了怎么办？",
  how_to_exchange_when_won_lottery: "我买的彩票中奖了，该怎么兑奖？",
  how_to_get_back_my_password: "我忘记登录密码，要怎么找回？",
  how_to_open_an_account_to_the_next_level: "我是代理我要怎么开帐号给下级？",
  how_to_register_account: "该怎么注册星辉帐号？",
  how_to_use_online_bank_recharge: "网银充值怎么用？",
  how_to_use_third_party_payment_channels_recharge: "我要怎么用虚拟货币渠道充值？",
  how_to_use_trace_number: "要怎么使用追号？",
  how_to_withdraw_money: "如何提现？",
  is_betting_safe: "在星辉下注安全吗？",
  is_it_safe_to_buy_the_lottery: "在星辉购彩安全吗？",
  is_money_safe: "在星辉内的资金安全吗？",
  what_are_the_betting_mode: "平台里有哪些下注模式？",
  what_are_the_conditions_for_withdraw_cash: "提现需要什么条件？",
  what_are_the_trace_number_mode: "追号模式有哪些？",
  what_is_limit_red: "系统提示超过限红，什么是限红？",
  what_is_stream: "流水是什么？",
  what_is_stream_limit: "流水限制是什么？",
  what_is_the_amount_of_limit: "星辉限红是多少？",
  what_is_trace_number: "什么是追号？",
  what_should_i_do_if_i_forget_my_password: "我忘记密码了该怎么办？",
  what_should_i_do_with_betting_slip_when_no_lottery: "如果官方彩票没开奖，我的投注单怎么办？",
  what_way_can_recharge: "如何充值？",
  where_can_i_change_my_profile: "哪里可以更改我的个人资料？",
  where_can_i_check_the_teams_information: "哪里可以看团队的概况、资讯？",
  where_can_i_check_the_transaction_record: "哪里可以看过去的交易纪录？",
  why_the_platform_will_have_a_bet_limit: "为什么平台会有下注上限？",
  will_my_account_be_stolen: "我的帐号会不会被盗用？",
  will_the_data_be_leaked: "我的资料会不会被泄漏出去？",
}
