export default {
  title_registration_problem: 'V/đ đăng ký',
  title_recharge_problem: 'Vấn đề nạp',
  title_game_problem: 'Vấn đề game',
  title_betting_problem: 'Vấn đề cược',
  title_lottery_introduction: 'Về xổ số',
  title_security_problem: 'Bảo mật',
  title_user_problem: 'Khu hội viên',
  title_agent_problem: 'Khu đại lý',
  title_withdraw_problem: 'Vấn đề rút',
  title_crypto_problem: 'Tiền ảo',
  title_deposit_guide: 'Hướng dẫn',
  cskh: 'CSKH',
  goback: 'Trung tâm hỗ trợ',
  more: 'Xem thêm',
  period: 'Kỳ {number}',
  period_per_day: 'Mỗi ngày {number} kỳ',
  period_per_week: 'Mỗi tuần {number} kỳ',
  second: '{number} giây',
  bet_anytime: 'Đặt cược 24/7',
  betting_endtime: 'Hạn chót đặt cược',
  last_period_betting_endtime: 'Hạn chót đặt cược kỳ cuối',
  lottery_time: 'Thời gian mở thưởng',
  minute: '{number} phút',
  period_interval: 'Số kỳ liên tục',
  total_period: 'Tổng số kỳ',
  can_i_cancel_the_bet_order: 'Chưa mở thưởng, có thể hủy đơn không?',
  can_i_cancel_the_bet_order_answer:
    'Người chơi chủ động hủy đơn: Lệnh hủy vé cược của bất kỳ trò chơi nào phải được thực hiện trước khi ngừng đặt cược. Nếu gặp phải thời gian kết thúc đặt cược, thì không thể thực hiện hủy vé cược.{br}Trường hợp 1: cược dạng thông thường, nhấp vào "Lịch sử cược" để hủy vé cược.{br}Trường hợp 2: cược dạng đuổi số: ① Nhấp vào "Lịch sử đuổi số" để chấm dứt đuổi số, nhưng vé cược đã được tạo không nằm trong thao tác này, vé cược đã được tạo vào mục "lịch sử cược"; ②Nhấp vào "lịch sử cược" để hủy vé cược, bao gồm số kỳ chưa kết thúc của quá trình đuổi số (Hủy tất cả kỳ đuổi số chưa kết thúc của vé cược). [Hủy đơn đuổi số không được hủy đơn lẻ 1 số kỳ bất kỳ]',
  can_protect_the_safety_of_the_platform:
    'VT999 có thể đảm bảo tính bảo mật của tiền gửi và rút tiền trên trang không?',
  can_protect_the_safety_of_the_platform_answer:
    'Trang web này cung cấp nhiều phương thức nạp tiền, quản lý mạng trực tiếp và kênh thanh toán của bên thứ ba với nhiều ngân hàng, giải quyết hoàn toàn các yếu tố không an toàn của thanh toán qua chuyển khoản ngân hàng, cung cấp cho người dùng kênh thanh toán tiền an toàn, ổn định và nhanh chóng và bảo vệ triệt để người dùng được thanh toán an toàn nhất.',
  first_period_betting_endtime: 'Hạn chót đặt cược kỳ thứ nhất',
  game_problem: 'Vấn đề trò chơi',
  game_type: 'Loại xổ số',
  how_long_to_deposit_account: 'Rút tiền bao lâu tài khoản nhận được tiền',
  how_long_to_deposit_account_answer:
    'VT999 rút tiền 30 phút sau tài khoản nhận được, dịch vụ 24 giờ.',
  how_many_games: 'VT999 có những loại xổ số nào?',
  how_many_games_answer:
    'Bao gồm xổ số mỗi giờ của các nơi, 11 chọn 5, Nhanh 3, Fucai 3D, Top 3, PK10, Lucky 28, Xổ số Hông Kông và xổ số do VT999 đưa ra! Cung cấp cho bạn nhiều trò chơi phong phú.',
  how_to_avoid_account_theft: 'Làm thế nào để tránh bị đánh cắp tài khoản?',
  how_to_avoid_account_theft_answer:
    '1. Không đăng nhập các web không rõ nguồn gốc {br} 2. Không chấp nhận các tệp không rõ nguồn gốc {br} 3. Thường xuyên kiểm tra bảo mật của máy tính {br} 4. Đăng nhập vào trang lần đầu tiên, sửa đổi mật khẩu đăng nhập ban đầu và đặt mật khẩu quỹ tài khoản. Đồng thời mặc định tài khoản ngân hàng bạn dùng để rút tiền {br} 5. Khi nghi ngờ ai đó đã biết thông tin tài khoản của bạn, hãy nhanh chóng sửa đổi mật khẩu đăng nhập ban đầu, đặt lại mật khẩu quỹ tài khoản và khóa tài khoản ngân hàng bạn dùng để rút tiền {br} 6. Giữ kỹ mật khẩu đăng nhập và mật khẩu quỹ của bạn  {br} 7. Không tiết lộ mật khẩu đăng nhập và mật khẩu quỹ của tài khoản cho bất kỳ ai',
  how_to_bind_my_account_to_my_email: 'Làm cách nào để liên kết tài khoản của tôi với e-mail?',
  how_to_bind_my_account_to_my_email_answer:
    'Nhấp vào góc phải màn hình để vào trang chủ cá nhân, trong hồ sơ cá nhân nhấp vào mục “ Hộp thư an toàn ”, bạn có thể tiến hành quy trình mặc định hộp thư.',
  how_to_buy_lottery: 'Làm thế nào để mua xổ số?',
  how_to_buy_lottery_answer:
    'Sau khi nạp tiền thành công, hãy chọn trò chơi tương ứng để đặt cược và nhấp vào "Xác nhận lựa chọn" vào khu đặt cược để xác nhận và nhấp vào "Xác nhận đặt cược" để hoàn tất đặt cược.',
  how_to_change_the_password: 'Làm thế nào để thay đổi mật khẩu?',
  how_to_change_the_password_answer:
    'Nhấp vào góc phải màn hình để vào trang chủ cá nhân, trong hồ sơ cá nhân nhấp vào mục “ Mật khẩu đăng nhập ”, bạn có thể tiến hành sửa đổi mật khẩu đăng nhập.',
  how_to_do_when_not_receive_certification_letter:
    'Điện thoại / hộp thư không nhận được mã xác nhận làm thế nào ?',
  how_to_do_when_not_receive_certification_letter_answer:
    'Vui lòng xác nhận rằng hộp thư / điện thoại di động của bạn được điền chính xác và gửi lại mã xác thực / thư xác thực. Nếu bạn có bất kỳ câu hỏi nào, vui lòng liên hệ trực tiếp với nhân viên CSKH của trang.',
  how_to_do_when_registration_typo:
    'Quá trình đăng ký, nhập sai họ tên, tài khoản ngân hàng thì phải làm sao?',
  how_to_do_when_registration_typo_answer:
    'Để sửa đổi tên người thụ hưởng hoặc thẻ ngân hàng của bạn, vui lòng liên hệ trực tiếp với nhân viên CSKH của trang.',
  how_to_exchange_when_won_lottery: 'Xổ số tôi mua đã trúng thưởng, đổi thưởng bằng cách nào?',
  how_to_exchange_when_won_lottery_answer:
    'Sau khi mở thưởng 30 giây, hệ thống sẽ tự động gửi tất cả tiền thưởng vào tài khoản trang của hội viên. Hội viên có thể vào "Lịch sử đặt cược" trong mục "Trung tâm bảng biểu" để kiểm tra xem vé cược. Nếu hội viên thắng cược trong vòng 5 phút mà không nhận được tiền thưởng, vui lòng liên hệ CSKH và cung cấp vé cược để được hỗ trợ kiểm tra.',
  how_to_get_back_my_password: 'Tôi quên mật khẩu đăng nhập, làm cách nào để lấy lại?',
  how_to_get_back_my_password_answer:
    'Nếu bạn mất mật khẩu, vui lòng bấm vào quên mật khẩu khi đăng nhập, bạn có thể lựa chọn trả lời các câu hỏi bảo mật hoặc liên hệ CSKH để được hỗ trợ.',
  how_to_open_an_account_to_the_next_level:
    'Tôi là đại lý. Làm cách nào để tôi phát triển các cấp dưới?',
  how_to_open_an_account_to_the_next_level_answer:
    'Nhấp vào bên trái màn hình để vào trang chủ cá nhân> trung tâm đại lý> trung tâm mở tài khoản, bạn có thể sử dụng liên kết / mở thủ công một tài khoản cho cấp thấp hơn.',
  how_to_register_account:
    'Nhấp vào biểu tượng “Đại lý” ở góc phải màn hình, bạn có thể chia sẻ QR Code hoặc gửi link liên kết cho người mà bạn muốn họ đăng ký trở thành cấp dưới của bạn.',
  how_to_register_account_answer: 'Làm cách nào để đăng ký tài khoản tại VT999?',
  how_to_use_online_bank_recharge: 'Nạp tiền trực tiếp sử dụng như thế nào?',
  how_to_use_online_bank_recharge_answer:
    'Trong mục nạp tiền, chọn [Nạp tiền thông thường],sau khi chọn ngân hàng và nhập số tiền bạn muốn nạp,lệnh nạp tiền sẽ được hình thành. Vui lòng trong thời hạn chờ tiến hành chuyển khoản nạp. {br} {br} Hướng dẫn quy trình nạp tiền thông thường {br} Bước 1: Trang chủ VT999 > Nạp tiền (đầu trang) {br} Bước 2: Chọn "Nạp tiền thông thường"> Ngân hàng bạn muốn chuyển {br} Bước 3: Nhập số tiền muốn nạp (số tiền có thể chuyển sẽ có giới hạn khác nhau tùy theo kênh) {br} Bước 4: Nhấp vào " Đề xuất nạp tiền " {br} Bước 5: Khởi tạo thành công đơn nạp tiền (nhận thông tin chuyển tiền) {br} Lưu ý 1: Vui lòng hoàn tất việc nạp tiền trong vòng 30 phút sau khi bạn khởi tạo thành công đơn nạp tiền. {br} Lưu ý 2: Để tăng tốc thời gian xử lý khoản, vui lòng đảm bảo điền đầy đủ nội dung chuyển tiền. {br} Lưu ý 3: Vui lòng lựa chọn thông tin tài khoản gửi tiền hiện tại theo thời gian mỗi khi bạn nạp tiền, để đảm bảo rằng tài khoản bạn gửi tiền vào là tài khoản có hiệu lực và tránh những tổn thất không cần thiết.',
  how_to_use_third_party_payment_channels_recharge:
    'Làm cách nào để tôi nạp tiền với kênh thanh toán của bên thứ ba?',
  how_to_use_third_party_payment_channels_recharge_answer:
    'Chọn nạp tiền [Bên thứ ba] trong kênh nạp tiền và chọn kênh nạp tiền, nhập thông tin liên quan và làm theo các bước thanh toán của bên thứ ba để hoàn tất việc nạp tiền.{br}Bước 1: Trang chủ VT999 > Nạp tiền (trang trên).{br}Bước 2: Chọn "Nạp tiền của bên thứ ba" > Cách nạp tiền (Momo pay, Viettel pay, Zalo pay,v.v).{br}Bước 3: Nhập số tiền cần nạp (số tiền có thể bị giới hạn khác nhau tùy theo kênh nạp vào).{br}Bước 4: Nhấp vào "Đề xuất nạp tiền".{br}Bước 5: Nhận thông tin nạp tiền, nhấp vào "Chuyển đến nạp tiền".{br}Lưu ý 1: Vui lòng hoàn tất việc nạp tiền trong vòng 30 phút sau khi gửi đề xuất nạp tiền thành công.{br}{br}Cách nạp tiền bằng USDT :{br}Bước 1 : Trang chủ -> nạp tiền (Trên trang chủ){br}Bước 2 : Chọn (Thanh toán bằng tiền ảo){br}Bước 3 : Chọn USDT và loại giao dịch{br}Bước 4 : Nhập số tiền cần nạp{br}Bước 5 : Nhấp vào (Thanh toán ngay){br}Bước 6 : Đề xuất nạp tiền thành công (nhận thông tin chuyển khoản){br}Bước 7 : Vào ví tiền ảo để hoàn thành bước chuyển khoản{br}Bước 8 : Thanh toán thành công{br}Nhắc nhở 1 : Sau khi đã làm lệnh , vui lòng trong hoàn thành chuyển khoản trong vòng 30 phút.{br}Nhắc nhở 2 : Để đảm bảo rằng tài khoản chuyển tiền của bạn là tài khoản hợp lệ và tránh những tổn thất không đáng có , vui lòng cập nhật thông tin mới nhất trước khi tiến hành nạp tiền.',
  how_to_use_trace_number: 'Làm thế nào để sử dụng chức năng cược đuổi số?',
  how_to_use_trace_number_answer:
    'Chọn loại xổ số có thể cược với chức năng đuổi số, chọn " Đuổi số nhân đôi" hoặc " Đuổi số thông minh" và nhập điều kiện cài đặt, nhấp vào "Tạo phương án đuổi số", sau đó "Xác nhận đặt cược", cuối cùng nhấp vào "Đề xuất đơn" => Bạn đã thành công cài đặt chức năng đuổi số cho vé cược.',
  how_to_withdraw_money: 'Làm thế nào để rút tiền?',
  how_to_withdraw_money_answer:
    'Trước tiên người dùng phải cài mặc định thẻ ngân hàng trước khi rút tiền. Khi tổng cược hợp lệ đã đạt, bạn có thể rút tiền từ tài khoản của mình.{br}{br}Lưu ý: {br}(1) Tên ngân hàng và số tài khoản ngân hàng cần điền đảm bảo điền đúng, thông tin chủ tài khoản ngân hàng phải trùng khớp với họ tên thật của người thụ hưởng, việc đặt lại mật khẩu quỹ, v.v. người dùng đều không thể tự sửa đổi. {br}(2) Thông tin thẻ ngân hàng chỉ được sử dụng để rút tiền, bạn có thể để trống khi đăng ký, nhưng vui lòng điền thông tin chi tiết trước khi rút tiền. Tên chủ tài khoản thẻ ngân hàng phải trùng khớp ới họ tên thật đã cài đặt, nếu không việc rút tiền sẽ không thành công.',
  is_betting_safe: 'Đặt cược tại VT999 có an toàn không?',
  is_betting_safe_answer:
    'VT999 sử dụng công nghệ mã hóa SSL128-bit, cùng hệ thống quản lý bảo mật nghiêm ngặt, đảm bảo sự an toàn thông tin của khách hàng được đầy đủ và bảo mật nhất. Tất cả lịch sử cá cược của khách hàng đều được lưu trữ trong điều kiện hệ thống bảo mật cực kỳ nghiêm ngặt, và trên hết chúng tôi sẽ không bao giờ tiết lộ thông tin của khách hàng cho bất kỳ bên thứ ba nào. Chúng tôi sẽ thực hiện các biện pháp bảo mật đồng thời chống gian lận nghiêm ngặt cho mọi giao dịch để đảm bảo tính xác thực và hợp pháp của tài khoản người chơi, cùng với đó đảm bảo tính bảo mật của các giao dịch thanh toán cho người chơi. Nền tảng VT999 sử dụng đội ngũ kỹ thuật bảo mật đẳng cấp thế giới, đứng đầu trong ngành kỹ thuật bảo mật, quý khách hàng hoàn toàn có thể yên tâm và đặt cược tại VT999!',
  is_it_safe_to_buy_the_lottery: 'Mua xổ số tại VT999 có an toàn không?',
  is_it_safe_to_buy_the_lottery_answer:
    'Trang VT999 mở thưởng theo số Xổ số chính thức của Cơ quan Quản lý Xổ số Quốc gia. Nếu hội viên trúng thưởng, hệ thống sẽ tự động phân phối giải thưởng cho tài khoản trên trang và hội viên có thể đăng ký rút tiền trong tài khoản.',
  is_money_safe: 'Các khoản tiền trong VT999 có an toàn không?',
  is_money_safe_answer:
    'Mỗi lần thực hiện rút tiền, phải nhập mật khẩu quỹ trước khi gửi đề xuất rút tiền. Tên tài khoản thẻ ngân hàng được mặc định bởi người dùng phải khớp, nếu không việc rút tiền sẽ không thành công. Sau khi số tài khoản ngân hàng cài mặc định, người dùng không được phép tự ý sửa đổi. Nếu bạn muốn sửa đổi, bạn cần liên hệ với nhân viên CSKH để kiểm tra thông tin chi tiết trước khi bạn có thể sửa đổi.',
  what_are_the_betting_mode: 'Hệ thống có những loại hình thức cá cược nào ?',
  what_are_the_betting_mode_answer: 'Hệ thống có thể lựa chọn hình thức cá cược ₫ , hào , xu',
  what_are_the_conditions_for_withdraw_cash: 'Rút tiền cần những điều kiện gì?',
  what_are_the_conditions_for_withdraw_cash_answer:
    'Tài khoản phải cài mật khẩu quỹ và thẻ ngân hàng trước khi rút tiền. Nhấp vào nút rút tiền ở đầu trang và nhập thông tin liên quan để rút. {br} Rút tiền miễn là tổng số tiền đặt cược hiệu quả đạt hơn 100% số tiền nạp!',
  what_are_the_trace_number_mode: 'Hình thức đuổi số là gì ?',
  what_are_the_trace_number_mode_answer:
    'đuổi số kép: cần đặt "khoảng", "gấp đôi", nhấp vào "tạo ngay lập tức", hệ thống sẽ ngay lập tức tạo cược phù hợp với cài đặt.{br} Số đuổi thông minh: tức lợi nhuận tối thiểu được đảm bảo trong kế hoạch số đuổi, có ba cách, đó là "tỷ lệ lợi nhuận tối thiểu", "số tiền lãi tối thiểu", "tổng số tiền của toàn bộ quá trình", đặt "Tỉ lệ kép ban đầu", "số kỳ đuổi số" Sau khi nhấp vào "Tạo số" , hệ thống sẽ tính toán bội số cược cho bạn.Quy tắc đuổi số{br} (1) hội viên có thể đuổi số bất kỳ lúc nào , trong quá trình đuổi số , không ảnh hưởng đến các cá cược khác .{br} (2) Bắt đầu đơn hàng đuổi số, số tiền cần thiết cho kế hoạch đuổi số sẽ được khấu trừ một lần. Khi số dư tài khoản không đủ, nhiệm vụ sẽ không được thực hiện.{br} (3) Khi kế hoạc đuổi số đáp ứng các điều kiện ngưng đuổi số do hội viên đặt ra, hệ thống sẽ tự động chấm dứt đuổi số. Nếu còn các kỳ chưa thực hiện đuổi số, hệ thống sẽ tự động hoàn trả số tiền còn lại vào tài khoản của hội viên .{br} (4) VT999 các loại xổ số có hỗ trợ đuổi số là xổ số mỗi giờ (không bao gồm số cào ), 11 chọn 5 , nhanh 3 , tối đa được đuổi số 60 kỳ.',
  what_is_limit_red: 'Hệ thống nhắc nhở vượt quá giới hạn đỏ, giới hạn đỏ là gì?',
  what_is_limit_red_answer:
    'Mỗi loại xổ số của trang VT999 đều có tỉ lệ trả thưởng tối đa tương ứng, tức giới hạn đỏ (nếu tiền thưởng thắng cược vượt quá giới hạn đỏ tối đa của loại xổ số tương ứng, hệ thống chỉ trả tiền thưởng tối đa của giới hạn đỏ đó)',
  what_is_stream: 'Tổng cược là gì?',
  what_is_stream_answer: 'Tổng cược là tổng tiền đặt cược của bạn trên trang.',
  what_is_stream_limit: 'Hạn chế tổng cược là gì?',
  what_is_stream_limit_answer:
    'Sau khi nạp tiền, tổng số tiền đặt cược hợp lệ phải đáp ứng các yêu cầu của hệ thống để rút tiền, nếu không thì không thể thực hiện rút tiền. Tổng số tiền cược hợp lệ cần đạt 100% số tiền nạp để đăng ký rút tiền. Nếu hội viên A nạp lại 1,000 ₫ và đặt cược 1,000 ₫ vào xổ số, tức đã đủ tổng cược hợp lệ. Một số hoạt động khác có yêu cầu riêng về tỉ lệ vòng cược, vui lòng đọc nội dung hoạt động để biết chi tiết.{br}Sau khi nạp tiền, nếu vòng cược hợp lệ không đáp ứng yêu cầu hệ thống, hội viên vẫn muốn rút / chuyển khoản, VT999 bắt buộc phải tính một số phí hành chính.',
  what_is_the_amount_of_limit: 'Giới hạn đỏ của VT999 là gì?',
  what_is_the_amount_of_limit_answer:
    'Xổ số mỗi giờ, 11 chọn 5, Nhanh 3, PK10, xổ số Hồng Kông, xổ sổ tần suất thấp, Lucky 28, có giới hạn đỏ trúng thưởng cao nhất đạt 600 triệu; Sicbo có giới hạn đỏ trúng thưởng cao nhất đạt 200 triệu',
  what_is_trace_number: 'Đuổi số là gì?',
  what_is_trace_number_answer:
    'Đuổi số tức là trong liên tiếp nhiều kỳ xổ số mua cùng 1 con số. Hội viên sau khi gửi nhiệm vụ trên trang, hệ thống dựa theo yêu cầu nhiệm vụ liên tiếp nhiều kỳ tự động đặt cược.{br} Sicbo, PK10, Đua xe F1, Đua xe F2, Đua xe F3, Xổ số Hồng Kông, Xổ số Hồng Kông siêu tốc, Lucky 28 và xổ số tần suất thấp không hỗ trợ đuổi số.',
  what_should_i_do_if_i_forget_my_password: 'Tôi nên làm gì nếu tôi quên mật khẩu?',
  what_should_i_do_if_i_forget_my_password_answer:
    'Vui lòng truy cập trang đăng nhập và nhấp vào Quên mật khẩu và cung cấp thông tin cá nhân theo quy trình, để lấy lại tài khoản.{br}Nếu bạn quên mật khẩu đăng nhập trang của mình, bạn có thể truy xuất nó thông qua câu hỏi bảo mật (Trước tiên bạn phải đặt câu hỏi bảo mật của mình trên trang).',
  what_should_i_do_with_betting_slip_when_no_lottery:
    'Nếu sảnh xổ số không mở thưởng, thì vé cược của tôi giải quyết thế nào?',
  what_should_i_do_with_betting_slip_when_no_lottery_answer:
    'Nếu trong giai đoạn hiện tại không có xổ số mở thưởng, trang sẽ hủy đơn đặt cược (miễn phí) , và số tiền đặt cược cũng sẽ được hoàn trả vào tài khoản cho hội viên.',
  what_way_can_recharge: 'Làm thế nào để nạp tiền?',
  what_way_can_recharge_answer:
    'VT999 cung cấp kênh nạp tiền thông thường (nạp tiền nhanh qua Internet Banking) và các kênh nạp tiền nhanh của bên thứ ba.',
  where_can_i_change_my_profile: 'Tôi có thể thay đổi thông tin cá nhân của mình ở đâu?',
  where_can_i_change_my_profile_answer:
    'Nhấp vào góc phải màn hình để vào trang chủ cá nhân, trong hồ sơ cá nhân, bạn có thể thay đổi thông tin mà bạn muốn sửa đổi.',
  where_can_i_check_the_teams_information: 'Tôi có thể xem thông tin và hồ sơ của đội ở đâu?',
  where_can_i_check_the_teams_information_answer:
    'Nhấp vào biểu tượng “Đại lý” ở góc phải màn hình và chọn mục Tổng quan đội hoặc bạn cũng có thể vào “Trang chủ cá nhân”, sau đó vào Trung tâm đại lý, bạn có thể xem tổng quan về đội, lợi nhuận đội, lịch sử hoa hồng và các hạng mục thuộc quản lý hội viên,v.v',
  where_can_i_check_the_transaction_record: 'Tôi có thể xem lịch sử giao dịch trong quá khứ ở đâu?',
  where_can_i_check_the_transaction_record_answer:
    'Nhấp vào góc phải màn hình để vào trang chủ cá nhân, trong Trung tâm bảng biểu, bạn có thể xem các hạng mục lịch sử giao dịch khác nhau theo nhu cầu cá nhân.\nTrong trung tâm bảng biểu, bạn có thể tìm kiếm lịch sử cược, nạp - rút, chuyển điểm, thay đổi hoạt động tài khoản, v.v',
  why_the_platform_will_have_a_bet_limit: 'Tại sao nền tảng có giới hạn đặt cược?',
  why_the_platform_will_have_a_bet_limit_answer:
    'Để ngăn một số người chơi trục tổng cược, hệ thống có giới hạn về số vé cược được phép đặt cược cho từng loại xổ số (nếu vượt quá giới hạn, tổng cược sẽ không được tính).',
  will_my_account_be_stolen: 'Tài khoản của tôi sẽ bị đánh cắp hay không?',
  will_my_account_be_stolen_answer:
    'VT999 áp dụng 2 bước xác minh cho mật khẩu đăng nhập và mật khẩu quỹ, một khi người dùng mặc định thẻ ngân hàng thì không được phép tự sửa đổi để đảm bảo tính an toàn cho tiền trong tài khoản.',
  will_the_data_be_leaked: 'Thông tin của tôi sẽ bị tiết lộ ra ngoài?',
  will_the_data_be_leaked_answer:
    'VT999 sẽ xử lý chính xác thông tin cá nhân do hội viên cung cấp. Mục đích của trang khi sử dụng thông tin cá nhân của hội viên và thông tin khác nhằm xử lý các cược của hội viên, quản lý tài khoản của hội viên và cung cấp dịch vụ cho hội viên. VT999 là chủ sở hữu duy nhất các thông tin cá nhân của hội viên và VT999 sẽ không cung cấp, từ bỏ hoặc bán thông tin cho bất kỳ bên thứ ba nào. Tất cả dữ liệu cá nhân sẽ bị hủy khi không cần bảo lưu.',
  columns: {
    game_type: 'Loại xổ số',
    lottery_time: 'Thời gian mở thưởng',
    period_interval: 'Số kỳ liên tục',
    total_period: 'Tổng số kỳ',
    first_period_betting_endtime: 'Hạn chót đặt cược kỳ thứ nhất',
    last_period_betting_endtime: 'Hạn chót đặt cược kỳ cuối',
    betting_endtime: 'Hạn chót đặt cược',
  },
  lottery_code: {
    act11x5: 'Australia 11 chọn 5',
    actjc: 'Australia xổ số mỗi giờ',
    actk3: 'Australia nhanh 3',
    actracing: 'Đua xe Australia',
    ca11x5: 'Canada 11 chọn 5',
    cajc: 'Canada mỗi giờ',
    cak3: 'Canada nhanh 3',
    caracing: 'Canada đua xe',
    ah11x5: 'Anhui 11 chọn 5',
    bjk3: 'Beijing nhanh 3',
    btcffc: 'Bitcoin xổ số mỗi phút',
    cqc: '12 con giáp vui vẻ',
    dicecup: 'Xúc xắc',
    f1racing: 'Đua xe F1',
    f2racing: 'Đua xe F2',
    f3racing: 'Đua xe F3',
    fast3d: 'Siêu tốc 3D',
    fc3d: 'Fucai 3D',
    fmark6: 'Xổ số Hồng Kông siêu tốc',
    gd11x5: 'Guangdong 11 chọn 5',
    ggc: 'Cào trúng thưởng',
    gxk3: 'Guangxi nhanh 3',
    hquick3: 'Happy nhanh 3',
    hubk3: 'Hubei nhanh 3',
    jnd30s: 'Canada nhanh 3',
    js11x5: 'Jiangsu 11 chọn 5',
    jsk3: 'Jiangsu nhanh 3',
    jx11x5: 'Jiangxi 11 chọn 5',
    lottery1: '1 phút thưởng',
    lottery2: '2 phút thưởng',
    lottery5: '5 phút thưởng',
    lucky28: 'Lucky 28',
    mark6: 'Xổ số Hồng Kông',
    pk10: 'PK10',
    pl3: 'Top 3',
    quick3: '1 phút nhanh 3',
    sd11x5: 'Sandong 11 chọn 5',
    sh11x5: 'Shanghai 11 chọn 5',
    tg60: 'Thailand 60s',
    tjc: 'Tianjin xổ số mỗi giờ',
    tw11x5: 'Taiwan 11 chọn 5',
    twjc: 'Taiwan xổ số mỗi giờ',
    twk3: 'Taiwan nhanh 3',
    twracing: 'Taiwan đua xe',
    txffc: 'Tengxun xổ số mỗi phút',
    wxffc: 'Wechat xổ số mỗi phút',
    xjc: 'Xinjiang xổ số mỗi giờ',
    xyft: 'Tàu bay may mắn',
  },
  zodiac: {
    0: 'Tý',
    1: 'Dần',
    2: 'Mão',
    3: 'Thìn',
    4: 'Tỵ',
    5: 'Ngọ',
    6: 'Mùi',
    7: 'Thân',
    8: 'Dậu',
    9: 'Hợi',
  },
  intro: {
    dicecup:
      'Xúc xắc có nguồn gốc từ trò chơi xúc xắc Trung Quốc cổ đại, đã càn quét thế giới. Xúc xắc sử dụng kết hợp ba con xúc xắc như một bộ số xổ số, sử dụng các kịch bản cá cược trong sòng bạc, người chơi có thể chọn nhiều tỷ lệ cược khác nhau để chơi cùng một lúc, bao gồm: tài, xỉu, chẵn, lẻ, đôi, bão và tổng điểm, xúc xắc đôi, xúc xắc đơn. Có thể xem mô tả cách chơi bằng cách nhấp vào biểu tượng "?" trong khung cược. Khi đặt cược tài, xỉu, lẻ, chẵn, nếu kết quả là bão, thì không trúng thưởng.',
    ex5:
      '11chọn 5 là chọn 5 số trong tổng 11 số, từ số 01~11. Mỗi kỳ mở thưởng 5 con số, dự đoán tất cả hoặc một phần của 5 số trúng.',
    fc3d:
      'Trò chơi Xổ số Trung Quốc Fucai 3D (gọi tắt là 3D) là trò xổ số với bộ số tự nhiên gồm 3 chữ số làm số cá cược. Người đặt cược chọn một bộ số có 3 chữ số từ các số 000-999 để đặt cược.',
    k3:
      'Nhanh 3 là đặt cược dựa vào tổ hợp gồm 3 chữ số, mỗi số đặt cược là một trong bất kỳ số tự nhiên từ 1 ~ 6. Một vé cược sẽ phải gồm 3 số kết hợp thành tổ hợp.',
    lucky28:
      'Kết quả xổ số sẽ dựa theo kết quả xổ số Bắc Kinh Happy 8 tại trang web chính thức (Fucai Bắc Kinh) với mỗi kỳ xổ số gồm 20 chữ số. Lucky 28 sắp xếp 20 số mở thưởng theo thứ tự từ nhỏ đến lớn. Số cuối cùng của tổng điểm các số từ vị trí 1-6 sẽ là số đầu tiên mở thưởng cho Lucky 28. Số cuối cùng của tổng điểm các số từ vị trí 7-12 sẽ là số thứ hai mở thưởng cho Lucky 28. Số cuối cùng của tổng điểm các số từ vị trí 13-18 sẽ là số thứ ba mở thưởng cho Lucky 28. Tổng điểm 3 số mở thưởng đó (nghĩa là số đặc biệt) sẽ là kết quả cuối cùng của xổ số Lucky 28 .',
    mark6:
      'Xổ số Hồng Kông là từ số 1 đến 49, chọn ra bất kỳ 6 số đặt được, được tổ chức bởi công ty "Hong Kong J Racer Club Lotter Co., Ltd."',
    mark6_list: {
      text1:
        '1. Con số đặc biệt: con số mở thưởng cuối cùng trong kỳ xổ số đó, được gọi là số đặc biệt.',
      text10:
        '10.6 con giáp: chọn 6 con giáp kết hợp thành tổ hợp của 1 vé cược, thể loại đặt cược có thể lựa chọn tổ hợp sẽ trúng hoặc không trúng. Số đặc biệt được mở thưởng trong kỳ đó phù hợp với con giáp được lựa chọn và cả thể loại cá cược của vé cược, tức trúng thưởng, ngoài ra số 49 tức không trúng thưởng.',
      text11:
        '11.Con giáp liên tiếp: con giáp tương ứng với số đặc biệt được mở (mỗi con giáp tương ứng nhiều số khác nhau), bất kể con giáp tương ứng với số được mở thưởng xuất hiện 1 hoặc nhiều hơn 1 cũng chỉ được trả thưởng 1 lần. Mỗi con giáp đều có tỉ lệ cược riêng biệt, tổng tỉ lệ cược của tổ hợp đặt cược được lấy từ tỉ lệ cược tối thiểu trong những con giáp đó.',
      text12:
        '12. Số đuôi liên tiếp: số tương ứng của đuôi số giống như mục số đuôi liên tiếp (mỗi đuôi số tương ứng nhiều số khác nhau), bất kể cùng 1 số đuôi liên tiếp được mở thưởng 1 lần hay nhiều lần, đều chỉ tính trúng thưởng 1 lần. Tỉ lệ cược của mỗi số đuôi liên tiếp đều khác nhau, tổng tỉ lệ cược của tổ hợp đặt cược được lấy từ tỉ lệ cược tối thiểu trong những số đuôi liên tiếp đó.',
      text13:
        '13. 1-6 Long Hổ: so sánh số của các số chính từ 1-6, số trước lớn hơn số sau là Long, và ngược lại, số sau lớn hơn số trước là Hổ.',
      text14:
        '14. Tài xỉu: kết quả xổ số từ 25~48 tức tài,  kết quả xổ số từ 0~24 tức xỉu. Kết quả xổ số là 49 đều không trúng thưởng.',
      text15:
        '15. Chẵn lẻ: số lẻ như 1, 3, 5; số chẵn như 2, 4, 6; kết quả xổ số 49 là không trúng thưởng.',
      text16: '16. Gia cầm: Sửu, Ngọ, Mùi, Dậu, Tuất, Hợi. Thú: Tý, Dần, Mão, Thìn, Tỵ, Thân.',
      text17:
        '17. Tài xỉu đuôi: đuôi xỉu tức số đuôi của số được mở thưởng là số từ 0~4, đuôi tài tức số đuôi của số được mở thưởng là số từ 5~9. Số được mở thưởng là 49 tức không tính trúng thưởng',
      text18:
        '18. Tổng số tài xỉu: tất cả 7 số mở thưởng cộng lại bằng hoặc lớn hơn 175 tức tổng tài. Tất cả 7 số mở thưởng cộng lại bằng hoặc nhỏ hơn 174 tức tổng xỉu.',
      text19:
        '19. Tổng số chẵn lẻ: toàn bộ 7 số cộng lại là số lẻ tức tổng lẻ, toàn bộ 7 số cộng lại là số chẵn tức tổng chẵn.',
      text2:
        '2. Số chính: 6 số đầu tiên do Công ty Xổ số Hồng Kông phát hành trong kỳ số hiện tại được gọi là số chính. Số xuất hiện lần đầu tiên được gọi là số chính 1, tiếp theo là số chính 2, số chính 3… số chính 6, không theo thứ tự số lớn nhỏ.',
      text20:
        '20. Hợp số chẵn lẻ: kết quả xổ số gồm 2 chữ số cộng lại ra lẻ tức hợp lẻ, ví dụ: 01, 0+1 =1; kết quả xổ số gồm 2 chữ số cộng lại là chẵn tức hợp chẵn, ví dụ: 15, 1+5=6. Kết quả xổ số là 49 là không trúng thưởng,',
      text21: 'Các số cầu đỏ: 01, 02, 07, 08, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46.',
      text22: 'Các số cầu lam: 03, 04, 09, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48.',
      text23: 'Các số cầu lục:  05, 06, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49.',
      text24:
        'Con giáp: là tất cả 7 con số mở thưởng trong đó bao gồm 1 hoặc nhiều số tương ứng với con giáp mà nó tượng trưng, đều chỉ được phát thưởng 1 lần, tức những số cùng 1 con giáp xuất hiện 1 hoặc nhiều lần thì đều chỉ được phát thưởng 1 lần.',
      text25: 'Tý bao gồm: 2, 14, 26, 38.',
      text26: 'Sửu bao gồm: 1, 13, 25, 37, 49.',
      text27: 'Dần bao gồm: 12, 24, 36, 48',
      text28: 'Mão bao gồm: 11, 23, 35, 47',
      text29: 'Thìn bao gồm: 10, 22, 34, 46',
      text3:
        '3. Số chính 1-6: số chính 1, số chính 2, số chính 3, số chính 4, số chính 5, số chính 6. Tài xỉu chẵn lẻ và số đặc biệt đều quy tắc giống nhau. Ví dụ: số chính 1 là 25, tức là tài, là lẻ; số chính 2 là 4, tức là xỉu, là chẵn. Nếu là số 49 tức không trúng thưởng.',
      text30: 'Tỵ bao gồm: 9, 21, 33, 45',
      text31: 'Ngọ bao gồm: 8, 20, 32, 44',
      text32: 'Mùi bao gồm: 7, 19, 31, 43',
      text33: 'Thân bao gồm: 6, 18, 30, 42',
      text34: 'Dậu bao gồm: 5, 17, 29, 41',
      text35: 'Tuất bao gồm: 4, 16, 28, 40',
      text36: 'Hợi bao gồm: 3, 15, 27, 39',
      text37:
        'Số đuôi : trong 7 số mở thưởng xuất hiện 1 hoặc nhiều hơn có số đuôi bạn đặt cược, bất kể trúng 1 số hoặc nhiều hơn, đều chỉ tính thưởng 1 lần',
      text38: 'Số đuôi 0 bao gồm: 10, 20, 30, 40',
      text39: 'Số đuôi 1 bao gồm: 01, 11, 21, 31, 41',
      text4:
        '4. Số chính đặc biệt: tức đúng số chính 1, đúng số chính 2, đúng số chính 3, đúng số chính 4, đúng số chính 5, đúng số chính 6. Tức khi đặt cược, số chính đặc biệt trùng khớp với thứ tự trong dãy số mở thưởng, là trúng thưởng. Nếu kết quả dãy số mở thưởng có số chính 1 là 49, trong khi bạn đặt cược số chính 1 là 49, được tính là trúng thưởng, ngoài ra những số còn lại tức không trúng thưởng.',
      text40: 'Số đuôi 2 bao gồm: 02, 12, 22, 32, 42',
      text41: 'Số đuôi 3 bao gồm: 03, 13, 23, 33, 43',
      text42: 'Số đuôi 4 bao gồm: 04, 14, 24, 34, 44',
      text43: 'Số đuôi 5 bao gồm: 05, 15, 25, 35, 45',
      text44: 'Số đuôi 6 bao gồm: 06, 16, 26, 36, 46',
      text45: 'Số đuôi 7 bao gồm: 07, 17, 27, 37, 47',
      text46: 'Số đuôi 8 bao gồm: 08, 18, 28, 38, 48',
      text47: 'Số đuôi 9 bao gồm: 09, 19, 29, 39, 49',
      text5:
        '5. Số liên tiếp: tỉ lệ cược của mỗi số đều khác nhau. Tổng tỉ lệ cược của tổ hợp số được lấy từ tỉ lệ cược tối thiểu trong những số đó.',
      text6:
        '6. Nửa cầu: dựa theo màu cầu và đôi đơn tài xỉu của số đặc biệt tạo thành 1 tổ hợp cá cược, tức là trúng thưởng. Nếu kết quả mở thưởng số đặc biệt là 49 thì những số còn lại tức không trúng thưởng.',
      text7:
        '7. Không trúng thưởng: tỉ lệ cược của mỗi số đều khác nhau. Tổng tỉ lệ cược của tổ hợp số được lấy từ tỉ lệ cược tối thiểu trong những số đó.',
      text8:
        '8. 1 con giáp: chỉ kết quả 7 số mở thưởng, trong đó có 1 số hoặc nhiều hơn trùng khớp với số con giáp đã đặt cược, tức thắng cược, bất kể số con giáp xuất hiện 1 hoặc nhiều hơn 1 số cũng chỉ thể trả thưởng 1 lần.',
      text9:
        '9. Con giáp đặc biệt: con giáp tương ứng với số đặc biệt được mở thưởng trong kỳ trùng khớp với con giáp được đặt cược, tức trúng thưởng, ngoài ra tức không trúng thưởng, số 49 không được tính là hòa. Nếu đặt cược con giáp “Thìn” và kết quả mở thưởng là bất kỳ 1 số trong những số 8, 20, 32, 44, tức trúng thưởng, ngoài ra tức không trúng thưởng (Kết quả sử dụng trong kỳ 19015)',
    },
    pk10:
      'Thể loại xổ số số tần số cao dựa theo chủ đề đua xe do Trung tâm quản lý Xổ số phúc lợi Quốc gia phát hành. Mỗi kỳ mở thưởng PK10 dựa vào kết quả cuộc thi của 10 chiếc xe đua, số mở thưởng chính là số xe và thứ hạng tương ứng của từng xe.',
    xyft:
      'Thể loại xổ số số tần số cao dựa theo chủ đề đua xe do Trung tâm quản lý Xổ số phúc lợi Quốc gia phát hành. Mỗi kỳ mở thưởng PK10 dựa vào kết quả cuộc thi của 10 chiếc xe đua, số mở thưởng chính là số xe và thứ hạng tương ứng của từng xe.',
    pl3:
      'Top 3 được phát hành bởi Trung tâm quản lý Xổ số thể thao của Tổng cục thể thao Quốc gia. Người chơi cần chọn số cá cược gồm 3 chữ số từ 000 ~ 999 để đặt cược.',
    ssc:
      '12 con giáp vui vẻ và Xổ số mỗi giờ thuộc xổ số tần số cao. Cá cược được phân chia thành các số hàng vạn, hàng nghìn, hàng trăm, hàng chục và một số hàng đơn vị, phạm vi các số là từ 0~9. Mỗi kỳ từ các hàng sẽ mở ra 1 chữ số và kết hợp lại thành một tổ hợp số trúng thưởng.',
  },
}
