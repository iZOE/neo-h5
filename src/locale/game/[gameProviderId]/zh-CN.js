export default {
  all: '全部',
  transfer: '转账',
  transfer_now: '立即转账',
  transfer_in: '转入',
  transfer_out: '转出',
  center_wallet: '中心錢包',
  form: {
    input: {
      money: {
        label: '转账金额',
        placeholder: '请输入充值金额',
      },
    },
    validate: {
      message: {
        required: '必填',
        balance_insufficient: '帐户余额不足请重新输入',
      },
    },
  },
  dialog: {
    success: '转账成功',
  },
}
