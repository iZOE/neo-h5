export default {
  all: '全部',
  hot: '熱門',
  plz_enter_search_name: '请输入欲查询的游戏名称',
  not_open: '尚未开放，敬请期待',
  no_result: '无相符资料，请重新查询',
}
