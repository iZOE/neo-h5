export default {
  all: 'Toàn bộ',
  hot: 'Hot',
  plz_enter_search_name: 'Vui lòng nhập tên trò chơi cần tìm kiếm',
  not_open: 'Chưa mở, vì vậy hãy theo dõi',
  no_result: 'Không có dữ liệu , vui lòng kiểm tra lại',
}
