export default {
  all: '全部',
  transfer: 'Chuyển khoản',
  transfer_now: 'Chuyển ngay',
  transfer_in: 'Chuyển vào',
  transfer_out: 'Chuyển ra',
  center_wallet: 'Ví chính',
  form: {
    input: {
      money: {
        label: 'Số tiền chuyển',
        placeholder: 'Nhập số tiền chuyển',
      },
    },
    validate: {
      message: {
        required: 'Bắt buộc điền',
        balance_insufficient: 'Số dư tài khoản không đủ, vui lòng nhập lại.',
      },
    },
  },
  dialog: {
    success: 'Chuyển khoản thành công',
  },
}
