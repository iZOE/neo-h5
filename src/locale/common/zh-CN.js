export default {
  login: '登入',
  signup: '注册',
  confirm: '确定',
  cancel: '取消',
  close: '关闭',
  recharge: '充值',
  withdraw: '提现',
  transfer: '转帐',
  vip: 'VIP',
  all: '全部',
  fetch_more: '加载更多',
  more: '看更多',
  contact_customer: '联系客服',
  tabBar: {
    button1: '游戏',
    button2: '客服',
    button3: '活动',
    button4: '我的',
  },
  linkGroup: {
    appDownload: '手机客户端下载',
    contactCustomerService: '联系客服',
    antiDNSInject: '防劫持教程',
  },
  dialog: {
    login_tips: {
      title: '温馨提醒',
      content: '登录后可正式进入游戏',
      okText: '去登录',
      cancelText: '先不登录',
    },
    logout_tips: {
      content: '确定退出登录吗?',
    },
  },
  already_vertified1: '您绑定的{kind}是',
  already_vertified2: '若需要更改您绑定的{kind}，请直接与客服联系',
  date_menu: {
    today: '今天',
    day: '{num}天',
    customization: '自订',
    start_date: '开始日期',
    end_date: '结束日期',
    select_date: '自选时间',
  },
  loginAlert: {
    content: '您已在其他地方登入，即将跳转回登入页',
    ok: '确认'
  }
}
