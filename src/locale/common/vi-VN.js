export default {
  login: 'Đăng nhập',
  signup: 'Đăng ký',
  confirm: 'Xác nhận',
  cancel: 'hủy bỏ',
  close: 'Tắt',
  recharge: 'Nạp tiền',
  withdraw: 'Rút tiền',
  transfer: 'Chuyển khoản',
  vip: 'VIP',
  all: 'Toàn bộ',
  fetch_more: 'Tải thêm',
  more: 'Xem thêm',
  contact_customer: 'Liên hệ CSKH',
  tabBar: {
    button1: 'Game',
    button2: 'CSKH',
    button3: 'Hoạt động',
    button4: 'Của tôi',
  },
  linkGroup: {
    appDownload: 'Tải APP',
    contactCustomerService: 'CSKH',
    antiDNSInject: 'Hướng dẫn',
  },
  dialog: {
    login_tips: {
      title: 'Lưu ý',
      content: 'Bạn có thể tham gia trò chơi sau khi đăng nhập',
      okText: 'Đăng nhập',
      cancelText: 'Hủy bỏ',
    },
    logout_tips: {
      content: 'Xác nhận đăng xuất ?',
    },
  },
  already_vertified1: 'Nếu bạn cần thay đổi số {kind}',
  already_vertified2: 'vui lòng liên hệ trực tiếp CSKH',
  date_menu: {
    today: 'Hôm nay',
    day: '{num}Ngày',
    customization: 'Tự chọn',
    start_date: 'Ngày bắt đầu',
    end_date: 'Ngày kết thúc',
    select_date: 'Tự chọn thời gian',
  },
  loginAlert: {
    content: 'Bạn đã đăng nhập ở nơi khác, và bạn sẽ được chuyển hướng đến trang đăng nhập',
    ok: 'Xác nhận'
  }
}
