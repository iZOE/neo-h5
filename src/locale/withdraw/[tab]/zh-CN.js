export default {
    fund_password: {
        cskh: '联系客服',
        step1: {
            title: '验证资金密码',
            password: {
                placeholder: '请输入6位数资金密码',
            },
            submit: '下一步',
            explanation1: '*为确保您的帐户安全，若忘记资金密码，请直接{cskh}',
            validate: {
                password: {
                    required: '资金密码必须是 6位数字',
                    length: '资金密码必须是 6位数字',
                },
            },
        },
    },
    current_exchange: '当前汇率',
    exchange_rate: '汇率',
    validate: {
        balance: '提现金额不得大于帐户金额',
        withdraw_limit:'提现金额不得大于上限金额',
        no_password: {
            body: '尚未设定资金密码',
            ok: '前往设置',
            cancel: '取消'
        },
        turnover: {
            title: '流水未达标准!',
            body: '尚缺{need}流水，即可进行提现',
            ok: '前往游戏',
            cancel: '联系客服',
            detail: '查看详情'
        },
        no_card: {
            title: '请先新增提现账户',
            body: '请在提现前新增至少一个提现账户',
            ok: '进行绑定',
            cancel: '取消',
        },
        no_banks_card: {
            body: '要使用银行卡提现，请先绑定银行卡',
            ok: '进行绑定',
            cancel: '取消',
        },
        no_crypto_card: {
            body: '要使用虚拟币提现，请先绑定虚拟币钱包',
            ok: '进行绑定',
            cancel: '取消',
        },
    },
    success: '申请成功！进度可至提现纪录查询',
    exchange: {
        title: 'USDT是什么?',
        body: 'USDT简称泰达币（Tether），是Tether公司推出的基于稳定保值的货币：美元（USD）的代币Tether USD（简称USDT）。其该货币价值与美元对等，1USDT=1美元，该币种作为最稳定、最保值、最有保障的货币，在数字货币中独占鳌头。',
    },
    title: {
        step1: '提现申请',
        step2: '提现申请',
        step3: '提现审核结果',
        step99: '审核详情'
    },
    tab: {
        banks: '银行卡',
        crypto: '虚拟币钱包',
    },
    submit: {
        items: {
            title: '审核详情',
            turnover: '期间有效投注总额(有效流水)：',
            last_time: '本次结算起始时间：',
            last_time_hint: '(上次提现或转帐时间)',
            id: '项目ID',
            type: '类型',
            item: '项目',
            amount: '金额',
            multiple: '稽核倍数',
            betting: '所需流水',
            status: '状态',
        },
        banks: { title: '账户', card: '(卡号后四码 {card} )' },
        crypto: { title: '虚拟币钱包', card: '钱包地址后5码 {card} )' },
        balance: '申请金额',
        administration_fee: '行政费',
        prefer_amount: '优惠金额扣除',
        withdrawal_fee: '手续费',
        actually_amount: '实际可提领金额',
        detail: '審核詳情',
        button: '确认提领'
    },
    next: '下一步',
    balance: '账户余额',
    free_withdraw: '今日免手续费次数',
    withdraw_amount: '请输入提现金额',
    notice: {
        title: '温馨提醒：',
        select: [
            '单次提现最低{min}，最高{max}。',
            '若超过当日免手续费次数，会酌收手续费。每日00:00重新计算。',
            '提现时，若未达平台流水要求，会产生相关费用。',
            '提现单笔达 {sms}以上，需通过手机验证。',
        ],
        submit: [
            '若超过当日免手续费次数，会酌收手续费。每日 00:00 重新计算。',
            '提现时，若未达到平台流水要求，会产生相关费用。',
            '可点击項目說明按钮，查看審核详情。',
            '请确认银行卡与取款人姓名一致，提醒您，若取款人与银行卡号无法匹配，将不予出款。',
            '若有任何问题请{cs}。',
        ],
    },
    phone: {
        abort_editing: '是否要放弃编辑内容?',
        step1: { title: '设置手機號碼', submit: '获取验证码', placeholder: '请输入手机号码' },
        step2: { title: '短信认证', submit: '完成', placeholder: '请输入4位数验证码' },
        select_label: '国家/地区',
        phone_label: '请输入手机号码',
        validate: {
            ContactNumber: {
                required: '必须是 10 字的组合',
                length: '必须是 10 字的组合',
            },
            MappingCode: {
                required: '验证码为必填',
                length: '必须是 4 字的组合',
            },
        },
        edit_success: '手机号码设置完成',
        verified_kind_name: '手机号码',
        mapping_code_sent: '已发送4位数验证码至您的手机',
        mapping_code_will_expire: '{mmss} 后验证码失效',
        mapping_code_is_expired: '验证码已失效',
        get_code_button: '重新发送验证码',
    },
}
