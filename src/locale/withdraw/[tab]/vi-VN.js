export default {
    fund_password: {
        cskh: 'CSKH',
        step1: {
            title: 'Xác nhận mật khẩu quỹ',
            password: {
                placeholder: 'Nhập 6 số mật khẩu quỹ',
            },
            submit: 'Bước tiếp theo',
            explanation1: '*Vì lý do bảo mật tài khoản , nếu quên mật khẩu quỹ , vui lòng liên hệ {cskh}',
            validate: {
                password: {
                    required: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
                    length: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
                },
            },
        },
    },
    success: 'Đề xuất thành công ! Vui lòng kiểm tra tiến độ tại lịch sử rút tiền',
    validate: {
        balance: 'Số tiền rút không được vượt số dư tài khoản',
        withdraw_limit:'Số tiền rút không được vượt hạn mức tối đa',
        no_password: {
            body: 'Chưa cài đặt mật khẩu quỹ',
            ok: 'Tiến đến đặt cược',
            cancel: 'Hủy'
        },
        turnover: {
            title: 'Vòng cược chưa đạt!',
            body: 'Còn thiếu{need}vòng cược mới có thể rút khoản',
            ok: 'Tiến đến trò chơi',
            cancel: 'CSKH',
            detail: 'Xem chi tiết'
        },
        no_card: {
            title: 'Vui lòng thêm tài khoản rút tiền',
            body: 'Trước khi rút tiền , cần thêm tối thiểu 1 tài khoản rút tiền',
            ok: 'Tiến hành mặc định',
            cancel: 'Hủy',
        },
        no_banks_card: {
            body: 'Bạn muốn rút khoản bằng thẻ ngân hàng, vui lòng mặc định thẻ ngân hàng',
            ok: 'Tiến hành mặc định',
            cancel: 'Hủy',
        },
        no_crypto_card: {
            body: 'Bạn muốn rút khoản bằng tiền ảo, vui lòng mặc định ví tiền ảo',
            ok: 'Tiến hành mặc định',
            cancel: 'Hủy',
        },
    },
    title: {
        step1: 'Đề xuất rút tiền',
        step2: 'Đề xuất rút tiền',
        step3: 'Kết quả kiểm duyệt',
        step99: 'Chi tiết kiểm duyệt'
    },
    tab: {
        banks: 'Thẻ ngân hàng',
        crypto: 'Vií tiền ảo',
    },
    current_exchange: 'Tỷ giá hối đoái hiện hành',
    exchange_rate: 'Tỷ giá hối đoái',
    exchange: {
        title: "USDT là gì ?",
        body: "USDT được viết tắt là Tether, là một loại tiền tệ được Công ty Tether tung ra dựa trên giá trị ổn định: Đô la Mỹ (USD) token Tether USD (viết tắt là USDT). Giá trị của đơn vị tiền tệ này tương đương với đô la Mỹ, 1USDT = 1 USD . Đơn vị tiền tệ này, là loại tiền tệ ổn định nhất, có giá trị nhất và an toàn nhất, dẫn đầu trong số các loại tiền kỹ thuật số.",
    },
    next: 'Bước tiếp theo',
    balance: 'Số dư ví chính',
    free_withdraw: 'Số lần miễn phí rút hôm nay',
    withdraw_amount: 'Nhập số tiền rút',
    submit: {
        items: {
            title: 'Chi tiết kiểm duyệt',
            turnover: 'Tổng cược hợp lệ trong thời gian：',
            last_time: 'Thời gian bắt đầu kết toán kỳ này：',
            last_time_hint: '(Lần rút khoản gần nhất hoặc thời gian chuyển khoản)',
            id: 'Item ID',
            type: 'Thể loại',
            item: 'Hạng mục',
            amount: 'Số tiền',
            multiple: 'Bội số xét duyệt',
            betting: 'Doanh thu yêu cầu',
            status: 'Trạng thái'
        },
        banks: {title:"Tài khoản", card: '(4 số cuối trong số tài khoản {card} )'},
        crypto: {title:'Vií tiền ảo', card:'5 số cuối của liên kết ví tiền {card} )'},
        balance: "Số tiền đề xuất",
        administration_fee: "Phí hành chính",
        prefer_amount: "Khấu trừ điểm khuyến mãi",
        withdrawal_fee:"Phí thủ tục",
        actually_amount: "Số tiền rút thực tế",
        detail: "Xem chi tiết",
        button: 'xác nhận rút tiền'
    },
    notice: {
        title: 'Lưu ý:',
        select: [
            'Hạn mức rút tiền thấp nhất {min}, cao nhất {max}.',
            'Nếu vượt số lần miễn phí trong ngày, phí xử lý sẽ được tính. Cập nhật lại mỗi ngày vào lúc 23:00.',
            'Nếu chưa đạt tổng cược hợp lệ, sẽ bị tính phí hành chính ',
            'Rút tiền đạt đến hoặc vượt quá {sms}, cần xác minh số điện thoại.',
        ],
        submit: [
            'Nếu vượt số lần miễn phí trong ngày, phí xử lý sẽ được tính. Cập nhật lại mỗi ngày vào lúc 23:00.',
            'Nếu chưa đạt tổng cược hợp lệ, sẽ bị tính phí hành chính',
            'Nhấn vào nút biểu tượng kiểm tra để theo dõi chi tiết kiểm duyệt.',
            'Nếu tên người thụ hưởng không trùng khớp với tên chủ tài khoản ngân hàng, sẽ không được duyệt xuất khoản.',
            'Nếu bạn có bất cứ câu hỏi nào, vui lòng liên hệ bộ phận {cs}',
        ],

    },
    phone: {
        abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
        step1: {
            title: 'Thay đổi số điện thoại',
            submit: 'Hoàn thành',
            placeholder: 'Vui lòng nhập số điện thoại',
        },
        step2: {
            title: 'Thay đổi số điện thoại',
            submit: 'Hoàn thành',
            placeholder: 'Vui lòng nhập mã xác nhận gồm 4 chữ số',
        },
        select_label: 'Quốc gia / Khu vực',
        phone_label: 'Vui lòng nhập số điện thoại',
        validate: {
            ContactNumber: {
                required: 'Nhập 10 ký tự chữ',
                length: 'Nhập 10 ký tự chữ',
            },
            MappingCode: {
                required: 'Yêu cầu mã xác nhận',
                length: 'Nhập 4 ký tự chữ',
            },
        },
    }
}
