export default {
  member_center: 'Chức năng của tôi',
  logout: 'Đăng xuất',
  download: 'Tải xuống ứng dụng',
  report_bet: 'Lịch sử đặt cược',
  report_deposit: 'Lịch sử nạp tiền',
  report_financial: 'Lịch sử tài khoản',
  report_profit: 'Báo biểu lãi lỗ',
  report_transfer: 'Lịch sử chuyển điểm',
  report_withdraw: 'Lịch sử rút tiền',
  withdraw_manage: 'Quản lý tài khoản rút tiền',
  another_contact: 'Liên lạc khác',
  promo: 'Đại lý',
  setting: 'Cài đặt',
  setting_info: 'Chức năng của tôi',
  user_data: {
    no_session: {
      welcome: 'XIN CHÀO QUÝ HỘI VIÊN',
      please_login: 'ĐĂNG NHẬP NGAY , VẬN MAY LIỀN TAY',
    },
  },
  money: 'Ví chính',
}
