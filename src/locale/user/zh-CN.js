export default {
  member_center: '我的功能',
  logout: '登出',
  download: '手机端下载',
  report_bet: '投注纪录',
  report_deposit: '充值纪录',
  report_financial: '账变纪录',
  report_profit: '盈亏报表',
  report_transfer: '转账纪录',
  report_withdraw: '提现纪录',
  withdraw_manage: '提现帐户管理',
  another_contact: '其他联系方式',
  promo: '推广赚钱',
  setting: '设置',
  setting_info: '个人中心',
  user_data: {
    no_session: {
      welcome: '欢迎您~',
      please_login: '立即登入，开启财富',
    },
  },
  money: '中心钱包余额',
}
