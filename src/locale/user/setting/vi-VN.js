export default {
  title: 'Cài đặt',
  member_center: 'Trung tâm cá nhân',
  dark_mode: 'Giao diện ban đêm',
  support: 'Trung tâm hỗ trợ',
  feeback: 'Ý kiến phản hồi',
  about_us: 'Về chúng tôi',
  font_size_bigger: 'Phóng to cỡ chữ báo biểu',
  logout: 'Đăng xuất',
}
