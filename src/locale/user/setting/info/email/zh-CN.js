export default {
  abort_editing: '是否要放弃编辑内容?',
  title: '安全邮箱',
  input: {
    placeholder: '绑定安全邮箱',
    validate: '安全邮箱格式不正确',
  },
  button: '寄送验证信',
  dialog: {
    success: '验证邮件已发送至您的邮箱，您的激活链接在24小内有效',
  },
  bound_page: {
    tip_1: '您绑定的邮箱是',
    tip_2: '若需要更改您绑定的邮箱，请直接与客服联系。',
  },
}
