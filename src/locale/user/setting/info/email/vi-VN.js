export default {
  abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
  title: 'Hộp thư an toàn',
  input: {
    placeholder: 'Mặc định hộp thư an toàn',
    validate: 'Hộp thư an toàn không đúng',
  },
  button: 'Gửi thư xác minh',
  dialog: {
    success:
      'Thư xác nhận đã được gửi đến hộp thư của bạn,kích hoạt liên kết của bạn có hiệu lực trong 24 giờ.',
  },
  bound_page: {
    tip_1: 'Hộp thư bạn mặc định là',
    tip_2: 'Nếu bạn cần thay đổi hộp thư, vui lòng liên hệ CSKH',
  },
}
