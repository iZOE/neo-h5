export default {
  member_center: '个人中心',
  user_data: '个人资讯',
  nickname: '昵称',
  phone_number: '手机号',
  birthday: '生日',
  wechat: '微信',
  mail: '安全邮箱',
  qq: 'QQ号',
  login_password: '登录密码',
  fund_password: '资金密码',
  edit: '修改',
  unset: '未设置',
  security_info: '安全资讯',
  dialog: {
    birthday: {
      title: '生日已綁定',
      content: '您的生日 {birthday} 已绑定，若需修改，请透过客服处理',
    },
  },
}
