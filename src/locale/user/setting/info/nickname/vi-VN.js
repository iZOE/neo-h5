export default {
  abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
  title: 'Sửa biệt danh',
  placeholder: 'Vui lòng nhập biệt danh',
  submit: 'Hoàn thành',
  validate: {
    nick_name: {
      required: 'Nhập 1 -14 ký tự chữ',
      length: 'Nhập 1 -14 ký tự chữ',
    },
  },
  tips: 'Biệt danh này không phải tên đăng nhập , chỉ hiển thị trong phòng trò chuyện',
  edit_success: 'Chỉnh sửa thành công',
}
