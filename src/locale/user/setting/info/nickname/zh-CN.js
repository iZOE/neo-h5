export default {
  abort_editing: '是否要放弃编辑内容?',
  title: '修改昵称',
  placeholder: '请输入昵称',
  submit: '完成',
  validate: {
    nick_name: {
      required: '必须是 1 - 14 字的组合',
      length: '必须是 1 - 14 字的组合',
    },
  },
  tips: '此昵称非登录会员名，仅在聊天场景中显示',
  edit_success: '修改成功',
}
