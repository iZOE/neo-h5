export default {
  member_center: 'Trung tâm cá nhân',
  user_data: 'Thông tin cá nhân',
  nickname: 'Nhập biệt danh (bắt buộc)',
  phone_number: 'Số điện thoại',
  birthday: 'Sinh nhật',
  wechat: 'WeChat',
  mail: 'Hộp thư an toàn',
  qq: 'Số QQ',
  login_password: 'Mật khẩu đăng nhập',
  fund_password: 'Mật khẩu quỹ',
  edit: 'Chỉnh sửa',
  unset: 'Chưa cài đặt',
  security_info: 'Thông tin bảo mật',
  dialog: {
    birthday: {
      title: 'Đã mặc định ngày sinh',
      content:
        'Sinh nhật của bạn {birthday} đã được mặc định {br} Nếu bạn cần sửa đổi, vui lòng liên hệ CSKH',
    },
  },
}
