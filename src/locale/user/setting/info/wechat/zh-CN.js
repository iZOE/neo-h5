export default {
  abort_editing: '是否要放弃编辑内容?',
  submit: '完成',
  title: '修改微信',
  placeholder: '请输入微信号',
  validate: {
    weChat: {
      required: '必须是 6 - 20 字的组合',
      length: '必须是 6 - 20 字的组合',
    },
  },
  edit_success: '修改成功',
  verified_kind_name: '微信号码',
}
