export default {
  abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
  submit: 'Hoàn thành',
  title: 'Thay đổi wechat',
  placeholder: 'Nhập số wechat',
  validate: {
    weChat: {
      required: 'Nhập 6 - 20 ký tự chữ',
      length: 'Nhập 6 - 20 ký tự chữ',
    },
  },
  edit_success: 'Chỉnh sửa thành công',
  verified_kind_name: 'Wechat',
}
