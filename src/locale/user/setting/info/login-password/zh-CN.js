export default {
  abort_editing: '返回将放弃目前编辑内容',
  step1: {
    title: '验证登录密码',
    password: {
      placeholder: '原密码',
    },
    submit: '下一步',
    explanation1: '*为保护您的资料安全，修改密码前请先填写原密码',
    forgotPassword: '忘记密码',
    error: {
      account_logout: '账号已登出',
      content: '因安全考量，请您重新登入',
    },
  },
  step2: {
    title: '修改登录密码',
    setPassword: {
      label: '新密码',
      placeholder: '请输入新密码',
    },
    confirmSetPassword: {
      label: '确认新密码',
      placeholder: '请再次输入新密码',
    },
    validate: {
      not_same: '2次密码不一致，请重新输入',
      required: '密码必须是 6 - 16 位含英文及数字组合',
      length: '密码必须是 6 - 16 位含英文及数字组合',
    },
    explanation1: '*密码必须是 6 - 16 位含英文及数字组合',
    submit: '完成',
  },
  submit: '完成',
  edit_success: '修改成功',
}
