export default {
  abort_editing: 'Quay lại sẽ bỏ nội dung chỉnh sửa hiện tại',
  step1: {
    title: 'Xác nhận mật khẩu đăng nhập',
    password: {
      placeholder: 'Mật khẩu ban đầu',
    },
    submit: 'Bước tiếp theo',
    explanation1:
      '*Để bảo mật thông tin tài khoản, cần nhập mật khẩu ban đầu trước khi thay đổi mật khẩu',
    forgotPassword: 'Quên mật khẩu',
    error: {
      account_logout: 'Đã đăng xuất tài khoản',
      content: 'Vì lý do bảo mật, vui lòng đăng nhập lại.',
    },
  },
  step2: {
    title: 'Thay đổi mật khẩu đăng nhập',
    setPassword: {
      label: 'Mật khẩu mới',
      placeholder: 'Bắt buộc điền mật khẩu đăng nhập mới',
    },
    confirmSetPassword: {
      label: 'Xác nhận mật khẩu mới',
      placeholder: 'Vui lòng nhập mật khẩu mới lần nữa',
    },
    validate: {
      not_same: 'Mật khẩu lần hai không khớp, vui lòng nhập lại',
      required: 'Mật khẩu phải điền 6-16 ký tự chữ và số',
      length: 'Mật khẩu phải điền 6-16 ký tự chữ và số',
    },
    explanation1: '*Mật khẩu phải có 6-16 kí tự bao gồm chữ cái và số',
    submit: 'Hoàn thành',
  },
  submit: 'Hoàn thành',
  edit_success: 'Chỉnh sửa thành công',
}
