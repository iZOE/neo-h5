export default {
  abort_editing: 'Quay lại sẽ bỏ nội dung chỉnh sửa hiện tại',
  step1: {
    title: 'Xác nhận mật khẩu quỹ',
    password: {
      placeholder: 'Nhập 6 số mật khẩu quỹ',
    },
    submit: 'Bước tiếp theo',
    explanation1: '*Vì lý do bảo mật tài khoản , nếu quên mật khẩu quỹ , vui lòng liên hệ {CSKH}',
    validate: {
      password: {
        required: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
        length: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
      },
    },
  },
  step2: {
    title: 'Cài đặt mật khẩu quỹ',
    setPassword: {
      label: 'Mật khẩu mới',
      placeholder: 'Bắt buộc điền mật khẩu đăng nhập mới',
    },
    confirmSetPassword: {
      label: 'Xác nhận mật khẩu mới',
      placeholder: 'Vui lòng nhập lại mật khẩu quỹ gồm 6 chữ số.',
    },
    validate: {
      not_same: 'Mật khẩu lần hai không khớp, vui lòng nhập lại',
      setPassword: {
        required: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
        length: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
      },
    },
    cskh: 'CSKH',
    explanation1:
      '*Mật khẩu quỹ là mật khẩu xác thực duy nhất tại thời điểm thanh toán. Vui lòng nhớ mật khẩu và không tiết lộ mật khẩu quỹ cho người khác. {br} Vui lòng giữ mật khẩu của bạn đúng cách. Nếu bạn quên mật khẩu, vui lòng sử dụng chức năng câu hỏi bảo mật để lấy lại hoặc liên hệ {cskh} để xử lý.',
    submit: 'Hoàn thành',
  },
  submit: 'Hoàn thành',
  edit_success: 'Chỉnh sửa thành công',
}
