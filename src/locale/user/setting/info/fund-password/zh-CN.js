export default {
  abort_editing: '返回将放弃目前编辑内容',
  step1: {
    title: '验证资金密码',
    password: {
      placeholder: '请输入6位数资金密码',
    },
    submit: '下一步',
    explanation1: '*为确保您的帐户安全，若忘记资金密码，请直接{cskh}',
    validate: {
      password: {
        required: '资金密码必须是 6位数字',
        length: '资金密码必须是 6位数字',
      },
    },
  },
  step2: {
    title: '设置資金密码',
    setPassword: {
      label: '新密码',
      placeholder: '请输入新的資金密码',
    },
    confirmSetPassword: {
      label: '确认新密码',
      placeholder: '请再次输入新的資金密码',
    },
    validate: {
      not_same: '2次密码不一致，请重新输入',
      setPassword: {
        required: '资金密码必须是 6位数字',
        length: '资金密码必须是 6位数字',
      },
    },
    explanation1:
      '*资金密码为出款时唯一认证密码，请妥善保管好您的密码，切勿透漏给其他人，若忘记资金密码，请直接{cskh}协助处理',
    submit: '完成',
  },
  cskh: '联系客服',
  submit: '完成',
  edit_success: '修改成功',
}
