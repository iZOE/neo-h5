export default {
  title: '生日设置',
  bind: '绑定',
  input: {
    placeholder: '请输入出生年月日',
    error: '生日范围為 {min} ~ {max}，请重新選擇日期',
  },
  dialog: {
    back_alert: {
      content: '返回将放弃目前编辑内容',
    },
    success: {
      title: '生日設置成功',
      content: '您的生日 {birthday} 已绑定，若需修改，请透过客服处理',
    },
  },
  tips: '*生日只能绑定一次，不得更改{br}*若您的生日不在选单范围内，请联系客服人员为您设定',
}
