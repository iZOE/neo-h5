export default {
  title: 'Cài đặt ngày sinh nhật',
  bind: 'Đã mặc định',
  input: {
    placeholder: 'Nhập ngày tháng năm sinh',
    error: 'Phạm vi ngày sinh là "{min} ~ {max}", vui lòng chọn một ngày mới',
  },
  dialog: {
    back_alert: {
      content: 'Quay lại sẽ bỏ nội dung chỉnh sửa hiện tại',
    },
    success: {
      title: 'Cài đặt ngày sinh thành công',
      content:
        'Sinh nhật của bạn {birthday} đã cài đặt, nếu bạn cần sửa đổi , vui lòng liên hệ CSKH',
    },
  },
  tips:
    '* Ngày sinh nhật chỉ được mặc định 1 lần, không được sửa đổi{br}* Nếu ngày sinh nhật của bạn không có trong phạm vi lựa chọn, vui lòng liên hệ CSKH cài đặt',
}
