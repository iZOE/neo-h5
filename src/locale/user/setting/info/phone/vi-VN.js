export default {
  abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
  step1: {
    title: 'Thay đổi số điện thoại',
    submit: 'Hoàn thành',
    placeholder: 'Vui lòng nhập số điện thoại',
  },
  step2: {
    title: 'Thay đổi số điện thoại',
    submit: 'Hoàn thành',
    placeholder: 'Vui lòng nhập mã xác nhận gồm 4 chữ số',
  },
  select_label: 'Quốc gia / Khu vực',
  phone_label: 'Vui lòng nhập số điện thoại',
  validate: {
    ContactNumber: {
      required: 'Nhập 10 ký tự chữ',
      length: 'Nhập 10 ký tự chữ',
    },
    MappingCode: {
      required: 'Yêu cầu mã xác nhận',
      length: 'Nhập 4 ký tự chữ',
    },
  },
  edit_success: 'Số điện thoại đã cài đặt thành công.',
  verified_kind_name: 'Số điện thoại di động',
  mapping_code_sent: 'Đã gửi mã xác minh gồm 4 chữ số đến số điện thoại của bạn',
  mapping_code_will_expire: 'Sau {mmss} mã xác minh sẽ vô hiệu hóa',
  mapping_code_is_expired: 'Mã xác nhận đã vô hiệu hóa',
  get_code_button: 'Gửi lại mã xác nhận',
}
