export default {
  abort_editing: '是否要放弃编辑内容?',
  step1: { title: '设置手機號碼', submit: '获取验证码', placeholder: '请输入手机号码' },
  step2: { title: '短信认证', submit: '完成', placeholder: '请输入4位数验证码' },
  select_label: '国家/地区',
  phone_label: '请输入手机号码',
  validate: {
    ContactNumber: {
      required: '必须是 10 字的组合',
      length: '必须是 10 字的组合',
    },
    MappingCode: {
      required: '验证码为必填',
      length: '必须是 4 字的组合',
    },
  },
  edit_success: '手机号码设置完成',
  verified_kind_name: '手机号码',
  mapping_code_sent: '已发送4位数验证码至您的手机',
  mapping_code_will_expire: '{mmss} 后验证码失效',
  mapping_code_is_expired: '验证码已失效',
  get_code_button: '重新发送验证码',
}
