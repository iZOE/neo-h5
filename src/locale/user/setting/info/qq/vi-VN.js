export default {
  abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
  title: 'Thay đổi số QQ',
  submit: 'Hoàn thành',
  placeholder: 'Nhập số QQ',
  validate: {
    qq: {
      required: 'Nhập 4 - 10 ký tự chữ',
      length: 'Nhập 4 - 10 ký tự chữ',
    },
  },
  edit_success: 'Chỉnh sửa thành công',
  verified_kind_name: 'QQ',
}
