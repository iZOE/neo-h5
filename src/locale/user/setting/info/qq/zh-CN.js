export default {
  abort_editing: '是否要放弃编辑内容?',
  title: '修改QQ',
  submit: '完成',
  placeholder: '请输入QQ号',
  validate: {
    qq: {
      required: '必须是 4 - 10 字的组合',
      length: '必须是 4 - 10 字的组合',
    },
  },
  edit_success: '修改成功',
  verified_kind_name: 'QQ号码',
}
