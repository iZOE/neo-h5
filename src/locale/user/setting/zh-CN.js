export default {
  title: '设置',
  member_center: '个人中心',
  dark_mode: '夜间模式',
  support: '帮助中心',
  feeback: '意见反馈',
  about_us: '关于我们',
  font_size_bigger: '报表字体放大',
  logout: '登出',
}
