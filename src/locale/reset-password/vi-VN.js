export default {
  title: 'Quên mật khẩu',
  common: {
    next: 'Bước tiếp theo',
  },
  page1: {
    input: {
      accountId: {
        label: 'Tài khoản đăng nhập',
        placeholder: 'Nhập tên tài khoản',
      },
    },
    tips: {
      line_1: 'Vui lòng nhập tên tài khoản của bạn , tiện cho việc sau này tìm lại mật khẩu',
      line_2: 'Nếu bạn chưa mặc định số điện thoại , vui lòng liên hệ CSKH trực tuyến',
    },
    warning_tip:
      'Để đảm bảo tính bảo mật cho tài khoản của bạn , vui lòng liên hệ CSKH để nhận mật khẩu mới',
  },
  page2: {
    mail_validate: 'Tin nhắn xác nhận',
    tips:
      'Để đảm bảo tính bảo mật cho tài khoản của bạn, vui lòng chọn phương thức nhận lại mật khẩu',
  },
  page3: {
    sended_message: 'Đã gửi mã xác nhận gồm 4 chữ số đến số điện của bạn',
    input: {
      validation_code: {
        placeholder: 'Vui lòng nhập mã xác nhận gồm 4 chữ số',
      },
    },
    toast: 'Tin nhắn điện thoại xác minh thành công',
    get_code_button: 'Gửi lại mã xác minh',
  },
  page4: {
    title: 'Cài đặt lại mật khẩu',
    initial_error: 'Thông tin không đúng , vui lòng thao tác lại',
    dialog: {
      success: 'Cài đặt mật khẩu thành công , vui',
    },
    tips: 'Mật khẩu phải là 6-16 ký tự chữ và số',
    input: {
      password: {
        label: 'Mật khẩu mới',
      },
      password_confirm: {
        label: 'Xác nhận mật khẩu mới',
        rules: {
          confirm: 'Vui lòng nhập lại mật khẩu mới',
        },
      },
    },
    button: {
      finish: 'Hoàn thành',
    },
  },
}
