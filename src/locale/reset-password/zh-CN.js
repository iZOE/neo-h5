export default {
  title: '忘记密码',
  common: {
    next: '下一步',
  },
  page1: {
    input: {
      accountId: {
        label: '登入帐号',
        placeholder: '请输入您的帐户名称',
      },
    },
    tips: {
      line_1: '请输入您的登入帐号，以便后续找回密码流程',
      line_2: '若您没有设定手机号码，请直接联系客服',
    },
    warning_tip: '为确保此帐号安全，请联系客服取得新密码',
  },
  page2: {
    mail_validate: '短信验证',
    tips: '为确保帐户的安全性，请选择您要找回登录密码的管道',
  },
  page3: {
    sended_message: '已发送 4 位数验证码至您的手机',
    input: {
      validation_code: {
        placeholder: '请输入4位数验证码',
      },
    },
    toast: '手机短信验证成功',
    get_code_button: '重新发送验证码',
  },
  page4: {
    title: '重新设置密码',
    initial_error: '资料有误请重新操作',
    dialog: {
      success: '已成功设置密码，请重新登录',
    },
    tips: '密码必须是 6-16 位的英文字母、数字组合',
    input: {
      password: {
        label: '新密码',
      },
      password_confirm: {
        label: '确认新密码',
        rules: {
          confirm: '请确认您的新密码',
        },
      },
    },
    button: {
      finish: '完成',
    },
  },
}
