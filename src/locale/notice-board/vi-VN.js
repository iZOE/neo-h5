export default {
  tabs: {
    activity: 'Hoạt động',
    noticeboard: 'Thông báo',
  },
  typeName: {
    1: 'Hệ thống',
    2: 'Hội viên',
    3: 'Hoạt động',
  },
  emptyMessage: 'Tạm không có thông báo chính',
}
