export default {
  tabs: {
    activity: '活动',
    noticeboard: '公告'
  },
  typeName: {
    1: '系统',
    2: '会员',
    3: '活动',
  },
  emptyMessage: '暂无重大公告'
}
