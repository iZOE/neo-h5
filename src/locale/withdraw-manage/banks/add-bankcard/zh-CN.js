export default {
  bar_title: '添加银行卡',
  dialog_info:
    '取款人姓名必须与出款账户户名一致，否则不予出款，若您要更改您的取款人姓名请与客服联系。',
  bar_title2: '确认银行卡信息',
  CSKH: '联系客服',
  next_step: '下一步',
  accountName: '取款人',
  card_number: '银行卡号',
  open_account_bank: '开户银行',
  open_account_bank_please_select: '请选择开户银行',
  placeholder: {
    accountName: '请输入取款人姓名',
    card_number: '请输入银行卡号',
  },
  validate: {
    card_number: {
      required: '必填',
      format: '钱包地址格式错误',
    },
  },
  no_change_account_name:
    '取款人姓名必须与出款帐户户名一致，否则不予出款，若您要更改您的取款人姓名请与客服联系。',
  warm_warning: '温馨提醒:',
  warm_warning_item1: '取款人经绑定后即无法修改，请正确填写。',
  warm_warning_item2: '取款人姓名必须与银行卡号匹配，否则平台不予出款。',
  warm_warning_item3: '绑定的资料为忘记密码时认证用，请会员务必正确填写。',
  submit: '确认添加',
  api: {
    add_error_duplicate: {
      title: '新增失败',
      content: '此钱包地址已绑定，请您绑定其他钱包地址',
    },
    add_account_success: {
      title: '新增提现账户',
      content: '提现账户添加成功',
      confirm: '确定',
    },
  },
  alert_dialog: {
    not_set_fund_password_yet: '尚未设定资金密码',
    go_set_it: '前往设置',
    title: '新增提现账户',
    plz_do_phone_validate: '请完成手机验证，即可新增提现帐户',
    send_code: '发送验证码',
    will_send_sms_code:
      '为确保您的账户安全，系统将发送短信验证码到您绑定的手机，认证后即可完成提现账户新增',
  },
  // 資金密碼
  fund_password: {
    abort_editing: '返回将放弃目前编辑内容',
    step1: {
      title: '验证资金密码',
      password: {
        placeholder: '请输入6位数资金密码',
      },
      submit: '下一步',
      explanation1: '*为确保您的帐户安全，若忘记资金密码，请直接{cskh}',
      validate: {
        password: {
          required: '资金密码必须是 6位数字',
          length: '资金密码必须是 6位数字',
        },
      },
    },
    step2: {
      title: '设置資金密码',
      setPassword: {
        label: '新密码',
        placeholder: '请输入新的資金密码',
      },
      confirmSetPassword: {
        label: '确认新密码',
        placeholder: '请再次输入新的資金密码',
      },
      validate: {
        not_same: '2次密码不一致，请重新输入',
        setPassword: {
          required: '资金密码必须是 6位数字',
          length: '资金密码必须是 6位数字',
        },
      },
      explanation1:
        '*资金密码为出款时唯一认证密码，请妥善保管好您的密码，切勿透漏给其他人，若忘记资金密码，请直接{cskh}协助处理',
      submit: '完成',
    },
    cskh: '联系客服',
    submit: '完成',
    edit_success: '修改成功',
  },
  // 手機簡訊
  phone: {
    abort_editing: '是否要放弃编辑内容?',
    step1: { title: '设置手機號碼', submit: '获取验证码', placeholder: '请输入手机号码' },
    step2: { title: '短信认证', submit: '完成', placeholder: '请输入4位数验证码' },
    select_label: '国家/地区',
    phone_label: '请输入手机号码',
    validate: {
      ContactNumber: {
        required: '必须是 10 字的组合',
        length: '必须是 10 字的组合',
      },
      MappingCode: {
        required: '验证码为必填',
        length: '必须是 4 字的组合',
      },
    },
    edit_success: '手机号码设置完成',
    verified_kind_name: '手机号码',
    mapping_code_sent: '已发送4位数验证码至您的手机',
    mapping_code_will_expire: '{mmss} 后验证码失效',
    mapping_code_is_expired: '验证码已失效',
    get_code_button: '重新发送验证码',
  },
}
