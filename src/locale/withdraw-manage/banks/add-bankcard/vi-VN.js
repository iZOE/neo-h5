export default {
  bar_title: 'Thêm thẻ ngân hàng',
  dialog_info:
    'Tên người rút tiền phải đồng nhất với tên người thụ hưởng của tài khoản, nếu không sẽ không được rút tiền. Nếu bạn muốn thay đổi tên của người rút tiền, vui lòng liên hệ với CSKH',
  bar_title2: 'Xác nhận thông tin ngân hàng',
  CSKH: 'CSKH',
  next_step: 'Bước tiếp theo',
  accountName: 'Tên chủ tài khoản',
  card_number: 'Số tài khoản ngân hàng',
  open_account_bank: 'Tên ngân hàng',
  open_account_bank_please_select: 'Chọn ngân hàng',
  placeholder: {
    accountName: 'Nhập tên chủ tài khoản',
    card_number: 'Nhập số tài khoản ngân hàng',
  },
  validate: {
    card_number: {
      required: 'Bắt buộc điền',
      format: 'Định dạng liên kết ví tiền bị lỗi',
    },
  },
  no_change_account_name:
    'Tên người rút tiền phải đồng nhất với tên người thụ hưởng của tài khoản, nếu không sẽ không được rút tiền. Nếu bạn muốn thay đổi tên của người rút tiền, vui lòng liên hệ với CSKH',
  warm_warning: 'Lưu ý:',
  warm_warning_item1:
    'Thông tin thụ hưởng sau khi cài đặt thì không thể sửa đổi, vui lòng nhập chính xác thông tin',
  warm_warning_item2:
    'Tên của người thụ hưởng phải trùng khớp với tên chủ tài khoản ngân hàng, nếu không hệ thống sẽ không xuất khoản.',
  warm_warning_item3:
    'Thông tin mặc định được sử dụng để xác thực khi bạn quên mật khẩu. Vui lòng đảm bảo nhập thông tin chính xác.',
  submit: 'Xác nhận thêm',
  api: {
    add_error_duplicate: {
      title: 'Thêm mới không thành công',
      content: 'Liên kế ví tiền này đã được mặc định, bạn vui lòng thử các liên kết ví tiền khác',
    },
    add_account_success: {
      title: 'Thêm Tài khoản rút tiền',
      content: 'Đã thêm Tài khoản rút tiền thành công',
      confirm: 'Xác nhận',
    },
  },
  alert_dialog: {
    not_set_fund_password_yet: 'Chưa cài đặt mật khẩu quỹ',
    go_set_it: 'Cài đặt',
    title: 'Thêm Tài khoản rút tiền',
    plz_do_phone_validate: 'Vui lòng hoàn tất xác minh số điện thoại để thêm tài khoản rút tiền',
    send_code: 'Gửi mã xác nhận',
    will_send_sms_code:
      'Vì bảo mật an toàn cho tài khoản của bạn, hệ thống sẽ gửi mã xác minh đến số điện thoại của bạn, sau khi xác minh sẽ hoàn thành thêm tài khoản rút tiền',
  },
  // fund-password
  fund_password: {
    abort_editing: 'Quay lại sẽ bỏ nội dung chỉnh sửa hiện tại',
    step1: {
      title: 'Xác nhận mật khẩu quỹ',
      password: {
        placeholder: 'Nhập 6 số mật khẩu quỹ',
      },
      submit: 'Bước tiếp theo',
      explanation1: '*Vì lý do bảo mật tài khoản , nếu quên mật khẩu quỹ , vui lòng liên hệ {CSKH}',
      validate: {
        password: {
          required: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
          length: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
        },
      },
    },
    step2: {
      title: 'Cài đặt mật khẩu quỹ',
      setPassword: {
        label: 'Mật khẩu mới',
        placeholder: 'Bắt buộc điền mật khẩu đăng nhập mới',
      },
      confirmSetPassword: {
        label: 'Xác nhận mật khẩu mới',
        placeholder: 'Vui lòng nhập lại mật khẩu quỹ gồm 6 chữ số.',
      },
      validate: {
        not_same: 'Mật khẩu lần hai không khớp, vui lòng nhập lại',
        setPassword: {
          required: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
          length: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
        },
      },
      explanation1:
        '*Mật khẩu quỹ là mật khẩu xác thực duy nhất tại thời điểm thanh toán. Vui lòng nhớ mật khẩu và không tiết lộ mật khẩu quỹ cho người khác. {br} Vui lòng giữ mật khẩu của bạn đúng cách. Nếu bạn quên mật khẩu, vui lòng sử dụng chức năng câu hỏi bảo mật để lấy lại hoặc liên hệ {cskh} để xử lý.',
      submit: 'Hoàn thành',
    },
    cskh: 'CSKH',
    submit: 'Hoàn thành',
    edit_success: 'Chỉnh sửa thành công',
  },

  // phone, sms
  phone: {
    abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
    step1: {
      title: 'Thay đổi số điện thoại',
      submit: 'Hoàn thành',
      placeholder: 'Vui lòng nhập số điện thoại',
    },
    step2: {
      title: 'Thay đổi số điện thoại',
      submit: 'Hoàn thành',
      placeholder: 'Vui lòng nhập mã xác nhận gồm 4 chữ số',
    },
    select_label: 'Quốc gia / Khu vực',
    phone_label: 'Vui lòng nhập số điện thoại',
    validate: {
      ContactNumber: {
        required: 'Nhập 10 ký tự chữ',
        length: 'Nhập 10 ký tự chữ',
      },
      MappingCode: {
        required: 'Yêu cầu mã xác nhận',
        length: 'Nhập 4 ký tự chữ',
      },
    },
    edit_success: 'Số điện thoại đã cài đặt thành công.',
    verified_kind_name: 'Số điện thoại di động',
    mapping_code_sent: 'Đã gửi mã xác minh gồm 4 chữ số đến số điện thoại của bạn',
    mapping_code_will_expire: 'Sau {mmss} mã xác minh sẽ vô hiệu hóa',
    mapping_code_is_expired: 'Mã xác nhận đã vô hiệu hóa',
    get_code_button: 'Gửi lại mã xác nhận',
  },
}
