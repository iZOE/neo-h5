export default {
  bar_title: 'Vií tiền ảo',
  bar_title2: 'Xác nhận thông tin ví tiền ảo',
  CSKH: 'CSKH',
  next_step: 'Bước tiếp theo',
  wallet_protocol: 'Loại tiền giao dịch',
  wallet_address: 'Liên kết ví tiền ảo',
  wallet_nickname: 'Biệt danh ví tiền',
  placeholder: {
    please_enter_wallet_address: 'Nhập liên kết ví tiền',
    wallet_nickname: 'Nhập biệt danh ví tiền (dưới 30 ký tự)',
  },
  validate: {
    wallet_address: {
      required: 'Bắt buộc điền',
      format: 'Định dạng liên kết ví tiền bị lỗi',
    },
    wallet_nickname: {
      required: 'Định dạng là 30 ký tự, bao gồm, chữ cái, ký tự số và không có ký hiệu đặc biệt',
      length: 'Định dạng là 30 ký tự, bao gồm, chữ cái, ký tự số và không có ký hiệu đặc biệt',
      format: 'Định dạng là 30 ký tự, bao gồm, chữ cái, ký tự số và không có ký hiệu đặc biệt',
    },
  },
  warm_warning: 'Lưu ý:',
  warm_warning_item1:
    'Thông tin mặc định được sử dụng để xác thực khi bạn quên mật khẩu. Vui lòng đảm bảo nhập thông tin chính xác.',
  warm_warning_item2:
    'Hiện tại chỉ hỗ trợ rút tiền USDT , Vui lòng xác nhận liên kết ví tiền của bạn có hỗ trợ USDT , nếu không sẽ không thể nhận được tiền rút.',
  submit: 'Xác nhận thêm',
  api: {
    add_error_duplicate: {
      title: 'Thêm mới không thành công',
      content: 'Liên kế ví tiền này đã được mặc định, bạn vui lòng thử các liên kết ví tiền khác',
    },
    add_account_success: {
      title: 'Thêm Tài khoản rút tiền',
      content: 'Đã thêm Tài khoản rút tiền thành công',
      confirm: 'Xác nhận',
    },
  },
  alert_dialog: {
    not_set_fund_password_yet: 'Chưa cài đặt mật khẩu quỹ',
    go_set_it: 'Cài đặt',
    title: 'Thêm Tài khoản rút tiền',
    plz_do_phone_validate: 'Vui lòng hoàn tất xác minh số điện thoại để thêm tài khoản rút tiền',
    send_code: 'Gửi mã xác nhận',
    will_send_sms_code:
      'Vì bảo mật an toàn cho tài khoản của bạn, hệ thống sẽ gửi mã xác minh đến số điện thoại của bạn, sau khi xác minh sẽ hoàn thành thêm tài khoản rút tiền',
  },
  // fund-password
  fund_password: {
    abort_editing: 'Quay lại sẽ bỏ nội dung chỉnh sửa hiện tại',
    step1: {
      title: 'Xác nhận mật khẩu quỹ',
      password: {
        placeholder: 'Nhập 6 số mật khẩu quỹ',
      },
      submit: 'Bước tiếp theo',
      explanation1: '*Vì lý do bảo mật tài khoản , nếu quên mật khẩu quỹ , vui lòng liên hệ {CSKH}',
      validate: {
        password: {
          required: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
          length: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
        },
      },
    },
    step2: {
      title: 'Cài đặt mật khẩu quỹ',
      setPassword: {
        label: 'Mật khẩu mới',
        placeholder: 'Bắt buộc điền mật khẩu đăng nhập mới',
      },
      confirmSetPassword: {
        label: 'Xác nhận mật khẩu mới',
        placeholder: 'Vui lòng nhập lại mật khẩu quỹ gồm 6 chữ số.',
      },
      validate: {
        not_same: 'Mật khẩu lần hai không khớp, vui lòng nhập lại',
        setPassword: {
          required: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
          length: 'Mật khẩu quỹ phải bao gồm 6 chữ số',
        },
      },
      explanation1:
        '*Mật khẩu quỹ là mật khẩu xác thực duy nhất tại thời điểm thanh toán. Vui lòng nhớ mật khẩu và không tiết lộ mật khẩu quỹ cho người khác. {br} Vui lòng giữ mật khẩu của bạn đúng cách. Nếu bạn quên mật khẩu, vui lòng sử dụng chức năng câu hỏi bảo mật để lấy lại hoặc liên hệ {cskh} để xử lý.',
      submit: 'Hoàn thành',
    },
    cskh: 'CSKH',
    submit: 'Hoàn thành',
    edit_success: 'Chỉnh sửa thành công',
  },

  // phone, sms
  phone: {
    abort_editing: 'Dữ liệu của bạn đã có biến động, bạn xác nhận muốn từ bỏ thay đổi?',
    step1: {
      title: 'Thay đổi số điện thoại',
      submit: 'Hoàn thành',
      placeholder: 'Vui lòng nhập số điện thoại',
    },
    step2: {
      title: 'Thay đổi số điện thoại',
      submit: 'Hoàn thành',
      placeholder: 'Vui lòng nhập mã xác nhận gồm 4 chữ số',
    },
    select_label: 'Quốc gia / Khu vực',
    phone_label: 'Vui lòng nhập số điện thoại',
    validate: {
      ContactNumber: {
        required: 'Bắt buộc điền số điện thoại',
        length: 'Nhập 10 ký tự chữ',
      },
      MappingCode: {
        required: 'Bắt buộc điền mã xác nhận',
        length: 'Nhập 4 ký tự chữ',
      },
    },
    edit_success: 'Số điện thoại đã cài đặt thành công.',
    verified_kind_name: 'Số điện thoại di động',
    mapping_code_sent: 'Đã gửi mã xác minh gồm 4 chữ số đến số điện thoại của bạn',
    mapping_code_will_expire: 'Sau {mmss} mã xác minh sẽ vô hiệu hóa',
    mapping_code_is_expired: 'Mã xác nhận đã vô hiệu hóa',
    get_code_button: 'Gửi lại mã xác nhận',
  },
}
