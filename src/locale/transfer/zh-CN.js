export default {
  area: {
    auto_transfer: {
      switch: '免转钱包',
    },
    transfer_io: {
      action: {
        transfer: '立即转账',
      },
      field: {
        amount: {
          label: '转账金额',
          placeholder: '请输入转账金额',
        },
        transfer: {
          in: '转入',
          out: '转出',
        },
      },
    },
    wallet: {
      withdraw_all: '一键收回',
    }
  },
  shared: {
    main_wallet: '中心钱包',
    message: {
      sync_wallet: '钱包同步中',
      transfer: {
        fail: '转账失败',
        success: '转账成功',
      },
    },
    validator: {
      amount_is_insufficient: '帐户馀额不足请重新输入',
      is_required: '必填',
    },
  },
  title: '转账',
}
