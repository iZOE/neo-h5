export default {
  area: {
    auto_transfer: {
      switch: 'Tự động chuyển điểm',
    },
    transfer_io: {
      action: {
        transfer: 'Chuyển điểm ngay',
      },
      field: {
        amount: {
          label: 'Số điểm chuyển',
          placeholder: 'Vui lòng nhập số điểm chuyển',
        },
        transfer: {
          in: 'Chuyển vào',
          out: 'Chuyển ra',
        },
      },
    },
    wallet: {
      withdraw_all: 'Chuyển về ví chính',
    }
  },
  shared: {
    main_wallet: 'Ví chính',
    message: {
      sync_wallet: 'Đang đồng bộ ví tiền',
      transfer: {
        fail: 'Chuyển khoản thất bại',
        success: 'Chuyển khoản thành công',
      },
    },
    validator: {
      amount_is_insufficient: 'Số dư tài khoản không đủ, vui lòng nhập lại.',
      is_required: 'Bắt buộc điền',
    },
  },
  title: 'Chuyển khoản ngay',
}
