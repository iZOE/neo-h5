export default {
  signup: '注册',
  signUp: {
    submitBTNText: '注册',
    placeholder: {
      accountId: '请输入您的帐户名称',
      password: '请输入您的帐户密码',
      confirmPassword: '请重复输入密码',
      nickName: '请输入昵称（选填)',
      referralCode: '请输入邀请码',
    },
    inputValidate: {
      accountRequired: '帐号为必填',
      verificationCodeRequired: '请输入验证码',
      accountIdLength: '需为 6 个以上的英数字组合，不含特殊字元、中文及空白',
      nickNamePlainText: '1-14 个字符，不支持空白键',
      nickNameNoAllDigit: '昵称请避免包含 8 个数字组合',
      passwordRequired: '密码为必填',
      noAllDigits: '不能全部是数字，不支持中文字、空白键',
      confirmPasswordRequired: '密码确认为必填',
      confirmPasswordSame: '密码确认不一致',
      referralCodeLength: '邀请码必须是 8 个字母',
    },
    errorCodes: {
      '0000': {
        title: '注册成功',
        content: '即将跳转回登录页面',
        confirm: '立即登录',
      },
      1003: {
        title: '注册失败',
        content: '请重新尝试注册',
        confirm: '确定',
      },
      8010: {
        title: '注册失败',
        content: '单一IP超过上限',
        confirm: '联系客服',
      },
      8001: {
        content: '发生异常，请重新尝试',
        confirm: '确定',
      },
      8004: {
        content: '帐户名称错误，请重新输入',
        confirm: '确定',
      },
      8005: {
        content: '帐户密码错误，请重新输入',
        confirm: '确定',
      },
      8006: {
        content: '两次密码不一致，请重新输入',
        confirm: '确定',
      },
      8007: {
        content: '发生异常，请重新尝试',
        confirm: '确定',
      },
      8008: {
        content: '返点等级超出范围，请重新选择',
        confirm: '确定',
      },
      1042: {
        content: '该帐户名称已存在',
        confirm: '确定',
      },
      1043: {
        content: '该帐户暱称已存在',
        confirm: '确定',
      },
      8009: {
        content: '该会员资料已被设置',
        confirm: '确定',
      },
      '800a': {
        content: '发生异常，请重新尝试(800a)',
        confirm: '确定',
      },
      '800b': {
        content: '发生异常，请重新尝试(800b)',
        confirm: '确定',
      },
      '100A': {
        content: '发生异常，请重新尝试(100A)',
        confirm: '确定',
      },
      '100B': {
        content: '发生异常，请重新尝试(100B)',
        confirm: '确定',
      },
      '800d': {
        content: '发生异常，请重新尝试(800d)',
        confirm: '确定',
      },
      '800c': {
        content: '发生异常，请重新尝试(800c)',
        confirm: '确定',
      },
      1013: {
        content: '发生异常，请重新尝试(1013)',
        confirm: '确定',
      },
    },
  },
}
