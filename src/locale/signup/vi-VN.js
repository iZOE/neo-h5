export default {
  signup: 'Đăng ký',
  signUp: {
    submitBTNText: 'Đăng ký',
    placeholder: {
      accountId: 'Tên đăng nhập',
      password: 'Mật khẩu đăng nhập',
      confirmPassword: 'Vui lòng nhập lại mật khẩu',
      nickName: 'Nhập biệt danh (bắt buộc)',
      referralCode: 'Vui lòng nhập mã giới thiệu',
    },
    inputValidate: {
      accountRequired: 'Bắt buộc điền tài khoản',
      verificationCodeRequired: 'Vui lòng nhập mã xác minh',
      accountIdLength: 'Tài khoản phải có 6-16 kí tự bao gồm chữ cái và số',
      nickNamePlainText: '3 - 14 ký tự, không nhập khoảng trắng',
      nickNameNoAllDigit: 'Biệt danh không sử dụng 8 ký tự số liên tiếp',
      passwordRequired: 'Bắt buộc điền mật khẩu',
      noAllDigits: 'Mật khẩu phải có 6-16 kí tự bao gồm chữ cái và số',
      confirmPasswordRequired: 'Vui lòng nhập lại mật khẩu',
      confirmPasswordSame: 'Mật khẩu không trùng khớp, vui lòng nhập lại',
      referralCodeLength: 'Mã giới thiệu phải có 8 chữ cái',
    },
    errorCodes: {
      '0000': {
        title: 'Đăng ký thành công',
        content: 'Trở về trang đăng nhập',
        confirm: 'Đăng nhập ngay',
      },
      1003: {
        title: 'Đăng ký thất bại',
        content: 'Vui lòng đăng ký lại',
        confirm: 'Xác nhận',
      },
      8010: {
        title: 'Đăng ký thất bại',
        content: 'Đường truyền IP vượt hạn mức',
        confirm: 'Liên hệ CSKH',
      },
      8001: {
        content: 'Xảy ra lỗi , vui lòng thử lại',
        confirm: 'Xác nhận',
      },
      8004: {
        content: 'Tên tài khoản không đúng , vui lòng nhập lại',
        confirm: 'Xác nhận',
      },
      8005: {
        content: 'Mật khẩu không đúng , vui lòng thử lại',
        confirm: 'Xác nhận',
      },
      8006: {
        content: 'Mật khẩu không đồng nhất , vui lòng thử lại',
        confirm: 'Xác nhận',
      },
      8007: {
        content: 'Xảy ra lỗi , vui lòng thử lại',
        confirm: 'Xác nhận',
      },
      8008: {
        content: 'Cấp bậc hoàn trả vượt phạm vi cho phép , vui lòng chọn lại',
        confirm: 'Xác nhận',
      },
      1042: {
        content: 'Tên tài khoản này đã tồn tại',
        confirm: 'Xác nhận',
      },
      1043: {
        content: 'Biệt danh này đã tồn tại',
        confirm: 'Xác nhận',
      },
      8009: {
        content: 'Dữ liệu hội viên này đã được mặc định',
        confirm: 'Xác nhận',
      },
      '800a': {
        content: 'Xảy ra lỗi , vui lòng thử lại(800a)',
        confirm: 'Xác nhận',
      },
      '800b': {
        content: 'Xảy ra lỗi , vui lòng thử lại(800b)',
        confirm: 'Xác nhận',
      },
      '100A': {
        content: 'Xảy ra lỗi , vui lòng thử lại(100A)',
        confirm: 'Xác nhận',
      },
      '100B': {
        content: 'Xảy ra lỗi , vui lòng thử lại(100B)',
        confirm: 'Xác nhận',
      },
      '800d': {
        content: 'Xảy ra lỗi , vui lòng thử lại(800d)',
        confirm: 'Xác nhận',
      },
      '800c': {
        content: 'Xảy ra lỗi , vui lòng thử lại(800c)',
        confirm: 'Xác nhận',
      },
      1013: {
        content: 'Xảy ra lỗi , vui lòng thử lại(1013)',
        confirm: 'Xác nhận',
      },
    },
  },
}
