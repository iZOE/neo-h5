const withPWA = require('next-pwa')
const runtimeCaching = require('next-pwa/cache')

runtimeCaching[0].handler = 'StaleWhileRevalidate'

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

module.exports = withBundleAnalyzer(
  withPWA({
    future: {
      webpack5: true,
      strictPostcssConfiguration: true,
    },
    pwa: {
      dest: 'public',
      mode: 'production',
      skipWaiting: false,
      runtimeCaching,
    },
    poweredByHeader: false,
    webpack(config) {
      config.module.rules.push({
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      })

      return config
    },
    experimental: {
      granularChunks: true,
    },
    async exportPathMap(defaultPathMap, { dev, dir, outDir, distDir, buildId }) {
      return {
        '/': { page: '/' },
        '/about-us': { page: '/about-us' },
        '/android': { page: '/android' },
        '/user': { page: '/user' },
        '/user/setting': { page: '/user/setting' },
        '/user/setting/info': { page: '/user/setting/info' },
        '/user/setting/info/birthday': { page: '/user/setting/info/birthday' },
        '/user/setting/info/qq': { page: '/user/setting/info/qq' },
        '/user/setting/info/wechat': { page: '/user/setting/info/wechat' },
        '/user/setting/info/email': { page: '/user/setting/info/email' },
        '/user/setting/info/nickname': { page: '/user/setting/info/nickname' },
        '/user/setting/info/picture': { page: '/user/setting/info/picture' },
        '/user/setting/info/phone': { page: '/user/setting/info/phone' },
        '/user/setting/info/fund-password': { page: '/user/setting/info/fund-password' },
        '/report/bet': { page: '/report/bet' },
        '/report/bet/detail': { page: '/report/bet/detail' },
        '/report/deposit': { page: '/report/deposit' },
        '/report/deposit/detail': { page: '/report/deposit/detail' },
        '/report/financial': { page: '/report/financial' },
        '/report/financial/detail': { page: '/report/financial/detail' },
        '/report/fund-password': { page: '/report/fund-password' },
        '/report/promo': { page: '/report/promo' },
        '/report/transfer': { page: '/report/transfer' },
        '/report/transfer/detail': { page: '/report/transfer/detail' },
        '/report/withdraw': { page: '/report/withdraw' },
        '/report/withdraw/detail': { page: '/report/withdraw/detail' },
        '/reset-password': { page: '/reset-password' },
        '/signup': { page: '/signup' },
        '/login': { page: '/login' },
        '/service': { page: '/service' },
        '/activity': { page: '/activity' },
        '/notice-board': { page: '/notice-board' },
        '/withdraw': { page: '/withdraw' },
        '/deposit': { page: '/deposit' },
        '/vip': { page: '/vip' },
        '/transfer': { page: '/transfer' },
        '/download': { page: '/download' },
        '/promo': { page: '/promo' },
        '/promo/share': { page: '/promo/share' },
        '/promo/trial': { page: '/promo/trial' },
        '/debug': { page: '/debug' },
        '/support': { page: '/support' },
        '/support/deposit-guide': { page: '/support/deposit-guide' },
        '/demo': { page: '/demo' },
        '/withdraw-manage/banks': { page: '/withdraw-manage/banks' },
        '/withdraw-manage/crypto': { page: '/withdraw-manage/crypto' },
        '/withdraw-manage/crypto/add-wallet': { page: '/withdraw-manage/crypto/add-wallet' },
        '/withdrawal': { page: '/withdrawal' },
      }
    },
  }),
)
