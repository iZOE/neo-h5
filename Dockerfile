FROM node:16 AS builder

WORKDIR /app

COPY . /app

ARG AGENT_CODE
ENV AGENT_CODE=$AGENT_CODE
ARG END_POINT_VERSION
ENV END_POINT_VERSION=$END_POINT_VERSION
ARG TAG
ENV TAG=$TAG
ENV PATH /app/node_modules/.bin:$PATH

RUN yarn

RUN yarn build

# set up production environment
FROM node:16 AS publish

WORKDIR /app
COPY --from=builder /app/out ./out
COPY --from=builder /app/.next ./.next
COPY ./public ./public
COPY ./scripts/pm2.config.js ./pm2.config.js
COPY ./src/server.js ./server.js

RUN yarn global add pm2
RUN yarn add next koa @koa/router mobile-detect react react-dom koa-send

CMD [ "pm2-runtime", "start", "pm2.config.js" ]