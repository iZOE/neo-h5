const dotenv = require('dotenv')
const fs = require('fs')
const fetch = require('node-fetch')
const loginAndSignupColorSet = require('./.mock.api/loginandsignup/loginandsignup.json')
const downloadColorSet = require('./.mock.api/download/download.json')

const hasReferralCodeAgent = ['VT999']
const hasQQWechatSettingAgent = ['STA01']

dotenv.config()
const ENV = []
const checkApiUrl = `${process.env.END_POINT_VERSION}/api/v2/check?agentcode=${process.env.AGENT_CODE}&platform=${process.env.PLATFORM}`

const whenAllENVSettleDown = () => {
  fs.writeFile('.env.local', ENV.join('\n'), err => {
    if (err) throw err
  })
}

let PHONE_COUNTRY_CODE_OPTIONS

if (process.env.AGENT_CODE === 'VT999') {
  PHONE_COUNTRY_CODE_OPTIONS = [
    {
      value: 'VN',
      name: 'Việt Nam+84',
    },
    {
      value: 'CN',
      name: '中國大陸+86',
    },
  ]
} else {
  PHONE_COUNTRY_CODE_OPTIONS = [
    {
      value: 'CN',
      name: '中國大陸+86',
    },
    {
      value: 'VN',
      name: 'Việt Nam+84',
    },
  ]
}

const transDataOnFetcherSuccess = res => {
  const options = res.data

  const csOptions = options.map(item => {
    let href
    switch (item.mode) {
      case 2:
        href = `tel:${item.content}`
        break
      case 3:
        href = `https://${item.content}`
        break
      default:
        href = item.content
    }
    return { href, name: item.name }
  })

  ENV.push(`NEXT_PUBLIC_NOT_LOGIN_CUSTOMER_SERVICE_OPTIONS=${JSON.stringify(csOptions)}`)

  whenAllENVSettleDown()
}

fetch(checkApiUrl)
  .then(response => response.json())
  .then(data => {
    ENV.push(`NEXT_PUBLIC_TAG=${process.env.TAG || 'no tag'}`)
    ENV.push(`NEXT_PUBLIC_APP_KEY=${process.env.APP_ENV || ''}`)
    ENV.push(`NEXT_PUBLIC_CLIENT_ID=${process.env.CLIENT_ID}`)
    ENV.push(`NEXT_PUBLIC_AGENT_CODE=${process.env.AGENT_CODE}`)
    ENV.push(`NEXT_PUBLIC_CHECK_API_URL=${checkApiUrl}`)
    ENV.push(`NEXT_PUBLIC_CUSTOMER_SERVICE=${JSON.stringify(data.data.customerService)}`)
    ENV.push(`NEXT_PUBLIC_AGENT_LOCALE_LANGUAGE=${data.data.locale.language}`)
    ENV.push(`NEXT_PUBLIC_AGENT_LOCALE_COUNTRY=${data.data.locale.contry}`)
    ENV.push(`NEXT_PUBLIC_AGENT_LOCALE_CURRENCY=${data.data.locale.currency}`)
    ENV.push(
      `NEXT_PUBLIC_AGENT_VERSIONS_DOWNLOADS_ANDROID=${JSON.stringify(
        data.data.versions.android.downloads,
      )}`,
    )
    ENV.push(`NEXT_PUBLIC_AGENT_VERSIONS_LATEST_ANDROID=${data.data.versions.android.latest}`)
    ENV.push(`NEXT_PUBLIC_AGENT_VERSIONS_RELEASE_ANDROID=${data.data.versions.android.releaseDate}`)
    ENV.push(
      `NEXT_PUBLIC_AGENT_VERSIONS_DOWNLOADS_IOS=${JSON.stringify(
        data.data.versions.ios.downloads,
      )}`,
    )
    ENV.push(`NEXT_PUBLIC_AGENT_VERSIONS_LATEST_IOS=${data.data.versions.ios.latest}`)
    ENV.push(`NEXT_PUBLIC_AGENT_VERSIONS_RELEASE_IOS=${data.data.versions.ios.releaseDate}`)

    ENV.push(`NEXT_PUBLIC_AGENT_VIP_PATH=${data.data.vip.path}`)

    if (
      data &&
      data.data &&
      data.data.endpoints &&
      data.data.endpoints.videos &&
      data.data.endpoints.videos.promo
    ) {
      ENV.push(`NEXT_PUBLIC_PROMOTION_VIDEO_URL=${data.data.endpoints.videos.promo[0]}`)
    }

    ENV.push(`NEXT_PUBLIC_PHONE_COUNTRY_CODE_LIST=${JSON.stringify(PHONE_COUNTRY_CODE_OPTIONS)}`)
    // 有的站要開通一些設置, 有的不用
    ENV.push(
      `NEXT_PUBLIC_ENABLE_USER_QQ_CONFIG=${hasQQWechatSettingAgent.includes(
        process.env.AGENT_CODE,
      )}`,
    )
    ENV.push(
      `NEXT_PUBLIC_ENABLE_USER_WECHAT_CONFIG=${hasQQWechatSettingAgent.includes(
        process.env.AGENT_CODE,
      )}`,
    )

    // 寫死的scaffold設定
    ENV.push('NEXT_PUBLIC_SCAFFOLD_LOGINANDREGISTER_LAYOUT=A')
    ENV.push('NEXT_PUBLIC_SCAFFOLD_LOBBY_LAYOUT=A')

    // get global color set
    const lightColorSet = {
      background: '#F9F9F9',
      header: '#214164',
      cateMenu: {
        bg: '#ebecee',
        activeBg: '#d1e6f1',
      },
      sharedComponent: {
        typography: {
          textColor: '#5E7184',
          linkColor: '#1D8AEB',
        },
        navigationBar: {
          bg: '#ffffff',
          centerTextColor: '#95A3B0',
          rightTextColor: '#95A3B0',
          iconColor: '#95A3B0',
        },
        container: {
          bg: '#F9F9F9',
        },
        input: {
          label: {
            color: '#5E7184',
          },
          bg: '#FFFFFF',
          color: '#333333',
          borderColor: '#CCCCCC',
          placeholderColor: '#CCCCCC',
          passwordIconColor: '#95A3B0',
          focus: {
            borderColor: '#333333',
          },
          warning: {
            borderColor: '#EE5650',
            messageColor: '#EE5650',
          },
        },
        button: {
          primary: {
            normal: {
              bg: '#1D8AEB',
              color: '#FFFFFF',
            },
            active: {
              bg: '#1A72C0',
              color: '#FFFFFF',
            },
            disabled: {
              bg: '#DDDDDD',
              color: '#FFFFFF',
            },
          },
          secondary: {
            normal: {
              bg: '#FFFFFF',
              color: '#95A3B0',
              borderColor: '#95A3B0',
            },
            active: {
              bg: '#ECECEC',
              color: '#95A3B0',
              borderColor: '#95A3B0',
            },
            disabled: {
              bg: '#DDDDDD',
              color: '#FFFFFF',
            },
          },
          tertiary: {
            normal: {
              bg: '#FFFFFF',
              color: '#1D8AEB',
            },
            active: {
              bg: '#ECECEC',
              color: '#1D8AEB',
            },
            disabled: {
              bg: '#DDDDDD',
              color: '#FFFFFF',
            },
          },
        },
        switchtab: {
          normal: {
            bg: 'transparent',
            color: '#51A1FF',
          },
          active: {
            bg: '#51A1FF',
            color: '#FFFFFF',
          },
        },
        tabs: {
          normal: {
            bg: ['transparent', 'transparent'],
          },
          active: {
            bg: ['#50BEFC', '#3985EB'],
          },
        },
      },
    }

    const darkColorSet = {
      background: '#112024',
      header: '#294164',
      cateMenu: {
        bg: '#ebfcee',
        activeBg: '#d1e7f1',
      },
      sharedComponent: {
        typography: {
          textColor: '#95A3B0',
          linkColor: '#1D8AEB',
        },
        navigationBar: {
          bg: '#161616',
          centerTextColor: '#95A3B0',
          rightTextColor: '#95A3B0',
          iconColor: '#95A3B0',
        },
        container: {
          bg: '#22222E',
        },
        input: {
          label: {
            color: '#95A3B0',
          },
          bg: '#2A2A38',
          color: '#FFFFFF',
          borderColor: '#676783',
          placeholderColor: '#676783',
          passwordIconColor: '#95A3B0',
          focus: {
            borderColor: '#FFFFFF',
          },
          warning: {
            borderColor: '#EE5650',
            messageColor: '#EE5650',
          },
        },
        button: {
          primary: {
            normal: {
              bg: '#1D8AEB',
              color: '#FFFFFF',
            },
            active: {
              bg: '#1A72C0',
              color: '#FFFFFF',
            },
            disabled: {
              bg: '#353340',
              color: '#47475B',
            },
          },
          secondary: {
            normal: {
              bg: '#22222E',
              color: '#95A3B0',
              borderColor: '#95A3B0',
            },
            active: {
              bg: '#333333',
              color: '#95A3B0',
              borderColor: '#95A3B0',
            },
            disabled: {
              bg: '#353340',
              color: '#47475B',
            },
          },
          tertiary: {
            normal: {
              bg: '#353340',
              color: '#1D8AEB',
            },
            active: {
              bg: '#47475B',
              color: '#1D8AEB',
            },
            disabled: {
              bg: '#353340',
              color: '#47475B',
            },
          },
        },
        switchtab: {
          normal: {
            bg: 'transparent',
            color: '#51A1FF',
          },
          active: {
            bg: '#51A1FF',
            color: '#FFFFFF',
          },
        },
        tabs: {
          normal: {
            bg: 'transparent',
          },
          active: {
            bg: ['#50BEFC', '#3985EB'],
          },
        },
      },
    }

    const colorSet = JSON.stringify({
      light: lightColorSet,
      dark: darkColorSet,
    })
    // for global
    ENV.push(`NEXT_PUBLIC_THEME_COLOR_SET=${colorSet}`)

    // for login and signup template
    ENV.push(
      `NEXT_PUBLIC_LOGIN_AND_SIGNUP_COLOR_SET=${JSON.stringify(
        loginAndSignupColorSet[process.env.AGENT_CODE],
      )}`,
    )
    // for download
    ENV.push(
      `NEXT_PUBLIC_DOWNLOAD_COLOR_SET=${JSON.stringify(downloadColorSet[process.env.AGENT_CODE])}`,
    )

    // endpoints
    ENV.push(`NEXT_PUBLIC_END_POINT_BU=${data.data.endpoints.bu[0]}`)
    ENV.push(`NEXT_PUBLIC_END_POINT_PORTAL=${data.data.endpoints.portal[0]}`)
    ENV.push(`NEXT_PUBLIC_END_POINT_SIGNALR=${data.data.endpoints.signalr[0]}`)
    ENV.push(`NEXT_PUBLIC_END_POINT_STORAGE=${data.data.endpoints.storage[0]}`)
    ENV.push(`NEXT_PUBLIC_END_POINT_OAUTH=${data.data.endpoints.oauth[0]}`)
    ENV.push(`NEXT_PUBLIC_END_POINT_BF=${JSON.stringify(data.data.endpoints.bf)}`)
    ENV.push(`NEXT_PUBLIC_END_POINT_WEB_FQDN=${JSON.stringify(data.data.endpoints.web)}`)
    ENV.push(`NEXT_PUBLIC_END_POINT_MOBILE_FQDN=${JSON.stringify(data.data.endpoints.mobile)}`)
    ENV.push(
      `NEXT_PUBLIC_WITH_REFERRALCODE=${
        hasReferralCodeAgent.includes(process.env.AGENT_CODE) ? 'true' : 'false'
      }`,
    )
    ENV.push('NEXT_PUBLIC_TIME_FORMAT_L=YYYY/MM/DD')
    ENV.push('NEXT_PUBLIC_TIME_FORMAT_LT=YYYY/MM/DD HH:mm:ss')

    const customerServiceOptionsUrl = `${data.data.endpoints.portal[0]}v2/api/customerservice/otherContact/visitor?agentCode=${process.env.AGENT_CODE}`

    fetch(customerServiceOptionsUrl)
      .then(response => response.json())
      .then(transDataOnFetcherSuccess)
      .catch(err => {
        console.error(
          '！！！！！！  customer service options(in not login situation) fetch failed or parse failed',
          err,
        )
        process.exit(1)
      })
  })
  .catch(err => {
    console.error('CHECK API fetch failed', err)
    process.exit(1)
  })
